
This package contains the MATLAB scripts and custom functions used for
the modelling of the end-to-end signal-chain of the Square Kilometre
Array (SKA) Phase-1 Correlator and Beamformer (CBF) of the Mid
Telescope. This model simulates the evaluation of auto- and cross-
correlations of a coherent broadband noise emitted by a point celestial
source received by two or more receptors. The angular speed of the
celestial source and the positions of the receptors can be set
arbitrarily. Note that this model assumes an ideal receiver and an ideal
analogue-to-digital convertor (ADC).

See the full documentation at: https://developer.skatelescope.org/projects/mid-cbf-model/
