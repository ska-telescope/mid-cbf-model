%This MATLAB script performs a 'Running Simulation' of a 'Realizable' Imaging 
%Process of the Mid.CBF with the Frequency Slice Approach. Here, the test 
%sequences with corresponding geometric propagation delays are generated 
%using an efficient implementation of 'Sum of Sinusoidal' method  along with 
%that the corresponding 'First Order Delay Polynomials - FODP'. For processing, 
%first, both Signal are channelized using the 'Coarse Channelizer (CoCh)'. 
%Then the delayed signals are Resampled using Fractional-Delay Filter-Banks 
%and Phase-Corrected with Phase-Rotators. The delay and phase aligned signals
%are then processed by the Imaging Channelizer producing 16,384 channels.
%The channel outputs are then cross-correlated and accumulated in order to
%demonstrate the recovered visibilities.

%Custom MATLAB functions used
%(1) [SI_Vec,TVD_Vec,BD,FODSR,FOPSR] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d_Vec,Fs_Vec,DCSpecs) 
% : In order to Evaluate the Geometrical Delay and the First order Delay 
%Polynomials for the Duration of the Test Sequence
%Alternatively, use 
%[SI_Vec,TVD_Vec,BD,FODSR,FOPSR] = Arbitrary_Delay_Est(HDM,T_s,Ns,Fs_Vec,DSpecs)
%(2) [x, ~] = Rand_Seq_TVD_ChirpZ(Specs,Fs_Vec,ST_Vec,TVD_Vec) : In order to evaluate 
%the Reference and Delayed Signals
%(3) y = OS_Poly_DFT_FB_SC(x,BRI,OSFB_Config_para) : In order to model CC using an over-Sampled
%Polyphase DFT Filter-Bank
%%(4) [RSx1,V] = ReSampler_ERP_Corr(CCx,F_Rat,SI1_CC,SI0_CC,FODSR,FOPSR,ReSamp_Config_para); : In order to Calculate
%the Delay and Phase Corrections to correct the delay and phase with respect 
%to the Delay-Center at BoresightIn and apply those corrections
%(6) yCSPDFB = CS_Poly_DFT_FB_Batch(x,CSFB_Config_para) : In order to model the Imaging
%Channelizer using a Critically-Sampled Polyphase DFT Filter-Bank

%Note that for SKA1 Band 3, the signal is sampled at ~3.168 Gsps. Hence, in
%Mid.CBF, those signals are upsampled to ~3.96 Gsps at the input using an
%5/4 Up-Sampler.

% By Thushara Kanchana Gunaratne - RCO National Research Council Canada
% Original Release Date : 2018-08-10
% Modified 2019-01-02 > To add arbitrary delay model and add a validity flag
%for the data

close all; clear; clc;
 
%profile on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation Specs
%Number of Samples per Input Sample-Block 
% Note - Set this accordingly depending on the Memory Resources available 
%on the Simulation Computer
Ns_O = 2^24; %

%Number of Receptors
N_Rcp = 2;

%SKA1 Band 
Band_ID = 2; % Select from [1,2,3,4,5a = 5, 5b = 6];
%The Selected 'Frequency-Slice'(FS) for the Subsequent Processing
%Band 1, 2 & 3 = 0-9 | Band 4, 5a & 5b = 0-14
FS_ID = 5;

%Additional Frequency Down-Shift at the DISH 
if (Band_ID == 5) || (Band_ID == 6)  
    %For Band 5a - from 0 to 1.45 GHz
    %For Band 5b - from 0 to 6.5 GHz
    Ad_B5_Fsft = 1E9;
end %IF

%Wideband Frequency Shift (i.e. to propaly set the Zoom Windows)
WB_Fsft = 1*16E6; %In Hz
%The Frequency Offset desired in shifting Fine Imaging Channels
F_off = 1*13440/2; %In Hz

%Simulation Start Time Offset
T_si = 13300/13440; % In sec
%Simulation Duration
T_Sim =  1*0.14; %In sec

%% Signal Spcifications

%The Correlation Coefficient
p = 2*0.5;

%Relative Interference Power Compared to the System Noise
%Interference Specs
F_Int = 945E6; % In Hz
A_Int_dB = -50; % in dB
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Antenna Positions | The Distance vector in Cartesian Coordinates with 
%respect to the Array Reference Point

%For the Antenna-#1
Dist_1 = 1*160E3; %In meters - In SKA1 Max Baseline is 160 kms
Theta_1 = 10; %In Degrees
Phi_1 = 150; %In Degrees
%Offset Index with SCFO sampling for Antenna-#1
FO_Idx1 = 1*1000;  

%%%%%%%%%%%%%%%%%%%
%For the Antenna-#2
Dist_2 = 1*60E3; %In meters - In SKA1 Max Baseline is 160 kms
Theta_2 = 45; %In Degrees
Phi_2 = -130; %In Degrees
%Offset Index with SCFO sampling for Antenna-#2
FO_Idx2 = 1*1001;

% %%%%%%%%%%%%%%%%%%%
% %For the Antenna-#3
% Dist_3 = 1*10E3; %In meters - In SKA1 Max Baseline is 160 kms
% Theta_3 = 75; %In Degrees
% Phi_3 = 0; %In Degrees
% %Offset Index with SCFO sampling for Antenna-#2
% FO_Idx3 = 1*1002;
% 
% %%%%%%%%%%%%%%%%%%%
% %For the Antenna-#4
% Dist_4 = 1*10E3; %In meters - In SKA1 Max Baseline is 160 kms
% Theta_4 = 20; %In Degrees
% Phi_4 = 0; %In Degrees
% %Offset Index with SCFO sampling for Antenna-#2
% FO_Idx4 = 1*1003;

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Position of the Delay Center on the Sky at the Start of the Observation             
%Specified by the Declination and the Right-Assertion Angles of the Delay 
%Center at the 1PPS  
DCen.DecA0 = 00; %IN Degrees
DCen.RAsc0 = 60; %IN Degrees

%Assigning the Declanation and the Right-Assetion Angles of the Source 
%Position at the 1PPS  
SPos.DecA0 = DCen.DecA0+00.000; %IN Degrees
SPos.RAsc0 = DCen.RAsc0+00.000; %IN Degrees

%% Sample Quantizing
%Data
%Input Sample Bit-Width
if (Band_ID == 1) || (Band_ID == 2) || (Band_ID == 3)
    W_IP = 8;
    Peak_to_IP_RMS_Ratio = 44.5; % Approximately 2^7/2.872 - That allows 99% Correlator Efficiency
else % i.e. (Band_ID == 4) || (Band_ID == 5a) || (Band_ID == 5a)
    W_IP = 4;
    Peak_to_IP_RMS_Ratio = 2.682; % Approximately 2^3/2.983 - That allows 98.85% Correlator Efficiency
end %IF 
W_US = 8; %OutputUp-Sampler Bit-Width | Only for Band 3
W_CC = 16; %Output of the CC Bit-Width
W_RS = 16; %Output of the ReSampler Bit-Width
W_IC = 9; %Output of the IC Bit-Width

%Coefficient
Wc = 19; %Coefficient Word length

%Scaling Factors
Peak_to_US_RMS_Ratio = 44.5; % Only for Band 3 - Approximately 2^7/2.872 - That allows 99% Correlator Efficiency
Peak_to_CC_RMS_Ratio = 3600; % Approximately 2^15/9.124 - That allows 99.9% Correlator Efficiency
Peak_to_RS_RMS_Ratio = 3600; % Approximately 2^15/9.124 - That allows 99.9% Correlator Efficiency
Peak_to_IC_RMS_Ratio = 8; 

%% Threshold Detection Parameters at the Input and the Output of Coarse Channelizer
%Short-Temp Epoch for Power Calculation
STL = 128; %In Samples
%Forgetting factor for Long-Term Power Calculations
C = 1-2^-12;

%% Threshold Detection Parameters at the Output of Imaging Channelizer
%Short-Temp Epoch for Power Calculation
STL_IC = 6;
%Forgetting factor for Long-Term Power Calculations
C_IC = 1-2^-8;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% NOTE - NOT Reccomended to Change Parameters Beyond this Point           %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%File Name for Saving the Configuration parameters and Simulation Results
Data_Time_Now = clock;
FolderName = sprintf('SKA1_Mid_CBF_Normal_Imaging_Realizable_Sim_%d%02d%02d_%02d%02d',Data_Time_Now(1:5));
mkdir(FolderName);
save(fullfile(FolderName,'Configuration_Parameters.mat'));

%% Band Specification
Band_Name_List = {'1','2','3','4','5A','5B'};

switch Band_ID
    case 1
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 1 spans 0.35 - 1.05 GHz
        Fl = 0.35E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.05E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 0E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 1; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 2
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 2 spans 0.95 - 1.76 GHz
        Fl = 0.95E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.76E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 0E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 1; %The ratio between the Original Sample Rate and the Sample Rate at the Input to the Mid.CBF
    case 3
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 3 spans 1.65 - 3.05 GHz
        Fl = 0.05E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.45E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 1.6E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 2; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; %  %The Base Sampling Frequency with SCFO Sampling - in Hz |
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 4/5; %The ratio between the Original Sample Rate and the Sample Rate at the Input to the Mid.CBF
    case 4
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 4 spans 2.8 - 5.18 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 2.05E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 8/3; %The ratio between the Original Sample Rate and the Sample Rate at the Input to the Mid.CBF
    case 5
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 5a spans 4.6 - 8.5 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 4.5E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling - NOTE even if Band 5a is 
        %sampled in Nyquist Zone 2, the proposed frequency shift in extracting 
        %the observation band make it as if it was sampled in Nyquist Zone 1
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling 
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 3/2; %The ratio between the Original Sample Rate and the Sample Rate at the Input to the Mid.CBF
    case 6
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 5b spans 8.3 - 15.3 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 9.1E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling - - NOTE even if Band 5b is 
        %sampled in Nyquist Zone 2, the proposed frequency shift in extracting 
        %the observation band make it as if it was sampled in Nyquist Zone 1
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 8/3; %The ratio between the Original Sample Rate and the Sample Rate at the Input to the Mid.CBF
end %SWITCH

%% Additional Signal and Noise Specification

%Input Standard Deviation
x_sigma = 1;
%Common Component Standard Deviation
s_sigma = x_sigma*sqrt(p);
%Noise Standard Deviation
n_sigma = x_sigma*sqrt(1-abs(p));

%Scaling the Interferer
Int_Scale = sqrt(2)*(10.^(A_Int_dB/20))*x_sigma; 

%% Sampling Specifications
%NOTE Mid.CBF Employs Sample Clock Frequency Offset (SCFO) Scheme
%Offset Sample Frequency 
DeltaF = 1.8E3; %Offset Resolution - In Hz

%Initializing
Fs_Vec = [];
d_Vec = [];
for n = 1 : N_Rcp
    %The Actual Sampling Frequency - In Hz
    eval(sprintf('F%d = F_Base + (FSF*DeltaF*FO_Idx%d);',n,n));
    %Append that to the Sample Frequency Vector
    eval(sprintf('Fs_Vec = [Fs_Vec,F%d];',n));
    
    %The Position Vectors of the Antennas
    %The Distance vector in Cartesian Coordinates
    eval(sprintf('d%d_Vec = Dist_%d*[cosd(Theta_%d).*cosd(Phi_%d);cosd(Theta_%d).*sind(Phi_%d);sind(Theta_%d)];',n,n,n,n,n,n,n));
    %Append that to the Distance Vector 
    eval(sprintf('d_Vec = [d_Vec,d%d_Vec];',n));    
end %For k

 %% The Parmaters for Partial Integer Delay Correction with DDR4 Block Shifts
%Coarse Delay Update Period 
BDUDP = 200; % In Sec
%DDR4 Block Size
DDR4BS = 54; % In Samples

%% Setting the Configuration Parameters for Threshold Detection
%Specifying the Parameters for Threshold Detection after the Coarse Channalizer
Threshold_para_IP.STAL = STL; % Short Term Accumulation Length
Threshold_para_IP.STPTL = 8000; % Short Term Power Threshold
Threshold_para_IP.LTAC = C; % Long Term Accumulation Coefficient

%Specifying the Parameters for Threshold Detection after the Coarse Channalizer
Threshold_para_CC.STAL = STL; % Short Term Accumulation Length
Threshold_para_CC.STPTL = 100000; % Short Term Power Threshold
Threshold_para_CC.LTAC = C; % Long Term Accumulation Coefficient

%Specifying the Parameters for Threshold Detection after the Imaging Channalizer
Threshold_para_IC.STAL = STL_IC; % Short Term Accumulation Length
Threshold_para_IC.STPTL_Vec = 5*ones(1,16384); % Short Term Power Threshold
Threshold_para_IC.LTAC = C_IC; % Long Term Accumulation Coefficient

%% Specifying the Channelizer Specs for the Band 3 UpSampler
%% Custom MATLAB function 'B3_DUC' is used to model UpSampler

if Band_ID == 3
    
    %The Selectable Filters
    B3_UPS = load('US_Prototype_FIR_OS_16_15_L4_M5.mat');
    h0 = B3_UPS.h;
    L0 = B3_UPS.L;
    M0 = B3_UPS.M;
    clear B3_UPS
    
    %The Up-Sampling Factor
    USF = M0/L0;
    %Note Fs_Rat = 1/USF
        
    %The Length of the Prototype Filter
    Nh0 = length(h0);
    %Integer-Sample Delay through the UpSampler
    Insft_US_OP = ceil(-(Nh0-0)/2/L0); %In Samples
    
    %The Number of Initial Samples to Drop due to unfilled Processing Pipeline
    NDrop0 = ceil(Nh0/M0)+1;
    
    %Configuration Parameters of the UpSampler for Band 3
    %UnQuantized
    DUC_Config_para.h = h0;
    DUC_Config_para.L = L0;
    DUC_Config_para.M = M0;
    
    %Quantized
    Max_h0 = max(abs(h0));
    DUC_Config_para_Q.h = Max_h0*double(sfi(h0./Max_h0,Wc,Wc-1));
    DUC_Config_para_Q.L = L0;
    DUC_Config_para_Q.M = M0;
end

%% Specifying the Channelizer Specs for the Coarse-Channelizer (CoCh)
%% Custom MATLAB function 'OS_Poly_DFT_FB_SC' is used to model the CoCh
%The Prototype FIR Filter for the CoCh
%NOTE This is an Oversampled Polyphase DFT FB
 
%Band Specific CC - Selected above
%OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat');
%OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat');
 
h1 = OSProtoFltSpecs.h;
Num1 = OSProtoFltSpecs.Num;
Den1 = OSProtoFltSpecs.Den;
Nc1 = OSProtoFltSpecs.Nc;
 
%The Length of the Prototype Filter
Nh1 = length(h1);
%The Oversampling factor
Os1 = Num1/Den1;
%Input Sample Length
M1 = Nc1/Os1;
%Maximum Number of Taps per Polyphase Arm
Nt1 = ceil(Nh1/Nc1);
 
%Shift the Output to compensate the Delay-Through the Polyphase Filter-Bank
%Integer-Sample Delay
Insft_CC_OP = ceil(-(Nh1-1)/2/M1); %In Samples
%The Residual Fractional Delay
Frsft_CC_OP = Insft_CC_OP + (Nh1-1)/2/M1; %In Samples
 
%Initiating Temp Matrices
h1_2D = zeros(Nc1,Nt1);
%Filling the 2D Filter Mask
for k1 = 1 : Nc1
    L1 = length(h1(k1:Nc1:end));
    h1_2D(k1,1:L1) = h1(k1:Nc1:end);
end %FOR k
clear k1 L1;
Max_h1_2D = max(max(h1_2D));
 
%Generating Twiddle-Factors
%NOTE - Depends on the Selected Coarse Channel
TWD2D_SC = exp(2j*pi/Nc1*FS_ID*(0:1:Nc1-1).')*exp(2j*pi*FS_ID/Num1*(0:1:Num1-1));
 
%Sample rates out of the CoCh
FCC0 = F0/M1; Fs_CC_Vec = Fs_Vec/M1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Configuration Parameters for the Over-Sampling Polyphase DFT Filter-Bank
%UnQuantized
OSFB_Config_para.Nc = Nc1;
OSFB_Config_para.Num = Num1;
OSFB_Config_para.Den = Den1;
OSFB_Config_para.h2D = sqrt(Nc1)*h1_2D;
OSFB_Config_para.TWD2D_SC = TWD2D_SC;
OSFB_Config_para.N_Cons = ceil(Nh1/M1); %Note this is the Maximum propagation
%time in the filter interms of Output Samples

%Quantized
OSFB_Config_para_Q.Nc = Nc1;
OSFB_Config_para_Q.Num = Num1;
OSFB_Config_para_Q.Den = Den1;
OSFB_Config_para_Q.h2D = sqrt(Nc1)*Max_h1_2D*double(sfi(h1_2D./Max_h1_2D,Wc,Wc-1));
OSFB_Config_para_Q.TWD2D_SC = double(sfi(TWD2D_SC,Wc,Wc-1));
OSFB_Config_para_Q.N_Cons = ceil(Nh1/M1); %Note this is the Maximum propagation
%time in the filter interms of Output Samples

%% Specifying the Parameters for the Imaging Channelizer (IC)
%% Custom MATLAB function 'CS_Poly_DFT_FB_Batch' is used to model the IC
%The Prototype FIR Filter for the Imaging Channelizer
%NOTE This is a Critically-Sampled Polyphase DFT FB
CSProtoFltSpecs = load('CS_Prototype_FIR_CH16384.mat');
h2 = CSProtoFltSpecs.h;
Nc2 = CSProtoFltSpecs.Nc;
Os2 = 1; %The Imaging Channelizer is Critically Sampled -> Os2 = 1
%The Length of the Prototype Filter
Nh2 = length(h2);

%Shift the Output to compensate the Delay-Through the Polyphase Filter-Bank
%Integer-Sample Delay
Insft_IC_OP = round(-Nh2/2/Nc2); %In Samples
%NOTE Idealy the Integer Delay is round(-(Nh2-1)/2/Nc2). However, the
%filter coefficients are arrnaged to get the required 
%The Residual Fractional Delay
Frsft_IC_OP = Insft_IC_OP + Nh2/2/Nc2; %In Samples | Ideally Should be 0
 
%Maximum Number of Taps per Polyphase Arm
Nt2 = ceil(Nh2/Nc2);
%The 2D Coefficient-Mask
h2_2D = zeros(Nc2,Nt2);
%Filling the 2D Filter Mask
for k2 = 1 : Nc2
    L2 = length(h2(k2:Nc2:end));
    h2_2D(k2,1:L2) = h2(k2:Nc2:end);
end %FOR k2
clear k2 L2;
Max_h2_2D = max(max(h2_2D));
 
%Sample rates out of the Imaging Channelizer
F_IC = FCC0/Nc2*Os2;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Configuration Parameters for the Criticalled-Sampled Polyphase DFT 
%Filter-Bank
%UnQuantized
CSFB_Config_para.Nc = Nc2;
CSFB_Config_para.h2D = sqrt(Nc2)*h2_2D;

%Quantized
CSFB_Config_para_Q.Nc = Nc2;
CSFB_Config_para_Q.h2D = sqrt(Nc2)*Max_h2_2D*double(sfi(h2_2D/Max_h2_2D,Wc,Wc-1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters for Calculating the Frequency Shift to Align Subsequant Channels across Coarse 
%Calculating the Frequency-Shift 'FA_Sft' for aligning subsequency 
%channels across CCs
%Procedure
% Step-1 : Define C2BW = (FCC0/Nc2/Os2)
% Step-2 : Find nk such that | F_o + C2BW*nk - CC_CF0 | < 0.5*C2BW
% Step-3 : Evalaute F_ST = F_o + C2BW*nk - CC_CF0
CC_CF0 = F0/Nc1*FS_ID;
C2BW = (F0/M1/Nc2/Os2); %Channel Separation in Subsequant Channelizer
nk = floor((CC_CF0+0.5*C2BW)/C2BW);
FA_Sft = F_off + C2BW*nk - CC_CF0; % In Hz
%FA_Sft = FA_Sft - FA_Sft;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Geometric Delay Model Specifications
%The Order of the Higher Order Polynomials that is Converted Internally
N_Poly = 4;
%The Duration of Validity for the first Order Delay Polynomial 
Tv = 10E-3; % In sec
 
%Sample Delay Specification
%The Speed of EM Wave Propagation
c = 299792458; % In m/s
 
%Earth's Rotation Angular Velocity
%NOTE - ACTUAL Value is We = 7.2921150E-5 | Increased to Study Non Sidereal
%Tracking. However, Mid.CBF is required to track delay centers that are
%moving at the rate of 20 times the sidereal rate.
We = 7.2921150E-5; % In s^-1 - Sidereal
%We = 2E1*7.2921150E-5; % In s^-1 - 20 Times Sidereal
 
%Assigning Geometrical Delay Specs to be used for the function
%Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d1_Vec,F1,DSpecs)
DCSpecs.Tv = Tv; %Tv - The Valid Time for the First Order Delay Model
if Band_ID == 3 %F0 - The Nominal Sampling Frequency
    DCSpecs.F0 = F0/USF; % In Hz /USF
else
    DCSpecs.F0 = F0; % In Hz
end
DCSpecs.Nqst_Zone = Nqst_Zone; %Nqst_Zone - The Nyquist Zone of Sampling
DCSpecs.N_Poly = N_Poly; %N_Poly - The Order of the Higher Order Polynomial 
DCSpecs.c = c; %c - The Speed of Light
DCSpecs.We = We; % We - The Angular Velocity of the Earth
DCSpecs.N_DPE = 1E2; %Number of Sample Points Used in Estimating the First Order Model
DCSpecs.BDUDP = BDUDP; % BDUDP - Coarse Delay Update Period - In Sec
DCSpecs.DDR4BS = DDR4BS; % DDR4BS - DDR4 Block Size - In Samples
DCSpecs.N_BDE = 1E2; %Number of Sample Points Used in Estimating the Block Delay
%The Net Frequency Down-Shift of the Band before Bulk Delay correction
DCSpecs.F_DSft_BBD = F_DSft ; % In Hz
%The Net Frequency Down-Shift of the Band after Bulk Delay correction
DCSpecs.F_DSft_ABD = WB_Fsft; % In Hz
DCSpecs.Nc1 = Nc1; %The Number of Channels in the Coarse Channelizer
DCSpecs.Os1 = Os1; %The Over-Sampling Factor in the Coarse Channelizer
DCSpecs.FS_ID = FS_ID; %The Index of the Selected Coase Frequency Slice
DCSpecs.FA_Sft = FA_Sft; %An Additiona Frequency Shift to be Performed 
%The ratio between the Original Sample Rate and the Sample rate at the Input to the Mid.CBF
if Band_ID == 3
    DCSpecs.USF = USF; %The Upsample factor between input data rate and the data rate at Bulk Delay is corrected
else
    DCSpecs.USF = 1;
end
%The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
DCSpecs.Fs_Rat = Fs_Rat;
 
%% Specifying the Signal Specs for the Custom MATLAB function  
%% 'Rand_Seq_TVD_ChirpZ' that generate the Sequences for A0 & A1
%Signal Specs
if Band_ID == 3
    SSpecs.F0 = F0/USF; % Nominal Sample Rate - In Hz 
    SSpecs.Fs_Rat = 1; % ie Fs_Rat*USF %The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
else
    SSpecs.F0 = F0; % Nominal Sample Rate - In Hz
    SSpecs.Fs_Rat = Fs_Rat; %The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
end %IF
SSpecs.Fl = Fl; % In Hz
SSpecs.Fu = Fu; % In Hz
SSpecs.F_Dsft = F_DSft; % In Hz; 
SSpecs.Nqst_Zone = Nqst_Zone; %Either 1 or 2
SSpecs.dError = 1E-5; % In Relative Units
 
%% Configuring the Delay | Phase Synthesizer and the ReSampler
%Estimating the Time Varying Delay and Phase Corrections and Correction for 
%the Doppler Shift using a First-Order Delay Polynomial. Then these
%estimates are used to determine the Required Integer Delay Correction, the
%Index of the Fractional-Delay Filter and the Index of the Sine-Cosine LUT
%for Phase Correction
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Selecting the Fractional-Delay Filter-Bank
 
%Coefficient Sets for the Interpolation Filter
%512 Delay-Step
%FractFltBankSpecs = load('Fract_Dly_FB_CVX_DS_512.mat');

%1024 Delay-Step
FractFltBankSpecs = load('Fract_Dly_FB_CVX_DS_1024.mat');

hFD_FB = (FractFltBankSpecs.hFD_FB).';
Max_hFD_FB = max(max(hFD_FB));
% Os1 = FractFltBankSpecs.Os; % Redundent
% DStep = FractFltBankSpecs.DSteps; % Redundent
Nh_FD = FractFltBankSpecs.N_FD;
clear FractFltBankSpecs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Phase Correction factors with 'N_SC_LUT' stored Coefficients in a LUT
N_SC_LUT = 2^18;
SC_LUT = exp(2j*pi*(0:1:N_SC_LUT-1).'/N_SC_LUT);
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Configuering the ReSampler_ERP_Corr
%UnQuantized
ReSamp_Config_para.F0 = FCC0;
ReSamp_Config_para.hFD_FB = hFD_FB;
ReSamp_Config_para.SC_LUT = SC_LUT;

%Quantized
ReSamp_Config_para_Q.F0 = FCC0;
ReSamp_Config_para_Q.hFD_FB = Max_hFD_FB*double(sfi(hFD_FB/Max_hFD_FB,Wc,Wc-1));
ReSamp_Config_para_Q.SC_LUT = double(sfi(SC_LUT,Wc,Wc-1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Specifying Parameters for Wideband Frequency-Shifter
%UnQuantized
WBFS_Specs.N_SC_LUT = N_SC_LUT;
WBFS_Specs.SC_LUT = SC_LUT;
%UnQuantized
WBFS_Specs_Q.N_SC_LUT = N_SC_LUT;
WBFS_Specs_Q.SC_LUT = double(sfi(SC_LUT,Wc,Wc-1));

%% Configuring the CoCh Ripple Correction
%[GC_Vec] = Coch_MagResp_Correction(Fs_CC_Vec,GCCorrSpecs);
GCCorrSpecs.h1 = h1; %The coefficients of the Coch
GCCorrSpecs.M1 = M1; %The Down Sampling Factor of the Coch
GCCorrSpecs.Os1 = Os1; %The Over-Sampling Sampling Factor of the Coch
GCCorrSpecs.Nc2 = Nc2; %The Number of Channels in the Subsequent Channelizer or FFT
GCCorrSpecs.F0 = F0/M1; %The Nominal Sample Rate
GCCorrSpecs.FS_ID = FS_ID; %The Index of the Selected Frequency Slice
GCCorrSpecs.FA_Sft = FA_Sft; %The Frequency Shift applied to Align the Subsequent Channels
GCCorrSpecs.N_Edge = 100; %Number of Channels Corrected byond the Processing Edge 

%Evalauting the Gain Corrections
[GC_Vec] = CoCh_MagResp_Correction(Fs_CC_Vec,GCCorrSpecs); 

%% Configuring the CoCh Scaling Factor
CC_Scale_Fac_Config.FS_ID = FS_ID; %Frequency Slice Index
CC_Scale_Fac_Config.M = M1; %Down Sampling Factor
CC_Scale_Fac_Config.Nc = Nc1; %Number of Channels
CC_Scale_Fac_Config.Fl = Fl; %Lower Edge of the Spectrum - in Hz
CC_Scale_Fac_Config.Fu = Fu; %Uppder Edge of the Spectrum - in Hz
CC_Scale_Fac_Config.WB_Fsft = WB_Fsft; %Wideband Frequency Shift - in Hz
CC_Scale_Fac_Config.C_Fac = 1.035; %Correction Factor - NOTE the Factor  1.035 is an empirical observation

CC_ScaleF = zeros(1,N_Rcp);
for n = 1 : N_Rcp
    CC_ScaleF(n) = CC_Scale_Fac(Fs_Vec(n),CC_Scale_Fac_Config);
end %FOR n_Rcp

%% Evalauting the IC Scaling Factor
%The Lower Edge of the Frequency Slice
FS_Fl = CC_CF0 - 0.5*FCC0;
%The Lower Edge of the Frequency Slice
FS_Fu = CC_CF0 + 0.5*FCC0;

%The Band edges after windeband Frequency Shift
Fl_sft = Fl + WB_Fsft;
Fu_sft = Fu + WB_Fsft;

%Determining the Spectral Occupancy
if (Fl_sft <= FS_Fl) && (Fu_sft >= FS_Fu)
    Rel_Spect_Ocp_IC = FCC0;
elseif (Fl_sft > FS_Fl) && (Fu_sft >= FS_Fu)
    Rel_Spect_Ocp_IC = (FS_Fu - Fl_sft);
elseif (Fl_sft <= FS_Fl) && (Fu_sft < FS_Fu)
    Rel_Spect_Ocp_IC = (Fu_sft - FS_Fl);
else
    Rel_Spect_Ocp_IC = (Fu_sft - Fl_sft);
end
IC_Scale_Fac = 1.017*sqrt(FCC0/Rel_Spect_Ocp_IC); %Note the 1.017 is determined by observation
  
%% Some Other Parameters
%Number of Flags
N_Flg = 2;

%The Optimal Number of Samples in Wideband that fascilitate the 
Ns_N = round(Ns_O*F_IC/F0)*M1*Nc2;
N_OV = ceil((Nh1+M1*(Nh2+Nh_FD))*F_IC/F0); % In Samples - The Number of Samples at the Output of the Imaging Channelizer that needs to be Overlapped

if Band_ID == 3
    N_CC_OP = ceil(Ns_N*USF/M1); %The Number of Output Frames from CoCh
    T_AOV = -USF*Nh0/F0; % In sec - Additional Overlap for Band 3
else    
    N_CC_OP = ceil(Ns_N/M1); %The Number of Output Frames from CoCh
    T_AOV = 0; % In sec - Additional Overlap for Other bands
end% In sec
 
%The Number of Output Frames from Imaging Channelizer
N_IC_OP = ceil(N_CC_OP/Nc2);

%Number of iterations required to cover the Simulation Duration
NItt = ceil(T_Sim*F_IC./(N_IC_OP-N_OV));
%NItt = 1; %NOTE to run just one iteration for testing purposes

%Updating the Start time to Concide with a Imaging Channelizer Sample Time
T_si_IC = round(T_si*F_IC)/F_IC;

%Initialization the Auto and Cross-Correlation Accumulations Bins
AC_Vec = zeros(N_Rcp,Nc2); %Auto Correlation - UnQuantized
ACQ_Vec = zeros(N_Rcp,Nc2); %Auto Correlation - Quantized
%Number of Cross Correlation Pairs
N_XC_Pair = N_Rcp*(N_Rcp-1)/2;
XC_Vec = zeros(N_XC_Pair,Nc2) + 1j*zeros(N_XC_Pair,Nc2); % UnQuantized
XCQ_Vec = zeros(N_XC_Pair,Nc2) + 1j*zeros(N_XC_Pair,Nc2); % Quantized
%Composing a List of Cross Correlation Indecies
XC_List = zeros(N_XC_Pair,2);
XC_Idx = 1;
for n = 1 : N_Rcp-1
    for m = n+1 : N_Rcp
        XC_List(XC_Idx,:) = [n,m];
        XC_Idx = XC_Idx + 1;
    end
end

%% Starting the Iterations
fprintf('Running Simulations for Band %s - Frequency Slice %d\n\n',Band_Name_List{Band_ID},FS_ID);

for nItt = 1 : NItt
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('Running the Iteration %d of %d\n',nItt,NItt);
    fprintf('Started on %d-%02d-%02d || %02d:%02d:%02.2f\n',clock);

    %Re-Afirming the Number of Samples
    Ns = Ns_N; %As Band 3 is ReSampled in the run and changes Ns in the process
    
    %% Start Time at the Reference Antenna for nItt-th Iteration
    T_s = T_si_IC + ((nItt-1)*(N_IC_OP-N_OV) - (N_OV+Insft_IC_OP))/F_IC - T_AOV; % In sec    
    
    %The Sample Index at the Reference
    if Band_ID == 3
        SI0_BU_Sec_Start = floor(T_s);
        SI0_BU_Temp = transpose(int64((round((T_s-SI0_BU_Sec_Start)*F0/USF) + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_BU_Sec_Idx = int16(floor(double(SI0_BU_Temp)*USF./F0) +  SI0_BU_Sec_Start);
        SI0_BU_SS_Idx = mod(SI0_BU_Temp,F0/USF);
        %Packaging into a cell array
        SI0_BU = {SI0_BU_Sec_Idx,SI0_BU_SS_Idx};
        clear SI0_BU_Sec_Start SI0_BU_Temp SI0_BU_Sec_Idx SI0_BU_SS_Idx;
    else
        SI0_Sec_Start = floor(T_s);
        SI0_Temp = transpose(int64((round((T_s-SI0_Sec_Start)*F0) + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_Sec_Idx = int16(floor(double(SI0_Temp)./F0) +  SI0_Sec_Start);
        SI0_SS_Idx = mod(SI0_Temp,F0);
        %Packaging into a cell array
        SI0 = {SI0_Sec_Idx,SI0_SS_Idx};
        clear SI0_Sec_Start SI0_Temp SI0_Sec_Idx SI0_SS_Idx;
    end % IF
    
    %Using the Custom Function 'Geomatrical_Delay_Est'
    fprintf('Generating Geometric Delays & Corrections...')
    tic;
    if Band_ID == 3
        [SI_BU_Vec,TVD_Vec,BD_Vec,FODSR_Vec,FOPSR_Vec] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d_Vec,Fs_Vec/USF,DCSpecs);
    else
        [SI_Vec,TVD_Vec,BD_Vec,FODSR_Vec,FOPSR_Vec] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d_Vec,Fs_Vec,DCSpecs);
    end
    toc;
    
    %Sample Time Vectors
    ST_Vec = zeros(Ns,N_Rcp);
    if Band_ID == 3
        for n = 1 : N_Rcp
            ST_Vec(:,n) = double(SI_BU_Vec{n}{1})+double(SI_BU_Vec{n}{2})*USF/Fs_Vec(n);
        end %FOR n_Rcp
    else
        for n = 1 : N_Rcp
            ST_Vec(:,n) = double(SI_Vec{n}{1})+double(SI_Vec{n}{2})/Fs_Vec(n);
        end %FOR n_Rcp
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Using the function 'Rand_Seq_TVD_ChirpZ'
    fprintf('Generating Random Test Sequences...')
    tic;
    if Band_ID == 3
        if s_sigma ~= 0
            [s_BU_Vec,V_BU_Vec, ~] = Rand_Seq_TVD_ChirpZ(SSpecs,Fs_Vec/USF,ST_Vec,TVD_Vec);
            %Re-Scaling the Signal to Get Unity variance
            RMS_s_Vec = rms(s_BU_Vec,1); %Evaluating the Root Mean Square of Colum Vectors (Along Dimension '1')
            s_BU_Vec  = s_sigma*s_BU_Vec./RMS_s_Vec;
        else
            s_BU_Vec = zeros(Ns,N_Rcp); V_BU_Vec = true(Ns,N_Rcp); %No Signal Just Noise
        end % IF s_sigma ~= 0
        
        %Specifyng Guassian Noise
        n_BU_Vec = Gaussian_Noise([Fl,Fu],Fs_Vec/USF,Ns,n_sigma);
        %n_BU_Vec = n_sigma*randn(Ns,N_Rcp);
    else
        if s_sigma ~= 0
            [s_Vec,V_Vec, ~] = Rand_Seq_TVD_ChirpZ(SSpecs,Fs_Vec,ST_Vec,TVD_Vec);
            %Re-Scaling the Signal to Get Unity variance
            RMS_s_Vec = rms(s_Vec,1); %Evaluating the Root Mean Square of Colum Vectors (Along Dimension '1')
            s_Vec  = s_sigma*s_Vec./RMS_s_Vec;
        else
            s_Vec = zeros(Ns,N_Rcp); V_Vec = true(Ns,N_Rcp);  %No Signal Just Noise
        end %IF
        
        %Specifyng Guassian Noise
        n_Vec = Gaussian_Noise([Fl,Fu],Fs_Vec,Ns,n_sigma);
        %n_Vec = n_sigma*randn(Ns,N_Rcp);
    end
        
    %Interference
    int_Vec = Int_Scale*cos(2*pi*F_Int*ST_Vec);
        
    %Combining the common Signal, Noise and Interference
    if Band_ID == 3
        x_BU_Vec = (s_BU_Vec + n_BU_Vec + int_Vec);
        %clear sBU_Vec n_Vec int_Vec;
    else
        x_Vec = (s_Vec + n_Vec + int_Vec);
        %clear s_Vec n_Vec int_Vec;
    end
    toc;
    
     %% Quantising
    fprintf('Quantizing the Test Sequences...')
    tic;
    %Scaling for Optimum Quantizing and then Quantising        
    if nItt == 1 %Use the Same Scaling for all iterations
        IP_ScaleF = ones(1,N_Rcp);
        %if p ~= 1
        %    IP_ScaleF = rms(n_Vec,1)/sqrt(1-p)*IP_RMS_to_Peak_Ratio;
        %else
        %    IP_ScaleF = rms(s_Vec,1)*IP_RMS_to_Peak_Ratio;
        %end    
    end
    
    if Band_ID == 3
        %Saturation/Clipping Flag - Initiation
        Sat_Flag_BU_Vec = true(Ns,N_Rcp);
        %Scaling the Input before Quantizing
        x_BU_Vec = x_BU_Vec./IP_ScaleF;
        Temp_Scale_x_BU_Vec = x_BU_Vec/Peak_to_IP_RMS_Ratio;
        for n = 1 : N_Rcp
            Sat_Flag_BU_Vec(abs(Temp_Scale_x_BU_Vec(:,n)) > 1,n) = false;
        end %FOR n_Rcp
        xQ_BU_Vec = Peak_to_IP_RMS_Ratio.*double(sfi(Temp_Scale_x_BU_Vec,W_IP,W_IP-1));
        clear sBU_Vec n_Vec int_Vec Temp_Scale_x_BU_Vec;
    else
        %Saturation/Clipping Flag - Initiation
        Sat_Flag_Vec = true(Ns,2);
        %Scaling the Input before Quantizing
        x_Vec = x_Vec./IP_ScaleF;
        Temp_Scale_x_Vec = x_Vec/Peak_to_IP_RMS_Ratio;
        for n = 1 : N_Rcp
            Sat_Flag_Vec(abs(Temp_Scale_x_Vec(:,n)) > 1,n) = false;
        end %FOR n_Rcp
        xQ_Vec = Peak_to_IP_RMS_Ratio.*double(sfi(Temp_Scale_x_Vec,W_IP,W_IP-1));
        clear s_Vec n_Vec int_Vec Temp_Scale_x_Vec;
    end
    toc;      
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Plot the Unquantized and Quantized Sequences
%     figure('name','Unquantized and Quantized Wideband Sequences');
%     Color_List1 = linspecer(N_Rcp);
%     hold on;
%     for n = 1 : N_Rcp
%         plot(ST_Vec(:,n),x_Vec(:,n),'Color',Color_List1(n,:),'LineStyle','-.','LineWidth',1);
%         plot(ST_Vec(:,n),xQ_Vec(:,n),'Color',Color_List1(n,:),'LineStyle',':','LineWidth',1);
%     end
%     xlim([min(ST_Vec(1,:)),max(ST_Vec(end,:))]);
%     clear ST_Vec
    
    %% Combining the Flags for Data with Flags of RFI
    if Band_ID == 3
        %Note there are two Streams
        Flg_BU_Vec = cell(1,N_Rcp);
         Flg_BU_Vec{2} = cell(1,N_Flg);
        for n = 1 : N_Rcp
            Flg_BU_Vec{n} = cell(1,N_Flg);
            Flg_BU_Vec{n}{1} = V_BU_Vec(:,n);
            Flg_BU_Vec{n}{2} = Flg_BU_Vec{n}{1} & Sat_Flag_BU_Vec(:,n);
            %Flg_BU_Vec{n}{3} = Flg_BU_Vec{n}{1} & NO_RFI_Flg(:,n);
        end %FOR flg_idx
        clear V_BU_Vec Sat_Flag_BU_Vec;
    else
        %Note there are two Streams
        Flg_Vec = cell(1,N_Rcp);
         Flg_Vec{2} = cell(1,N_Flg);
        for n = 1 : N_Rcp
            Flg_Vec{N_Rcp} = cell(1,N_Flg);
            Flg_Vec{n}{1} = V_Vec(:,n);
            Flg_Vec{n}{2} = Flg_Vec{n}{1} & Sat_Flag_Vec(:,n);
            %Flg_Vec{n}{3} = Flg_Vec{n}{1} & NO_RFI_Flg(:,n);
        end %FOR flg_idx
        clear V_Vec Sat_Flag_Vec;
    end %IF
    
    %% Threshold Detection at the Input
    fprintf('Threshold detection and Flagging Input Seqiuences ...');
    tic;
    if Band_ID == 3
        %Initializing Output
        Flg_TD_BU_Vec  = cell(1,N_Rcp); %Valid
        IP_LTPS_Est_Vec = zeros(Ns,N_Rcp); %Input Long Term Power Sum
        IP_STPS_Est_Vec = zeros(Ns,N_Rcp); %Input Short Term Power Sum

        for n = 1 : N_Rcp
            Flg_TD_BU_Vec{n} = cell(1,N_Flg); %N_Flgs's for Each Stream
            
            %Assigning Valid Flag
            Flg_TD_BU_Vec{n}{1} = Flg_BU_Vec{n}{1};
            %Using the Custom Function Threshold_Det to Evalaute the Thresholding Flags
            [Flg_TD_BU_Vec{n}{2},IP_LTPS_Est_Vec(:,n),IP_STPS_Est_Vec(:,n)] = Threshold_Det(xQ_BU_Vec(:,n),Flg_BU_Vec{n}{2},Threshold_para_IP);
            %Assigning Ideal RFI Flags
            %Flg_TD_BU_Vec{N_Rcp}{3} = Flg_BU_Vec{N_Rcp}{3};            
        end %FOR n_Rcp
        clear FLg_BU_Vec;
    else
        %Initializing Output
        Flg_TD_Vec  = cell(1,N_Rcp); %Valid
        IP_LTPS_Est_Vec = zeros(Ns,N_Rcp); %Input Long Term Power Sum
        IP_STPS_Est_Vec = zeros(Ns,N_Rcp); %Input Short Term Power Sum

        for n = 1 : N_Rcp
            Flg_TD_Vec{n} = cell(1,N_Flg); %N_Flgs's for Each Stream
            
            %Assigning Valid Flag
            Flg_TD_Vec{n}{1} = Flg_Vec{n}{1};
            %Using the Custom Function Threshold_Det to Evalaute the Thresholding Flags
            [Flg_TD_Vec{n}{2},IP_LTPS_Est_Vec(:,n),IP_STPS_Est_Vec(:,n)] = Threshold_Det(xQ_Vec(:,n),Flg_Vec{n}{2},Threshold_para_IP);
            %Assigning Ideal RFI Flags
            %Flg_TD_Vec{N_Rcp}{3} = Flg_Vec{N_Rcp}{3};            
        end %FOR n_Rcp
        clear Flg_Vec;
    end
    toc;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Upsampling in the Band 3
    if Band_ID == 3
        fprintf('Up-Sampling the input Sequence in Band 3 ...');
        tic;
        %The Length of the Expected Output
        Ns = ceil(Ns*USF);
        x_Vec =  zeros(Ns,N_Rcp);
        xQ_Vec = zeros(Ns,N_Rcp);
        SI_Vec = cell(1,N_Rcp);
        Flg_TD_Vec = cell(1,N_Rcp);
        
        for n = 1 : N_Rcp
            %UnQuantized
            %Up-Sampling with B3_DUC
            [x_Vec(:,n),~] = B3_DUC(x_BU_Vec(:,n),Flg_TD_BU_Vec{n},SI_BU_Vec{n}{2}(1),DUC_Config_para);
            
            %Correcting the Integer Sample Delay due to the Up-Sampler
            x_Vec(:,n) = circshift(x_Vec(:,n),[Insft_US_OP,0]);
            
            %Quantized
            %Up-Sampling with B3_DUC
            [xQ_Vec(:,n),Flg_TD_Vec{n}] = B3_DUC(xQ_BU_Vec(:,n),Flg_TD_BU_Vec{n},SI_BU_Vec{n}{2}(1),DUC_Config_para_Q);
            
            %Correcting the Integer Sample Delay due to the Up-Sampler
            xQ_Vec(:,n) = circshift(xQ_Vec(:,n),[Insft_US_OP,0]);
            for n_flg = 1 : N_Flg
                Flg_TD_Vec{n}{n_flg} = circshift(Flg_TD_Vec{n}{n_flg},[Insft_US_OP,0]);
            end %FOR n_flg
            
            %%Re-Scaling and Re-Quantizing at the Output of the Band 3 Up-Sampler
            if nItt == 1
                %B3_US_OP_ScaleF = [rms(x_Vec(Flg_TD_Vec{1}{1},1)),rms(x_Vec(Flg_TD_Vec{2}{1},2))];
                B3_US_OP_ScaleF = ones(N_Rcp,1)/USF;
            end
            
            x_Vec(:,n) = x_Vec(:,n)/B3_US_OP_ScaleF(n);
            xQ_Vec(:,n) = Peak_to_US_RMS_Ratio*double(sfi(xQ_Vec(:,n)/B3_US_OP_ScaleF(n)/Peak_to_US_RMS_Ratio,W_US,W_US-1));
                        
            %Redefining the Sample Index Vector to Match
            %For the Antenna #n
            SIn_CC_Temp = transpose(int64((floor(double(SI_BU_Vec{n}{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
            %Evalauting Second and Sub-Second (SS) Indecies
            SIn_Sec_Idx = int16(floor(double(SIn_CC_Temp)./Fs_Vec(n))) +  SI_BU_Vec{n}{1}(1);
            SIn_SS_CC_Idx = mod(SIn_CC_Temp,Fs_Vec(n));
            %Packaging into a cell array
            SI_Vec{n} = {SIn_Sec_Idx,SIn_SS_CC_Idx};
            clear SIn_Temp SIn_Sec_Idx SIn_SS_Idx
        end %FOR
        toc;
        
        %Redefining the Sample Index Vector to Match
        %For the Reference Antenna
        SI0_Temp = transpose(int64((floor(double(SI0_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_Sec_Idx = int16(floor(double(SI0_Temp)./F0)) +  SI0_BU{1}(1);
        SI0_SS_Idx = mod(SI0_Temp,F0);
        %Packaging into a cell array
        SI0 = {SI0_Sec_Idx,SI0_SS_Idx};
        clear SI0_Temp SI0_Sec_Idx SI0_SS_Idx;
        
        clear xBU_Vec xBUQ_Vec SI0_BU SI_BU_Vec VBU_Vec;
    end %IF
    
    %% Applying the Partial Coarse Delay correction with the DDR4 Block Read offset
    %Checking whether there is an Change in the DDR4 Block Delay (DDR4_BD) and
    %if it does End the Processing
    
    %NOTE The Reference sample Sequence Doesn't Need to be Corrected for Bulk
    %Delay Correcting the Bulk Delay in the Antenna
    fprintf('Applying Bulk Delay Corrections....');
    tic;
    %NO RFI & UnQuantized
    [SI_BD_Vec,x_BD_Vec,Flg_BD_Vec] = Bulk_Delay_Corr(SI_Vec,x_Vec,Flg_TD_Vec,BD_Vec,Fs_Vec,DDR4BS);
    %NO RFI & Quantized
    [~,xQ_BD_Vec,~] = Bulk_Delay_Corr(SI_Vec,xQ_Vec,Flg_TD_Vec,BD_Vec,Fs_Vec,DDR4BS);    
    toc;
    clear SI_Vec x_Vec xQ_Vec Flg_TD_Vec;
    
      %% Performing Wideband Frequency Shifting SID1
    fprintf('Applying Wideband Frequency Shift....')
    tic;
    %UnQuantized
    x_FS_Vec = WB_FS(x_BD_Vec,SI_BD_Vec,WB_Fsft*ones(1,N_Rcp),Fs_Vec,WBFS_Specs);
    %Quantized
    xQ_FS_Vec = WB_FS(xQ_BD_Vec,SI_BD_Vec,WB_Fsft*ones(1,N_Rcp),Fs_Vec,WBFS_Specs_Q);
    toc;
    clear x_BD_Vec xQ_BD_Vec
    %NOTE - Frequency Shift is a Sample to Sample Operation and therefore
    %the Valid Flag Does not Change
    
    %% Plotting the Output of Wideband Frequency Shifting
    %If needed, add code to plot Outputs of Wideband Frequency Shifting
%     figure('name','Unquantized and Quantized Outputs of Wideband Frequency Shifting');
%     Color_List1 = linspecer(N_Rcp);
%     Temp_SI_BD_Idx_Vec = zeros(Ns,N_Rcp);    
%     for n = 1 : N_Rcp
%         Temp_SI_BD_Idx_Vec(:,n) = Fs_Vec(n).*double(SI_BD_Vec{n}{1})+double(SI_BD_Vec{n}{2});
%         subplot(211)
%         hold on;
%         plot(Temp_SI_BD_Idx_Vec(:,n),real(x_FS_Vec(:,n)),'Color',Color_List1(n,:),'LineStyle','-.');
%         plot(Temp_SI_BD_Idx_Vec(:,n),real(xQ_FS_Vec(:,n)),'Color',Color_List1(n,:),'LineStyle','--','LineWidth',1);
%         hold off
%         title('Real Components');
%         subplot(212)
%         hold on;
%         plot(Temp_SI_BD_Idx_Vec(:,n),imag(x_FS_Vec(:,n)),'Color',Color_List1(n,:),'LineStyle','-.');
%         plot(Temp_SI_BD_Idx_Vec(:,n),imag(xQ_FS_Vec(:,n)),'Color',Color_List1(n,:),'LineStyle','--','LineWidth',1);
%         hold off
%        title('Imaginary Components');
%     end
%     xlim([min(Temp_SI_BD_Idx_Vec(1,:)),max(Temp_SI_BD_Idx_Vec(end,:))]);
%     clear Temp_SI_BD_Idx_Vec;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Using the Function 'OS_Poly_DFT_FB_SC'
    %Zero-Padding the CoCh Input in order to get Integer number of frames
    CC_IP  = [x_FS_Vec; zeros(N_CC_OP*M1-Ns,N_Rcp)]; %UnQuantize
    CCQ_IP = [xQ_FS_Vec; zeros(N_CC_OP*M1-Ns,N_Rcp)]; %Quantize
    %Flags
    Flg_CC_IP = cell(1,N_Rcp); %Initializing
    for n = 1 : N_Rcp
        for n_flg = 1 : N_Flg
            Flg_CC_IP{n}{n_flg} = [Flg_BD_Vec{n}{n_flg}; false(N_CC_OP*M1-Ns,1)];
        end %For n_flg
    end %FOR n-Rcp
    clear x_Vec xQ_Vec Flg_BD_Vec;
    
    %In order to maintain the Correct sample order in Commutator of
    %Shift the Input by
    Nsft_CC_IP = zeros(1,N_Rcp); %Initiating
    for n = 1 : N_Rcp
        Nsft_CC_IP(n) = mod(SI_BD_Vec{n}{2}(1),M1); %In Samples
    end %For n_Rcp
    
    %Rotating the Input Vector to reflect the sample-order fed to the
    %Commutators of the OS Channelizer
    for n = 1 : N_Rcp
        %UnQuantized
        CC_IP(:,n)  = circshift(CC_IP(:,n),[Nsft_CC_IP(n)-1,0]); 
        %Quantized
        CCQ_IP(:,n) = circshift(CCQ_IP(:,n),[Nsft_CC_IP(n)-1,0]); 
        %Flags
        for n_flg = 1 : N_Flg
            Flg_CC_IP{n}{n_flg} = circshift(Flg_CC_IP{n}{n_flg},[Nsft_CC_IP(n)-1,0]);
        end %For n_flg
    end
            
    %The Sample Index Vectors for the CC Outputs [SI0,SI1]
    %Reference sequence for A#0
    SI0_CC_Temp = transpose(int64(floor(double(SI0{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI0_Sec_CC_Idx = int16(floor(double(SI0_CC_Temp)./FCC0)) + SI0{1}(1);
    SI0_SS_CC_Idx = mod(SI0_CC_Temp,FCC0);
    %Packaging into a cell array
    SI0_CC = {SI0_Sec_CC_Idx,SI0_SS_CC_Idx}; clear SI0_CC_Temp SI0_Sec_CC_Idx SI0_SS_CC_Idx SI0;
    
    %Redefining the Sample Index Vector For the Antenna #n
    SI_CC_Vec = cell(1,N_Rcp);
    for n = 1 : N_Rcp
        SIn_CC_Temp = transpose(int64((floor(double(SI_BD_Vec{n}{2}(1))/M1) + (0 : 1 : N_CC_OP-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SIn_Sec_CC_Idx = int16(floor(double(SIn_CC_Temp)./Fs_CC_Vec(n))) +  SI_BD_Vec{n}{1}(1);
        SIn_SS_CC_Idx = mod(SIn_CC_Temp,Fs_CC_Vec(n));
        %Packaging into a cell array
        SI_CC_Vec{n} = {SIn_Sec_CC_Idx,SIn_SS_CC_Idx}; 
    end %FOR n_Rcp
    clear SIn_CC_Temp SIn_Sec_CC_Idx SIn_SS_CC_Idx SI_BD_Vec;
    
    %The Barrel-Roller Indices
    BRI = zeros(1,N_Rcp); %Initiating
    for n = 1 : N_Rcp
        BRI(n) = mod(SI_CC_Vec{n}{2}(1),Num1);
    end %FOR n_Rcp
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Initializing
    CC_OP = zeros(N_CC_OP,N_Rcp); %UnQuantized
    CCQ_OP = zeros(N_CC_OP,N_Rcp); %Quantized
    Flg_CC_OP  = cell(1,N_Rcp); %Flags
    
    fprintf('Processing with the Coarse Channelizer...')
    tic;
    %UnQuantized
    for n = 1 : N_Rcp
        %For Sequence #n
        [CC_OP(:,n),~] = OS_Poly_DFT_FB_SC(CC_IP(:,n),Flg_CC_IP{n},BRI(n),OSFB_Config_para);
    end
    %Quantized
    for n = 1 : N_Rcp
        %For Sequence #n
        [CCQ_OP(:,n),Flg_CC_OP{n}] = OS_Poly_DFT_FB_SC(CCQ_IP(:,n),Flg_CC_IP{n},BRI(n),OSFB_Config_para_Q);
    end
    toc;
    clear CC_IP CCQ_IP Flg_CC_IP;
    
    %%Quantizing at the Output of the Coarse Channelizer
    if nItt == 1 %Use the Same Scalingfor all iterations
        %         CC_OP_RMS = zeros(1,N_Rcp);
        %         for n = 1 : N_Rcp
        %             CC_OP_RMS(n) = rms(CC_OP(Flg_CC_OP(:,n),n))*Peak_to_CC_RMS_Ratio;
        %         end % FOR n
        CC_OP_ScaleF = CC_ScaleF; %Pre Calculated
    end %IF
    
    %Re-Scaling & Re-Quantising    
    for n = 1 : N_Rcp
        CC_OP(:,n) = CC_OP(:,n)/CC_OP_ScaleF(n);
        CCQ_OP(:,n) = Peak_to_CC_RMS_Ratio*double(sfi(CCQ_OP(:,n)/Peak_to_CC_RMS_Ratio/CC_OP_ScaleF(n),W_CC,W_CC-1));
    end %FOR n
    
    %Correcting the Integer Sample Delay due to Polyphase Filter-Bak
    %For Data
    CC_OP  = [CC_OP(1-Insft_CC_OP:end,:);  zeros(-Insft_CC_OP,N_Rcp)]; %UnQuantized
    CCQ_OP = [CCQ_OP(1-Insft_CC_OP:end,:); zeros(-Insft_CC_OP,N_Rcp)]; %Quantized
    %Flags
    for n = 1 : N_Rcp
        for n_flg = 1 : N_Flg
            Flg_CC_OP{n}{n_flg} = [Flg_CC_OP{n}{n_flg}(1-Insft_CC_OP:end,1); false(-Insft_CC_OP,1)];
        end %FOR n_flg
    end %FOR n
    
    %% Plotting the CoCh Output
    %If needed, add code to plot CoCh Output
%     figure('name','Unquantized and Quantized CC Outputs');
%     Color_List1 = linspecer(N_Rcp);
%     Temp_SI_CC_Idx_Vec = zeros(N_CC_OP,N_Rcp);    
%     for n = 1 : N_Rcp
%         Temp_SI_CC_Idx_Vec(:,n) = Fs_CC_Vec(n).*double(SI_CC_Vec{n}{1})+double(SI_CC_Vec{n}{2});
%         subplot(211)
%         hold on;
%         plot(Temp_SI_CC_Idx_Vec(:,n),real(CC_OP(:,n)),'Color',Color_List1(n,:),'LineStyle','-.');
%         plot(Temp_SI_CC_Idx_Vec(:,n),real(CCQ_OP(:,n)),'Color',Color_List1(n,:),'LineStyle','--','LineWidth',1);
%         hold off
%         title('Real Components');
%         subplot(212)
%         hold on;
%         plot(Temp_SI_CC_Idx_Vec(:,n),imag(CC_OP(:,n)),'Color',Color_List1(n,:),'LineStyle','-.');
%         plot(Temp_SI_CC_Idx_Vec(:,n),imag(CCQ_OP(:,n)),'Color',Color_List1(n,:),'LineStyle','--','LineWidth',1);
%         hold off
%        title('Imaginary Components');
%     end
%     xlim([min(Temp_SI_CC_Idx_Vec(1,:)),max(Temp_SI_CC_Idx_Vec(end,:))]);
%     clear Temp_SI_CC_Idx_Vec;
    
    %% Threshold Detection after the Coarse Channalizer
    
    fprintf('Threshold detection and Flagging Coarse Channel Output Sequences ...');
    tic;
    %Initializing Flags
    Flg_TD_CC_OP  = cell(1,N_Rcp); 
    CC_LTPS_Est_Vec = zeros(N_CC_OP,N_Rcp);
    CC_STPS_Est_Vec = zeros(N_CC_OP,N_Rcp);
    for n = 1 : N_Rcp
        Flg_TD_CC_OP{n} = cell(1,N_Flg); %N_Flgs's for Each Stream
        
        %Assigning Valid Flag
        Flg_TD_CC_OP{n}{1} = Flg_CC_OP{n}{1};
        %Using the Custom Function Threshold_Det
        [Flg_TD_CC_OP{n}{2},CC_LTPS_Est_Vec(:,n),CC_STPS_Est_Vec(:,n)] = Threshold_Det(CCQ_OP(:,n),Flg_CC_OP{n}{2},Threshold_para_CC);
        %Assigning Ideal RFI Flags
        %         Flg_TD_CC_OP{n}{3} = Flg_CC_OP{n}{3};
    end %FOR n
    toc;
    clear Flg_CC_OP
    
    %% Using the function 'ReSampler_ERP_Corr'
    %Initializing
    RS_OP_Vec = zeros(N_CC_OP,N_Rcp); %UnQuantized Data
    RSQ_OP_Vec = zeros(N_CC_OP,N_Rcp); %Quantized Data
    Flg_RS_OP = cell(1,2); %Flags    
    
    fprintf('Applying Delay and Phase Corrections...')        
    tic;
    for n = 1 : N_Rcp
        
        %UnQuantized - For Sequence #n
        [RS_OP_Vec(:,n),~] = ReSampler_ERP_Corr(CC_OP(:,n),Flg_TD_CC_OP{n},Fs_CC_Vec(n),SI_CC_Vec{n},SI0_CC,squeeze(FODSR_Vec(:,:,n)),squeeze(FOPSR_Vec(:,:,n)),ReSamp_Config_para);
        
        %Quantized - For Sequence #n
        [RSQ_OP_Vec(:,n),Flg_RS_OP{n}] = ReSampler_ERP_Corr(CCQ_OP(:,n),Flg_TD_CC_OP{n},Fs_CC_Vec(n),SI_CC_Vec{n},SI0_CC,squeeze(FODSR_Vec(:,:,n)),squeeze(FOPSR_Vec(:,:,n)),ReSamp_Config_para_Q);
        
    end %FOR n 
    toc;
    clear CC_OP CCQ_OP SI_CC_Vec Flg_TD_CC_OP;
   
    %Quantising the ReSampler Outputs
    if nItt == 1 %Use the Same Scalingfor all iterations
        %         RS_OP_RMS = zeros(1,N_Rcp);
        %         for n = 1 : N_Rcp
        %             RS_OP_RMS(n) = rms(RS_OP_Vec(V_RS_OP(:,n),n))*Peak_to_RS_RMS_Ratio;
        %         end %FOR n
        RS_OP_ScaleF = FCC0*ones(1,N_Rcp)./Fs_CC_Vec; 
        
    end %IF
    
    %Re-Scaling and Requantizing
    for n = 1 : N_Rcp
        RS_OP_Vec(:,n) = RS_OP_Vec(:,n)/RS_OP_ScaleF(n);
        RSQ_OP_Vec(:,n) = Peak_to_RS_RMS_Ratio*double(sfi(RSQ_OP_Vec(:,n)/Peak_to_RS_RMS_Ratio/RS_OP_ScaleF(n),W_RS,W_RS-1));
    end  %FOR n     
    
    %% Plotting ReSampler Output & Error
    %If needed, add code to plot ReSampler output
    
    %% Using the function 'CS_Poly_DFT_FB_Batch'
    
    %Zero-Padding the Input for the Imaging Channelizer Input
    IC_IP  = [RS_OP_Vec;  zeros(N_IC_OP*Nc2-N_CC_OP,N_Rcp)];
    ICQ_IP = [RSQ_OP_Vec; zeros(N_IC_OP*Nc2-N_CC_OP,N_Rcp)];
    %Flags
    Flg_IC_IP = cell(1,2);
    for n = 1 : N_Rcp
        for n_flg = 1 : N_Flg
            Flg_IC_IP{n}{n_flg} = [Flg_RS_OP{n}{n_flg}; false(N_IC_OP*Nc2-N_CC_OP,1)];
        end % FOR n_flg
    end % FOR n
    clear RS_OP RSQ_OP R_RSQ_OP Flg_RS_OP;
    
    %In order to maintain the Correct sample order in Commutator of
    %Shift the Input by
    Nsft_IC_IP = mod(SI0_CC{2}(1),Nc2); %In Samples
    %NOTE now both Sequences are have the Same Indices
    
    %Rotating the Input Vector to reflect the sample-order fed to the
    %Commutators of the OS Channelizer
    IC_IP  = circshift(IC_IP, [Nsft_IC_IP-1,0]);
    ICQ_IP = circshift(ICQ_IP,[Nsft_IC_IP-1,0]);
    %Flags
    for n = 1 : N_Rcp
    for n_flg = 1 : N_Flg
        Flg_IC_IP{n}{n_flg} = circshift(Flg_IC_IP{n}{n_flg},[Nsft_IC_IP-1,0]);
    end % FOR n_flg 
    end % FOR n
    
    %The Sample Index Vectors for the CC Outputs [SI0,SI1]
    SI0_IC_Temp = transpose(int64(floor(double(SI0_CC{2}(1))./Nc2) + (0 : 1 : N_IC_OP-1)));
    SI0_Sec_IC_Idx = int16(floor(double(SI0_IC_Temp)./F_IC)) + SI0_CC{1}(1);
    SI0_SS_IC_Idx = mod(SI0_IC_Temp,F_IC);
    %Packaging into a cell array
    SI0_IC = {SI0_Sec_IC_Idx,SI0_SS_IC_Idx}; clear SI0_IC_Temp SI0_Sec_IC_Idx SI0_SS_IC_Idx
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Initializing
    IC_OP = zeros(N_IC_OP,Nc2,N_Rcp); %UnQuantized
    ICQ_OP = zeros(N_IC_OP,Nc2,N_Rcp); %Quantized
    Flg_IC_OP = cell(1,N_Rcp); %Flags
    
    fprintf('Processing the Signals with Imaging Channelizer...')
    tic;
    for n = 1 : N_Rcp
        %UnQuantized - For Sequence #n
        [IC_OP(:,:,n),~] = CS_Poly_DFT_FB_Batch(IC_IP(:,n),Flg_IC_IP{n},CSFB_Config_para);
        
        %Quantized - For Sequence #n
        [ICQ_OP(:,:,n),Flg_IC_OP{n}] = CS_Poly_DFT_FB_Batch(ICQ_IP(:,n),Flg_IC_IP{n},CSFB_Config_para_Q);
    end %FOR n
    toc;
    clear IC_IP ICQ_IP Flg_IC_IP;
    
    %Correcting the Integer Sample Delay due to Polyphase Filter-Bank
    %For Data
    IC_OP  = [IC_OP(1-Insft_IC_OP:end,:,:);  zeros(-Insft_IC_OP,Nc2,N_Rcp)]; %UnQuantized
    ICQ_OP = [ICQ_OP(1-Insft_IC_OP:end,:,:); zeros(-Insft_IC_OP,Nc2,N_Rcp)]; %Quantized
    %Flags
    for n = 1 : N_Rcp
        for n_flg = 1 : N_Flg
            Flg_IC_OP{n}{n_flg} = [Flg_IC_OP{n}{n_flg}(1-Insft_IC_OP:end); false(-Insft_IC_OP,1)];
        end % FOR n_flg
    end %FOR n
    
    %Shedding invalid samples at the front and the back before correlating
    First_1_Ant = zeros(1,N_Rcp);
    Last_1_Ant  = zeros(1,N_Rcp);
    for n = 1 : N_Rcp
        First_1_Ant(1,n) = find(Flg_IC_OP{n}{1},1,'first');
        Last_1_Ant(1,n) = find(Flg_IC_OP{n}{1},1,'last');
    end
    First_1_Idx = min(First_1_Ant);
    Last_1_Idx = max(Last_1_Ant);
    
    %For Data
    IC_OP   = IC_OP(First_1_Idx:Last_1_Idx,:,:);
    ICQ_OP  = ICQ_OP(First_1_Idx:Last_1_Idx,:,:);
    %Flags 
    for n = 1 : N_Rcp
        for n_flg = 1 : N_Flg
            Flg_IC_OP{n}{n_flg} = Flg_IC_OP{n}{n_flg}(First_1_Idx:Last_1_Idx,:,:);
        end %FOR n_flg
    end %FOR n
    %For Sample Indicies
    SI0_IC = {SI0_IC{1}(First_1_Idx:Last_1_Idx), SI0_IC{2}(First_1_Idx:Last_1_Idx)};
        
    %Displaying the Range of Time Indicies evalautes
    fprintf('Imaging Channel Time Series | Evalauted from %d-%d to %d-%d \n',SI0_IC{1}(1),SI0_IC{2}(1),SI0_IC{1}(end),SI0_IC{2}(end));
    
    %% Applying the Gain Correction Vectors & ReQuantizing
    [NICE,~] =  size(IC_OP);
    for n = 1 : N_Rcp
        %UnQuantized - For Sequence #n
        IC_OP(:,:,n) = (ones(NICE,1)*GC_Vec(n,:)).*squeeze(IC_OP(:,:,n));
        
        %Quantized - For Sequence #n
        ICQ_OP(:,:,n) = (ones(NICE,1)*GC_Vec(n,:)).*squeeze(ICQ_OP(:,:,n));
    end
    
    %ReQuantising
    if nItt == 1
        %         IC_OP_RMS = zeros(1,Nc2,N_Rcp);
        %         for n = 1 : N_Rcp
        %             IC_OP_RMS(:,:,n) = rms(IC_OP(V_IC_OP(:,n),:,n))*Peak_to_IC_RMS_Ratio;
        %         end %FOR n
        IC_OP_ScaleF = IC_Scale_Fac*ones(N_Rcp,Nc2);
        
    end %IF
    
    %Re-Scaling and Re-Quantizing
    for n = 1 : N_Rcp
        IC_OP(:,:,n) = IC_OP(:,:,n)./(ones(NICE,1)*IC_OP_ScaleF(n,:));
        ICQ_OP(:,:,n) = Peak_to_IC_RMS_Ratio.*double(sfi(ICQ_OP(:,:,n)./(ones(NICE,1)*IC_OP_ScaleF(n,:))./Peak_to_IC_RMS_Ratio,W_IC,W_IC-1));
    end %FOR n
    
    %% Comparison of Imaging Channelizer Output for a Given Time-Instant
    %     Sel_Ts = ceil(NICE*rand);
    %     IC_OP_TS = squeeze(IC_OP(Sel_Ts,:,:));
    %     ICQ_OP_TS = squeeze(ICQ_OP(Sel_Ts,:,:));
    %
    %     %Plotting
    %     figure;
    %     subplot(221)
    %     plot(abs(IC_OP_TS));
    %     title('Magnitude - UnQuntized');
    %
    %     subplot(222)
    %     plot(abs(ICQ_OP_TS));
    %     title('Magnitude - Quntized');
    %
    %     subplot(223)
    %     plot(angle(IC_OP_TS));
    %     title('Magnitude - UnQuntized');
    %
    %     subplot(224)
    %     plot(angle(ICQ_OP_TS));
    %     title('Magnitude - Quntized');
    
     %% Saving Imaging Channel Time Series for further analysis 
    %%Saving the Flags, Imaging Channel Time Series 
    FileName = fullfile(FolderName,sprintf('Data_Pack_%04d',nItt));
    save(FileName,'SI0_IC','Flg_IC_OP','IC_OP','ICQ_OP');
    
    %% Calculating the Auto & Cross Correlations
    
    %Auto Correlations
    for n = 1 : N_Rcp
        %UnQuantized
        AC_Vec(n,:) = ((nItt-1)*AC_Vec(n,:) + sum(abs(squeeze(IC_OP(:,:,n))).^2,1))./nItt;
        %Quantized
        ACQ_Vec(n,:) = ((nItt-1)*ACQ_Vec(n,:) + sum(abs(squeeze(ICQ_OP(:,:,n))).^2,1))./nItt;
    end
    
    %Cross Correlations
    for n = 1 : N_XC_Pair
        %Identifying the
        Rcp = XC_List(n,:);
        %Evalauting the Cross Collelation
        %UnQuantized
        XC_Vec(n,:) = ((nItt-1)*XC_Vec(n,:) + sum(squeeze(IC_OP(:,:,Rcp(1))).*conj(squeeze(IC_OP(:,:,Rcp(2)))),1))./nItt;
        %Quantized
        XCQ_Vec(n,:) = ((nItt-1)*XCQ_Vec(n,:) + sum(squeeze(ICQ_OP(:,:,Rcp(1))).*conj(squeeze(ICQ_OP(:,:,Rcp(2)))),1))./nItt;
    end
    
    %pause(1);
    fprintf('\n')
end % FOR nItt
 
%% Plotting the Auto and Cross-Correlations
CH_Idx = (-0.5*Nc2:1:0.5*Nc2-1);  
NFCH_l = -0.5*Nc2+ceil(Nc2*(Os1-1)/Os1/2);
NFCH_u = -NFCH_l;

%Converting to dB
Max_PL_dB = max(10*log10(abs(AC_Vec(1,:)))); %Maximum Power-Level
%UnQuantized
AC_Vec_dB = 10*log10(abs(AC_Vec)) - Max_PL_dB;
XC_Vec_dB = 10*log10(abs(XC_Vec)) - Max_PL_dB;
%Quantized
ACQ_Vec_dB = 10*log10(abs(ACQ_Vec)) - Max_PL_dB;
XCQ_Vec_dB = 10*log10(abs(XCQ_Vec)) - Max_PL_dB;

%% Plotting
%Assigning Colos for baselines
Color_List = linspecer(N_Rcp + N_XC_Pair);

%Assigning one figure for each Baseline
for k = 1 : N_XC_Pair
    %%Magnitude
    figure('name','The Magnitude of the Auto and Cross Correlation Spectra - Normal Imaging');
    hold on;
    %UnQuantized
    plot(CH_Idx,fftshift(AC_Vec_dB(XC_List(k,1),:)),'Color',Color_List(XC_List(k,1),:),'LineStyle','-.');
    plot(CH_Idx,fftshift(AC_Vec_dB(XC_List(k,2),:)),'Color',Color_List(XC_List(k,2),:),'LineStyle','-.');
    plot(CH_Idx,fftshift(XC_Vec_dB(k,:)),'Color',Color_List(N_Rcp+k,:),'LineStyle','-.');
    %Quantized
    plot(CH_Idx,fftshift(ACQ_Vec_dB(XC_List(k,1),:)),'Color',Color_List(XC_List(k,1),:),'LineStyle',':','LineWidth',1);
    plot(CH_Idx,fftshift(ACQ_Vec_dB(XC_List(k,2),:)),'Color',Color_List(XC_List(k,1),:),'LineStyle',':','LineWidth',1);
    plot(CH_Idx,fftshift(XCQ_Vec_dB(k,:)),'Color',Color_List(N_Rcp+k,:),'LineStyle',':','LineWidth',1);
    %Guidelines
    plot([NFCH_l,NFCH_l],[-Max_PL_dB-10,5],'k-.','LineWidth',1);
    plot([NFCH_u,NFCH_u],[-Max_PL_dB-10,5],'k-.','LineWidth',1);
    hold off;
    box on; grid on;
    xlim([CH_Idx(1), CH_Idx(end)]);
    Title = sprintf('The Auto- and Cross-Correlation Magnitude Spectra \nfor Antennas A%d & A%d in Band - %s | Frequency Slice %d',XC_List(k,1),XC_List(k,2),Band_Name_List{Band_ID},FS_ID);
    title(Title);
    xlabel('Channel-Index'); ylabel('Magnitude - dB');
    Legend_Text = [sprintf("UA%d",XC_List(k,1));sprintf("UA%d",XC_List(k,2));sprintf("UA%dA%d",XC_List(k,1),XC_List(k,2));sprintf("QA%d",XC_List(k,1));sprintf("QA%d",XC_List(k,2));sprintf("QA%dA%d",XC_List(k,1),XC_List(k,2))];
    legend(Legend_Text);
    
    %%Phase
    figure('name','The Phase Angle of the Cross Correlation Spectra - Normal Imaging');
    hold on;
    plot(CH_Idx,fftshift(angle(XC_Vec(k,:))),'Color',Color_List(N_Rcp+k,:),'LineStyle','-.');
    plot(CH_Idx,fftshift(angle(XCQ_Vec(k,:))),'Color',Color_List(N_Rcp+k,:),'LineStyle',':','LineWidth',1);
    %
    plot([NFCH_l,NFCH_l],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
    plot([NFCH_u,NFCH_u],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
    hold off;
    box on; grid on;
    xlim([CH_Idx(1), CH_Idx(end)]); %ylim([-1.15E-2*pi,1.15E-2*pi]);
    Title = sprintf('The Phase Angle of the Cross-Correlation Spectra \nfor Antennas A%d & A%d in Band - %s | Frequency Slice %d',XC_List(k,1),XC_List(k,2),Band_Name_List{Band_ID},FS_ID);
    title(Title);
    xlabel('Channel-Index'); ylabel('Angle - rad');
    Legend_Text = [sprintf("UA%dA%d",XC_List(k,1),XC_List(k,2));sprintf("QA%dA%d",XC_List(k,1),XC_List(k,2))];
    legend(Legend_Text);    
    
end %For

%% Appending the Results to the Configuration parameters
%These Information is sufficient to regerate any figure or additional
%estimnations
%save(FileName,'AC_Vec','XC_Vec','-append');