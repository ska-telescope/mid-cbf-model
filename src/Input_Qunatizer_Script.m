%Input Quantizer

%Inputs
%x_Vec - A (Ns x 2) Vector assuming to have rms of unity
%Parameters
%Peak_to_IP_RMS_Ratio - Peak Level to Desired Input RMS Ratio 
%W_In - Word length in bits 
%Outputs
%xQ_Vec - Quantized signal Vector (In double precision)
%Sat_Flag_Vec - Saturated

%Saturation/Clipping Flag - Initiation
Sat_Flag_Vec = true(Ns,2);
%Scaling the Input before Quantizing
Temp_Scale_x_Vec = x_Vec/Peak_to_IP_RMS_Ratio;
%Flagging the Saturation
Sat_Flag_Vec(abs(Temp_Scale_x_Vec(:,1)) > 1,1) = false;
Sat_Flag_Vec(abs(Temp_Scale_x_Vec(:,2)) > 1,2) = false;
%Quantizing and converting back to double precision
xQ_Vec = IP_RMS_to_Peak_Ratio.*double(sfi(Temp_Scale_x_Vec,W_In,W_In-1));