%The Custom MATLAB function
%[SI_Vec,TVD_Vec,BD,FODSR,FOPSR,HODCP] = Arbitrary_Delay_Est(HDM,T_s,Ns,Fs_Vec,DSpecs)
%%%%%%%%%%
%Inputs
% HDM - Higher Order Delay Model yields in sec (Note - Highest order coefficient 
%should be on the left most position)
% T_s - Strat Time - In sec 
% Ns - Number of samples 
% Fs_Vec - Samples Rate - In Hz
% DSpecs - a Structure containing the following
%     Tv - The Valid Time for the First Order Delay Model
%     N_Poly - The Order of the Higher Order Delay Correction Polynomial 
%     F0 - The Nominal Sampling Frequency - in Hz
%     Nqst_Zone - The Nyquist Zone of Sampling
%     N_DPE - Number of Sample Points Used in Estimating the First Order Model
%     BDUDP - Coarse Delay Update Period - In Sec
%     DDR4BS - DDR4 Block Size - In Samples
%     N_BDE - Number of Sample Points Used in Estimating the Block Delay
%     F_DSft_BBD - Apparent Frequency Down-Shift at the Receiver/Digitizer before Bulk-Delay
%     F_DSft_ABD - Apparent Frequency Up-Shift at the Receiver/Digitizer after Bulk-DelayNc1 = DPSpecs.Nc1; %The Number of Channels in the Coarse Channelizer
%     Nc1 - The Number of Channels in the Coarse Channelizer
%     Os1 - The Over-Sampling Factor in the Coarse Channelizer
%     FS_ID - The Index of the Selected Coase Frequency Slice
%     FA_Sft - Any Additional Frequency Shift to be applied at the ReSampler
%     USF - The Upsample factor between input data rate and the data rate at Bulk Delay is corrected
%     Fs_Rat - The ratio between the Original Sample Rate and the Sample rate at the Input to the CoCh
%%%%%%%%%
%Outputs
% SI_Vec - The Sample Indices vector - In a cell array of Ns samples
%          For just one Antenna in the form of 
%          [Second Index (int16) : Sub-Second Index (int64)]
%          For more than one Antenna in the form of : 
%          {Antenna Index {Second Index (int16), Sub-Second Index (int64)}}
% TVD_Vec - Time Variable Delay Vector - In sec (double array of Ns samples)
% BD - Bulk Delay Corrections - in the format of 
%      [Delay-Block Segment, [Delay Block and Delay Correction Info], Antenna], 
%      where [Delay Block and Delay] is defined as
%      [Block-Delay start time - in sec, Block-Delay end time - in sec, Block-Delay - in DDR4 Blocks]
% FODSR - First Order Delay Correction Register Values - in the form of
%         [Valid-Segment, [Delay Correction Info], Antenna], where
%         [Delay Correction Info] is defined 
%         [Sec-Index, Sub-Sec Index for Start of Delay Block, Sub-Sec Index
%         for End of Delay Block, Delay Correction Rate (D1), Delay Correction (D0)]
% FOPSR - First Order Phase Correction Register Values - in the form of
%         [Valid-Segment, [Phase Correction Info], Antenna], where
%         [Phase Correction Info] is defined 
%         [Sec-Index, Sub-Sec Index for Start of Phase Block, Sub-Sec Index for End of Phase Block, Phase Correction Rate (D1), Phase Correction (D0)]
% HODCP - Higher Order (i.e. N_Poly) Delay Correction Polynomial in the
%         form of
%         [Bulk-Delay Segment, [N_Poly Order Delay correction Polynomial], Antenna]
%
%This custom function yields the Time Variable Delay vector 'TVD_Vec' 
%corresponding to the Sample Index Vector 'SI_Vec' and the Block Delay 'BD' 
%applied with the DDR4 memory, the First Order Delay Synthesizer Registers 
%'FODSR' and the First Order Phase Synthesizer Registers 'FOPSR' for a set 
%of Higher Order Delay Polynomials 'HDM' for 'Ns' samples during the epoch 
%that starting at 'T_s'. The Sampling Rates ate specified by 'Fs_Vec' 
%where the other relevant specs are specified in 'DPSpecs'
%
%By Thushara Kanchana Gunaratne - - RO/RCO HAA, NRC CANADA
%Start date > 2017-04-18
%Modified > 2017-07-20 : To Model the Block Memory Delay Correction by DDR4
%in the Wideband
%Modified > 2017-07-28 : To yield the First Order Delay Polynomial and the
%First Order Phase Polynomials 
%Modified 2017-08-02 > To compute the Register Values for the First Order
%Delay and Phase Synthesizers
%Modified 2017-09-01 > To separate Delay polynomial from the SCFO scheme
%and insert the Frequency Shift to align the fine channels of all Frequency
%Slices
%Modified 2017-09-26 > To facilitate the additional frequency shift needed
%in Nyquist Zone 2 Sampling with the SCFO Scheme
%Modified 2017-11-28 > To change the way how Delay Correction Polynomial is
%derived
%Modified 2018-12-19 > To Incorporate Wideband Frequency Shifts after Bulk
%Delays and Signal Down Sampling in the Dish
%Modified 2019-01-10 > To specify the Time Indices in Second | Sub-Second 
%Time Stampling Indexing Scheme

function [SI_Vec,TVD_Vec,BD,FODSR,FOPSR,HODCP] = Arbitrary_Delay_Est(HODP,T_s,Ns,Fs_Vec,DPSpecs)

%% Extracting the Specs

%The Number of Higher Order Delay Polynomials and Highest Order
[N_DP,ON] = size(HODP); %[Number of Delay Polynomials, Order (+1) of the Delay Polynomials]
ON = ON-1; %Evaluating the Order of the Delay Polynomial
[~,NFs] = size(Fs_Vec);

if (N_DP ~= NFs)
    error('Error! The Number of Higher Order Delay Polynomials is different from the Number of Sampling Frequencies!')
end
%clear Nseq;

%Extracting Specs
Tv = DPSpecs.Tv; %Tv - The Valid Time for the First Order Delay Model
N_Poly = DPSpecs.N_Poly; %N_Poly - The Order of the Higher Order Polynomial 
F0 = DPSpecs.F0; %F0 - The Nominal Sampling Frequency - in Hz
Nqst_Zone = DPSpecs.Nqst_Zone; %Nqst_Zone - The Nyquist Zone of Sampling
N_DPE = DPSpecs.N_DPE; %Number of Sample Points Used in Estimating the First Order Model
BDUDP = DPSpecs.BDUDP; % CDUDP - Coarse Delay Update Period - In Sec
DDR4BS = DPSpecs.DDR4BS; % DDR4BS - DDR4 Block Size - In Samples
N_BDE = DPSpecs.N_BDE; %Number of Sample Points Used in Estimating the Block Delay
F_DSft_BBD = DPSpecs.F_DSft_BBD; % Apparent Frequency Down-Shift at the Receiver/Digitizer before Bulk-Delay
F_DSft_ABD = DPSpecs.F_DSft_ABD; %Apparent Frequency Up-Shift at the Receiver/Digitizer after Bulk-DelayNc1 = DPSpecs.Nc1; %The Number of Channels in the Coarse Channelizer
Nc1 = DPSpecs.Nc1; %The Number of Channels in the Coarse Channelizer
Os1 = DPSpecs.Os1; %The Over-Sampling Factor in the Coarse Channelizer
FS_ID = DPSpecs.FS_ID; %The Index of the Selected Coase Frequency Slice
FA_Sft = DPSpecs.FA_Sft; %Any Additional Frequency Shift to be applied at the ReSampler
USF = DPSpecs.USF; %The Upsample factor between input data rate and the data rate at Bulk Delay is corrected
Fs_Rat = DPSpecs.Fs_Rat; %The ratio between the Original Sample Rate and the Sample rate at the Input to the CoCh

%% Pre-Calculations for Earth's rotation Phase Correction
%(1) The Down-Sampling factor for the Coarse Channelizer
M1 = Nc1/Os1;
%(2) The Frequency Downshift performed by the Receiver/Digitizer - i.e. Before Bulk Delay)
%F_DSft_BBD - In Hz
%(3) The Frequency Downshift performed by the Mid.CBF - i.e. After Bulk Delay)
%F_Dsft_ABD - In Hz
%(4) The Apparent Frequency Downshift at the Coarse-Channelizer -
%Center-Frequency of the Selected Channel 
CC_CF_Vec = FS_ID*USF*Fs_Vec/Nc1; % In Hz
%(5) The Frequency Shift due to the Sample Clock Frequency Offset Scheme 
%Note there is an additional Frequency offset for Nyquist Zone 2 sampling
if Nqst_Zone == 1
    F_Dsft_Vec = F_DSft_BBD*ones(1,N_DP); % In Hz 
    SCFOS_Sft_Vec = FS_ID*(Fs_Vec-F0)*USF/Nc1; % In Hz : NOTE (Fn-F0) is the Frequency Offset for each Fn
elseif Nqst_Zone == 2
    %Note there is an additional Frequency offset for Nyquist Zone 2 sampling
    %NOTE This is a function of the  Sample Rates at the Receiver
    Ny2_OffSet = 0.5*Fs_Rat*(Fs_Vec-F0)*USF; % In Hz  
    F_Dsft_Vec = F_DSft_BBD*ones(1,N_DP) + Ny2_OffSet; % In Hz : NOTE (Fn-F0) is the Frequency Offset for each Fn
    SCFOS_Sft_Vec = FS_ID*(Fs_Vec-F0)*USF/Nc1 + Ny2_OffSet; % In Hz : NOTE (Fn-F0) is the Frequency Offset for each Fn
else
    error('Nyquist Zone should be either 1 or 2!');
end
%(6) The Ratio between the Original and Reference Sample Rates
Frat_Vec = Fs_Vec/F0;
%(6) The Original Sampling rates into the ReSampler
FCC_Vec = Fs_Vec*USF/M1;
%(7) The Reference Sampling rate out of the ReSampler
FCC0 = F0*USF/M1;

%% For the Coarse DDR4 Block-Shift Delay Correction Estimation

%The Boundaries of the DDR4 Block Delay Shift
TBD_l = floor(T_s/BDUDP)*BDUDP;
TBD_u = ceil((T_s+(Ns-1)/(USF*F0))/BDUDP)*BDUDP; %Note - USF*F0 used as the Bulk-Delay Is corrected after the Up-Sampling

%The Number of Block Segments has to be Estimated
NBD_Seg = round((TBD_u - TBD_l)/BDUDP);

%Initializing the Container for Block Delay Segments
BD = zeros(NBD_Seg,3,N_DP);
%The Container for Higher Order Delay Correction Polynomials
HODCP = zeros(NBD_Seg,N_Poly+1,N_DP);
%The Threshold error in the Delay correction Estimate
dE = 1E-18; % In sec

for n_dp = 1 : N_DP %For the 'N_DP' Number of Delay Polynomials
        
    for k = 1 : NBD_Seg
        
        %The Sample Time for the Duration of the Block Delay Update Period
        ST_BD = transpose(linspace(TBD_l+(k-1)*BDUDP,TBD_l+k*BDUDP,N_BDE));
        
        %Time Variable Delay during the Block Delay Update Period
        TVD_B = HODP(n_dp,end)*ones(N_BDE,1);
        for on = 1 : ON
            TVD_B = TVD_B + (ST_BD.^on)*HODP(n_dp,end-on);
        end %FOR on
        
        %% Evaluating the Time Variable delay correction at Each Sample Point
        %Initiation
        TVC_B = zeros(N_BDE,1);
        TVE_B = zeros(N_BDE,1);
        Itr_Idx = zeros(N_BDE,1);
        
        for n = 1 : N_BDE
            
            Itr_Idx(n) = 1;
            TVC_B(n) = TVD_B(n);
            TVE_B(n) = HODP(n_dp,end);
            for on = 1 : ON
                TVE_B(n) = TVE_B(n) + ((ST_BD(n)-TVC_B(n)).^on)*HODP(n_dp,end-on);
            end %FOR on
            
            while abs(TVC_B(n) - TVE_B(n)) > dE
                %Approximating the Current Estimate for the Delay Correction with
                %what is predicted by the function
                TVC_B(n) = TVE_B(n);
                
                TVE_B(n) = HODP(n_dp,end);
                for on = 1 : ON
                    TVE_B(n) = TVE_B(n) + ((ST_BD(n)-TVC_B(n)).^on)*HODP(n_dp,end-on);
                end %FOR on
                
                Itr_Idx(n) = Itr_Idx(n) + 1;
            end %WHILE
            
        end % FOR n
        
        %% Fitting Higher Order Polynomial for the Delay Correction        
        HODCP(k,:,n_dp) = polyfit(ST_BD,TVC_B,N_Poly);
        
       %% Evaluating the Bulk Delay in Wideband Samples
        %The Average Number of Samples Advance/Delay in DDR4 Blocks
        Avg_SD_DB = round(mean(TVC_B)*USF*Fs_Vec(n_dp)/DDR4BS);
        
        %Updating the BD_Segment with the Required Shift in DDR4 Blocks
        BD(k,:,n_dp) = int16([TBD_l+(k-1)*BDUDP, TBD_l+k*BDUDP,Avg_SD_DB]);
        
    end % FOR k
    
end % FOR a

%% For the First Order Delay Polynomial Estimation and Derivation of the Register values for Delay/Phase Synthesizers

%The Valid Time Segments 
Tv_l = floor(T_s/Tv)*Tv;
Tv_u = ceil((T_s+(Ns-1)/(USF*F0))/Tv)*Tv; %Note - USF*F0 used as the Delay is corrected after the Up-Sampling

%The Number of Valid Time Segments has to be Estimated
NDP_seg = round((Tv_u - Tv_l)/Tv);

%Initializing
%Container for the Total First Order Delay Polynomials
FOTDP = zeros(NDP_seg,5,N_DP);
%Container for the Registers of the First Order Delay Synthesizer
FODSR = zeros(NDP_seg,5,N_DP);
%Container for the Registers of the First Order Phase Polynomials
FOPSR = zeros(NDP_seg,5,N_DP);

for n_dp = 1 : N_DP %For the 'N_DP' Number of Delay Polynomials
    
    %The Index of the Block-Delay
    n_BD = 1;
    %The Second Index in the Start 
    Sec_Idex_C = floor(T_s);
    
    for k = 1 : NDP_seg
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Checking whether the FODP is in the Same Block-Delay Epoch
        if (Tv_l+(k-1)*Tv) >= double(BD(n_BD,2,n_dp))
            n_BD = n_BD + 1;
        end
        
        %Checking whether the Tv segment in the Same Second Index
        if floor((Tv_l+(k-1)*Tv)) > Sec_Idex_C
            Sec_Idex_C = Sec_Idex_C + 1;
        end
        
        %The Block Delay in Seconds
        T_BD = double(BD(n_BD,3,n_dp))*DDR4BS/(USF*Fs_Vec(n_dp)); % In s
        
        %The Sample Time Segment
        ST_Seg = transpose(linspace(Tv_l+(k-1)*Tv,Tv_l+k*Tv,N_DPE));
        
        %The Corresponding Time Variable Delay calculation
        TVDC_Seg = HODCP(n_BD,end,n_dp)*ones(size(ST_Seg));
        for on = 1 : N_Poly
            TVDC_Seg = TVDC_Seg + (ST_Seg.^on)*HODCP(n_BD,end-on,n_dp);
        end %FOR no
        
        %Estimation of First Order Delay Polynomial for Delay Correction
        %NOTE we use the First Order Polynomial Approximation Tou(t) = m*t + c
        FOP = polyfit(ST_Seg,TVDC_Seg,1);
        %The Total Delay Correction is Tou(t) = m*t/(1+m) + c/(1+m)
        mD = FOP(1); 
        cD = FOP(2);
        FOTDP(k,:,n_dp) = [Sec_Idex_C,Tv_l+(k-1)*Tv,Tv_l+k*Tv,mD,cD];
        
        %The Corresponding Boundary Samples
        N_l = round((Tv_l+(k-1)*Tv)*FCC0);
        N_u = round((Tv_l+k*Tv)*FCC0)-1;
        
        %The Residual Delay Correction in sec is Tou_C(t) = mD*t + (cD - T_BD)
        %The Residual Delay Correction in Samples is Tou_S(n) = F1/F0*m*n + F1*(c-TBD)
        %Finding the Register Values for the First Order Delay Synthesizer
        %Delay Rate for the Duration
        D1_S = Frat_Vec(n_dp)*mD;
        %Delay at N_l
        D0_S = double(N_l)*D1_S + FCC_Vec(n_dp)*(cD-T_BD);
        %Inserting the Register Values for the First Order Delay Synthesizer
        %FODSR(k,:,n_dp) = [N_l,N_u,D1_S,D0_S];
        
        %Estimation of First Order Phase Polynomial for Phase Correction
        %The Gradient
        %mP = ((CC_CF + F_Dsft)*mD - SCFOS_Sft)/FCC0;
        %The Intercept
        %cP = (CC_CF + F_Dsft)*cD - CC_CF*T_BD;
        %Finding the Register Values for the First Order Phase Synthesizer
        P1_S = (SCFOS_Sft_Vec(n_dp) + FA_Sft - (F_Dsft_Vec(n_dp) + CC_CF_Vec(n_dp) - F_DSft_ABD)*mD)/FCC0;
        P0_S = double(N_l)*P1_S + (CC_CF_Vec(n_dp) - F_DSft_ABD)*T_BD  - (F_Dsft_Vec(n_dp) + CC_CF_Vec(n_dp) - F_DSft_ABD)*cD;
        P0_S = mod(P0_S,1); %For Phase Rotations only the Fractional part is Significant
        %Inserting the Register Values for the First Order Delay Synthesizer
        %FOPSR(k,:,n_dp) = [N_l,N_u,P1_S,P0_S]; 
        
        %For Second Sub-Second Indexing
        N_l = mod(N_l,FCC0);
        N_u = mod(N_u,FCC0);
        
        FODSR(k,:,n_dp) = [Sec_Idex_C,N_l,N_u,D1_S,D0_S];
        FOPSR(k,:,n_dp) = [Sec_Idex_C,N_l,N_u,P1_S,P0_S]; 
        
    end %FOR k
    
end %FIR N_DP

%% For Actual Delay Estimation using the Higher Order Model

if N_DP == 1
    
    %Calculating Approximated Arbitrary delay at t = T_s
    Tn_s = T_s - (FOTDP(1,4,1)*T_s + FOTDP(1,5,1));
    
    %The Temporary Sample Index Vector
    SI_Temp = transpose(int64(round(Tn_s*Fs_Vec) + (0 : 1 : Ns-1)));
    %The Temporary Sample Time Vector
    ST_Temp = double(SI_Temp)/Fs_Vec;
    
    %Initializing
    TVD_Vec = HODP(1,end)*ones(Ns,1);
    for on = 1 : ON
        TVD_Vec = TVD_Vec + HODP(1,end-on)*((ST_Temp).^on);
    end %FOR no
    
    %Specifying the Second Sub-Second Format
    Sec_Idx = int16(floor(double(SI_Temp)./Fs_Vec));
    SS_Idx = mod(SI_Temp,Fs_Vec);
    %Packaging into a cell array
    SI_Vec = {Sec_Idx,SS_Idx};
    
else
    
    %Initializing
    TVD_Vec = zeros(Ns,N_DP);
    SI_Vec  = cell(1,N_DP);
    
    for n_dp = 1 : N_DP %For the 'N_DP' Number of Delay Polynomials
        
        %Calculating Approximated Arbitrary delay at t = T_s
        Tn_s = T_s - (FOTDP(1,4,n_dp)*T_s + FOTDP(1,5,n_dp));
        
        %The Temporary Sample Index Vector
        SI_Temp = transpose(int64(round(Tn_s*Fs_Vec(n_dp)) + (0 : 1 : Ns-1)));
        %The Temporary Sample Time Vector
        ST_Temp = double(SI_Temp)/Fs_Vec(n_dp);
        
        %Initializing
        TVD_Vec(:,n_dp) = HODP(n_dp,end)*ones(Ns,1);
        for on = 1 : ON
            TVD_Vec(:,n_dp) = TVD_Vec(:,n_dp) + HODP(n_dp,end-on)*((ST_Temp).^on);
        end %FOR no
        
        %Specifying the Second Sub-Second Format
        Sec_Idx = int16(floor(double(SI_Temp)./Fs_Vec(n_dp)));
        SS_Idx = mod(SI_Temp,Fs_Vec(n_dp));
        %Packaging into a cell array
        SI_Vec{n_dp} = {Sec_Idx,SS_Idx};
        
    end %FOR n_dp
    
end %IF