%The Custom function
%Cell_Array_Out = Cell_CircShift(Cell_Array_In,Shift)
%
%Input
% Cell_Array_In = A cell array up to 2-levels consisted of Column vectors of 
%                 integers or logicals need to be circularly shifted along
%                 its length
% Shift = An Integer corresponding to the number of elements to be shifted.
%         Positive integer shifts down and negetive integershifts up.
%
%Output 
% Cell_Array_Out = A cell array with the arrangement of the input 
%
% By Thushara Kanchana Gunaratne - RCO National Research Council Canada
% Start date -> 2019-01-17

function Cell_Array_Out = Cell_CircShift(Cell_Array_In,Shift)

%Checking whether 'Shift is an integer
if ~isinteger(Shift)
    error('Error! Shift is not an Integer!')
end

%Finding Out How Many Cells in the Cell_Array_In
if ~iscell(Cell_Array_In)
    %Level-0
    Cell_Array_Out = circshift(Cell_Array_In,[Shift,0]);
else
    %Level-1
    %Find the number of cell arrays in Level-1
    N1 = length(Cell_Array_In);
    %Initializing Output
    Cell_Array_Out = cell(1,N1);
    
    if ~iscell(Cell_Array_In{1})        
        %Iteratively assigning the rotated cells
        for n1 = 1 : N1
            Cell_Array_Out{n1} = circshift(Cell_Array_In{n1},[Shift,0]);            
        end %FOR n1
    else
         %Level-2
         %Find the Number of Cell Array in Level-2
         N2 = length(Cell_Array_In{1});
         %Initializing Output
         for n1 = 1 : N1
             Cell_Array_Out{n1} = cell(1,N2);
         end %For n1           
         
         if ~iscell(Cell_Array_In{1}{1})             
             
             %Iteratively assigning the rotated cells
             for n1 = 1 : N1
                 for n2 = 1 : N2
                     Cell_Array_Out{n1}{n2} = circshift(Cell_Array_In{n1}{n2},[Shift,0]);
                 end %FOR n2
             end %FOR n1
             
        else
            error('Error! This will only go two levels!');
        end % IF ~iscell(Cell_Array_In{1}{1})
        
    end % IF ~iscell(Cell_Array_In{1})
    
end % IF ~iscell(Cell_Array_In)




