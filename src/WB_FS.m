%This MATLAB Function 'WB_FS' Performs the Frequency Shift specified by 
%'FSft_Vec' of sampled sequenced 'x_Vec' sampled at 'Fs_Vec' and associated
%with the Sample Indecies 'SI_Vec' using the Sine-Cosine values storefd in a LUT. 
%
%By - Thushara Kanchana Gunaratne - RO/RCO - NSI-NRC
%Start Date - 2018-08-02
%Modified - 2019-01-11 - To Support the Second | Sub-Second Time Stamp
%Indexing Scheme

%# codegen
function y_Vec = WB_FS(x_Vec,SI_Vec,FSft_Vec,Fs_Vec,WBFS_Specs)

%Validation of the Inputs
N_Fs_Seq = length(Fs_Vec); %Number of Sequences to operate

if N_Fs_Seq ~= length(FSft_Vec)
   error('Error! the Number of Frequency Shift are Differnt from Number of Sample Sequences!')
end

%Checking whether x_Vec is a row or colum vector and converting into a
%column vector if it is not
[Nrow,Ncol] = size(x_Vec);
if Nrow == N_Fs_Seq
    x_Vec = x_Vec.';
    Ns_S = Ncol;
else
    Ns_S = Nrow;
end

%Checking the Dimenssionality of the SI_Vec
if iscell(SI_Vec{1})
    N_SI_Seq = length(SI_Vec);
    N_SI_L = length(SI_Vec{1}{1});
else
    N_SI_Seq = 1;
    N_SI_L = length(SI_Vec{1});
end %IF
%Checking whether the number of 
if N_Fs_Seq ~= N_SI_Seq
   error('Error! the Number of Frequency Shift are Differnt from Number of Sample Indecies!')
end

if N_SI_L ~= Ns_S
   error('Error! the Number of Samples are Differnt from Number of Indecies!')
end

%%Extracting the parameters
N_SC_LUT = WBFS_Specs.N_SC_LUT;
SC_LUT = (WBFS_Specs.SC_LUT);

%Initializing 
y_Vec = zeros(N_SI_L,N_Fs_Seq);

for n_seq = 1 : N_Fs_Seq
    
    if FSft_Vec(n_seq) == 0
        y_Vec(:,n_seq) = x_Vec(:,n_seq);
    else        
        %The Phase variation - With Modulus
        Phs_Vr = mod(FSft_Vec(n_seq)*double(SI_Vec{n_seq}{1}) + FSft_Vec(n_seq)/Fs_Vec(n_seq)*double(SI_Vec{n_seq}{2}),1);
        %The Phase variation - With Out Modulus
        %Phs_Vr = FSft_Vec(n_seq)*double(SI_Vec{n_seq}{1}) + FSft_Vec(n_seq)/Fs_Vec(n_seq)*double(SI_Vec{n_seq}{2});
        %Adding Phase Diyther between (-0.5,0.5) of Phase Step
        Phs_Dither = (rand(N_SI_L,1)-0.5); 
        %Finding the Indecies of the SC_LUT to multiply
        Idx_SC_LUT = mod(round(Phs_Vr*N_SC_LUT+Phs_Dither),N_SC_LUT);
        y_Vec(:,n_seq) = x_Vec(:,n_seq).*SC_LUT(Idx_SC_LUT+1);
    end %IF

end %FOR
    

