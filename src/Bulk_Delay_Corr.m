%This Custom MATLAB script adjuest the indices of the of the wideband
%sequences correcting for Bulk Delay
% [SIBD_Vec,xBD_Vec,Flg_BD_Vec] = Bulk_Delay_Corr(SI_Vec,x_Vec,Flg_Vec,BD_Vec)
%Inputs
%SI_Vec = The Sample Indicies Vector of the Inputs
%x_Vec = The Sampl Vectors
%Flg_Vec = The Flag Vector for the inputs
%BD_Vec = The Bulk Delay Correction Vector for each Input Stream
%Fs_Vec - The Sampling rate vector
%Output
%SIBD_Vec = The Sample Indicies Vector after correcting for Bulk Delay
%xBD_Vec = The Sample Vetors after correcting for Bulk Delay
%Flg_BD_Vec = The Flag Vector for the inputs after correcting for Bulk Delay

%This MATLAB function applies the 'Bulk-Delay-Corrections' for each input
%sequence in 'x_Vec' that is associated with the corresponding sample indicies
%in 'SI_Vec' as specified by 'BD_Vec'. If there is no change in the Bulk
%delay during the interval, the bulk delay correction is applied as a shift
%to the sample index. However, if tehre is a change then the data and the
%flags are changed accordingly.

%By Thushara Kanchana Gunaratne - RCO HAA, NRC CANADA
%Start date > 2019-01-09

function [SIBD_Vec,xBD_Vec,Flg_BD_Vec] = Bulk_Delay_Corr(SI_Vec,x_Vec,Flg_Vec,BD_Vec,Fs_Vec,DDR4BS)

%Checking whether SI_Vec is a cell-array of cell-arrays or cell-array of
%integers. This will be used to determine the number of sequences and the
%length of the sequences
if iscell(SI_Vec{1})
    SI_Seq = length(SI_Vec);
    SI_L = length(SI_Vec{1}{1});
else
    SI_Seq = 1;
    SI_L = length(SI_Vec{1});
end %IF

%For the Sample Sequences
[x_L,x_Seq] = size(x_Vec);

%For the Bulk Delay Vector
[~,~,BD_Seq] = size(BD_Vec);

%For the Sample Frequency Vector
Fs_Seq = length(Fs_Vec);

if (SI_Seq ~= x_Seq ) || (BD_Seq ~= x_Seq ) || (Fs_Seq ~= x_Seq )
    error('Error! The Number of Sequences in Sample Vectors, Sample Incidies, Bulk-Delay Corrections and Frequencies Do Not Match!')
end

%Checking whether Flg_Vec is a cell-array of cell-arrays or cell-array of
%integers. This will be used to determine the number of Flags and the
%length of the Flag sequences
if SI_Seq == 1
    Flg_Seq = 1;
    N_Flg = length(Flg_Vec);
    Flg_L = length(Flg_Vec{1});
else
    if iscell(Flg_Vec{1})
        Flg_Seq = length(Flg_Vec);
        N_Flg = length(Flg_Vec{1});
        Flg_L = length(Flg_Vec{1}{1});
    else
        error('Flag Vectos should be Cell arrays or Cell arrays of Cell arrays...');
    end %IF
end %IF SI_Seq == 1

if (Flg_Seq ~= x_Seq )
    error('Error! The Number of Sequences in Sample Vectors and Flags Do Not Match!')
end

%Checking whether the dimensionalities match
if (SI_L ~= x_L ) || (Flg_L ~= x_L )
    error('Error! The Length of Sample Vectors, Sample Incidies and Flags Do Not Match!')
end


%Initializing the Outputs
if x_Seq == 1
    
    %Extracting Bulk Delay Segment
    BD = BD_Vec(:,:,1);
    %Number of Bulk Delay Segments
    [NBD_Seg,~] = size(BD);
    %Sampling Frequency
    Fs = Fs_Vec(1);
    %The Sample Index
    SI = SI_Vec ; clear SI_Vec;
    %Sample Sequence
    x = x_Vec; clear x_Vec;
    %Flag Sequence
    Flg = Flg_Vec; clear Flg_Vec;
    
    if (NBD_Seg == 1)
        %The Bulk Delay in Samples
        BD_n_S = DDR4BS*BD(1,3);
        
        %Modifying the Sample Index to Represent the Block Delay correction in DDR4
        %NOTE - '+' to Shift Right Partially Compansating the an Advance
        Sec_Idx_Start = SI{1}(1); %Initial Seconds Index
        SS_Temp = SI{2} + int64(SI{1} - Sec_Idx_Start)*Fs + int64(BD_n_S);
        Sec_Idx = int16(floor(double(SS_Temp)./Fs)) + Sec_Idx_Start;
        SS_Idx = mod(SS_Temp,Fs);
        SIBD_Vec = {Sec_Idx,SS_Idx}; clear Sec_Idx_Start SS_Temp Sec_Idx SS_Idx;
        xBD_Vec = x; %No Change
        Flg_BD_Vec = Flg; %No Change
        
    else %NOTE due to the practical limitations max(NBD_Seg)is 2 in these simulations
        if (BD(2,3) == BD(1,3)) %Checking whether the both segments has the same Bulk Delay Correction
            %The Bulk Delay in Samples
            BD_n_S = DDR4BS*BD(1,3);
            
            %Modifying the Sample Index to Represent the Block Delay correction in DDR4
            %NOTE - '+' to Shift Right Partially Compansating the an Advance
            Sec_Idx_Start = SI{1}(1); %Initial Seconds Index
            SS_Temp = SI{2} + int64(SI{1} - Sec_Idx_Start)*Fs + int64(BD_n_S);
            Sec_Idx = int16(floor(double(SS_Temp)./Fs)) + Sec_Idx_Start;
            SS_Idx = mod(SS_Temp,Fs);
            SIBD_Vec = {Sec_Idx,SS_Idx}; clear Sec_Idx_Start SS_Temp Sec_Idx SS_Idx;
            xBD_Vec = x; %No Change
            Flg_BD_Vec = Flg; %No Change
            
        else
            %Extracting the corresponding data
            Sec_Idx_Start = SI{1}(1); %Initial Seconds Index
            SS_Temp = SI{2} + int64(SI{1} - Sec_Idx_Start)*Fs;
            
            %%%%%%%%%%%%%%%%%%%%%%%
            %For the First Segment
            %The Bulk Delay in Samples
            BD_S1 = int64(DDR4BS*BD(1,3));
            
            %Finding the First Index of the Delayed Sequence
            SIBD_1_First = SS_Temp(1) + BD_S1;
            
            %Finding out at what Sample Index the Bulk Delay change occurts
            BD_Chg_Idx = round((BD(1,2)-int64(Sec_Idx_Start))*Fs) - SIBD_1_First;
            
            %Modifying the Sample Index to Represent the Block Delay correction in DDR4
            %NOTE - '+' to Shift Right Partially Compansating the an Advance
            SSBD_Temp_1 = SS_Temp(1:BD_Chg_Idx) + BD_S1;
            
            %Assigning the Data for the 1st Segment
            x_1 = x(1:BD_Chg_Idx,1);
            %Assigning the Valid Flags for the 1st Segment
            Flg_1 = cell(1,N_Flg);
            for n_flg = 1 : N_Flg
                Flg_1{n_flg} = Flg{n_flg}(1:BD_Chg_Idx,1);
            end %FOR n_flg            
            
            %%%%%%%%%%%%%%%%%%%%%%%
            %For the Second Segment
            %The Bulk Delay in Samples
            BD_S2 = DDR4BS*BD(2,3);
            
            %Finding the Corresponding Sample Index for Original Sequence
            SI_2_First = SSBD_Temp_1(end) + int64(1 - BD_S2);
            
            %The Delay Corrected Sample Indicies for Second Segment
            SSBD_Temp_2 = transpose(SSBD_Temp_1(end) + int64((1 : 1 : x_L-BD_Chg_Idx)));
            
            if SI_2_First > SS_Temp(end)
                x_2 = zeros(x_L-BD_Chg_Idx,1);
                Flg_2 = cell(1,N_Flg);
                for n_flg = 1 : N_Flg
                    Flg_2{n_flg} = false(x_L-BD_Chg_Idx,1);
                end %FOR n_flg
            elseif (SI_2_First + x_L - BD_Chg_Idx -1) > SS_Temp(end)
                Idx_SI_2_First = find(SS_Temp==SI_2_First);
                N_Mis = double(SI_2_First + x_L - BD_Chg_Idx -1 - SS_Temp(end));
                x_2  = [x(Idx_SI_2_First:end,1); zeros(N_Mis,1)];
                %Note the Initial Invalid Flag indicate the dicontinuity in date that needs to pass through the CoCh
                Flg_2 = cell(1,N_Flg);
                for n_flg = 1 : N_Flg
                    Flg_2{n_flg} = [false; Flg{n_flg}(Idx_SI_2_First+1:end,1); false(N_Mis,1)];
                end %FOR n_flg                
            else
                Idx_SI_2_First = find(SS_Temp==SI_2_First);
                N_Rest = x_L - BD_Chg_Idx - 1;
                x_2  = x(Idx_SI_2_First:Idx_SI_2_First+N_Rest,1);
                %Note the Initial Invalid Flag indicate the dicontinuity in date that needs to pass through the CoCh
                Flg_2 = cell(1,N_Flg);
                for n_flg = 1 : N_Flg
                    Flg_2{n_flg} = [false; Flg{n_flg}(Idx_SI_2_First+1:Idx_SI_2_First+N_Rest,1)];
                end %FOR n_flg                
            end %IF SI1_2 > SI1(end)
            
            %Combining
            SSBD_Temp = [SSBD_Temp_1; SSBD_Temp_2]; clear SS_Temp_1 SS_Temp_2;
            Sec_BD_Idx = int16(floor(double(SSBD_Temp)./Fs)) + Sec_Idx_Start;
            SS_BD_Idx = mod(SSBD_Temp,Fs);
            %Packaging into a cell array
            SIBD_Vec = {Sec_BD_Idx,SS_BD_Idx}; %clear SSBD_Temp Sec_BD_Idx SS_BD_Idx;
            xBD_Vec   = [x_1; x_2]; %clear x_1 x_2;
            Flg_Temp = cell(1,N_Flg);
            for n_flg = 1 : N_Flg
                Flg_Temp{n_flg} = [Flg_1{n_flg}; Flg_2{n_flg}]; 
            end 
            Flg_BD_Vec = Flg_Temp;
            %clear Flg_1 Flg_2;
        end %IF BD(2,3) ~= BD(2,3)
        
    end % IF (NBD_Seg == 1)
    
else %If more than one sequence is processed
    %Initializing
    SIBD_Vec = cell(1,SI_Seq);
    xBD_Vec = zeros(x_L,x_Seq);
    Flg_BD_Vec = cell(1,Flg_Seq);
    
    for n_seq = 1 : x_Seq
        
        %Extracting Bulk Delay Segment
        BD = BD_Vec(:,:,n_seq);
        %Number of Bulk Delay Segments
        [NBD_Seg,~] = size(BD);
        %Sampling Frequency
        Fs = Fs_Vec(n_seq);
        %Sample Index
        SI = SI_Vec{n_seq};
        %Sample Sequence
        x = x_Vec(:,n_seq);
        %Flag Sequence
        Flg = Flg_Vec{n_seq};
        
        if (NBD_Seg == 1)
            %The Bulk Delay in Samples
            BD_n_S = DDR4BS*BD(1,3);
            
            %Modifying the Sample Index to Represent the Block Delay correction in DDR4
            %NOTE - '+' to Shift Right Partially Compansating the an Advance
            Sec_Idx_Start = SI{1}(1); %Initial Seconds Index
            SS_Temp = SI{2} + int64(SI{1} - Sec_Idx_Start)*Fs + int64(BD_n_S);
            Sec_Idx = int16(floor(double(SS_Temp)./Fs)) + Sec_Idx_Start;
            SS_Idx = mod(SS_Temp,Fs);
            SIBD_Vec{n_seq} = {Sec_Idx,SS_Idx}; clear Sec_Idx_Start SS_Temp Sec_Idx SS_Idx;
            xBD_Vec(:,n_seq) = x; %No Change
            Flg_BD_Vec{n_seq} = Flg; %No Change
            
        else %NOTE due to the practical limitations max(NBD_Seg)is 2 in these simulations
            if (BD(2,3) == BD(1,3)) %Checking whether the both segments has the same Bulk Delay Correction
                %The Bulk Delay in Samples
                BD_n_S = DDR4BS*BD(1,3);
                
                %Modifying the Sample Index to Represent the Block Delay correction in DDR4
                %NOTE - '+' to Shift Right Partially Compansating the an Advance
                Sec_Idx_Start = SI{1}(1); %Initial Seconds Index
                SS_Temp = SI{2} + int64(SI{1} - Sec_Idx_Start)*Fs + int64(BD_n_S);
                Sec_Idx = int16(floor(double(SS_Temp)./Fs)) + Sec_Idx_Start;
                SS_Idx = mod(SS_Temp,Fs);
                SIBD_Vec{n_seq} = {Sec_Idx,SS_Idx}; clear Sec_Idx_Start SS_Temp Sec_Idx SS_Idx;
                xBD_Vec(:,n_seq) = x; %No Change
                Flg_BD_Vec{n_seq} = Flg; %No Change
                
            else
                %Extracting the corresponding data
                Sec_Idx_Start = SI{1}(1); %Initial Seconds Index
                SS_Temp = SI{2} + int64(SI{1} - Sec_Idx_Start)*Fs;
                
                %%%%%%%%%%%%%%%%%%%%%%%
                %For the First Segment
                %The Bulk Delay in Samples
                BD_S1 = int64(DDR4BS*BD(1,3));
                
                %Finding the First Index of the Delayed Sequence
                SIBD_1_First = SS_Temp(1) + BD_S1;
                
                %Finding out at what Sample Index the Bulk Delay change occurts
                BD_Chg_Idx = round((BD(1,2)-int64(Sec_Idx_Start))*Fs) - SIBD_1_First;
                
                %Modifying the Sample Index to Represent the Block Delay correction in DDR4
                %NOTE - '+' to Shift Right Partially Compansating the an Advance
                SSBD_Temp_1 = SS_Temp(1:BD_Chg_Idx) + BD_S1;
                
                %Assigning the Data for the 1st Segment
                x_1 = x(1:BD_Chg_Idx,1);
                %Assigning the Valid Flags for the 1st Segment
                Flg_1 = cell(1,N_Flg);
                for n_flg = 1 : N_Flg
                    Flg_1{n_flg} = Flg{n_flg}(1:BD_Chg_Idx,1);
                end %FOR n_flg
                
                %%%%%%%%%%%%%%%%%%%%%%%
                %For the Second Segment
                %The Bulk Delay in Samples
                BD_S2 = DDR4BS*BD(2,3);
                
                %Finding the Corresponding Sample Index for Original Sequence
                SI_2_First = SSBD_Temp_1(end) + int64(1 - BD_S2);
                
                %The Delay Corrected Sample Indicies for Second Segment
                SSBD_Temp_2 = transpose(SSBD_Temp_1(end) + int64((1 : 1 : x_L-BD_Chg_Idx)));
                
                if SI_2_First > SS_Temp(end)
                    x_2 = zeros(x_L-BD_Chg_Idx,1);
                    Flg_2 = cell(1,N_Flg);
                    for n_flg = 1 : N_Flg
                        Flg_2{n_flg} = false(x_L-BD_Chg_Idx,1);
                    end %FOR n_flg
                elseif (SI_2_First + x_L - BD_Chg_Idx -1) > SS_Temp(end)
                    Idx_SI_2_First = find(SS_Temp==SI_2_First);
                    N_Mis = double(SI_2_First + x_L - BD_Chg_Idx -1 - SS_Temp(end));
                    x_2  = [x(Idx_SI_2_First:end,1); zeros(N_Mis,1)];
                    %Note the Initial Invalid Flag indicate the dicontinuity in date that needs to pass through the CoCh
                    Flg_2 = cell(1,N_Flg);
                    for n_flg = 1 : N_Flg
                        Flg_2{n_flg} = [false; Flg{n_flg}(Idx_SI_2_First+1:end,1); false(N_Mis,1)];;
                    end %FOR n_flg
                else
                    Idx_SI_2_First = find(SS_Temp==SI_2_First);
                    N_Rest = x_L - BD_Chg_Idx - 1;
                    x_2  = x(Idx_SI_2_First:Idx_SI_2_First+N_Rest,1);
                    %Note the Initial Invalid Flag indicate the dicontinuity in date that needs to pass through the CoCh
                    Flg_2 = cell(1,N_Flg);
                    for n_flg = 1 : N_Flg
                        Flg_2{n_flg} = [false; Flg{n_flg}(Idx_SI_2_First+1:Idx_SI_2_First+N_Rest,1)];
                    end %FOR n_flg
                end %IF SI1_2 > SI1(end)
                
                %Combining
                SSBD_Temp = [SSBD_Temp_1; SSBD_Temp_2]; clear SS_Temp_1 SS_Temp_2;
                Sec_BD_Idx = int16(floor(double(SSBD_Temp)./Fs)) + Sec_Idx_Start;
                SS_BD_Idx = mod(SSBD_Temp,Fs);
                %Packaging into a cell array
                SIBD_Vec{n_seq} = {Sec_BD_Idx,SS_BD_Idx}; %clear SSBD_Temp Sec_BD_Idx SS_BD_Idx;
                xBD_Vec(:,n_seq) = [x_1; x_2]; %clear x_1 x_2;
                %Combining Flags
                Flg_Temp = cell(1,N_Flg);
                for n_flg = 1 : N_Flg
                    Flg_Temp{n_flg} = [Flg_1{n_flg}; Flg_2{n_flg}];
                end
                Flg_BD_Vec{n_seq} = Flg_Temp;
                %clear Flg_1 Flg_2 Flg_Temp;
            end %IF BD(2,3) ~= BD(2,3)
            
        end % IF (NBD_Seg == 1)
        
    end %FOR n_seq
    
end %IF N_seq == 1


