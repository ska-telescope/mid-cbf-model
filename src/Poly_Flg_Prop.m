%The custom MATLAB function 
%           Flg_Out = Poly_Flg_Prop(Flg_In,CL,N_Cons,No)
% evaluates the propagation of flags through a polyphase filter structure
% Inputs
% Flg_In =  Flag Input - [Boolean]
% CL = Commutator Length - [UInt16]
% N_Cons = Polyphase Filter Propagation Length [UInt16]
% No = Length of the Output Flags [UInt64]
%
% Outputs
%Flg_Out = Flag_Output - [Boolean]

function Flg_Out = Poly_Flg_Prop(Flg_In,CL,N_Cons,No)

%Initialising 
Flg_Out = false(No+1,1); % Output
Inval_Count = N_Cons; % Invalid Count
%Recasting Integers
CL_U64 = uint64(CL);

%% Performing the iteration
for n = 1 : No
    if all(Flg_In((n-1)*CL_U64+1:1:n*CL_U64))
        Inval_Count = Inval_Count - 1;
        if Inval_Count <= 0
            Flg_Out(n+1) = true;
            Inval_Count = uint16(0);
        else
            Flg_Out(n+1) = false;
        end %IF Inval_Count == 0
    else
        Inval_Count = N_Cons;
        Flg_Out(n+1) = false;
    end %IF all(Flg_In((n-1)*CL+1:1:n*CL))
end %FOR n

Flg_Out = Flg_Out(1:No,1);