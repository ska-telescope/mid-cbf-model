%The custom MATLAB Function 'CORE_GEN_DUC' peforms the core functionality
%of a General Digital Up-Convertor (DUC)
%
% By Thushara Gunaratne - RO/RCO - NSI-NRC CANADA
% Start Date - 2019-10-31

function y_DUC = CORE_Gen_DUC(x,xM,IP_Idx,PFlt_Idx,L,M,PNh,Nc,No,Ph)

%Initiating the Output Vector for the Selected-Channel
y_DUC = zeros(No,1);

for n = 1 : Nc
    
    %The Linear Shift of Input through the FIFO
    %Shift the Current Samples by L to the Right
    xM(1,L+1:end) = xM(1,1:end-L);
    %Assign the New Input Samples for the first L samples
    xM(1,1:L) = flipud(x((n-1)*L+1:n*L));%Note the Flip (Up-Down) place the Newest sample to the front
    
    %Multiplying the Indexed Input Mask and Filter Mask elements and
    %accumulating
    %Initiate the Output mask
    y = zeros(M,1);
    
    for k = 1 : M
        xk = IP_Idx(1,k);
        pk = PFlt_Idx(1,k);
        y(k) = L*sum(xM(L-xk+1:1:L-xk+PNh).*Ph(pk,:));        
    end % For k
    
    %Assigning the Output
    y_DUC((n-1)*M+1:n*M) = y;
    
end %FOR n

return %Function CORE_Gen_DUC