%The Custom MATLAB Function
% [yCSPDFB,Flg_Out] = CS_Poly_DFT_FB_Batch(x,Flg_In,CSFB_Config_Para)
%Inputs
%  x = Input Vector (Double-Flaoting)
%  Flg_In = Input Falg Vectos (A Cell array)
%  CSFB_Config_Para = Configuration parameter (A Structure) contains following
%     CSFB_Config_Para.Nc - Number of Channels
%     CSFB_Config_Para.h2D - 2D Filter Mask of size (Nc x Nt)
%Outputs
% yCSPDFB = Output vector of size (No,Nc) (Double-Flaoting)
% Flg_Out = Flag Vectors for Outputs (Cell array)
%
% The MATLAB function 'CS_Poly_DFT_FB_Batch'
% performs the function of the Critically Sampled Polyphase DFT Filter on
% the input vector 'x' and the Valid vector 'V_In' producing the 2D output 
%'yCSPDFB' and Valid Output . 
%
% By Thushara Gunaratne - RO/RCO - NSI-NRC CANADA
% Start Date - 2017-05-15
% Modified 2018-12-21 > To Add valid Input and Output
% Modified 2019-01-21 > To Add Multiple Flags and Use a Counter-Based
% approach to propagate the Flags across the filter-bank
 
function [yCSPDFB,Flg_Out_Temp] = CS_Poly_DFT_FB_Batch(x,Flg_In,CSFB_Config_Para)
 
%Finding the Length of the Input Sample Vector
[Nrow,Ncol] = size(x);
if Ncol ~= 1
    x = x.';
    Ni = Ncol; %Input Vector Length
else
    Ni = Nrow; %Input Vector Length
end 
clear Nrow Ncol;

%Evalaute the number of Flags sequences and the length of the Flag sequences
N_Flg = length(Flg_In);
Flg_L = length(Flg_In{1});

if Ni ~= Flg_L
    error('Error! Input Length and Flag Lengths DO NOT match...');
end %IF

%Loading Configuration Parameters
Nc = CSFB_Config_Para.Nc;
h2D = CSFB_Config_Para.h2D;
 
%The Number of Output Samples
No = ceil(Ni/Nc);
 
%The 2D Data-Mask
[~,Nt] = size(h2D);
xM2D = zeros(Nc,Nt);


%Initiating
yCSPDFB = zeros(No,Nc); %Output Vector
Flg_Out_Temp = cell(1,N_Flg); %Falg Out Cell Array
for n_flgs = 1 : N_Flg
    Flg_Out_Temp{n_flgs} = false(No+1,1);    
end %FOR n_flg
Inval_Count = Nt*ones(N_Flg,1); %Invalid Counter
 
%Iterative Filtering
for n = 1 : No
    
    %% For Data
    %Polyphase Filtering
    yPFB = sum(h2D.*xM2D,2);
    %Updating the 2D Input Sample-Mask
    xM2D(:,2:end) = xM2D(:,1:end-1);
    %Inserting the New Samples | NOTE the newest sample is on the TOP
    xM2D(:,1) = x(n*Nc:-1:(n-1)*Nc+1);
    
    %The IDFT
    yCSPDFB(n,:) = (Nc)*ifft(yPFB).';
    
    %% For Flags
    for n_flgs = 1 : N_Flg
        if all(Flg_In{n_flgs}(n*Nc:-1:(n-1)*Nc+1))
            Inval_Count(n_flgs) = Inval_Count(n_flgs) - 1;
            if Inval_Count(n_flgs) <= 0
                Flg_Out_Temp{n_flgs}(n+1) = true;
                Inval_Count(n_flgs) = 0;
            else
                Flg_Out_Temp{n_flgs}(n+1) = false;
            end %IF Inval_Count == 0
        else
            Inval_Count(n_flgs) = Nt;
            Flg_Out_Temp{n_flgs}(n+1) = false;
        end %IF all(Flg_In((n-1)*M+1:1:n*M))
    end %FOR n_flg    
    
end %FOR n

%Removing the extra flags at the end
Flg_Out = cell(1,N_Flg); %Falg Out Cell Array
for n_flgs = 1 : N_Flg
    Flg_Out{n_flgs} = Flg_Out_Temp{n_flgs}(1:No);    
end %FOR n_flg