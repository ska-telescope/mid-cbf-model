%% quantization.m
%
% Quantization of a signal
% 
%% Syntax
%
%   a=quantize(x,n)
%
%% Inputs
% 
% x = input signal vector
% nlvl = Number of quantization levels (even or odd)
%
%% Output
%
% a = quantized signal
%
%% Description
%
% Quantize the input vector to integers, with truncation at +/- n
%
%% Changes
%
%  Author            Date           Comments
%  ---------------   -----------
%  ---------------------------------------
%  G.Comoretto        Feb-04-15   Original version
%  S.Chiarucci        Jun-29-17   Added complex input case
%  S.Chiarucci        Aug-8-17    Added quantization with an even nlvl
%  
%  ------------------------------------------------------------------------
%
%% Code 
% Quantize the input vector to integers, with truncation at +/- n
%
function a = quantization(x,nlvl)
    n = (nlvl-1)/2;   
     if isreal(x)
         if mod(nlvl,2)==1
             a = max(min(round(x),n),-n);
         else
             a = max(min(sign(x+1E-16) .* (floor(abs(x))+0.5) ,n),-n);
         end
     else
         if mod(nlvl,2)==1           
             a0 = round(x);
             a_real = max(min(real(a0),n),-n);
             a_im = max(min(imag(a0),n),-n);             
         else
             a_real = max(min(sign(real(x)+1E-16) .* (floor(abs(real(x)))+0.5) ,n),-n);
             a_im = max(min(sign(imag(x)+1E-16) .* (floor(abs(imag(x)))+0.5) ,n),-n);
         end
         a = a_real + a_im*1i;
     end
end
