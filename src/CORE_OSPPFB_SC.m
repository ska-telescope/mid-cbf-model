%The Custom MATLAB function
% y_SC = CORE_OSPPFB_SC(x,BRI,Nc,Num,CL,h2D,TWD2D)
%performs the Over-Sampled Channelization of the input vector 'x' and 
%yeilds the time series 'y_SC' for the selected-channel determined by
%TWD2D. This is the MATLAB equivalent of an efficient C function 
%
%Inputs
% x => The Input Vector [Complex Double]
% BRI => Barrel Rotator Index. Adjust this to avoid phase offsets. [UINT16]
% Nc => Number of Channels [UINT16]
% Num => Numerator of the Over-Sampling-Factor Os = Num/Den [UINT16]
% CL => Commutator Length [UINT16]
% h2D => [Nc,Nt] size 2D Matrix containing the Segmented filter
%        coefficients where Nc is the number of channels and Nt is the 
%        maximum number of tap per polyphase arm. [Complex Double]
% TWD2D => The 2D Twiddle Factor Matrix [Complex Double]
% Nt => Length of the Filter [UINT64]
% No => Output Samples [UINT64]
%
%Output
%y_SC = Output Vector [Complex Double]
%
%
% By Thushara Gunaratne - RO/RCO - NSI-NRC CANADA
% Start Date - 2019-07-17

function y_SC = CORE_OSPPFB_SC(x,BRI,Nc,Num,CL,h2D,TWD2D_SC,Nt,No)
%Specifying Values
CL_U64 = uint64(CL);

%The 1D data-Mask
NcNt_U64 = uint64(Nc)*Nt; NcNt_U16 = uint16(NcNt_U64);
xM = zeros(NcNt_U64,1) + 1j*zeros(NcNt_U64,1);
%Initiation the Temporarly Output of the Filter-Bank
yPFB = zeros(Nc,1) + 1j*zeros(Nc,1);
%Initiating the Output Vector for the Selected-Channel
y_SC = zeros(No,1) + 1j*zeros(No,1);

%Iterative Filtering
for n = 1 : No
    
    %% Polyphase Filtering
    %ReShaping xM
    %xM2D = reshape(xM,[Nc,Nt]);
    for k = 1 : Nc
            yPFB(k,1) = (h2D(k,:)*xM(k:Nc:NcNt_U16));
    end %FOR k
    %Updating the Input Vector
    %Shift the Current Samples by M to the Right
    xM(CL_U64+1:end) = xM(1:end-CL_U64);
    %Assign the New Input Samples for the first M samples
    xM(1:CL) = x(n*CL_U64:-1:(n-1)*CL_U64+1);%Note the Flip (Left-Right) place the Newest sample to the front
    
    %% Twiddle-Factor Multiplication with 
    y_SC(n) = sum(yPFB.*TWD2D_SC(:,BRI+1));
    
    %Updating the Barrel-Roter Index
    BRI = mod(BRI+1,Num);    
end %FOR n