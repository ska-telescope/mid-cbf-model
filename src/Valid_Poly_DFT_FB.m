%The Custom MATLAB function 'Valid_OS_Poly_DFT_FB' evalautes the Vailidity
%of the Signal Sequence processed through the 'OS_Poly_DFT_FB_SC'. The
%input 'V_In' is checked through the filter mask of the prototype filter
%of length 'Nh' to see all inputs are valid and the output 'V_Out' is
%markede accordingly.
%
% By Thushara Gunaratne - RO/RCO - NSI-NRC CANADA
% Start Date - 2018-12-21

function V_Out = Valid_Poly_DFT_FB(V_In,Valid_Ckeck_Config)

%Loading Configuration Parameters
M = Valid_Ckeck_Config.M; %The downsampling factor
Nh = Valid_Ckeck_Config.Nh; %The length of the prototype filter

[Nrow,Ncol] = size(V_In);
if Ncol ~= 1
    V_In = V_In.';
    Ni = Ncol; %Input Vector Length
else
    Ni = Nrow; %Input Vector Length
end 
clear Nrow Ncol;

%The Number of Output Samples
No = ceil(Ni/M);
%I nitiating the Output Vector
V_Out = false(No,1);

%The Filter Mask and Input Maks
Flt_Mask = true(Nh,1);
Input_Mast = false(Nh,1);

%Iterative Checking
for n = 1 : No
    
    %Checking for all Va;lid Sample with in the Input mask
    V_Out(n) = all(Input_Mast & Flt_Mask);
    %Updating the Input Mask
    %Shift the Current Samples by M to the Right
    Input_Mast(M+1:end) = Input_Mast(1:end-M);
    %Assign the New Input Samples for the first M samples
    Input_Mast(1:M) = V_In(n*M:-1:(n-1)*M+1);%Note the Flip (Left-Right) place the Newest sample to the front
    
end %FOR n
    
