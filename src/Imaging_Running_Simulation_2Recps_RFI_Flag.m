%This MATLAB script performs a 'Running Simulation' of a 'Realizable' Imaging 
%Process of the Mid.CBF with the Frequency Slice Approach. Here, the test 
%sequences with corresponding geometric propagation delays are generated 
%using an efficient implementation of 'Sum of Sinusoidal' method  along with 
%that the corresponding 'First Order Delay Polynomials - FODP'. For processing, 
%first, both Signal are channelized using the 'Coarse Channelizer (CoCh)'. 
%Then the delayed signals are Resampled using Fractional-Delay Filter-Banks 
%and Phase-Corrected with Phase-Rotators. The delay and phase aligned signals
%are then processed by the Imaging Channelizer producing 16,384 channels.
%The channel outputs are then cross-correlated and accumulated in order to
%demonstrate the recovered visibilities.
%Special case - 2 Element Interferometry

%Custom MATLAB functions used
%(1) [SI_Vec,TVD_Vec,BD,FODSR,FOPSR] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d_Vec,Fs_Vec,DCSpecs) 
% : In order to Evaluate the Geometrical Delay and the First order Delay 
%Polynomials for the Duration of the Test Sequence
%Alternatively, use 
%[SI_Vec,TVD_Vec,BD,FODSR,FOPSR] = Arbitrary_Delay_Est(HDM,T_s,Ns,Fs_Vec,DSpecs)
%(2) [x, ~] = Rand_Seq_TVD_ChirpZ(Specs,Fs_Vec,ST_Vec,TVD_Vec) : In order to evaluate 
%the Reference and Delayed Signals
%(3) y = OS_Poly_DFT_FB_SC(x,BRI,OSFB_Config_para) : In order to model CC using an over-Sampled
%Polyphase DFT Filter-Bank
%%(4) [RSx1,V] = ReSampler_ERP_Corr(CCx,F_Rat,SI1_CC,SI0_CC,FODSR,FOPSR,ReSamp_Config_para); : In order to Calculate
%the Delay and Phase Corrections to correct the delay and phase with respect 
%to the Delay-Center at BoresightIn and apply those corrections
%(6) yCSPDFB = CS_Poly_DFT_FB_Batch(x,CSFB_Config_para) : In order to model the Imaging
%Channelizer using a Critically-Sampled Polyphase DFT Filter-Bank

%Note that for SKA1 Band 3, the signal is sampled at ~3.168 Gsps. Hence, in
%Mid.CBF, those signals are upsampled to ~3.96 Gsps at the input using an
%5/4 Up-Sampler.

% By Thushara Kanchana Gunaratne - RCO National Research Council Canada
% Original Release Date : 2018-08-10
% Modified 2019-01-02 > To add arbitrary delay model and add a validity flag
% for the data
% Modified 2019-01-17 > To add multiple flags (i.e. Valid, RFI, Noise
% Diode), to Specify the time stamp by Second and Sub-Second Time-Stapmping 
% Index format and to calculate Data Valid Count (DVC) and Time Centroid 
% Index (TCI)
% Modified 2019-06-07 > To include Threshold detection RFI flagging after
% the VCC
% Modified 2019-06-11 > To have time varying RFI and accumulating flagged 
% and unflagged  
% Modified 2019-06-19 > To have signal scaling done with spectral
% occupancy of diffrent stages of the signal chain
% Modified 2019-06-20 > To Process signals with No-RFI, RFI with Flagging
% and RFI with No-Flagging
% Modified 2019-08-14 > To Add periodic DME pulses as psecified by Gary
% Hovey

close all; clear; clc;
 
%profile on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation Specs
%Number of Samples per Input Sample-Block 
% Note - Set this accordingly depending on the Memory Resources available 
%on the Simulation Computer
Ns_O = 39636173; %13440*2^14*18

%SKA1 Band 
Band_ID = 5; % Select from [1,2,3,4,5a = 5, 5b = 6];
%The Selected 'Frequency-Slice'(FS) for the Subsequent Processing
%Band 1, 2 & 3 = 0-9 | Band 4, 5a & 5b = 0-14
FS_ID = 6;

%Additional Frequency Down-Shift at the DISH 
if (Band_ID == 5) || (Band_ID == 6)  
    %For Band 5a - from 0 to 1.45 GHz
    %For Band 5b - from 0 to 6.5 GHz
    Ad_B5_Fsft = 1E9;
end %IF

%Wideband Frequency Shift (i.e. to propaly set the Zoom Windows)
WB_Fsft = 0*10E6; %In Hz
%The Frequency Offset desired in shifting Fine Imaging Channels
F_off = 1*13440/2; %In Hz

%Simulation Start Time Offset
T_si = 0/13440; % In sec
%Simulation Duration
T_Sim =  1*0.010; %In sec

%% Signal Spcifications
%The Correlation Coefficient
p = 0*0.5;

%Relative Interference Power Compared to the System Noise
%Interference Specs
F_Int = 1112E6; % In Hz - Frequency
%Full Test
% A_Int_dB = [19.96,19.96;24.09,24.09;20.87,20.87;24.39,24.39;27.60,27.59;34.59,34.58;23.66,23.65;26.59,26.59]; % In dB - Amplitude
% Rel_Delay = [0,0.1162;0,-0.0624;0,0.0358;0,0.111;0,0.0746;0,0.0218;0,0.1162;0,-0.0278]*1E-6; %In s
A_Int_dB = [19.96,19.96;24.09,24.09;20.87,20.87;24.39,24.39;27.60,27.59;23.66,23.65;26.59,26.59]; % In dB - Amplitude
Rel_Delay = [0,0.1162;0,-0.0624;0,0.0358;0,0.111;0,0.0746;0,0.1162;0,-0.0278]*1E-6; %In s
%Extreme Test
% A_Int_dB = [34.59,34.58]; % In dB - Amplitude
% Rel_Delay = [0,0.0218;]*1E-6; %In s
%
[N_DMEs,~] = size(A_Int_dB);
P_Rep_Int = 12E-6; % In s - Pulse Repitition
P_Width = 0.00022; %Relative Pulse Width
PP_Int = 0.01; %In s

%Fine Channel Range
RFIC_IC_CH_Rng = (10100:10900);
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Antenna Positions | The Distance vector in Cartesian Coordinates with 
%respect to the Array Reference Point
%For Reference Antenna-#0
% Dist_0 = 0; %In meters - In SKA1 Max Baseline is 160 kms
% Theta_0 = 0; %In Degrees
% Phi_0 = 0; %In Degrees 
% d0_Vec = [0;0;0];
 
%For the Antenna-#1 [e.g. SKA062]
%In (r,\theta,\pi) Coordinates
% Dist_1 = 1*160E3; %In meters - In SKA1 Max Baseline is 160 kms
% Theta_1 = 10; %In Degrees
% Phi_1 = 150; %In Degrees

%In (x,y,x) Coordinates
d1_Vec = [ 5109230.92399;...
           2006739.12133;...
          -3239050.84666]; %In m
      
%Offset Index with SCFO sampling for Antenna-#1
FO_Idx1 = 1*1000;  

%%%%%%%%%%%%%%%%%%%
%For the Antenna-#2 [e.g. SKA 065]
%In (r,\theta,\pi) Coordinates
% Dist_2 = 1*60E3; %In meters - In SKA1 Max Baseline is 160 kms
% Theta_2 = 45; %In Degrees
% Phi_2 = -130; %In Degrees

%In (x,y,x) Coordinates
d2_Vec = [ 5109236.26074;...
           2006706.17006;...
          -3239062.76287]; %In m

%Offset Index with SCFO sampling for Antenna-#2
FO_Idx2 = 1*1001;

%Reoridenting the Antennas by making the Array Center inbetween those 
d0_Vec = 0.5*(d1_Vec + d2_Vec);
d1_Vec = d1_Vec - d0_Vec;
d2_Vec = d2_Vec - d0_Vec;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Position of the Delay Center on the Sky at the Start of the Observation             
%Specified by the Declination and the Right-Assertion Angles of the Delay 
%Center at the 1PPS  
DCen.DecA0 = 00; %IN Degrees
DCen.RAsc0 = 00; %IN Degrees

%Assigning the Declanation and the Right-Assetion Angles of the Source 
%Position at the 1PPS  
SPos.DecA0 = DCen.DecA0+00.000; %IN Degrees
SPos.RAsc0 = DCen.RAsc0+00.000; %IN Degrees

%% Sample Quantizing
%Data
%Input Sample Bit-Width
if (Band_ID == 1) || (Band_ID == 2) || (Band_ID == 3)
    W_In = 8;
    Peak_to_IP_RMS_Ratio =  44.5; % Approximately 2^7/2.872 - That allows 99% Correlator Efficiency
else
    W_In = 4;
    Peak_to_IP_RMS_Ratio =  2.682; % Approximately 2^3/2.983 - That allows 98.85% Correlator Efficiency
end %IF 
W_US = 8; %OutputUp-Sampler Bit-Width | Only for Band 3
W_CC = 16; %Output of the CC Bit-Width
W_RS = 16; %Output of the ReSampler Bit-Width
W_IC = 9; %Output of the IC Bit-Width

%Coefficient
Wc = 19; %Coefficient Word length

%Scaling Factors
Peak_to_US_RMS_Ratio = 44.5; % Only for Band 3 - Approximately 2^7/2.872 - That allows 99% Correlator Efficiency
Peak_to_CC_RMS_Ratio = 3500; % Approximately 2^15/9.124 - That allows 99.9% Correlator Efficiency
Peak_to_RS_RMS_Ratio = 3500; % Approximately 2^15/9.124 - That allows 99.9% Correlator Efficiency
Peak_to_IC_RMS_Ratio = 8; 

%% Threshold Detection Parameters at the Input and the Output of Coarse Channelizer
%Short-Temp Epoch for Power Calculation
STL = 128; %In Samples
%Forgetting factor for Long-Term Power Calculations
C = 1-2^-12;

%% Threshold Detection Parameters at the Output of Imaging Channelizer
%Short-Temp Epoch for Power Calculation
STL_IC = 6;
%Forgetting factor for Long-Term Power Calculations
C_IC = 1-2^-8;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% NOTE - NOT Reccomended to Change Parameters Beyond this Point           %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Creating a Folder for Saving the Configuration parameters and Simulation results
Data_Time_Now = clock;
FolderName = sprintf('SKA1_Mid_CBF_Normal_Imaging_Realizable_Sim_%d%02d%02d_%02d%02d',Data_Time_Now(1:5));
mkdir(FolderName);
save(fullfile(FolderName,'Configuration_Parameters.mat'));

%% Band Specification
Band_Name_List = {'1','2','3','4','5A','5B'};

switch Band_ID
    case 1
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 1 spans 0.35 - 1.05 GHz
        Fl = 0.35E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.05E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 0E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 1; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF        
    case 2
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 2 spans 0.95 - 1.76 GHz
        Fl = 0.95E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.76E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 0E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 1; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 3
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 3 spans 1.65 - 3.05 GHz
        Fl = 0.05E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.45E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 1.6E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 2; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; % 3.96E9; %  %The Base Sampling Frequency with SCFO Sampling - in Hz |
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 4/5; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 4
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 4 spans 2.8 - 5.18 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 2.05E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 8/3; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 5
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 5a spans 4.6 - 8.5 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 4.5E9+Ad_B5_Fsft; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling - NOTE even if Band 5a is 
        %sampled in Nyquist Zone 2, the proposed frequency shift in extracting 
        %the observation band make it as if it was sampled in Nyquist Zone 1
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling 
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 3/2; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 6
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 5b spans 8.3 - 15.3 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 9.1E9+Ad_B5_Fsft; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling - - NOTE even if Band 5b is 
        %sampled in Nyquist Zone 2, the proposed frequency shift in extracting 
        %the observation band make it as if it was sampled in Nyquist Zone 1
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 8/3; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
end %SWITCH

%% Additional Signal and Noise Specification
%Input Standard Deviation
x_sigma = 1;
%Common Component Standard Deviation
s_sigma = x_sigma*sqrt(p);
%Noise Standard Deviation
n_sigma = x_sigma*sqrt(1-abs(p));

%Scaling the Interferer
Int_Scale = sqrt(2)*(10.^(A_Int_dB/20))*x_sigma; 

%% Sampling Specifications
%NOTE Mid.CBF Employs Sample Clock Frequency Offset (SCFO) Scheme
%Offset Sample Frequency 
DeltaF = 1.8E3; %Offset Resolution - In Hz
 
%Actual Sampling Frequency with SCFO Sampling- In Hz
F1 = F_Base + (FSF*DeltaF*FO_Idx1); % For Antenna #1 : In Hz
F2 = F_Base + (FSF*DeltaF*FO_Idx2); % For Antenna #2 : In Hz

 %% The Parmaters for Partial Integer Delay Correction with DDR4 Block Shifts
%Coarse Delay Update Period 
BDUDP = 200; % In Sec
%DDR4 Block Size
DDR4BS = 54; % In Samples

%% Setting the Configuration Parameters for Threshold Detection
%Specifying the Parameters for Threshold Detection after the Coarse Channalizer
Threshold_para_IP.STAL = STL; % Short Term Accumulation Length
Threshold_para_IP.STPTL = 8000; % Short Term Power Threshold
Threshold_para_IP.LTAC = C; % Long Term Accumulation Coefficient

%Specifying the Parameters for Threshold Detection after the Coarse Channalizer
Threshold_para_CC.STAL = STL; % Short Term Accumulation Length
Threshold_para_CC.STPTL = 100000; % Short Term Power Threshold
Threshold_para_CC.LTAC = C; % Long Term Accumulation Coefficient

%Specifying the Parameters for Threshold Detection after the Imaging Channalizer
Threshold_para_IC.STAL = STL_IC; % Short Term Accumulation Length
Threshold_para_IC.STPTL_Vec = 5*ones(1,16384); % Short Term Power Threshold
Threshold_para_IC.LTAC = C_IC; % Long Term Accumulation Coefficient

%% Specifying the Channelizer Specs for the Band 3 UpSampler
%% Custom MATLAB function 'B3_DUC' is used to model UpSampler
if Band_ID == 3
    
    %The Selectable Filters
    B3_UPS = load('US_Prototype_FIR_OS_16_15_L4_M5.mat');
    h0 = B3_UPS.h;
    L0 = B3_UPS.L;
    M0 = B3_UPS.M;
    clear B3_UPS
    
    %The Up-Sampling Factor
    USF = M0/L0;
    %Note Fs_Rat = 1/USF
    
    Max_h0 = max(abs(h0));
    
    %The Length of the Prototype Filter
    Nh0 = length(h0);
    %Integer-Sample Delay through the UpSampler
    Insft_US_OP = ceil(-(Nh0-0)/2/L0); %In Samples
    
    %Configuration Parameters of the UpSampler for Band 3
    %UnQuantized
    DUC_Config_para.h = h0;
    DUC_Config_para.L = L0;
    DUC_Config_para.M = M0;
    
    %Quantized
    DUC_Config_para_Q.h = Max_h0*double(sfi(h0./Max_h0,Wc,Wc-1));
    DUC_Config_para_Q.L = L0;
    DUC_Config_para_Q.M = M0;
end

%% Specifying the Channelizer Specs for the Coarse-Channelizer (CoCh)
%% Custom MATLAB function 'OS_Poly_DFT_FB_SC' is used to model the CoCh
%The Prototype FIR Filter for the CoCh
%NOTE This is an Oversampled Polyphase DFT FB
 
%Band Specific CC - Selected above
%OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat');
%OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat');
 
h1 = OSProtoFltSpecs.h;
Num1 = OSProtoFltSpecs.Num;
Den1 = OSProtoFltSpecs.Den;
Nc1 = OSProtoFltSpecs.Nc;
 
%The Length of the Prototype Filter
Nh1 = length(h1);
%The Oversampling factor
Os1 = Num1/Den1;
%Input Sample Length
M1 = Nc1/Os1;
%Maximum Number of Taps per Polyphase Arm
Nt1 = ceil(Nh1/Nc1);
 
%Shift the Output to compensate the Delay-Through the Polyphase Filter-Bank
%Integer-Sample Delay
Insft_CC_OP = ceil(-(Nh1-1)/2/M1); %In Samples
%The Residual Fractional Delay
Frsft_CC_OP = Insft_CC_OP + (Nh1-1)/2/M1; %In Samples
 
%Initiating Temp Matrices
h1_2D = zeros(Nc1,Nt1);
%Filling the 2D Filter Mask
for k1 = 1 : Nc1
    L1 = length(h1(k1:Nc1:end));
    h1_2D(k1,1:L1) = h1(k1:Nc1:end);
end %FOR k
clear k1 L1;
Max_h1_2D = max(max(h1_2D));
 
%Generating Twiddle-Factors
%NOTE - Depends on the Selected Coarse Channel
TWD2D_SC = exp(2j*pi/Nc1*FS_ID*(0:1:Nc1-1).')*exp(2j*pi*FS_ID/Num1*(0:1:Num1-1));
 
%Sample rates out of the CoCh
FCC0 = F0/M1; FCC1 = F1/M1; FCC2 = F2/M1;

%CoCh Int Re-Scaling Factor 
CC_Int_RSF =  sqrt(Nc1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Configuration Parameters for the Over-Sampling Polyphase DFT Filter-Bank
%UnQuantized
OSFB_Config_para.Nc = Nc1;
OSFB_Config_para.Num = Num1;
OSFB_Config_para.Den = Den1;
OSFB_Config_para.h2D = CC_Int_RSF.*h1_2D;
OSFB_Config_para.TWD2D_SC = TWD2D_SC;
OSFB_Config_para.N_Cons = ceil(Nh1/M1); %Note this is the Maximum propagation
%time in the filter interms of Output Samples

%Quantized
OSFB_Config_para_Q.Nc = Nc1;
OSFB_Config_para_Q.Num = Num1;
OSFB_Config_para_Q.Den = Den1;
OSFB_Config_para_Q.h2D = CC_Int_RSF.*Max_h1_2D*double(sfi(h1_2D./Max_h1_2D,Wc,Wc-1));
OSFB_Config_para_Q.TWD2D_SC = double(sfi(TWD2D_SC,Wc,Wc-1));
OSFB_Config_para_Q.N_Cons = ceil(Nh1/M1); %Note this is the Maximum propagation
%time in the filter interms of Output Samples
 
%% Specifying the Parameters for the Imaging Channelizer (IC)
%% Custom MATLAB function 'CS_Poly_DFT_FB_Batch' is used to model the IC
%The Prototype FIR Filter for the Imaging Channelizer
%NOTE This is a Critically-Sampled Polyphase DFT FB
CSProtoFltSpecs = load('CS_Prototype_FIR_CH16384.mat');
h2 = CSProtoFltSpecs.h;
Nc2 = CSProtoFltSpecs.Nc;
Os2 = 1; %The Imaging Channelizer is Critically Sampled -> Os2 = 1
%The Length of the Prototype Filter
Nh2 = length(h2);

%Shift the Output to compensate the Delay-Through the Polyphase Filter-Bank
%Integer-Sample Delay
Insft_IC_OP = round(-Nh2/2/Nc2); %In Samples
%NOTE Idealy the Integer Delay is round(-(Nh2-1)/2/Nc2). However, the
%filter coefficients are arrnaged to get the required 
%The Residual Fractional Delay
Frsft_IC_OP = Insft_IC_OP + Nh2/2/Nc2; %In Samples | Ideally Should be 0
 
%Maximum Number of Taps per Polyphase Arm
Nt2 = ceil(Nh2/Nc2);
%The 2D Coefficient-Mask
h2_2D = zeros(Nc2,Nt2);
%Filling the 2D Filter Mask
for k2 = 1 : Nc2
    L2 = length(h2(k2:Nc2:end));
    h2_2D(k2,1:L2) = h2(k2:Nc2:end);
end %FOR k2
clear k2 L2;
Max_h2_2D = max(max(h2_2D));

%Sample rates out of the Imaging Channelizer
F_IC = FCC0/Nc2*Os2;

%IC Internal Re-Scaling Factor 
IC_Int_RSF =  sqrt(Nc2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Configuration Parameters for the Criticalled-Sampled Polyphase DFT 
%Filter-Bank
%UnQuantized
CSFB_Config_para.Nc = Nc2;
CSFB_Config_para.h2D = IC_Int_RSF.*h2_2D;

%Quantized
CSFB_Config_para_Q.Nc = Nc2;
CSFB_Config_para_Q.h2D = IC_Int_RSF.*Max_h2_2D*double(sfi(h2_2D/Max_h2_2D,Wc,Wc-1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters for Calculating the Frequency Shift to Align Subsequant Channels across Coarse 
%Calculating the Frequency-Shift 'FA_Sft' for aligning subsequency 
%channels across CCs
%Procedure
% Step-1 : Define C2BW = (FCC0/Nc2/Os2)
% Step-2 : Find nk such that | F_o + C2BW*nk - CC_CF0 | < 0.5*C2BW
% Step-3 : Evalaute F_ST = F_o + C2BW*nk - CC_CF0
CC_CF0 = F0/Nc1*FS_ID;
C2BW = (F0/M1/Nc2/Os2); %Channel Separation in Subsequant Channelizer
nk = floor((CC_CF0+0.5*C2BW)/C2BW);
FA_Sft = F_off + C2BW*nk - CC_CF0; % In Hz
%FA_Sft = FA_Sft - FA_Sft; %Special Case to Test without Shifting

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Geometric Delay Model Specifications
%The Order of the Higher Order Polynomials that is Converted Internally
N_Poly = 3;
%The Duration of Validity for the first Order Delay Polynomial 
Tv = 10E-3; % In sec
 
%Sample Delay Specification
%The Speed of EM Wave Propagation
c = 299792458; % In m/s
 
%Earth's Rotation Angular Velocity
%NOTE - ACTUAL Value is We = 7.2921150E-5 | Increased to Study Non Sidereal
%Tracking. However, Mid.CBF is required to track delay centers that are
%moving at the rate of 20 times the sidereal rate.
We = 7.2921150E-5; % In s^-1 - Sidereal
%We = 2E1*7.2921150E-5; % In s^-1 - 20 Times Sidereal
 
%Assigning Geometrical Delay Specs to be used for the function
%Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d1_Vec,F1,DSpecs)
DCSpecs.Tv = Tv; %Tv - The Valid Time for the First Order Delay Model
if Band_ID == 3 %F0 - The Nominal Sampling Frequency
    DCSpecs.F0 = F0/USF; % In Hz /USF
else
    DCSpecs.F0 = F0; % In Hz
end
DCSpecs.Nqst_Zone = Nqst_Zone; %Nqst_Zone - The Nyquist Zone of Sampling
DCSpecs.N_Poly = N_Poly; %N_Poly - The Order of the Higher Order Polynomial 
DCSpecs.c = c; %c - The Speed of Light
DCSpecs.We = We; % We - The Angular Velocity of the Earth
DCSpecs.N_DPE = 1E2; %Number of Sample Points Used in Estimating the First Order Model
DCSpecs.BDUDP = BDUDP; % BDUDP - Coarse Delay Update Period - In Sec
DCSpecs.DDR4BS = DDR4BS; % DDR4BS - DDR4 Block Size - In Samples
DCSpecs.N_BDE = 1E2; %Number of Sample Points Used in Estimating the Block Delay
%The Net Frequency Down-Shift of the Band before Bulk Delay correction
DCSpecs.F_DSft_BBD = F_DSft ; % In Hz
%The Net Frequency Down-Shift of the Band after Bulk Delay correction
DCSpecs.F_DSft_ABD = WB_Fsft; % In Hz
DCSpecs.Nc1 = Nc1; %The Number of Channels in the Coarse Channelizer
DCSpecs.Os1 = Os1; %The Over-Sampling Factor in the Coarse Channelizer
DCSpecs.FS_ID = FS_ID; %The Index of the Selected Coase Frequency Slice
DCSpecs.FA_Sft = FA_Sft; %An Additiona Frequency Shift to be Performed 
%The ratio between the Original Sample Rate and the Sample rate at the Input to the Mid.CBF
if Band_ID == 3
    DCSpecs.USF = USF; %The Upsample factor between input data rate and the data rate at Bulk Delay is corrected
else
    DCSpecs.USF = 1;
end
%The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
DCSpecs.Fs_Rat = Fs_Rat;

%The Position Vectors of the Antennas
%The Distance vector in Cartesian Coordinates
%For the Reference Antenna A0
%d0_Vec = [0;0;0]; %Note the d0_Vec is by default at the Array Center

%For the Antenna A1
% d1_Vec = Dist_1*[cosd(Theta_1).*cosd(Phi_1);...
%                  cosd(Theta_1).*sind(Phi_1);...
%                  sind(Theta_1)];
             
%For the Antenna A1
% d2_Vec = Dist_2*[cosd(Theta_2).*cosd(Phi_2);...
%                  cosd(Theta_2).*sind(Phi_2);...
%                  sind(Theta_2)];
 
%% Specifying the Signal Specs for the Custom MATLAB function  
%% 'Rand_Seq_TVD_ChirpZ' that generate the Sequences for A0 & A1
%Signal Specs
%Signal Specs
if Band_ID == 3
    SSpecs.F0 = F0/USF; % Nominal Sample Rate - In Hz 
    SSpecs.Fs_Rat = 1; % ie Fs_Rat*USF %The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
else
    SSpecs.F0 = F0; % Nominal Sample Rate - In Hz
    SSpecs.Fs_Rat = Fs_Rat; %The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
end %IF
SSpecs.Fl = Fl; % In Hz
SSpecs.Fu = Fu; % In Hz
SSpecs.F_Dsft = F_DSft; % In Hz; 
SSpecs.Nqst_Zone = Nqst_Zone; %Either 1 or 2
SSpecs.dError = 1E-5; % In Relative Units
 
%% Configuring the Delay | Phase Synthesizer and the ReSampler
%Estimating the Time Varying Delay and Phase Corrections and Correction for 
%the Doppler Shift using a First-Order Delay Polynomial. Then these
%estimates are used to determine the Required Integer Delay Correction, the
%Index of the Fractional-Delay Filter and the Index of the Sine-Cosine LUT
%for Phase Correction
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Selecting the Fractional-Delay Filter-Bank
 
%Coefficient Sets for the Interpolation Filter
%512 Delay-Step
%FractFltBankSpecs = load('Fract_Dly_FB_CVX_DS_512.mat');

%1024 Delay-Step
FractFltBankSpecs = load('Fract_Dly_FB_CVX_DS_1024.mat');

hFD_FB = (FractFltBankSpecs.hFD_FB).';
Max_hFD_FB = max(max(hFD_FB));
% Os1 = FractFltBankSpecs.Os; % Redundent
% DStep = FractFltBankSpecs.DSteps; % Redundent
Nh_FD = FractFltBankSpecs.N_FD;
clear FractFltBankSpecs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Phase Correction factors with 'N_SC_LUT' stored Coefficients in a LUT
N_SC_LUT = 2^18;
SC_LUT = exp(2j*pi*(0:1:N_SC_LUT-1).'/N_SC_LUT);
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Configuering the ReSampler_ERP_Corr
%UnQuantized
ReSamp_Config_para.F0 = FCC0;
ReSamp_Config_para.hFD_FB = hFD_FB;
ReSamp_Config_para.SC_LUT = SC_LUT;

%Quantized
ReSamp_Config_para_Q.F0 = FCC0;
ReSamp_Config_para_Q.hFD_FB = Max_hFD_FB*double(sfi(hFD_FB/Max_hFD_FB,Wc,Wc-1));
ReSamp_Config_para_Q.SC_LUT = double(sfi(SC_LUT,Wc,Wc-1));

%% Specifying Parameters for Wideband Frequency-Shifter
%UnQuantized
WBFS_Specs.N_SC_LUT = N_SC_LUT;
WBFS_Specs.SC_LUT = SC_LUT;
%UnQuantized
WBFS_Specs_Q.N_SC_LUT = N_SC_LUT;
WBFS_Specs_Q.SC_LUT = double(sfi(SC_LUT,Wc,Wc-1));

%% Configuring the CoCh Ripple Correction
%[GC_Vec] = Coch_MagResp_Correction(Fs_CC_Vec,GCCorrSpecs);
GCCorrSpecs.h1 = h1; %The coefficients of the Coch
GCCorrSpecs.M1 = M1; %The Down Sampling Factor of the Coch
GCCorrSpecs.Os1 = Os1; %The Over-Sampling Sampling Factor of the Coch
GCCorrSpecs.Nc2 = Nc2; %The Number of Channels in the Subsequent Channelizer or FFT
GCCorrSpecs.F0 = FCC0; %The Nominal Sample Rate
GCCorrSpecs.FS_ID = FS_ID; %The Index of the Selected Frequency Slice
GCCorrSpecs.FA_Sft = FA_Sft; %The Frequency Shift applied to Align the Subsequent Channels
GCCorrSpecs.N_Edge = 100; %Number of Channels Corrected byond the Processing Edge 

%Evalauting the Gain Corrections
[GC_Vec] = CoCh_MagResp_Correction([FCC1,FCC2],GCCorrSpecs); 

%% Configuring the CoCh Scaling Factor
CC_Scale_Fac_Config.FS_ID = FS_ID; %Frequency Slice Index
CC_Scale_Fac_Config.M = M1; %Down Sampling Factor
CC_Scale_Fac_Config.Nc = Nc1; %Number of Channels
CC_Scale_Fac_Config.Fl = Fl; %Lower Edge of the Spectrum - in Hz
CC_Scale_Fac_Config.Fu = Fu; %Uppder Edge of the Spectrum - in Hz
CC_Scale_Fac_Config.WB_Fsft = WB_Fsft; %Wideband Frequency Shift - in Hz
CC_Scale_Fac_Config.C_Fac = 1.035; %Correction Factor - NOTE the Factor  1.035 is an empirical observation

CC_ScaleF = [CC_Scale_Fac(F1,CC_Scale_Fac_Config) CC_Scale_Fac(F2,CC_Scale_Fac_Config)];

%% Evalauting the IC Scaling Factor
%The Lower Edge of the Frequency Slice
FS_Fl = CC_CF0 - 0.5*FCC0;
%The Lower Edge of the Frequency Slice
FS_Fu = CC_CF0 + 0.5*FCC0;

%The Band edges after windeband Frequency Shift
Fl_sft = Fl + WB_Fsft;
Fu_sft = Fu + WB_Fsft;

%Determining the Spectral Occupancy
if (Fl_sft <= FS_Fl) && (Fu_sft >= FS_Fu)
    Rel_Spect_Ocp_IC = FCC0;
elseif (Fl_sft > FS_Fl) && (Fu_sft >= FS_Fu)
    Rel_Spect_Ocp_IC = (FS_Fu - Fl_sft);
elseif (Fl_sft <= FS_Fl) && (Fu_sft < FS_Fu)
    Rel_Spect_Ocp_IC = (Fu_sft - FS_Fl);
else
    Rel_Spect_Ocp_IC = (Fu_sft - Fl_sft);
end
IC_Scale_Fac = 1.017*sqrt(FCC0/Rel_Spect_Ocp_IC); %Note the 1.017 is determined by observation
  
%% Some Other Parameters
%Number of Flags
N_Flg = 3;

%The Optimal Number of Samples in Wideband that fascilitate the 
Ns_N = round(Ns_O*F_IC/F0)*M1*Nc2;
N_OV = ceil((Nh1+M1*(Nh2+Nh_FD))*F_IC/F0); % In Samples - The Number of Samples at the Output of the Imaging Channelizer that needs to be Overlapped

if Band_ID == 3
    N_CC_OP = ceil(Ns_N*USF/M1); %The Number of Output Frames from CoCh
    T_AOV = -USF*Nh0/F0; % In sec - Additional Overlap for Band 3
else    
    N_CC_OP = ceil(Ns_N/M1); %The Number of Output Frames from CoCh
    T_AOV = 0; % In sec - Additional Overlap for Other bands
end% In sec
 
%The Number of Output Frames from Imaging Channelizer
N_IC_OP = ceil(N_CC_OP/Nc2);

%Number of iterations required to cover the Simulation Duration
NItt = ceil(T_Sim*F_IC./(N_IC_OP-N_OV));
%NItt = 1; %NOTE to run just one iteration

%Updating the Start time to Concide with a Imaging Channelizer Sample Time
T_si_IC = round(T_si*F_IC)/F_IC;

%Initialization the Auto and Cross-Correlation Accumulations Bins
%No-RFI
%UnQuantized
AC_A1 = zeros(1,Nc2);
AC_A2 = zeros(1,Nc2);
XC_A1A2 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
VXC_A1A2 = zeros(1,Nc2);
%Quantized
ACQ_A1 = zeros(1,Nc2);
ACQ_A2 = zeros(1,Nc2);
XCQ_A1A2 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
VXCQ_A1A2 = zeros(1,Nc2);

%With RFI & With-Out Flags & Quantized
WO_ACQ_A1 = zeros(1,Nc2);
WO_ACQ_A2 = zeros(1,Nc2);
WO_XCQ_A1A2 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
WO_VXCQ_A1A2 = zeros(1,Nc2);

%With RFI & With Flags & Quantized
WF_ACQ_A1 = zeros(1,Nc2);
WF_ACQ_A2 = zeros(1,Nc2);
WF_XCQ_A1A2 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
WF_VXCQ_A1A2 = zeros(1,Nc2);

%Data Valid Counts
DVC_ICQ_A1 = zeros(1,Nc2);
DVC_ICQ_A2 = zeros(1,Nc2);
DVC_ICQ_A1A2 = zeros(1,Nc2);

%Initiating the RFI Free Time Percentage
RFI_Free_Pct = zeros(1,2);

%% Starting the Iterations
fprintf('Running Simulations for Band %s - Frequency Slice %d\n\n',Band_Name_List{Band_ID},FS_ID);

for nItt = 1 : NItt
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('Running the Iteration %d of %d\n',nItt,NItt);
    fprintf('Started on %d-%02d-%02d || %2d:%2d:%2.2f\n',clock);
    
    %Re-Afirming the Number of Samples
    Ns = Ns_N; %As Band 3 is ReSampled
      
    %% Start Time at the Reference Antenna for nItt-th Iteration
    T_s = T_si_IC + ((nItt-1)*(N_IC_OP-N_OV) - (N_OV+Insft_IC_OP))/F_IC - T_AOV; % In sec
     
    %The Sample Index at the Reference
    if Band_ID == 3
        SI0_BU_Sec_Start = floor(T_s);
        SI0_BU_Temp = transpose(int64((round((T_s-SI0_BU_Sec_Start)*F0/USF) + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_BU_Sec_Idx = int16(floor(double(SI0_BU_Temp)*USF./F0) +  SI0_BU_Sec_Start);
        SI0_BU_SS_Idx = mod(SI0_BU_Temp,F0/USF);
        %Packaging into a cell array
        SI0_BU = {SI0_BU_Sec_Idx,SI0_BU_SS_Idx};
        clear SI0_BU_Sec_Start SI0_BU_Temp SI0_BU_Sec_Idx SI0_BU_SS_Idx;
    else
        SI0_Sec_Start = floor(T_s);
        SI0_Temp = transpose(int64((round((T_s-SI0_Sec_Start)*F0) + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_Sec_Idx = int16(floor(double(SI0_Temp)./F0) +  SI0_Sec_Start);
        SI0_SS_Idx = mod(SI0_Temp,F0);
        %Packaging into a cell array
        SI0 = {SI0_Sec_Idx,SI0_SS_Idx};
        clear SI0_Sec_Start SI0_Temp SI0_Sec_Idx SI0_SS_Idx;
    end % IF
    
    %Using the Custom Function 'Geomatrical_Delay_Est'
    fprintf('Generating Geometric Delays & Corrections...')
    tic;
    if Band_ID == 3
        [SI1_BU,TVD1,BD1,FODSR1,FOPSR1] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d1_Vec,F1/USF,DCSpecs);
        [SI2_BU,TVD2,BD2,FODSR2,FOPSR2] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d2_Vec,F2/USF,DCSpecs);        
    else
        [SI1,TVD1,BD1,FODSR1,FOPSR1] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d1_Vec,F1,DCSpecs);
        [SI2,TVD2,BD2,FODSR2,FOPSR2] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d2_Vec,F2,DCSpecs);        
    end
    toc;
    
    %% Specifying Sample Time Vectors
    if Band_ID == 3
        %Combine the
        ST_Vec = [double(SI1_BU{1})+double(SI1_BU{2})*USF/F1,...
                  double(SI2_BU{1})+double(SI2_BU{2})*USF/F2];
    else
        ST_Vec = [double(SI1{1})+double(SI1{2})/F1,...
                  double(SI2{1})+double(SI2{2})/F2];
    end %IF
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Using the function 'Rand_Seq_TVD_ChirpZ' and specifying Guassian Noise
    fprintf('Generating Random Test Sequences...')
    tic;
    if Band_ID == 3
        if s_sigma ~= 0
            [s_BU_Vec,V_BU_Vec, ~] = Rand_Seq_TVD_ChirpZ(SSpecs,[F1,F2]/USF,ST_Vec,[TVD1,TVD2]);
            %Re-Scaling the Signal to Get Unity variance
            RMS_s_Vec = rms(s_BU_Vec,1); %Evaluating the Root Mean Square of Colum Vectors (Along Dimension '1')
            s_BU_Vec  = s_sigma*s_BU_Vec./RMS_s_Vec;
        else
            s_BU_Vec = zeros(Ns,2); V_BU_Vec = true(Ns,2); %No Signal Just Noise
        end % IF s_sigma ~= 0
        
        %Specifyng Guassian Noise
        n_BU_Vec = Gaussian_Noise([Fl,Fu],[F1,F2]/USF,Ns,n_sigma);
        %n_BU_Vec = n_sigma*randn(Ns,2);
    else
        if s_sigma ~= 0
            [s_Vec,V_Vec, ~] = Rand_Seq_TVD_ChirpZ(SSpecs,[F1,F2],ST_Vec,[TVD1,TVD2]);
            %Re-Scaling the Signal to Get Unity variance
            RMS_s_Vec = rms(s_Vec,1); %Evaluating the Root Mean Square of Colum Vectors (Along Dimension '1')
            s_Vec  = s_sigma*s_Vec./RMS_s_Vec;
        else
            s_Vec = zeros(Ns,2); V_Vec = true(Ns,2);  %No Signal Just Noise
        end %IF s_sigma ~= 0
        
        %Specifyng Guassian Noise
        n_Vec = Gaussian_Noise([Fl,Fu],[F1,F2],Ns,n_sigma);
        %n_Vec = n_sigma*randn(Ns,2);
    end
    clear TVD1 TVD2;
    
    %% Adding Signal, Noise and Interference and Scaling before Quantization
    %Interference
    %Continuous Interference
    %int_Vec = Int_Scale.*cos(2*pi*F_Int*ST_Vec);
    
    if (Band_ID == 1) || (Band_ID == 2)
        %Intermittent Interference
        int_Vec = zeros(Ns,2);
        NO_RFI_Flg = true(Ns,2);
        %
        DME_fst = floor((double(SI0{1}(1))+double(SI0{2}(1))/F0)/PP_Int);
        DME_lst = ceil((double(SI0{1}(end))+double(SI0{2}(end))/F0)/PP_Int);
        dly = (DME_fst : 1 : DME_lst)*PP_Int;
        for n_DMEs = 1 : N_DMEs
            T_Start = 1*rand*Ns/F0;
            dly1 = T_Start + dly + Rel_Delay(n_DMEs,1);
            int_Temp = Int_Scale(n_DMEs,1)*pulstran(ST_Vec(:,1),dly1,'gauspuls',F_Int,P_Width);
            int_Temp = int_Temp + Int_Scale(n_DMEs,1)*pulstran(ST_Vec(:,1),dly1+P_Rep_Int,'gauspuls',F_Int,P_Width);
            NO_RFI_Flg(abs(int_Temp) > 1E-5,1) = false;
            int_Vec(:,1) = int_Vec(:,1) + int_Temp;
            
            dly2 = T_Start + dly + Rel_Delay(n_DMEs,2);
            int_Temp = Int_Scale(n_DMEs,2)*pulstran(ST_Vec(:,2),dly2,'gauspuls',F_Int,P_Width);
            int_Temp = int_Temp + Int_Scale(n_DMEs,2)*pulstran(ST_Vec(:,2),dly2+P_Rep_Int,'gauspuls',F_Int,P_Width);
            NO_RFI_Flg(abs(int_Temp) > 1E-5,2) = false;
            int_Vec(:,2) = int_Vec(:,2) + int_Temp;
        end
        clear dly dly1 dly2 int_Temp;
        %Evalauting the RFI Free Time Percentage
        RFI_Free_Pct = sum(NO_RFI_Flg(N_OV+1:end,:),1)./([1,1]*(Ns-N_OV));
    else
        int_Vec = zeros(Ns,2);  
        NO_RFI_Flg = true(Ns,2);
    end %IF
    clear ST_Vec 
    
        
    %Combining the common Signal and Noise and Scaling for Optimum
    %Quantizing
    if Band_ID == 3
        x_BU_Vec = (s_BU_Vec + n_BU_Vec); %With Out RFI
        R_x_BU_Vec = (s_BU_Vec + n_BU_Vec + int_Vec);   %With RFI      
    else
        x_Vec = (s_Vec + n_Vec); %With Out RFI
        R_x_Vec = (s_Vec + n_Vec + int_Vec); %With RFI
    end
    toc;
    
    %% Quantising
    fprintf('Quantizing the Test Sequences...')
    tic;
    %Determining the Scaling Factor
    if nItt == 1 %Use the Same Scaling for all iterations   
        IP_ScaleF = ones(1,2);

        %if p ~= 1
        %    IP_ScaleF = rms(n_Vec,1)/sqrt(1-p)*IP_RMS_to_Peak_Ratio;
        %else
        %    IP_ScaleF = rms(s_Vec,1)*IP_RMS_to_Peak_Ratio;
        %end    
    end
    
    if Band_ID == 3
        %Saturation/Clipping Flag - Initiation
        Sat_Flag_BU_Vec = true(Ns,2);
        %Scaling the Input before Quantizing
        x_BU_Vec = x_BU_Vec./IP_ScaleF;
        xQ_BU_Vec = Peak_to_IP_RMS_Ratio.*double(sfi(x_BU_Vec/Peak_to_IP_RMS_Ratio,W_In,W_In-1));
        R_x_BU_Vec = R_x_BU_Vec./IP_ScaleF;
        Temp_Scale_R_x_BU_Vec = R_x_BU_Vec/Peak_to_IP_RMS_Ratio;
        Sat_Flag_BU_Vec(abs(Temp_Scale_R_x_BU_Vec(:,1)) > 1,1) = false;
        Sat_Flag_BU_Vec(abs(Temp_Scale_R_x_BU_Vec(:,2)) > 1,2) = false;
        R_xQ_BU_Vec = Peak_to_IP_RMS_Ratio.*double(sfi(Temp_Scale_R_x_BU_Vec,W_In,W_In-1));
        clear sBU_Vec n_Vec int_Vec Temp_Scale_R_x_BU_Vec;
    else
        %Saturation/Clipping Flag - Initiation
        Sat_Flag_Vec = true(Ns,2);
        %Scaling the Input before Quantizing
        x_Vec = x_Vec./IP_ScaleF;
        xQ_Vec = Peak_to_IP_RMS_Ratio.*double(sfi(x_Vec/Peak_to_IP_RMS_Ratio,W_In,W_In-1));
        R_x_Vec = R_x_Vec./IP_ScaleF;
        Temp_Scale_R_x_Vec = R_x_Vec/Peak_to_IP_RMS_Ratio;
        Sat_Flag_Vec(abs(Temp_Scale_R_x_Vec(:,1)) > 1,1) = false;
        Sat_Flag_Vec(abs(Temp_Scale_R_x_Vec(:,2)) > 1,2) = false;
        R_xQ_Vec = Peak_to_IP_RMS_Ratio.*double(sfi(Temp_Scale_R_x_Vec,W_In,W_In-1));
        clear s_Vec n_Vec int_Vec Temp_Scale_R_x_Vec;
    end
    toc;
    
    %Plotting
    %     figure; subplot(211); hold on; plot(x_Vec(:,1),'b--'); plot(x_Vec(:,2),'r--'); plot(xQ_Vec(:,1),'c-.');  plot(xQ_Vec(:,2),'m-.'); hold off; box on; grid on; title('Signal Without RFI');
    %     subplot(212); hold on; plot(R_x_Vec(:,1),'b--'); plot(R_x_Vec(:,2),'r--'); plot(R_xQ_Vec(:,1),'g-.'); plot(R_xQ_Vec(:,2),'y-.'); hold off; box on; grid on; title('Signal With RFI')
    
    %figure; hold on; plot(R_x_Vec(:,1),'b--'); plot(R_x_Vec(:,2),'r--'); plot(R_xQ_Vec(:,1),'g-.'); plot(R_xQ_Vec(:,2),'y-.'); hold off; box on; grid on; xlim([1, Ns]); title('Input Signal With RFI'); xlabel('Sample Index'); ylabel('Amplitude');
    
    clear R_x_Vec;
    %% Combining the Flags for Data with Flags of RFI
    if Band_ID == 3
        %Note there are two Streams
        Flg_BU_Vec = cell(1,2);
        Flg_BU_Vec{1} = cell(1,N_Flg); Flg_BU_Vec{2} = cell(1,N_Flg);
        for Ant_idx = 1 : 2
            Flg_BU_Vec{Ant_idx}{1} = V_BU_Vec(:,Ant_idx);
            Flg_BU_Vec{Ant_idx}{2} = Flg_BU_Vec{Ant_idx}{1} & Sat_Flag_BU_Vec(:,Ant_idx);
            Flg_BU_Vec{Ant_idx}{3} = Flg_BU_Vec{Ant_idx}{1} & NO_RFI_Flg(:,Ant_idx);
        end %FOR flg_idx
        clear V_BU_Vec Sat_Flag_BU_Vec;
    else
        %Note there are two Streams
        Flg_Vec = cell(1,2);
        Flg_Vec{1} = cell(1,N_Flg); Flg_Vec{2} = cell(1,N_Flg);
        for Ant_idx = 1 : 2
            Flg_Vec{Ant_idx}{1} = V_Vec(:,Ant_idx);
            Flg_Vec{Ant_idx}{2} = Flg_Vec{Ant_idx}{1} & Sat_Flag_Vec(:,Ant_idx);
            Flg_Vec{Ant_idx}{3} = Flg_Vec{Ant_idx}{1} & NO_RFI_Flg(:,Ant_idx);
        end %FOR flg_idx
        clear V_Vec Sat_Flag_Vec;
    end %IF
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Threshold Detection at the Input
    fprintf('Threshold detection and Flagging Input Seqiuences ...');
    tic;
    if Band_ID == 3
        %Initializing Output
        Flg_TD_BU_Vec  = cell(1,2); %Valid
        Flg_TD_BU_Vec{1} = cell(1,N_Flg); Flg_TD_BU_Vec{2} = cell(1,N_Flg); %N_Flgs's for Each Stream
        
        %Assigning Valid Flag
        Flg_TD_BU_Vec{1}{1} = Flg_BU_Vec{1}{1};
        Flg_TD_BU_Vec{2}{1} = Flg_BU_Vec{2}{1};
        %Using the Custom Function Threshold_Det to Evalaute the Thresholding Flags
        [Flg_TD_BU_Vec{1}{2},IP_LTPS_Est1,IP_STPS_Est1] = Threshold_Det(R_xQ_BU_Vec(:,1),Flg_BU_Vec{1}{2},Threshold_para_IP);
        [Flg_TD_BU_Vec{2}{2},IP_LTPS_Est2,IP_STPS_Est2] = Threshold_Det(R_xQ_BU_Vec(:,2),Flg_BU_Vec{2}{2},Threshold_para_IP);
        %Assigning Ideal RFI Flags
        Flg_TD_BU_Vec{1}{3} = Flg_BU_Vec{1}{3};
        Flg_TD_BU_Vec{2}{3} = Flg_BU_Vec{2}{3};
        %clear FLg_BU_Vec;
    else
        %Initializing Output
        Flg_TD_Vec  = cell(1,2); %Valid
        Flg_TD_Vec{1} = cell(1,N_Flg); Flg_TD_Vec{2} = cell(1,N_Flg); %N_Flgs's for Each Stream

        %Assigning Valid Flag
        Flg_TD_Vec{1}{1} = Flg_Vec{1}{1};
        Flg_TD_Vec{2}{1} = Flg_Vec{2}{1};
        %Using the Custom Function Threshold_Det
        [Flg_TD_Vec{1}{2},IP_LTPS_Est1,IP_STPS_Est1] = Threshold_Det(R_xQ_Vec(:,1),Flg_Vec{1}{2},Threshold_para_IP);
        [Flg_TD_Vec{2}{2},IP_LTPS_Est2,IP_STPS_Est2] = Threshold_Det(R_xQ_Vec(:,2),Flg_Vec{2}{2},Threshold_para_IP);
        %Assigning Ideal RFI Flags
        Flg_TD_Vec{1}{3} = Flg_Vec{1}{3};
        Flg_TD_Vec{2}{3} = Flg_Vec{2}{3};
        %clear Flg_Vec;
    end
    toc;
    
    %% Plotting the Threshold Outputs
%     %The Power Levels
%     P_Est_IP = median([IP_LTPS_Est1,IP_LTPS_Est2]);
%     
%     figure('name','Short and Long Term Power Estimates and Flags - Input');
%     
%     subplot(321);
%     hold on;
%     plot(IP_STPS_Est1,'b-');
%     plot([1,Ns],P_Est_IP(1)*[1,1],'k--')
%     plot([1,Ns],P_Est_IP(1)*(1+4*sqrt(2/STL))*[1,1],'k--')
%     plot([1,Ns],P_Est_IP(1)*(1-4*sqrt(2/STL))*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Short-Term Power Sum - Seq-1');
%     
%     subplot(322);
%     hold on;
%     plot(IP_STPS_Est2,'b-');
%     plot([1,Ns],P_Est_IP(2)*[1,1],'k--')
%     plot([1,Ns],P_Est_IP(2)*(1+4*sqrt(2/STL))*[1,1],'k--')
%     plot([1,Ns],P_Est_IP(2)*(1-4*sqrt(2/STL))*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Short-Term Power Sum - Seq-2');
%     
%     subplot(323);
%     hold on;
%     plot(IP_LTPS_Est1,'b-');
%     plot([1,Ns],P_Est_IP(1)*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Long-Term Power Sum - Seq-1');
%     
%     subplot(324);
%     hold on;
%     plot(IP_LTPS_Est2,'b-');
%     plot([1,Ns],P_Est_IP(2)*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Long-Term Power Sum - Seq-2');
%     
%     subplot(325);
%     hold on;
%     if Band_ID == 3
%         stairs(~Flg_TD_BU_Vec{1}{2},'k-.');
%         stairs(~Flg_BU_Vec{1}{2},'g:');
%     else
%         stairs(~Flg_TD_Vec{1}{2},'k-.');
%         stairs(~Flg_Vec{1}{2},'g:');
%     end
%     hold off;
%     box on; grid on;
%     ylim([-0.05, 1.05]);
%     xlabel('Sample Index'); ylabel('Invalid');
%     title('Signal Invalid - Seq-1');
%     
%     subplot(326);
%     hold on;
%     if Band_ID == 3
%         stairs(~Flg_TD_BU_Vec{2}{2},'k-.');
%         stairs(~Flg_BU_Vec{2}{2},'g:');
%     else
%         stairs(~Flg_TD_Vec{2}{2},'k-.');
%         stairs(~Flg_Vec{2}{2},'g:');
%     end
%     hold off;
%     box on; grid on;
%     ylim([-0.05, 1.05]);
%     xlabel('Sample Index'); ylabel('Invalid');
%     title('Signal Invalid - Seq-2');
%     pause(0.01);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Upsampling in the Band 3
    if Band_ID == 3
        fprintf('Up-Sampling the input Sequence in Band 3 ...');
        tic;
        %The Number of Samples after up-Sampling
        Ns = ceil(Ns*USF);
        
        %NO-RFI & UnQuantized
        %Initializing the Outputs
        x_Vec = zeros(Ns,2);
        Flg_TD_Vec = cell(1,2);        
        %Up-Sampling with B3_DUC
        [x_Vec(:,1),Flg_TD_Vec{1}] = B3_DUC(x_BU_Vec(:,1),Flg_TD_BU_Vec{1},SI1_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,1))));
        [x_Vec(:,2),Flg_TD_Vec{2}] = B3_DUC(x_BU_Vec(:,2),Flg_TD_BU_Vec{2},SI2_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,2))));
        clear x_BU_Vec;
        
        %Correcting the Integer Sample Delay due to the Up-Sampler
        x_Vec(:,1) = circshift(x_Vec(:,1),[Insft_US_OP,0]);
        x_Vec(:,2) = circshift(x_Vec(:,2),[Insft_US_OP,0]);
        for n_flg = 1 : N_Flg
            Flg_TD_Vec{1}{n_flg} = circshift(Flg_TD_Vec{1}{n_flg},[Insft_US_OP,0]);
            Flg_TD_Vec{2}{n_flg} = circshift(Flg_TD_Vec{2}{n_flg},[Insft_US_OP,0]);
        end % FOR n_flg
                
        %NO_RFI & Quantized
        %Initializing the Outputs
        xQ_Vec = zeros(Ns,2);
        %Up-Sampling with B3_DUC
        [xQ_Vec(:,1),~] = B3_DUC(xQ_BU_Vec(:,1),Flg_TD_BU_Vec{1},SI1_BU{2}(1),DUC_Config_para_Q); % figure; plot(abs(fft(sBU_Vec(:,1))));
        [xQ_Vec(:,2),~] = B3_DUC(xQ_BU_Vec(:,2),Flg_TD_BU_Vec{2},SI2_BU{2}(1),DUC_Config_para_Q); % figure; plot(abs(fft(sBU_Vec(:,2))));
        clear xQ_BU_Vec;

        %Correcting the Integer Sample Delay due to the Up-Sampler
        xQ_Vec(:,1) = circshift(xQ_Vec(:,1),[Insft_US_OP,0]);
        xQ_Vec(:,2) = circshift(xQ_Vec(:,2),[Insft_US_OP,0]); 
        
        %With RFI & Quantized
        %Initializing the Outputs
        R_xQ_Vec = zeros(Ns,2);
        %Up-Sampling with B3_DUC
        [R_xQ_Vec(:,1),~] = B3_DUC(R_xQ_BU_Vec(:,1),Flg_TD_BU_Vec{1},SI1_BU{2}(1),DUC_Config_para_Q); % figure; plot(abs(fft(sBU_Vec(:,1))));
        [R_xQ_Vec(:,2),~] = B3_DUC(R_xQ_BU_Vec(:,2),Flg_TD_BU_Vec{2},SI2_BU{2}(1),DUC_Config_para_Q); % figure; plot(abs(fft(sBU_Vec(:,2))));
        clear xQ_BU_Vec;

        %Correcting the Integer Sample Delay due to the Up-Sampler
        R_xQ_Vec(:,1) = circshift(R_xQ_Vec(:,1),[Insft_US_OP,0]);
        R_xQ_Vec(:,2) = circshift(R_xQ_Vec(:,2),[Insft_US_OP,0]); 
                        
        %%Re-Quantizing at the Output of the Band 3 Up-Sampler
        if nItt == 1
            %B3_US_OP_ScaleF = [rms(x_Vec(Flg_TD_Vec{1}{1},1)),rms(x_Vec(Flg_TD_Vec{2}{1},2))];
            B3_US_OP_ScaleF = ones(2,1)/USF;
        end
        x_Vec(:,1) = x_Vec(:,1)/B3_US_OP_ScaleF(1);
        x_Vec(:,2) = x_Vec(:,2)/B3_US_OP_ScaleF(2);
        xQ_Vec(:,1) = Peak_to_US_RMS_Ratio*double(sfi(xQ_Vec(:,1)/B3_US_OP_ScaleF(1)/Peak_to_US_RMS_Ratio,W_US,W_US-1));
        xQ_Vec(:,2) = Peak_to_US_RMS_Ratio*double(sfi(xQ_Vec(:,2)/B3_US_OP_ScaleF(2)/Peak_to_US_RMS_Ratio,W_US,W_US-1));
        R_xQ_Vec(:,1) = Peak_to_US_RMS_Ratio*double(sfi(R_xQ_Vec(:,1)/B3_US_OP_ScaleF(1)/Peak_to_US_RMS_Ratio,W_US,W_US-1));
        R_xQ_Vec(:,2) = Peak_to_US_RMS_Ratio*double(sfi(R_xQ_Vec(:,2)/B3_US_OP_ScaleF(2)/Peak_to_US_RMS_Ratio,W_US,W_US-1));
        
        %Redefining the Sample Index Vector to Match
        %For the Reference Antenna
        SI0_Temp = transpose(int64((floor(double(SI0_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_Sec_Idx = int16(floor(double(SI0_Temp)./F0)) +  SI0_BU{1}(1);
        SI0_SS_Idx = mod(SI0_Temp,F0);
        %Packaging into a cell array
        SI0 = {SI0_Sec_Idx,SI0_SS_Idx};
        clear SI0_Temp SI0_Sec_Idx SI0_SS_Idx;
        
        %For the Antenna #1
        SI1_Temp = transpose(int64((floor(double(SI1_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI1_Sec_Idx = int16(floor(double(SI1_Temp)./F1)) +  SI1_BU{1}(1);
        SI1_SS_Idx = mod(SI1_Temp,F1);
        %Packaging into a cell array
        SI1 = {SI1_Sec_Idx,SI1_SS_Idx};
        clear SI1_Temp SI1_Sec_Idx SI1_SS_Idx
        
        %For the Antenna #2
        SI2_Temp = transpose(int64((floor(double(SI2_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI2_Sec_Idx = int16(floor(double(SI2_Temp)./F2)) +  SI2_BU{1}(1);
        SI2_SS_Idx = mod(SI2_Temp,F2);
        %Packaging into a cell array
        SI2 = {SI2_Sec_Idx,SI2_SS_Idx};
        clear SI2_Temp SI2_Sec_Idx SI2_SS_Idx
                
        toc;
    end %IF       
    
    %% Applying the Partial Coarse Delay correction with the DDR4 Block Read offset
    %Checking whether there is an Change in the DDR4 Block Delay (DDR4_BD) and
    %if it does End the Processing
    
    %NOTE The Reference sample Sequence Doesn't Need to be Corrected for Bulk
    %Delay Correcting the Bulk Delay in the Antenna
    fprintf('Applying Bulk Delay Corrections....');
    tic;
    %NO RFI & UnQuantized
    [SI_BD_Vec,x_BD_Vec,Flg_BD_Vec] = Bulk_Delay_Corr({SI1,SI2},x_Vec,Flg_TD_Vec,cat(3,BD1,BD2),[F1,F2],DDR4BS);
    %NO RFI & Quantized
    [~,xQ_BD_Vec,~] = Bulk_Delay_Corr({SI1,SI2},xQ_Vec,Flg_TD_Vec,cat(3,BD1,BD2),[F1,F2],DDR4BS);
    %With RFI & Quantized
    [~,R_xQ_BD_Vec,~] = Bulk_Delay_Corr({SI1,SI2},R_xQ_Vec,Flg_TD_Vec,cat(3,BD1,BD2),[F1,F2],DDR4BS);
    toc;
    clear SI1 SI2 x_Vec xQ_Vec R_xQ_Vec Flg_TD_Vec;
    
    %% Performing Wideband Frequency Shifting SID1
    fprintf('Applying Wideband Frequency Shift....')
    tic;
    %NO RFI & UnQuantized
    x_FS_Vec = WB_FS(x_BD_Vec,SI_BD_Vec,[WB_Fsft,WB_Fsft],[F1,F2],WBFS_Specs);
    %NO RFI & Quantized
    xQ_FS_Vec = WB_FS(xQ_BD_Vec,SI_BD_Vec,[WB_Fsft,WB_Fsft],[F1,F2],WBFS_Specs_Q);
    %With RFI & Quantized
    R_xQ_FS_Vec = WB_FS(R_xQ_BD_Vec,SI_BD_Vec,[WB_Fsft,WB_Fsft],[F1,F2],WBFS_Specs_Q);
    toc;
    clear x_BD_Vec xQ_BD_Vec R_xQ_BD_Vec;
    %NOTE - Frequency Shift is a Sample to Sample Operation and therefore
    %the Flags Does not Change
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Using the Function 'OS_Poly_DFT_FB_SC'
    %Zero-Padding the CoCh Input in order to get Integer number of frames
    CC_IP  = [x_FS_Vec;  zeros(N_CC_OP*M1-Ns,2)]; %NO RFI & UnQuantized
    CCQ_IP = [xQ_FS_Vec; zeros(N_CC_OP*M1-Ns,2)]; %NO RFI & Quantized
    R_CCQ_IP = [R_xQ_FS_Vec; zeros(N_CC_OP*M1-Ns,2)]; %With RFI & Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_CC_IP{1}{n_flg} = [Flg_BD_Vec{1}{n_flg}; false(N_CC_OP*M1-Ns,1)];
        Flg_CC_IP{2}{n_flg} = [Flg_BD_Vec{2}{n_flg}; false(N_CC_OP*M1-Ns,1)];
    end %For n_flg
    clear x_FS_Vec xQ_FS_Vec R_xQ_FS_Vec Flg_BD_Vec;
    
    %In order to maintain the Correct sample order in Commutator of
    %Shift the Input by
    Nsft_CC_IP = mod([SI_BD_Vec{1}{2}(1), SI_BD_Vec{2}{2}(1)],M1); %In Samples
    
    %Rotating the Input Vector to reflect the sample-order fed to the
    %Commutators of the OS Channelizer
    %NO RFI & UnQuantized
    CC_IP(:,1)  = circshift(CC_IP(:,1), [Nsft_CC_IP(1)-1,0]);
    CC_IP(:,2)  = circshift(CC_IP(:,2), [Nsft_CC_IP(2)-1,0]);
    %NO RFI & Quantized
    CCQ_IP(:,1) = circshift(CCQ_IP(:,1),[Nsft_CC_IP(1)-1,0]);
    CCQ_IP(:,2) = circshift(CCQ_IP(:,2),[Nsft_CC_IP(2)-1,0]);
    %With RFI & Quantized
    R_CCQ_IP(:,1) = circshift(R_CCQ_IP(:,1),[Nsft_CC_IP(1)-1,0]);
    R_CCQ_IP(:,2) = circshift(R_CCQ_IP(:,2),[Nsft_CC_IP(2)-1,0]);
    %Falgs
    for n_flg = 1 : N_Flg
        Flg_CC_IP{1}{n_flg} = circshift(Flg_CC_IP{1}{n_flg},[Nsft_CC_IP(1)-1,0]);
        Flg_CC_IP{2}{n_flg} = circshift(Flg_CC_IP{2}{n_flg},[Nsft_CC_IP(2)-1,0]);
    end %For n_flg
        
    %The Sample Index Vectors for the CC Outputs [SI0,SI1]
    %Reference sequence for A#0
    SI0_CC_Temp = transpose(int64(floor(double(SI0{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI0_Sec_CC_Idx = int16(floor(double(SI0_CC_Temp)./FCC0)) + SI0{1}(1);
    SI0_SS_CC_Idx = mod(SI0_CC_Temp,FCC0);
    %Packaging into a cell array
    SI0_CC = {SI0_Sec_CC_Idx,SI0_SS_CC_Idx}; clear SI0_CC_Temp SI0_Sec_CC_Idx SI0_SS_CC_Idx
    %For  sequence for A#1
    SI1_CC_Temp = transpose(int64(floor(double(SI_BD_Vec{1}{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI1_Sec_CC_Idx = int16(floor(double(SI1_CC_Temp)./FCC1)) + SI_BD_Vec{1}{1}(1);
    SI1_SS_CC_Idx = mod(SI1_CC_Temp,FCC1);
    %Packaging into a cell array
    SI1_CC = {SI1_Sec_CC_Idx,SI1_SS_CC_Idx}; clear SI1_CC_Temp SI1_Sec_CC_Idx SI1_SS_CC_Idx
    %For  sequence for A#2
    SI2_CC_Temp = transpose(int64(floor(double(SI_BD_Vec{2}{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI2_Sec_CC_Idx = int16(floor(double(SI2_CC_Temp)./FCC2)) + SI_BD_Vec{2}{1}(1);
    SI2_SS_CC_Idx = mod(SI2_CC_Temp,FCC2);
    %Packaging into a cell array
    SI2_CC = {SI2_Sec_CC_Idx,SI2_SS_CC_Idx}; clear SI2_CC_Temp SI2_Sec_CC_Idx SI2_SS_CC_Idx
    clear SI0 SI_BD_Vec;
    
    %The Barrel-Roller Indices
    BRI = mod([SI1_CC{2}(1),SI2_CC{2}(1)],Num1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Initializing Output
    CC_OP = zeros(N_CC_OP,2); %NO RFI & UnQuantized
    CCQ_OP = zeros(N_CC_OP,2); %NO RFI & Quantized
    R_CCQ_OP = zeros(N_CC_OP,2); %With RFI & Quantized
    Flg_CC_OP  = cell(1,2); %Valid
    
    fprintf('Processing with the Coarse Channelizer...')
    tic;
    %NO RFI & UnQuantized
    %For Sequence #1
    [CC_OP(:,1),~] = OS_Poly_DFT_FB_SC(CC_IP(:,1),Flg_CC_IP{1},BRI(1),OSFB_Config_para);
    %For Sequence #2
    [CC_OP(:,2),~] = OS_Poly_DFT_FB_SC(CC_IP(:,2),Flg_CC_IP{2},BRI(2),OSFB_Config_para);
    %NO RFI & Quantized
    %For Sequence #1
    [CCQ_OP(:,1),~] = OS_Poly_DFT_FB_SC(CCQ_IP(:,1),Flg_CC_IP{1},BRI(1),OSFB_Config_para_Q);
    %For Sequence #2
    [CCQ_OP(:,2),~] = OS_Poly_DFT_FB_SC(CCQ_IP(:,2),Flg_CC_IP{2},BRI(2),OSFB_Config_para_Q);
    %With RFI & Quantized
    %For Sequence #1
    [R_CCQ_OP(:,1),Flg_CC_OP{1}] = OS_Poly_DFT_FB_SC(R_CCQ_IP(:,1),Flg_CC_IP{1},BRI(1),OSFB_Config_para_Q);
    %For Sequence #2
    [R_CCQ_OP(:,2),Flg_CC_OP{2}] = OS_Poly_DFT_FB_SC(R_CCQ_IP(:,2),Flg_CC_IP{2},BRI(2),OSFB_Config_para_Q);
    toc;
    clear CC_IP CCQ_IP Flg_CC_IP;
    
    %%Quantizing at the Output of the Coarse Channelizer
    if nItt == 1 %Use the Same Scalingfor all iterations
        %CC_OP_ScaleF = [rms(CC_OP(Flg_CC_OP{1}{1},1)),rms(CC_OP(Flg_CC_OP{2}{1},2))];
        CC_OP_ScaleF = CC_ScaleF; %Pre Calculated
    end
    CC_OP(:,1) = CC_OP(:,1)/CC_OP_ScaleF(1);
    CC_OP(:,2) = CC_OP(:,2)/CC_OP_ScaleF(2);
    CCQ_OP(:,1) = Peak_to_CC_RMS_Ratio*double(sfi(CCQ_OP(:,1)/CC_OP_ScaleF(1)/Peak_to_CC_RMS_Ratio,W_CC,W_CC-1));
    CCQ_OP(:,2) = Peak_to_CC_RMS_Ratio*double(sfi(CCQ_OP(:,2)/CC_OP_ScaleF(2)/Peak_to_CC_RMS_Ratio,W_CC,W_CC-1));
    R_CCQ_OP(:,1) = Peak_to_CC_RMS_Ratio*double(sfi(R_CCQ_OP(:,1)/CC_OP_ScaleF(1)/Peak_to_CC_RMS_Ratio,W_CC,W_CC-1));
    R_CCQ_OP(:,2) = Peak_to_CC_RMS_Ratio*double(sfi(R_CCQ_OP(:,2)/CC_OP_ScaleF(2)/Peak_to_CC_RMS_Ratio,W_CC,W_CC-1));
    
    %Correcting the Integer Sample Delay due to Polyphase Filter-Bak
    %For Data
    CC_OP  = [CC_OP(1-Insft_CC_OP:end,:);  zeros(-Insft_CC_OP,2)]; %NO RFI & UnQuantized
    CCQ_OP = [CCQ_OP(1-Insft_CC_OP:end,:); zeros(-Insft_CC_OP,2)]; %NO RFI & Quantized
    R_CCQ_OP = [R_CCQ_OP(1-Insft_CC_OP:end,:); zeros(-Insft_CC_OP,2)]; %With RFI & Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_CC_OP{1}{n_flg} = [Flg_CC_OP{1}{n_flg}(1-Insft_CC_OP:end,1); false(-Insft_CC_OP,1)];
        Flg_CC_OP{2}{n_flg} = [Flg_CC_OP{2}{n_flg}(1-Insft_CC_OP:end,1); false(-Insft_CC_OP,1)];
    end %For n_flg
    
    %% Plotting the CoCh Output
%         figure('name','Coarse Channelizer Output');
%         subplot(321); hold on; plot(real(CC_OP(:,1)),'b-.'); plot(real(CC_OP(:,2)),'r-.'); plot(real(CCQ_OP(:,1)),'c-.'); plot(real(CCQ_OP(:,2)),'m-.'); plot(real(R_CCQ_OP(:,1)),'g-.'); plot(real(R_CCQ_OP(:,2)),'y-.'); hold off; title('Real'); box on; grid on; legend('UA1','UA2','QA1','QA2','RQA1','RQA2');
%         subplot(322); hold on; plot(imag(CC_OP(:,1)),'b-.'); plot(imag(CC_OP(:,2)),'r-.'); plot(imag(CCQ_OP(:,1)),'c-.'); plot(imag(CCQ_OP(:,2)),'m-.'); plot(imag(R_CCQ_OP(:,1)),'g-.'); plot(imag(R_CCQ_OP(:,2)),'y-.'); hold off; title('Imaginary'); box on; grid on; legend('UA1','UA2','QA1','QA2','RQA1','RQA2');
%         subplot(323); hold on; plot(real(CC_OP(:,1)-CCQ_OP(:,1)),'k--'); plot(real(CC_OP(:,2)-CCQ_OP(:,2)),'g--'); hold off; title('Real'); box on; grid on; legend('UA1-QA1','UA2-QA2');
%         subplot(324); hold on; plot(imag(CC_OP(:,1)-CCQ_OP(:,1)),'k--'); plot(imag(CC_OP(:,2)-CCQ_OP(:,2)),'g--'); hold off; title('Imaginary'); box on; grid on; legend('UA1-QA1','UA2-QA2');
%         subplot(325); hold on; plot(real(CC_OP(:,1)-R_CCQ_OP(:,1)),'k--'); plot(real(CC_OP(:,2)-R_CCQ_OP(:,2)),'g--'); hold off; title('Real'); box on; grid on; legend('UA1-RQA1','UA2-RQA2');
%         subplot(326); hold on; plot(imag(CC_OP(:,1)-R_CCQ_OP(:,1)),'k--'); plot(imag(CC_OP(:,2)-R_CCQ_OP(:,2)),'g--'); hold off; title('Imaginary'); box on; grid on; legend('UA1-RQA1','UA2-RQA2');
%     
    %% Threshold Detection after the Coarse Channalizer
    
    fprintf('Threshold detection and Flagging Coarse Channel Output Sequences ...');
    tic;
    %Initializing Output
    Flg_TD_CC_OP  = cell(1,2); %Valid
    Flg_TD_CC_OP{1} = cell(1,N_Flg); Flg_TD_CC_OP{2} = cell(1,N_Flg); %N_Flgs's for Each Stream
    
    %Assigning Valid Flag
    Flg_TD_CC_OP{1}{1} = Flg_CC_OP{1}{1};
    Flg_TD_CC_OP{2}{1} = Flg_CC_OP{2}{1};
    %Using the Custom Function Threshold_Det
    [Flg_TD_CC_OP{1}{2},CC_LTPS_Est1,CC_STPS_Est1] = Threshold_Det(R_CCQ_OP(:,1),Flg_CC_OP{1}{2},Threshold_para_CC);
    [Flg_TD_CC_OP{2}{2},CC_LTPS_Est2,CC_STPS_Est2] = Threshold_Det(R_CCQ_OP(:,2),Flg_CC_OP{2}{2},Threshold_para_CC);
    %Assigning Ideal RFI Flags
    Flg_TD_CC_OP{1}{3} = Flg_CC_OP{1}{3};
    Flg_TD_CC_OP{2}{3} = Flg_CC_OP{2}{3};
    %clear Flg_CC_OP
    toc;
    
    %% Plotting the Threshold Outputs
%     %The Power Levels
%     P_Est_CC = median([CC_LTPS_Est1,CC_LTPS_Est2]);
%     
%     figure('name','Short and Long Term Power Estimates and Flags - CoCh Output');
%     
%     subplot(321);
%     hold on;
%     plot(CC_STPS_Est1,'b-');
%     plot([1,N_CC_OP],P_Est_CC(1)*[1,1],'k--')
%     plot([1,N_CC_OP],P_Est_CC(1)*(1+4*sqrt(2/STL))*[1,1],'k--')
%     plot([1,N_CC_OP],P_Est_CC(1)*(1-4*sqrt(2/STL))*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Short-Term Power Sum - Seq-1');
%     
%     subplot(322);
%     hold on;
%     plot(CC_STPS_Est2,'b-');
%     plot([1,N_CC_OP],P_Est_CC(2)*[1,1],'k--')
%     plot([1,N_CC_OP],P_Est_CC(2)*(1+4*sqrt(2/STL))*[1,1],'k--')
%     plot([1,N_CC_OP],P_Est_CC(2)*(1-4*sqrt(2/STL))*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Short-Term Power Sum - Seq-2');
%     
%     subplot(323);
%     hold on;
%     plot(CC_LTPS_Est1,'b-');
%     plot([1,N_CC_OP],P_Est_CC(1)*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Long-Term Power Sum - Seq-1');
%     
%     subplot(324);
%     hold on;
%     plot(CC_LTPS_Est2,'b-');
%     plot([1,N_CC_OP],P_Est_CC(2)*[1,1],'k--')
%     hold off;
%     box on; grid on;
%     xlabel('Sample Index'); ylabel('Magnitude');
%     title('Long-Term Power Sum - Seq-2');
%     
%     subplot(325);
%     hold on;
%     stairs(~Flg_TD_CC_OP{1}{2},'k-.');
%     stairs(~Flg_CC_OP{1}{2},'g:');
%     hold off;
%     box on; grid on;
%     ylim([-0.05, 1.05]);
%     xlabel('Sample Index'); ylabel('Invalid');
%     title('Signal Invalid - Seq-1');
%     
%     subplot(326);
%     hold on;
%     stairs(~Flg_TD_CC_OP{2}{2},'k-.');
%     stairs(~Flg_CC_OP{2}{2},'g:');
%     hold off;
%     box on; grid on;
%     ylim([-0.05, 1.05]);
%     xlabel('Sample Index'); ylabel('Invalid');
%     title('Signal Invalid - Seq-2');
%     pause(0.01);
        
    %% Using the function 'ReSampler_ERP_Corr'
    %Initializing
    RS_OP = zeros(N_CC_OP,2); %NO RFI & UnQuantized Data
    RSQ_OP = zeros(N_CC_OP,2); %NO RFI & Quantized Data
    R_RSQ_OP = zeros(N_CC_OP,2); %With RFI & Quantized Data
    Flg_RS_OP = cell(1,2); %Flags
    
    fprintf('Applying Delay and Phase Corrections...')
    tic;
    %Using the ReSampler Function to Apply both Delay and Phase Corrections
    %NO RFI & UnQunatized
    [RS_OP(:,1),Flg_RS_OP{1}] = ReSampler_ERP_Corr(CC_OP(:,1),Flg_TD_CC_OP{1},FCC1,SI1_CC,SI0_CC,FODSR1,FOPSR1,ReSamp_Config_para);
    [RS_OP(:,2),Flg_RS_OP{2}] = ReSampler_ERP_Corr(CC_OP(:,2),Flg_TD_CC_OP{2},FCC2,SI2_CC,SI0_CC,FODSR2,FOPSR2,ReSamp_Config_para);
    %NO RFI & Quantized
    [RSQ_OP(:,1),~] = ReSampler_ERP_Corr(CCQ_OP(:,1),Flg_TD_CC_OP{1},FCC1,SI1_CC,SI0_CC,FODSR1,FOPSR1,ReSamp_Config_para_Q);
    [RSQ_OP(:,2),~] = ReSampler_ERP_Corr(CCQ_OP(:,2),Flg_TD_CC_OP{2},FCC2,SI2_CC,SI0_CC,FODSR2,FOPSR2,ReSamp_Config_para_Q);
    %With RFI & Quantized
    [R_RSQ_OP(:,1),~] = ReSampler_ERP_Corr(R_CCQ_OP(:,1),Flg_TD_CC_OP{1},FCC1,SI1_CC,SI0_CC,FODSR1,FOPSR1,ReSamp_Config_para_Q);
    [R_RSQ_OP(:,2),~] = ReSampler_ERP_Corr(R_CCQ_OP(:,2),Flg_TD_CC_OP{2},FCC2,SI2_CC,SI0_CC,FODSR2,FOPSR2,ReSamp_Config_para_Q);
    toc;
    clear CC_OP CCQ_OP R_CCQ_OP Flg_CC_OP SI1_CC SI2_CC;    
   
    %Quantising the ReSampler Outputs
    if nItt == 1 %Use the Same Scalingfor all iterations
        %RS_OP_ScaleF = [rms(RS_OP(Flg_RS_OP{1}{1},1)),rms(RS_OP(Flg_RS_OP{2}{1},2))];
        RS_OP_ScaleF = [F0/F1 F0/F2];
    end
    RS_OP(:,1) = RS_OP(:,1)/RS_OP_ScaleF(1);
    RS_OP(:,2) = RS_OP(:,2)/RS_OP_ScaleF(2);
    RSQ_OP(:,1) = Peak_to_RS_RMS_Ratio*double(sfi(RSQ_OP(:,1)/RS_OP_ScaleF(1)/Peak_to_RS_RMS_Ratio,W_RS,W_RS-1));
    RSQ_OP(:,2) = Peak_to_RS_RMS_Ratio*double(sfi(RSQ_OP(:,2)/RS_OP_ScaleF(2)/Peak_to_RS_RMS_Ratio,W_RS,W_RS-1));
    R_RSQ_OP(:,1) = Peak_to_RS_RMS_Ratio*double(sfi(R_RSQ_OP(:,1)/RS_OP_ScaleF(1)/Peak_to_RS_RMS_Ratio,W_RS,W_RS-1));
    R_RSQ_OP(:,2) = Peak_to_RS_RMS_Ratio*double(sfi(R_RSQ_OP(:,2)/RS_OP_ScaleF(2)/Peak_to_RS_RMS_Ratio,W_RS,W_RS-1));
    
    %% Plotting ReSampler Output & Error
%     %Error ReSampling
%     Est_DError1 = (RS_OP(:,1) - RS_OP(:,2)).*double(Flg_RS_OP{1}{1});
%     Est_DError2 = (RSQ_OP(:,1) - RSQ_OP(:,2)).*double(Flg_RS_OP{2}{1});
%     %Error Quant
%     %Est_DError1 = (RS_OP(:,1) - RSQ_OP(:,1)).*double(Flg_RS_OP{1}{1});
%     %Est_DError2 = (RS_OP(:,2) - RSQ_OP(:,2)).*double(Flg_RS_OP{2}{1});
%     %Defining the Indexing Vector
%     SI0_RS = int64(SI0_CC{1})*FCC0 + SI0_CC{2};
%     Tstart = SI0_RS(1);
%     Tend = SI0_RS(end);
%     
%     figure('name','ReSampler Output');
%     
%     subplot(221);
%     hold on;
%     plot(SI0_RS,real(RS_OP(:,1)),'b-.');
%     plot(SI0_RS,real(RS_OP(:,2)),'r-.');
%     plot(SI0_RS,real(RSQ_OP(:,1)),'c-.');
%     plot(SI0_RS,real(RSQ_OP(:,2)),'m-.');
%     plot(SI0_RS,real(R_RSQ_OP(:,1)),'g-.');
%     plot(SI0_RS,real(R_RSQ_OP(:,2)),'y-.');
%     hold off;
%     box on; grid on;
%     xlim([Tstart, Tend]);
%     title('Real-Part');
%     xlabel('Sample-Index'); ylabel('Amplitude');
%     legend('UA0','UA1','QA0','QA1');
%     
%     subplot(222);
%     hold on;
%     plot(SI0_RS,imag(RS_OP(:,1)),'b-.');
%     plot(SI0_RS,imag(RS_OP(:,2)),'r-.');
%     plot(SI0_RS,imag(RSQ_OP(:,1)),'c-.');
%     plot(SI0_RS,imag(RSQ_OP(:,2)),'m-.');
%     hold off;
%     box on; grid on;
%     xlim([Tstart, Tend]);
%     title('Imaginary-Part');
%     xlabel('Sample-Index'); ylabel('Amplitude');
%     legend('UA0','UA1','QA0','QA1');
%     
%     subplot(223);
%     hold on;
%     plot(SI0_RS,real(Est_DError1),'k-.');
%     plot(SI0_RS,real(Est_DError2),'g-.');
%     hold off;
%     box on; grid on;
%     xlim([Tstart, Tend]);
%     title('Error between Real-Parts');
%     xlabel('Sample-Index'); ylabel('Amplitude');
%     legend('UA0-QA0','UA1-QA1');
%     
%     subplot(224);
%     hold on;
%     plot(SI0_RS,imag(Est_DError1),'k-.');
%     plot(SI0_RS,imag(Est_DError2),'g-.');
%     hold off;
%     box on; grid on;
%     xlim([Tstart, Tend]);
%     title('Error between Imaginary-Part');
%     xlabel('Sample-Index'); ylabel('Amplitude');
%     legend('UA0-QA0','UA1-QA1');
    
    %% Using the function 'CS_Poly_DFT_FB_Batch'
    
    %Zero-Padding the Input for the Imaging Channelizer Input
    IC_IP  = [RS_OP;  zeros(N_IC_OP*Nc2-N_CC_OP,2)]; %NO RFI & UnQuantized
    ICQ_IP = [RSQ_OP; zeros(N_IC_OP*Nc2-N_CC_OP,2)]; %NO RFI & Quantized
    R_ICQ_IP = [R_RSQ_OP; zeros(N_IC_OP*Nc2-N_CC_OP,2)]; %With RFI & Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_IC_IP{1}{n_flg} = [Flg_RS_OP{1}{n_flg}; false(N_IC_OP*Nc2-N_CC_OP,1)];
        Flg_IC_IP{2}{n_flg} = [Flg_RS_OP{2}{n_flg}; false(N_IC_OP*Nc2-N_CC_OP,1)]; 
    end % FOR n_flg
    clear RS_OP RSQ_OP R_RSQ_OP Flg_RS_OP;
    
    %In order to maintain the Correct sample order in Commutator of
    %Shift the Input by
    Nsft_IC_IP = mod(SI0_CC{2}(1),Nc2); %In Samples
    %NOTE now both Sequences are have the Same Indices
    
    %Rotating the Input Vector to reflect the sample-order fed to the
    %Commutators of the OS Channelizer
    IC_IP  = circshift(IC_IP, [Nsft_IC_IP-1,0]); %NO RFI & UnQuantized
    ICQ_IP = circshift(ICQ_IP,[Nsft_IC_IP-1,0]); %NO RFI & Quantized
    R_ICQ_IP = circshift(R_ICQ_IP,[Nsft_IC_IP-1,0]); %With RFI & Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_IC_IP{1}{n_flg} = circshift(Flg_IC_IP{1}{n_flg},[Nsft_IC_IP-1,0]);
        Flg_IC_IP{2}{n_flg} = circshift(Flg_IC_IP{2}{n_flg},[Nsft_IC_IP-1,0]);
    end % FOR n_flg
    %The Sample Index Vectors for the CC Outputs [SI0,SI1]
    SI0_IC_Temp = transpose(int64(floor(double(SI0_CC{2}(1))./Nc2) + (0 : 1 : N_IC_OP-1)));
    SI0_Sec_IC_Idx = int16(floor(double(SI0_IC_Temp)./F_IC)) + SI0_CC{1}(1);
    SI0_SS_IC_Idx = mod(SI0_IC_Temp,F_IC);
    %Packaging into a cell array
    SI0_IC = {SI0_Sec_IC_Idx,SI0_SS_IC_Idx}; clear SI0_IC_Temp SI0_Sec_IC_Idx SI0_SS_IC_Idx
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Initialize
    IC_OP = zeros(N_IC_OP,Nc2,2); %NO RFI & UnQuantized
    ICQ_OP = zeros(N_IC_OP,Nc2,2); %NO RFI & Quantized
    R_ICQ_OP = zeros(N_IC_OP,Nc2,2); %With RFI & Quantized
    Flg_IC_OP = cell(1,2); %Flags

    fprintf('Processing the Signals with Imaging Channelizer...')
    tic;
    %NO RFI & UnQuantized
    %For Sequence #1
    [IC_OP(:,:,1),Flg_IC_OP{1}] = CS_Poly_DFT_FB_Batch(IC_IP(:,1),Flg_IC_IP{1},CSFB_Config_para);
    %For Sequence #2
    [IC_OP(:,:,2),Flg_IC_OP{2}] = CS_Poly_DFT_FB_Batch(IC_IP(:,2),Flg_IC_IP{2},CSFB_Config_para);
    %NO RFI & Quantized
    %For Sequence #1
    [ICQ_OP(:,:,1),~] = CS_Poly_DFT_FB_Batch(ICQ_IP(:,1),Flg_IC_IP{1},CSFB_Config_para_Q);
    %For Sequence #2
    [ICQ_OP(:,:,2),~] = CS_Poly_DFT_FB_Batch(ICQ_IP(:,2),Flg_IC_IP{2},CSFB_Config_para_Q);
    %With RFI & Quantized
    %For Sequence #1
    [R_ICQ_OP(:,:,1),~] = CS_Poly_DFT_FB_Batch(R_ICQ_IP(:,1),Flg_IC_IP{1},CSFB_Config_para_Q);
    %For Sequence #2
    [R_ICQ_OP(:,:,2),~] = CS_Poly_DFT_FB_Batch(R_ICQ_IP(:,2),Flg_IC_IP{2},CSFB_Config_para_Q);
    toc;
    clear IC_IP ICQ_IP V_IC_IP;
    
    %Correcting the Integer Sample Delay due to Polyphase Filter-Bank
    %For Data
    IC_OP  = [IC_OP(1-Insft_IC_OP:end,:,:);  zeros(-Insft_IC_OP,Nc2,2)]; %NO RFI & UnQuantized
    ICQ_OP = [ICQ_OP(1-Insft_IC_OP:end,:,:); zeros(-Insft_IC_OP,Nc2,2)]; %NO RFI & Quantized
    R_ICQ_OP = [R_ICQ_OP(1-Insft_IC_OP:end,:,:); zeros(-Insft_IC_OP,Nc2,2)]; %With RFI & Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_IC_OP{1}{n_flg} = [Flg_IC_OP{1}{n_flg}(1-Insft_IC_OP:end); false(-Insft_IC_OP,1)];
        Flg_IC_OP{2}{n_flg} = [Flg_IC_OP{2}{n_flg}(1-Insft_IC_OP:end); false(-Insft_IC_OP,1)];
    end % FOR n_flg
    
    %Shedding invalid samples at the front and the back before correlating
    %Note the first flag is associated with the signal validity thus
    %compared here
    First_1_Ant = zeros(1,2);
    Last_1_Ant  = zeros(1,2);
    for n = 1 : 2
        First_1_Ant(1,n) = find(Flg_IC_OP{n}{1},1,'first');
        Last_1_Ant(1,n)  = find(Flg_IC_OP{n}{1},1,'last');
    end
    First_1_Idx = min(First_1_Ant);
    Last_1_Idx = max(Last_1_Ant);
    
    %For Data
    IC_OP   = IC_OP(First_1_Idx:Last_1_Idx,:,:); %NO RFI & UnQuantized
    ICQ_OP  = ICQ_OP(First_1_Idx:Last_1_Idx,:,:); %NO RFI & Quantized
    R_ICQ_OP  = R_ICQ_OP(First_1_Idx:Last_1_Idx,:,:); %With RFI & Quantized
    %Flags 
    for n_flg = 1 : N_Flg
        Flg_IC_OP{1}{n_flg} = Flg_IC_OP{1}{n_flg}(First_1_Idx:Last_1_Idx,:,:);
        Flg_IC_OP{2}{n_flg} = Flg_IC_OP{2}{n_flg}(First_1_Idx:Last_1_Idx,:,:);
    end %FOR n_flg
    %For Sample Indicies
    SI0_IC = {SI0_IC{1}(First_1_Idx:Last_1_Idx), SI0_IC{2}(First_1_Idx:Last_1_Idx)};
        
    %Displaying the Range of Time Indicies evalautes
    fprintf('Imaging Channel Time Series | Evalauted from %d-%d to %d-%d \n',SI0_IC{1}(1),SI0_IC{2}(1),SI0_IC{1}(end),SI0_IC{2}(end));
    
    %% Gain Correction
    [NICE,~] =  size(IC_OP);
    %NO RFI & UnQuantized
    IC_OP(:,:,1) = (ones(NICE,1)*GC_Vec(1,:)).*IC_OP(:,:,1);
    IC_OP(:,:,2) = (ones(NICE,1)*GC_Vec(2,:)).*IC_OP(:,:,2);
    %NO RFI & Quantized
    ICQ_OP(:,:,1) = (ones(NICE,1)*GC_Vec(1,:)).*ICQ_OP(:,:,1);
    ICQ_OP(:,:,2) = (ones(NICE,1)*GC_Vec(2,:)).*ICQ_OP(:,:,2);
    %With RFI & Quantized
    R_ICQ_OP(:,:,1) = (ones(NICE,1)*GC_Vec(1,:)).*R_ICQ_OP(:,:,1);
    R_ICQ_OP(:,:,2) = (ones(NICE,1)*GC_Vec(2,:)).*R_ICQ_OP(:,:,2);
    
    %ReQuantising
    if nItt == 1
        %IC_OP_ScaleF = [rms(IC_OP(Flg_IC_OP{1}{1},:,1));rms(IC_OP(Flg_IC_OP{2}{1},:,2))];
        IC_OP_ScaleF = IC_Scale_Fac*ones(2,Nc2);
    end
    IC_OP(:,:,1) = IC_OP(:,:,1)./(ones(NICE,1)*IC_OP_ScaleF(1,:));
    IC_OP(:,:,2) = IC_OP(:,:,2)./(ones(NICE,1)*IC_OP_ScaleF(2,:));
    ICQ_OP(:,:,1) = Peak_to_IC_RMS_Ratio.*double(sfi(ICQ_OP(:,:,1)./(ones(NICE,1)*IC_OP_ScaleF(1,:))/Peak_to_IC_RMS_Ratio,W_IC,W_IC-1));
    ICQ_OP(:,:,2) = Peak_to_IC_RMS_Ratio.*double(sfi(ICQ_OP(:,:,2)./(ones(NICE,1)*IC_OP_ScaleF(2,:))/Peak_to_IC_RMS_Ratio,W_IC,W_IC-1));
    R_ICQ_OP(:,:,1) = Peak_to_IC_RMS_Ratio.*double(sfi(R_ICQ_OP(:,:,1)./(ones(NICE,1)*IC_OP_ScaleF(1,:))/Peak_to_IC_RMS_Ratio,W_IC,W_IC-1));
    R_ICQ_OP(:,:,2) = Peak_to_IC_RMS_Ratio.*double(sfi(R_ICQ_OP(:,:,2)./(ones(NICE,1)*IC_OP_ScaleF(2,:))/Peak_to_IC_RMS_Ratio,W_IC,W_IC-1));

    %% Comparison of Imaging Channelizer Output for a Given Time-Instant
    %     %Selecting a Time-Instant to Compare
    %     Sel_TS = ceil(rand*(length(ICx0(:,1))-1));
    %     %Sel_TS = 225;
    %
    %     %Unquantized
    %     Sel_ICx0 = ICx0(Sel_TS,:);
    %     Sel_ICx1 = ICx1(Sel_TS,:);
    %     %Quantized
    %     Sel_ICQx0 = ICQx0(Sel_TS,:);
    %     Sel_ICQx1 = ICQx1(Sel_TS,:);
    %
    %     M_Est_ICError0 = abs(Sel_ICx0) - abs(Sel_ICQx0);
    %     M_Est_ICError1 = abs(Sel_ICx1) - abs(Sel_ICQx1);
    %     A_Est_ICError0 = asin(sin(angle(Sel_ICx0) - angle(Sel_ICQx0)));
    %     A_Est_ICError1 = asin(sin(angle(Sel_ICx1) - angle(Sel_ICQx1)));
    %
    %     NFCH_l = ceil(Nc2/Os1/2);
    %     NFCH_u = Nc2-NFCH_l;
    %     Max_SL = 600; %Maximum Signal-Level
    %
    %     figure('name',sprintf('Imaging Channelizer Output | Magnitude - Phase for Time Slice %d',Sel_TS));
    %
    %     subplot(221);
    %     hold on;
    %     plot(abs(Sel_ICx0),'b-.');
    %     plot(abs(Sel_ICx1),'r-.');
    %     plot(abs(Sel_ICQx0),'c-.');
    %     plot(abs(Sel_ICQx1),'m-.');
    %     plot([NFCH_l,NFCH_l],[0,Max_SL],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[0,Max_SL],'k-.','LineWidth',1);
    %     hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Imaging Channels - Magnitude');
    %     xlabel('Channel-Index'); ylabel('Magnitude');
    %     legend('A0-U','A1-U','A0-Q','A1-Q');
    %
    %     subplot(222);
    %     hold on;
    %     plot(angle(Sel_ICx0),'b-.');
    %     plot(angle(Sel_ICx1),'r-.');
    %     plot(angle(Sel_ICQx0),'c-.');
    %     plot(angle(Sel_ICQx1),'m-.');
    %     plot([NFCH_l,NFCH_l],[-1.5*pi,1.5*pi],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[-1.5*pi,1.5*pi],'k-.','LineWidth',1);hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Imaging Channels - Phase Angle');
    %     xlabel('Channel-Index'); ylabel('Angle - rad');
    %     legend('A0-U','A1-U','A0-Q','A1-Q');
    %
    %     subplot(223);
    %     hold on;
    %     plot(M_Est_ICError0,'k-.');
    %     plot(M_Est_ICError1,'g-.');
    %     plot([NFCH_l,NFCH_l],[-Max_SL,Max_SL],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[-Max_SL,Max_SL],'k-.','LineWidth',1);hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Error in Magnitude');
    %     xlabel('Channel-Index'); ylabel('Magnitude');
    %     legend('A0-U - A0-Q','A1-U - A1-Q');
    %
    %     subplot(224);
    %     hold on;
    %     plot(A_Est_ICError0,'k-.');
    %     plot(A_Est_ICError1,'g-.');
    %     plot([NFCH_l,NFCH_l],[-2*pi,2*pi],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[-2*pi,2*pi],'k-.','LineWidth',1);hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Error In Phase');
    %     xlabel('Channel-Index'); ylabel('Angle - rad');
    %     legend('A0-U - A0-Q','A1-U - A1-Q');
    
    %% Saving Imaging Channel Time Series for further analysis 
    %%Saving the Flags, Imaging Channel Time Series 
    FileName = fullfile(FolderName,sprintf('Data_Pack_%04d',nItt));
    save(FileName,'SI0_IC','Flg_IC_OP','IC_OP','ICQ_OP','R_ICQ_OP','RFI_Free_Pct');
    
    %% Calculating the Auto & Cross Correlations
    
    %% Integration - NO RFI (Hence NO Flagging Required)
        
    %Estimating the Variance of the Mean Estimate Iteratively
    Mean_XC_A1A2 = mean(IC_OP(:,:,1).*conj(IC_OP(:,:,2)),1); %UnQuanrized
    Mean_XCQ_A1A2 = mean(ICQ_OP(:,:,1).*conj(ICQ_OP(:,:,2)),1); %Quantized
    if nItt > 1
        %NO RFI & UnQuantized
        ME = (XC_A1A2 - Mean_XC_A1A2);        
        VXC_A1A2 = (nItt-2)/(nItt-1)*VXC_A1A2 + ME.*conj(ME)/nItt;
        %NO RFI & Quantized
        MEQ = (XCQ_A1A2 - Mean_XCQ_A1A2);        
        VXCQ_A1A2 = (nItt-2)/(nItt-1)*VXCQ_A1A2 + MEQ.*conj(MEQ)/nItt;
    end %IF
    
    %UnQuanrized
    AC_A1 = ((nItt-1)*AC_A1 + mean(abs(IC_OP(:,:,1)).^2,1))./nItt;
    AC_A2 = ((nItt-1)*AC_A2 + mean(abs(IC_OP(:,:,2)).^2,1))./nItt;    
    XC_A1A2 = ((nItt-1)*XC_A1A2 + Mean_XC_A1A2)./nItt;

    %Quantized
    ACQ_A1 = ((nItt-1)*ACQ_A1 + mean(abs(ICQ_OP(:,:,1)).^2,1))./nItt;
    ACQ_A2 = ((nItt-1)*ACQ_A2 + mean(abs(ICQ_OP(:,:,2)).^2,1))./nItt;    
    XCQ_A1A2 = ((nItt-1)*XCQ_A1A2 + Mean_XCQ_A1A2)./nItt; 
    
    %% RFI Contaminated Signal - Integration Without Flagging
    
    %Estimating the Variance of the Mean Estimate Iteratively
    Mean_WO_XCQ_A1A2 = mean(R_ICQ_OP(:,:,1).*conj(R_ICQ_OP(:,:,2)),1);
    if nItt > 1
        %With RFI & Quantized
        WO_MEQ = (WO_XCQ_A1A2 - Mean_WO_XCQ_A1A2);        
        WO_VXCQ_A1A2 = (nItt-2)/(nItt-1)*WO_VXCQ_A1A2 + WO_MEQ.*conj(WO_MEQ)/nItt;
    end %IF

    %Quanrized
    WO_ACQ_A1 = ((nItt-1)*WO_ACQ_A1 + mean(abs(R_ICQ_OP(:,:,1)).^2,1))./nItt;
    WO_ACQ_A2 = ((nItt-1)*WO_ACQ_A2 + mean(abs(R_ICQ_OP(:,:,2)).^2,1))./nItt;
    WO_XCQ_A1A2 = ((nItt-1)*WO_XCQ_A1A2 + Mean_WO_XCQ_A1A2)./nItt;
    
    %% RFI Contaminated Signal - Integration With Flagging
    
%     %Empirical Threshold-based flagging
%     fprintf('Threshold detection and Flagging Imaging Channel Output Sequences ...');
%     tic;
%     %Initializing Output
%     Flg_TD_IC_OP  = cell(1,2); %Valid
%     
%     %Using the Custom Function Threshold_Det
%     [Flg_TD_IC_OP{1},IC_LTPS_Est1,IC_STPS_Est1] = Threshold_Det_NCh(R_ICQ_OP(:,:,1),Flg_IC_OP{1},Threshold_para_IC);
%     [Flg_TD_IC_OP{2},IC_LTPS_Est2,IC_STPS_Est2] = Threshold_Det_NCh(R_ICQ_OP(:,:,2),Flg_IC_OP{2},Threshold_para_IC);
%     toc;
    
        %Ideal Flagging
        Flg_TD_IC_OP  = cell(1,2); %Valid
        Flg_TD_IC_OP{1} = true(NICE,Nc2);
        Flg_TD_IC_OP{2} = true(NICE,Nc2);
        for n2_ch = RFIC_IC_CH_Rng(1) : RFIC_IC_CH_Rng(end)
            Flg_TD_IC_OP{1}(:,n2_ch) = Flg_IC_OP{1}{3};
            Flg_TD_IC_OP{2}(:,n2_ch) = Flg_IC_OP{1}{3};
        end %FOR n2_ch
    
    %Estimating the Variance of the Mean Estimate Iteratively
    Valid_R_ICQ_OP1 = R_ICQ_OP(:,:,1).*Flg_TD_IC_OP{1}; Temp_DVC_R_ICQ_OP1 = sum(Flg_TD_IC_OP{1});
    Valid_R_ICQ_OP2 = R_ICQ_OP(:,:,2).*Flg_TD_IC_OP{2}; Temp_DVC_R_ICQ_OP2 = sum(Flg_TD_IC_OP{2});
    XP_R_ICQ_A1A2 = Valid_R_ICQ_OP1.*conj(Valid_R_ICQ_OP2); Temp_DCV_R_ICQ_A1A2 = sum(Flg_TD_IC_OP{1} & Flg_TD_IC_OP{2} == true,1);
    
    Mean_WF_XCQ_A1A2 = sum(XP_R_ICQ_A1A2,1)./Temp_DCV_R_ICQ_A1A2;
    if nItt > 1        
        %With RFI & Quantized
        WF_MEQ = (WF_XCQ_A1A2 - Mean_WF_XCQ_A1A2);        
        WF_VXCQ_A1A2 = (nItt-2)/(nItt-1)*WF_VXCQ_A1A2 + WF_MEQ.*conj(WF_MEQ)/nItt;
    end %IF   
    
    %With RFI & With Flagging & Quanrized
    WF_ACQ_A1 = ((nItt-1)*WF_ACQ_A1 + sum(Valid_R_ICQ_OP1.*conj(Valid_R_ICQ_OP1),1)./Temp_DVC_R_ICQ_OP1)./nItt;
    WF_ACQ_A2 = ((nItt-1)*WF_ACQ_A2 + sum(Valid_R_ICQ_OP2.*conj(Valid_R_ICQ_OP2),1)./Temp_DVC_R_ICQ_OP2)./nItt;    
    WF_XCQ_A1A2 = ((nItt-1)*WF_XCQ_A1A2 + Mean_WF_XCQ_A1A2)./nItt; 
    
    %Updating Data Valid Counts
    DVC_ICQ_A1 = DVC_ICQ_A1 + Temp_DVC_R_ICQ_OP1;
    DVC_ICQ_A2 = DVC_ICQ_A2 + Temp_DVC_R_ICQ_OP2;
    DVC_ICQ_A1A2 = DVC_ICQ_A1A2 + Temp_DCV_R_ICQ_A1A2;
    %pause(1);
    fprintf('\n')
end % FOR nItt
 
%% Plotting the Auto and Cross-Correlations
CH_Idx = (-0.5*Nc2:1:0.5*Nc2-1);  
NFCH_l = -0.5*Nc2+ceil(Nc2*(Os1-1)/Os1/2);
NFCH_u = -NFCH_l;

%Converting to dB - Note the 'fftshift' is used to re orient the channels
%such that it can be easily interpretted

%NO RFI & UnQuantized
AC_A1_dB = 10*log10(abs(fftshift(AC_A1))); Max_PL_dB = max(AC_A1_dB); %Maximum Power-Level
AC_A1_dB = AC_A1_dB - Max_PL_dB;
AC_A2_dB = 10*log10(abs(fftshift(AC_A2))) - Max_PL_dB;
XC_A1A2_dB = 10*log10(abs(fftshift(XC_A1A2))) - Max_PL_dB;

%NO RFI & Quantized
ACQ_A1_dB = 10*log10(abs(fftshift(ACQ_A1))); Max_PLQ_dB = max(ACQ_A1_dB); %Maximum Power-Level
ACQ_A1_dB = ACQ_A1_dB - Max_PLQ_dB;
ACQ_A2_dB = 10*log10(abs(fftshift(ACQ_A2))) - Max_PLQ_dB;
XCQ_A1A2_dB = 10*log10(abs(fftshift(XCQ_A1A2))) - Max_PLQ_dB;

%With RFI & Quantized & With Out Flagging
% WO_ACQ_A1_dB = 10*log10(abs(fftshift(WO_ACQ_A1))); WO_Max_PLQ_dB = max(WO_ACQ_A1_dB); %Maximum Power-Level
% WO_ACQ_A1_dB = WO_ACQ_A1_dB - WO_Max_PLQ_dB;
WO_ACQ_A1_dB = 10*log10(abs(fftshift(WO_ACQ_A1))) - Max_PLQ_dB;
WO_ACQ_A2_dB = 10*log10(abs(fftshift(WO_ACQ_A2))) - Max_PLQ_dB;
WO_XCQ_A1A2_dB = 10*log10(abs(fftshift(WO_XCQ_A1A2))) - Max_PLQ_dB;

%With RFI & Quantized & With Flagging
% WO_ACQ_A1_dB = 10*log10(abs(fftshift(WO_ACQ_A1))); WO_Max_PLQ_dB = max(WO_ACQ_A1_dB); %Maximum Power-Level
% WO_ACQ_A1_dB = WO_ACQ_A1_dB - WO_Max_PLQ_dB;
WF_ACQ_A1_dB = 10*log10(abs(fftshift(WF_ACQ_A1))) - Max_PLQ_dB;
WF_ACQ_A2_dB = 10*log10(abs(fftshift(WF_ACQ_A2))) - Max_PLQ_dB;
WF_XCQ_A1A2_dB = 10*log10(abs(fftshift(WF_XCQ_A1A2))) - Max_PLQ_dB;

%% Plotting
%% NO RFI
%%Magnitude
figure('name','The Magnitude of the Auto and Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,AC_A1_dB,'b-.');
plot(CH_Idx,AC_A2_dB,'r-.');
plot(CH_Idx,XC_A1A2_dB,'k-.');
plot(CH_Idx,ACQ_A1_dB,'c-.');
plot(CH_Idx,ACQ_A2_dB,'m-.');
plot(CH_Idx,XCQ_A1A2_dB,'g-.');
plot([NFCH_l,NFCH_l],[-Max_PL_dB-100,5],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-Max_PL_dB-100,5],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]);
Title = sprintf('The Magnitude of the Auto- and Cross-Correlation Spectra \n for Band %s : Frequency Slice %d \n UnQuantized and Quantized With NO RFI',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Magnitude - dB');
legend('UA1','UA2','UA1UA1','QA1','QA2','QA1QA2');

%%Phase
figure('name','The Phase Angle of the Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,angle(fftshift(XC_A1A2)),'k:');
plot(CH_Idx,angle(fftshift(XCQ_A1A2)),'g:');
plot([NFCH_l,NFCH_l],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); %ylim([-1.15E-2*pi,1.15E-2*pi]);
Title = sprintf('The Phase Angle of the Cross-Correlation Spectra \n for Band %s : Frequency Slice %d \n UnQuantized and Quantized With NO RFI ',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Angle - rad');
legend('UA1UA2','QA1QA2');

%% With RFI & Without Flagging
%%Magnitude
figure('name','The Magnitude of the Auto and Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,ACQ_A1_dB,'b-.');
plot(CH_Idx,ACQ_A2_dB,'r-.');
plot(CH_Idx,XCQ_A1A2_dB,'k-.');
plot(CH_Idx,WO_ACQ_A1_dB,'c-.');
plot(CH_Idx,WO_ACQ_A2_dB,'m-.');
plot(CH_Idx,WO_XCQ_A1A2_dB,'g-.');
plot([NFCH_l,NFCH_l],[-Max_PL_dB-80,5],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-Max_PL_dB-80,5],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]);
Title = sprintf('The Magnitude of the Auto- and Cross-Correlation Spectra \n for Band %s : Frequency Slice %d \n [Quantized & With Out RFI] and [Quantized & With RFI & With Out Flagging]',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Magnitude - dB');
legend('QA1','QA2','QA1QA1','QA1-WO','QA2-WO','QA1QA2-WO');

%%Phase
figure('name','The Phase Angle of the Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,angle(fftshift(XCQ_A1A2)),'k:');
plot(CH_Idx,angle(fftshift(WO_XCQ_A1A2)),'g:');
plot([NFCH_l,NFCH_l],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); %ylim([-1.15E-2*pi,1.15E-2*pi]);
Title = sprintf('The Phase Angle of the Cross-Correlation Spectra \n for Band %s : Frequency Slice %d \n [Quantized & With Out RFI] and [Quantized & With RFI & With Out Flagging]',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Angle - rad');
legend('QA1QA2','QA1QA2-WO');

%% With RFI & With Flagging
%%Magnitude
figure('name','The Magnitude of the Auto and Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,ACQ_A1_dB,'b-.');
plot(CH_Idx,ACQ_A2_dB,'r-.');
plot(CH_Idx,XCQ_A1A2_dB,'k-.');
plot(CH_Idx,WF_ACQ_A1_dB,'c-.');
plot(CH_Idx,WF_ACQ_A2_dB,'m-.');
plot(CH_Idx,WF_XCQ_A1A2_dB,'g-.');
plot([NFCH_l,NFCH_l],[-Max_PL_dB-80,5],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-Max_PL_dB-80,5],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]);
Title = sprintf('The Magnitude of the Auto- and Cross-Correlation Spectra \n for Band %s : Frequency Slice %d \n [Quantized & With Out RFI] and [Quantized & With RFI & With Flagging]',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Magnitude - dB');
legend('QA1','QA2','QA1QA1','QA1-WF','QA2-WF','QA1QA2-WF');

%%Phase
figure('name','The Phase Angle of the Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,angle(fftshift(XCQ_A1A2)),'k:');
plot(CH_Idx,angle(fftshift(WF_XCQ_A1A2)),'g:');
plot([NFCH_l,NFCH_l],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); %ylim([-1.15E-2*pi,1.15E-2*pi]);
Title = sprintf('The Phase Angle of the Cross-Correlation Spectra \n for Band %s : Frequency Slice %d \n [Quantized & With Out RFI] and [Quantized & With RFI & With Flagging]',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Angle - rad');
legend('QA1QA2','QA1QA2-WF');

%%Data Valid Count
Max_DVC = max([max(DVC_ICQ_A1),max(DVC_ICQ_A1)]);
figure('name','The Data Valid Counts for Auto and Cross Correlations - Normal Imaging');
hold on;
plot(CH_Idx,fftshift(DVC_ICQ_A1),'b-');
plot(CH_Idx,fftshift(DVC_ICQ_A2),'r--');
plot(CH_Idx,fftshift(DVC_ICQ_A1A2),'k-.');
plot([NFCH_l,NFCH_l],[-1,1.05*Max_DVC],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-1,1.05*Max_DVC],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); ylim([-1.15,1.15*Max_DVC]);
Title = sprintf('The Data valid Counts of the Auti and Cross -Correlation Spectra \n for Band %s : Frequency Slice %d \n Quantized & With RFI & With Flagging',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Angle - rad');
legend('QA1','QA2','QA1QA2');


%% Correlator Efficiency

%% NO RFI
Corr_Eff_BC = fftshift((abs(XCQ_A1A2)./sqrt(VXCQ_A1A2/NItt))./(abs(XC_A1A2)./sqrt(VXC_A1A2/NItt)));

figure('name','The Correlator Efficiency - Normal Imaging');
 
hold on;
plot(CH_Idx,Corr_Eff_BC*100,'k:');
plot([NFCH_l,NFCH_l],[0,115],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[0,115],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); ylim([90,102]);
title('The Correlator Efficiency - No RFI');
xlabel('Channel-Index'); ylabel('%');

%% With RFI & Without Flagging
Corr_Eff_WO = fftshift((abs(WO_XCQ_A1A2)./sqrt(WO_VXCQ_A1A2/NItt))./(abs(XC_A1A2)./sqrt(VXC_A1A2/NItt)));

figure('name','The Correlator Efficiency - Normal Imaging');
 
hold on;
plot(CH_Idx,Corr_Eff_WO*100,'k:');
plot([NFCH_l,NFCH_l],[0,115],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[0,115],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); ylim([90,102]);
title('The Correlator Efficiency - With RFI & Without Flagging');
xlabel('Channel-Index'); ylabel('%');

%% With RFI & With Flagging
Corr_Eff_WF = fftshift((abs(WF_XCQ_A1A2)./sqrt(WF_VXCQ_A1A2/NItt))./(abs(XC_A1A2)./sqrt(VXC_A1A2/NItt)));

figure('name','The Correlator Efficiency - Normal Imaging');
 
hold on;
plot(CH_Idx,Corr_Eff_WF*100,'k:');
plot([NFCH_l,NFCH_l],[0,115],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[0,115],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); ylim([90,102]);
title('The Correlator Efficiency - With RFI & With Flagging');
xlabel('Channel-Index'); ylabel('%');

% profile viewer;
% profile off;