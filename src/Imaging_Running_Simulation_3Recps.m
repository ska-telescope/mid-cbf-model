%This MATLAB script performs a 'Running Simulation' of a 'Realizable' Imaging 
%Process of the Mid.CBF with the Frequency Slice Approach. Here, the test 
%sequences with corresponding geometric propagation delays are generated 
%using an efficient implementation of 'Sum of Sinusoidal' method  along with 
%that the corresponding 'First Order Delay Polynomials - FODP'. For processing, 
%first, both Signal are channelized using the 'Coarse Channelizer (CoCh)'. 
%Then the delayed signals are Resampled using Fractional-Delay Filter-Banks 
%and Phase-Corrected with Phase-Rotators. The delay and phase aligned signals
%are then processed by the Imaging Channelizer producing 16,384 channels.
%The channel outputs are then cross-correlated and accumulated in order to
%demonstrate the recovered visibilities.
%Special case - 3 Element Phase Closure
 
%Custom MATLAB functions used
%(1) [SI_Vec,TVD_Vec,BD,FODSR,FOPSR] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d_Vec,Fs_Vec,DCSpecs) 
% : In order to Evaluate the Geometrical Delay and the First order Delay 
%Polynomials for the Duration of the Test Sequence
%Alternatively, use 
%[SI_Vec,TVD_Vec,BD,FODSR,FOPSR] = Arbitrary_Delay_Est(HDM,T_s,Ns,Fs_Vec,DSpecs)
%(2) [x, ~] = Rand_Seq_TVD_ChirpZ(Specs,Fs_Vec,ST_Vec,TVD_Vec) : In order to evaluate 
%the Reference and Delayed Signals
%(3) y = OS_Poly_DFT_FB_SC(x,BRI,OSFB_Config_para) : In order to model CC using an over-Sampled
%Polyphase DFT Filter-Bank
%%(4) [RSx1,V] = ReSampler_ERP_Corr(CCx,F_Rat,SI1_CC,SI0_CC,FODSR,FOPSR,ReSamp_Config_para); : In order to Calculate
%the Delay and Phase Corrections to correct the delay and phase with respect 
%to the Delay-Center at BoresightIn and apply those corrections
%(6) yCSPDFB = CS_Poly_DFT_FB_Batch(x,CSFB_Config_para) : In order to model the Imaging
%Channelizer using a Critically-Sampled Polyphase DFT Filter-Bank

%Note that for SKA1 Band 3, the signal is sampled at ~3.168 Gsps. Hence, in
%Mid.CBF, those signals are upsampled to ~3.96 Gsps at the input using an
%5/4 Up-Sampler.

% By Thushara Kanchana Gunaratne - RCO National Research Council Canada
% Original Release Date : 2018-08-10
% Modified 2019-01-02 > To add arbitrary delay model and add a validity flag
%for the data

close all; clear; clc;
 
%profile on;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation Specs
%Number of Samples per Input Sample-Block 
% Note - Set this accordingly depending on the Memory Resources available 
%on the Simulation Computer
Ns_O = 2^24; %

%SKA1 Band 
Band_ID = 4; % Select from [1,2,3,4,5a = 5, 5b = 6];
%The Selected 'Frequency-Slice'(FS) for the Subsequent Processing
%Band 1, 2 & 3 = 0-9 | Band 4, 5a & 5b = 0-14
FS_ID = 5;

%Additional Frequency Down-Shift at the DISH 
if (Band_ID == 5) || (Band_ID == 6)  
    %For Band 5a - from 0 to 1.45 GHz
    %For Band 5b - from 0 to 6.5 GHz
    Ad_B5_Fsft = 1E9;
end %IF

%Wideband Frequency Shift (i.e. to propaly set the Zoom Windows)
WB_Fsft = 1*10E6; %In Hz
%The Frequency Offset desired in shifting Fine Imaging Channels
F_off = 1*13440/2; %In Hz

%Simulation Start Time Offset
T_si = 0.0299; % In sec
%Simulation Duration
T_Sim =  1*0.0010; %In sec
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Antenna Positions | The Distance vector in Cartesian Coordinates with 
%respect to the Array Reference Point
%For Reference Antenna-#0
% Dist_0 = 0; %In meters - In SKA1 Max Baseline is 160 kms
% Theta_0 = 0; %In Degrees
% Phi_0 = 0; %In Degrees 
 
%For the Antenna-#1
Dist_1 = 1*160E3; %In meters - In SKA1 Max Baseline is 160 kms
Theta_1 = 10; %In Degrees
Phi_1 = 150; %In Degrees

%Offset Index with SCFO sampling for Antenna-#1
FO_Idx1 = 1*1000;  

%%%%%%%%%%%%%%%%%%%
%For the Antenna-#2
Dist_2 = 1*60E3; %In meters - In SKA1 Max Baseline is 160 kms
Theta_2 = 45; %In Degrees
Phi_2 = -130; %In Degrees

%Offset Index with SCFO sampling for Antenna-#2
FO_Idx2 = 1*1001;

%%%%%%%%%%%%%%%%%%%
%For the Antenna-#3
Dist_3 = 1*10E3; %In meters - In SKA1 Max Baseline is 160 kms
Theta_3 = 75; %In Degrees
Phi_3 = 0; %In Degrees

%Offset Index with SCFO sampling for Antenna-#2
FO_Idx3 = 1*1002;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Position of the Delay Center on the Sky at the Start of the Observation             
%Specified by the Declination and the Right-Assertion Angles of the Delay 
%Center at the 1PPS  
DCen.DecA0 = 00; %IN Degrees
DCen.RAsc0 = 60; %IN Degrees

%Assigning the Declanation and the Right-Assetion Angles of the Source 
%Position at the 1PPS  
SPos.DecA0 = DCen.DecA0+00.000; %IN Degrees
SPos.RAsc0 = DCen.RAsc0+00.000; %IN Degrees

%% Sample Quantizing
%Data
%Input Sample Bit-Width
if (Band_ID == 1) || (Band_ID == 2) || (Band_ID == 3)
    W_In = 8;
    IP_RMS_to_Peak_Ratio = 5;
else
    W_In = 4;
    IP_RMS_to_Peak_Ratio = 2.5;
end %IF 
W_US = 8; %OutputUp-Sampler Bit-Width | Only for Band 3
W_CC = 16; %Output of the CC Bit-Width
W_RS = 16; %Output of the ReSampler Bit-Width
W_IC = 9; %Output of the IC Bit-Width

%Coefficient
Wc = 19; %Coefficient Word length

%Scaling Factors
US_RMS_to_Peak_Ratio = 5; %Only for  3
CC_RMS_to_Peak_Ratio = 5;
RS_RMS_to_Peak_Ratio = 5;
IC_RMS_to_Peak_Ratio = 5; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
% NOTE - NOT Reccomended to Change Parameters Beyond this Point           %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Creating a Folder for Saving the Configuration parameters and Simulation results
Data_Time_Now = clock;
FolderName = sprintf('SKA1_Mid_CBF_Normal_Imaging_Realizable_Sim_%d%02d%02d_%02d%02d',Data_Time_Now(1:5));
mkdir(FolderName);
save(fullfile(FolderName,'Configuration_Parameters.mat'));

%% Band Specification
Band_Name_List = {'1','2','3','4','5A','5B'};

switch Band_ID
    case 1
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 1 spans 0.35 - 1.05 GHz
        Fl = 0.35E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.05E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 0E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 1; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 2
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 2 spans 0.95 - 1.76 GHz
        Fl = 0.95E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.76E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 0E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 3.96E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 1; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 3
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 3 spans 1.65 - 3.05 GHz
        Fl = 0.05E3; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 1.45E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 1.6E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 2; %Nyquist Zone of Sampling
        F0 = (13440*2^14*18); %The Nominal Sample Clock Frequency - In Hz
        F_Base = F0; % 3.96E9; %  %The Base Sampling Frequency with SCFO Sampling - in Hz |
        FSF = 1; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat'); %The CC-OSPPFB
        Fs_Rat = 4/5; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 4
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 4 spans 2.8 - 5.18 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 2.05E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 8/3; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 5
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 5a spans 4.6 - 8.5 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 4.5E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling - NOTE even if Band 5a is 
        %sampled in Nyquist Zone 2, the proposed frequency shift in extracting 
        %the observation band make it as if it was sampled in Nyquist Zone 1
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling 
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 3/2; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
    case 6
        %Spectral Occupancy of the Test Signal
        %SKA1 Band 5b spans 8.3 - 15.3 GHz
        Fl = 0.25E9; %% The Lower-Limit Frequency in the Base-Band - in Hz  |
        Fu = 2.75E9; %% The Upper-Limit Frequency in the Base-Band - in Hz |
        F_DSft = 9.1E9; % The Net Frequency Down-Shift to the Base-Band - in Hz |
        Nqst_Zone = 1; %Nyquist Zone of Sampling - - NOTE even if Band 5b is 
        %sampled in Nyquist Zone 2, the proposed frequency shift in extracting 
        %the observation band make it as if it was sampled in Nyquist Zone 1
        F0 = (13440*2^14*27); %The Nominal Sample Clock Frequency - In Hz
        F_Base = 5.94E9; %The Base Sampling Frequency with SCFO Sampling
        FSF = 3/2; %The Frequency Scaling Factor for the Offset
        OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat'); %The CC-OSPPFB
        Fs_Rat = 8/3; %The ratio between the Original Sample Rate and the Sample rate at the Bulk-Delay Correction at Mid.CBF
end %SWITCH

%% Sampling Specifications
%NOTE Mid.CBF Employs Sample Clock Frequency Offset (SCFO) Scheme
%Offset Sample Frequency 
DeltaF = 1.8E3; %Offset Resolution - In Hz
 
%Actual Sampling Frequency with SCFO Sampling- In Hz
F1 = F_Base + (FSF*DeltaF*FO_Idx1); % For Antenna #1 : In Hz
F2 = F_Base + (FSF*DeltaF*FO_Idx2); % For Antenna #2 : In Hz
F3 = F_Base + (FSF*DeltaF*FO_Idx3); % For Antenna #3 : In Hz

 %% The Parmaters for Partial Integer Delay Correction with DDR4 Block Shifts
%Coarse Delay Update Period 
BDUDP = 10; % In Sec
%DDR4 Block Size
DDR4BS = 54; % In Samples

%% Specifying the Channelizer Specs for the Band 3 UpSampler
%% Custom MATLAB function 'B3_DUC' is used to model UpSampler

if Band_ID == 3
    
    %The Selectable Filters
    B3_UPS = load('US_Prototype_FIR_OS_16_15_L4_M5.mat');
    h0 = B3_UPS.h;
    L0 = B3_UPS.L;
    M0 = B3_UPS.M;
    clear B3_UPS
    
    %The Up-Sampling Factor
    USF = M0/L0;
    %Note Fs_Rat = 1/USF
    
    Max_h0 = max(abs(h0));
    
    %The Length of the Prototype Filter
    Nh0 = length(h0);
    %Integer-Sample Delay through the UpSampler
    Insft_US_OP = ceil(-(Nh0-0)/2/L0); %In Samples    
    
    %Configuration Parameters of the UpSampler for Band 3
    %UnQuantized
    DUC_Config_para.h = h0;
    DUC_Config_para.L = L0;
    DUC_Config_para.M = M0;
    
    %Quantized
    DUC_Config_para_Q.h = Max_h0*double(sfi(h0./Max_h0,Wc,Wc-1));
    DUC_Config_para_Q.L = L0;
    DUC_Config_para_Q.M = M0;
end

%% Specifying the Channelizer Specs for the Coarse-Channelizer (CoCh)
%% Custom MATLAB function 'OS_Poly_DFT_FB_SC' is used to model the CoCh
%The Prototype FIR Filter for the CoCh
%NOTE This is an Oversampled Polyphase DFT FB
 
%Band Specific CC - Selected above
%OSProtoFltSpecs = load('OS_Prototype_FIR_CH20.mat');
%OSProtoFltSpecs = load('OS_Prototype_FIR_CH30.mat');
 
h1 = OSProtoFltSpecs.h;
Num1 = OSProtoFltSpecs.Num;
Den1 = OSProtoFltSpecs.Den;
Nc1 = OSProtoFltSpecs.Nc;
 
%The Length of the Prototype Filter
Nh1 = length(h1);
%The Oversampling factor
Os1 = Num1/Den1;
%Input Sample Length
M1 = Nc1/Os1;
%Maximum Number of Taps per Polyphase Arm
Nt1 = ceil(Nh1/Nc1);
 
%Shift the Output to compensate the Delay-Through the Polyphase Filter-Bank
%Integer-Sample Delay
Insft_CC_OP = ceil(-(Nh1-1)/2/M1); %In Samples
%The Residual Fractional Delay
Frsft_CC_OP = Insft_CC_OP + (Nh1-1)/2/M1; %In Samples
 
%Initiating Temp Matrices
h1_2D = zeros(Nc1,Nt1);
%Filling the 2D Filter Mask
for k1 = 1 : Nc1
    L1 = length(h1(k1:Nc1:end));
    h1_2D(k1,1:L1) = h1(k1:Nc1:end);
end %FOR k
clear k1 L1;
Max_h1_2D = max(max(h1_2D));
 
%Generating Twiddle-Factors
%NOTE - Depends on the Selected Coarse Channel
TWD2D_SC = exp(2j*pi/Nc1*FS_ID*(0:1:Nc1-1).')*exp(2j*pi*FS_ID/Num1*(0:1:Num1-1));
 
%Sample rates out of the CoCh
FCC0 = F0/M1; FCC1 = F1/M1; FCC2 = F2/M1; FCC3 = F3/M1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Configuration Parameters for the Over-Sampling Polyphase DFT Filter-Bank
%UnQuantized
OSFB_Config_para.Nc = Nc1;
OSFB_Config_para.Num = Num1;
OSFB_Config_para.Den = Den1;
OSFB_Config_para.h2D = sqrt(Nc1)*h1_2D;
OSFB_Config_para.TWD2D_SC = TWD2D_SC;
OSFB_Config_para.N_Cons = ceil(Nh1/M1); %Note this is the Maximum propagation
%time in the filter interms of Output Samples

%Quantized
OSFB_Config_para_Q.Nc = Nc1;
OSFB_Config_para_Q.Num = Num1;
OSFB_Config_para_Q.Den = Den1;
OSFB_Config_para_Q.h2D = sqrt(Nc1)*Max_h1_2D*double(sfi(h1_2D./Max_h1_2D,Wc,Wc-1));
OSFB_Config_para_Q.TWD2D_SC = double(sfi(TWD2D_SC,Wc,Wc-1));
OSFB_Config_para_Q.N_Cons = ceil(Nh1/M1); %Note this is the Maximum propagation
%time in the filter interms of Output Samples
 
%% Specifying the Parameters for the Imaging Channelizer (IC)
%% Custom MATLAB function 'CS_Poly_DFT_FB_Batch' is used to model the IC
%The Prototype FIR Filter for the Imaging Channelizer
%NOTE This is a Critically-Sampled Polyphase DFT FB
CSProtoFltSpecs = load('CS_Prototype_FIR_CH16384.mat');
h2 = CSProtoFltSpecs.h;
Nc2 = CSProtoFltSpecs.Nc;
Os2 = 1; %The Imaging Channelizer is Critically Sampled -> Os2 = 1
%The Length of the Prototype Filter
Nh2 = length(h2);

%Shift the Output to compensate the Delay-Through the Polyphase Filter-Bank
%Integer-Sample Delay
Insft_IC_OP = round(-Nh2/2/Nc2); %In Samples
%NOTE Idealy the Integer Delay is round(-(Nh2-1)/2/Nc2). However, the
%filter coefficients are arrnaged to get the required 
%The Residual Fractional Delay
Frsft_IC_OP = Insft_IC_OP + Nh2/2/Nc2; %In Samples | Ideally Should be 0
 
%Maximum Number of Taps per Polyphase Arm
Nt2 = ceil(Nh2/Nc2);
%The 2D Coefficient-Mask
h2_2D = zeros(Nc2,Nt2);
%Filling the 2D Filter Mask
for k2 = 1 : Nc2
    L2 = length(h2(k2:Nc2:end));
    h2_2D(k2,1:L2) = h2(k2:Nc2:end);
end %FOR k2
clear k2 L2;
Max_h2_2D = max(max(h2_2D));

%Sample rates out of the Imaging Channelizer
F_IC = FCC0/Nc2*Os2;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Configuration Parameters for the Criticalled-Sampled Polyphase DFT 
%Filter-Bank
%UnQuantized
CSFB_Config_para.Nc = Nc2;
CSFB_Config_para.h2D = h2_2D;
CSFB_Config_para.Nh = Nh2;

%Quantized
CSFB_Config_para_Q.Nc = Nc2;
CSFB_Config_para_Q.h2D = Max_h2_2D*double(sfi(h2_2D/Max_h2_2D,Wc,Wc-1));
CSFB_Config_para_Q.Nh = Nh2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parameters for Calculating the Frequency Shift to Align Subsequant Channels across Coarse 
%Calculating the Frequency-Shift 'FA_Sft' for aligning subsequency 
%channels across CCs
%Procedure
% Step-1 : Define C2BW = (FCC0/Nc2/Os2)
% Step-2 : Find nk such that | F_o + C2BW*nk - CC_CF0 | < 0.5*C2BW
% Step-3 : Evalaute F_ST = F_o + C2BW*nk - CC_CF0
CC_CF0 = F0/Nc1*FS_ID;
C2BW = (F0/M1/Nc2/Os2); %Channel Separation in Subsequant Channelizer
nk = floor((CC_CF0+0.5*C2BW)/C2BW);
FA_Sft = F_off + C2BW*nk - CC_CF0; % In Hz
%FA_Sft = FA_Sft - FA_Sft;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Geometric Delay Model Specifications
%The Order of the Higher Order Polynomials that is Converted Internally
N_Poly = 6;
%The Duration of Validity for the first Order Delay Polynomial 
Tv = 10E-3; % In sec
 
%Sample Delay Specification
%The Speed of EM Wave Propagation
c = 299792458; % In m/s
 
%Earth's Rotation Angular Velocity
%NOTE - ACTUAL Value is We = 7.2921150E-5 | Increased to Study Non Sidereal
%Tracking. However, Mid.CBF is required to track delay centers that are
%moving at the rate of 20 times the sidereal rate.
We = 7.2921150E-5; % In s^-1 - Sidereal
%We = 2E1*7.2921150E-5; % In s^-1 - 20 Times Sidereal
 
%Assigning Geometrical Delay Specs to be used for the function
%Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d1_Vec,F1,DSpecs)
DCSpecs.Tv = Tv; %Tv - The Valid Time for the First Order Delay Model
if Band_ID == 3 %F0 - The Nominal Sampling Frequency
    DCSpecs.F0 = F0/USF; % In Hz /USF
else
    DCSpecs.F0 = F0; % In Hz
end
DCSpecs.Nqst_Zone = Nqst_Zone; %Nqst_Zone - The Nyquist Zone of Sampling
DCSpecs.N_Poly = N_Poly; %N_Poly - The Order of the Higher Order Polynomial 
DCSpecs.c = c; %c - The Speed of Light
DCSpecs.We = We; % We - The Angular Velocity of the Earth
DCSpecs.N_DPE = 1E2; %Number of Sample Points Used in Estimating the First Order Model
DCSpecs.BDUDP = BDUDP; % BDUDP - Coarse Delay Update Period - In Sec
DCSpecs.DDR4BS = DDR4BS; % DDR4BS - DDR4 Block Size - In Samples
DCSpecs.N_BDE = 1E2; %Number of Sample Points Used in Estimating the Block Delay
%The Net Frequency Down-Shift of the Band before Bulk Delay correction
DCSpecs.F_DSft_BBD = F_DSft ; % In Hz
%The Net Frequency Down-Shift of the Band after Bulk Delay correction
DCSpecs.F_DSft_ABD = WB_Fsft; % In Hz
DCSpecs.Nc1 = Nc1; %The Number of Channels in the Coarse Channelizer
DCSpecs.Os1 = Os1; %The Over-Sampling Factor in the Coarse Channelizer
DCSpecs.FS_ID = FS_ID; %The Index of the Selected Coase Frequency Slice
DCSpecs.FA_Sft = FA_Sft; %An Additiona Frequency Shift to be Performed 
%The ratio between the Original Sample Rate and the Sample rate at the Input to the Mid.CBF
if Band_ID == 3
    DCSpecs.USF = USF; %The Upsample factor between input data rate and the data rate at Bulk Delay is corrected
else
    DCSpecs.USF = 1;
end
%The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
DCSpecs.Fs_Rat = Fs_Rat;

%The Position Vectors of the Antennas
%The Distance vector in Cartesian Coordinates
%For the Reference Antenna A0
d0_Vec = [0;0;0]; %Note the d0_Vec is by default at the Array Center

%For the Antenna A1
d1_Vec = Dist_1*[cosd(Theta_1).*cosd(Phi_1);...
                 cosd(Theta_1).*sind(Phi_1);...
                 sind(Theta_1)];
             
%For the Antenna A2
d2_Vec = Dist_2*[cosd(Theta_2).*cosd(Phi_2);...
                 cosd(Theta_2).*sind(Phi_2);...
                 sind(Theta_2)];
             
%For the Antenna A3
d3_Vec = Dist_3*[cosd(Theta_3).*cosd(Phi_3);...
                 cosd(Theta_3).*sind(Phi_3);...
                 sind(Theta_3)];             
 
%% Specifying the Signal Specs for the Custom MATLAB function  
%% 'Rand_Seq_TVD_ChirpZ' that generate the Sequences for A0 & A1
%Signal Specs
%Signal Specs
if Band_ID == 3
    SSpecs.F0 = F0/USF; % Nominal Sample Rate - In Hz 
    SSpecs.Fs_Rat = 1; % ie Fs_Rat*USF %The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
else
    SSpecs.F0 = F0; % Nominal Sample Rate - In Hz
    SSpecs.Fs_Rat = Fs_Rat; %The ratio between the Digitizer Sample Rate and the Sample rate at the Input to the Mid.CBF
end %IF
SSpecs.Fl = Fl; % In Hz
SSpecs.Fu = Fu; % In Hz
SSpecs.F_Dsft = F_DSft; % In Hz; 
SSpecs.Nqst_Zone = Nqst_Zone; %Either 1 or 2
SSpecs.dError = 1E-5; % In Relative Units
 
%% Configuring the Delay | Phase Synthesizer and the ReSampler
%Estimating the Time Varying Delay and Phase Corrections and Correction for 
%the Doppler Shift using a First-Order Delay Polynomial. Then these
%estimates are used to determine the Required Integer Delay Correction, the
%Index of the Fractional-Delay Filter and the Index of the Sine-Cosine LUT
%for Phase Correction
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Selecting the Fractional-Delay Filter-Bank
 
%Coefficient Sets for the Interpolation Filter
%512 Delay-Step
%FractFltBankSpecs = load('Fract_Dly_FB_CVX_DS_512.mat');

%1024 Delay-Step
FractFltBankSpecs = load('Fract_Dly_FB_CVX_DS_1024.mat');

hFD_FB = (FractFltBankSpecs.hFD_FB).';
Max_hFD_FB = max(max(hFD_FB));
% Os1 = FractFltBankSpecs.Os; % Redundent
% DStep = FractFltBankSpecs.DSteps; % Redundent
Nh_FD = FractFltBankSpecs.N_FD;
clear FractFltBankSpecs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The Phase Correction factors with 'N_SC_LUT' stored Coefficients in a LUT
N_SC_LUT = 2^18;
SC_LUT = exp(2j*pi*(0:1:N_SC_LUT-1).'/N_SC_LUT);
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Configuering the ReSampler_ERP_Corr
%UnQuantized
ReSamp_Config_para.F0 = FCC0;
ReSamp_Config_para.hFD_FB = hFD_FB;
ReSamp_Config_para.SC_LUT = SC_LUT;

%Quantized
ReSamp_Config_para_Q.F0 = FCC0;
ReSamp_Config_para_Q.hFD_FB = Max_hFD_FB*double(sfi(hFD_FB/Max_hFD_FB,Wc,Wc-1));
ReSamp_Config_para_Q.SC_LUT = double(sfi(SC_LUT,Wc,Wc-1));

%% Specifying Parameters for Wideband Frequency-Shifter
%UnQuantized
WBFS_Specs.N_SC_LUT = N_SC_LUT;
WBFS_Specs.SC_LUT = SC_LUT;
%UnQuantized
WBFS_Specs_Q.N_SC_LUT = N_SC_LUT;
WBFS_Specs_Q.SC_LUT = double(sfi(SC_LUT,Wc,Wc-1));

%% Configuring the CoCh Ripple Correction
%[GC_Vec] = Coch_MagResp_Correction(Fs_CC_Vec,GCCorrSpecs);
GCCorrSpecs.h1 = h1; %The coefficients of the Coch
GCCorrSpecs.M1 = M1; %The Down Sampling Factor of the Coch
GCCorrSpecs.Os1 = Os1; %The Over-Sampling Sampling Factor of the Coch
GCCorrSpecs.Nc2 = Nc2; %The Number of Channels in the Subsequent Channelizer or FFT
GCCorrSpecs.F0 = FCC0; %The Nominal Sample Rate
GCCorrSpecs.FS_ID = FS_ID; %The Index of the Selected Frequency Slice
GCCorrSpecs.FA_Sft = FA_Sft; %The Frequency Shift applied to Align the Subsequent Channels
GCCorrSpecs.N_Edge = 100; %Number of Channels Corrected byond the Processing Edge 

%Evalauting the Gain Corrections
[GC_Vec] = CoCh_MagResp_Correction([F1,F2,F3]/M1,GCCorrSpecs); 
  
%% Some Other Parameters
%Number of Flags
N_Flg = 1;

%The Optimal Number of Samples in Wideband that fascilitate the 
Ns_N = round(Ns_O*F_IC/F0)*M1*Nc2;
N_OV = ceil((Nh1+M1*(Nh2+Nh_FD))*F_IC/F0); % In Samples - The Number of Samples at the Output of the Imaging Channelizer that needs to be Overlapped

if Band_ID == 3
    N_CC_OP = ceil(Ns_N*USF/M1); %The Number of Output Frames from CoCh
    T_AOV = -USF*Nh0/F0; % In sec - Additional Overlap for Band 3
else    
    N_CC_OP = ceil(Ns_N/M1); %The Number of Output Frames from CoCh
    T_AOV = 0; % In sec - Additional Overlap for Other bands
end% In sec
 
%The Number of Output Frames from Imaging Channelizer
N_IC_OP = ceil(N_CC_OP/Nc2);

%Number of iterations required to cover the Simulation Duration
NItt = ceil(T_Sim*F_IC./(N_IC_OP-N_OV));
%NItt = 1; %NOTE to run just one iteration

%Updating the Start time to Concide with a Imaging Channelizer Sample Time
T_si_IC = round(T_si*F_IC)/F_IC;

%Initialization the Auto and Cross-Correlation Accumulations Bins
%UnQuantized
AC_A1 = zeros(1,Nc2);
AC_A2 = zeros(1,Nc2);
AC_A3 = zeros(1,Nc2);

XC_A1A2 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
XC_A2A3 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
XC_A3A1 = zeros(1,Nc2) + 1j*zeros(1,Nc2);

%Quantized
ACQ_A1 = zeros(1,Nc2);
ACQ_A2 = zeros(1,Nc2);
ACQ_A3 = zeros(1,Nc2);

XCQ_A1A2 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
XCQ_A2A3 = zeros(1,Nc2) + 1j*zeros(1,Nc2);
XCQ_A3A1 = zeros(1,Nc2) + 1j*zeros(1,Nc2);

%% Starting the Iterations
fprintf('Running Simulations for Band %s - Frequency Slice %d\n\n',Band_Name_List{Band_ID},FS_ID);

for nItt = 1 : NItt
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('Running the Iteration %d of %d\n',nItt,NItt);
    fprintf('Started on %d-%02d-%02d || %2d:%2d:%2.2f\n',clock);

    %Re-Afirming the Number of Samples
    Ns = Ns_N; %As Band 3 is ReSampled
    
    %% Start Time at the Reference Antenna for nItt-th Iteration
    T_s = T_si_IC + ((nItt-1)*(N_IC_OP-N_OV) - (N_OV+Insft_IC_OP))/F_IC - T_AOV; % In sec
    
    %The Sample Index at the Reference
    if Band_ID == 3
        SI0_BU_Sec_Start = floor(T_s);
        SI0_BU_Temp = transpose(int64((round((T_s-SI0_BU_Sec_Start)*F0/USF) + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_BU_Sec_Idx = int16(floor(double(SI0_BU_Temp)*USF./F0) +  SI0_BU_Sec_Start);
        SI0_BU_SS_Idx = mod(SI0_BU_Temp,F0/USF);
        %Packaging into a cell array
        SI0_BU = {SI0_BU_Sec_Idx,SI0_BU_SS_Idx};
        clear SI0_BU_Sec_Start SI0_BU_Temp SI0_BU_Sec_Idx SI0_BU_SS_Idx;
    else
        SI0_Sec_Start = floor(T_s);
        SI0_Temp = transpose(int64((round((T_s-SI0_Sec_Start)*F0) + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_Sec_Idx = int16(floor(double(SI0_Temp)./F0) +  SI0_Sec_Start);
        SI0_SS_Idx = mod(SI0_Temp,F0);
        %Packaging into a cell array
        SI0 = {SI0_Sec_Idx,SI0_SS_Idx};
        clear SI0_Sec_Start SI0_Temp SI0_Sec_Idx SI0_SS_Idx;
    end % IF
    
    %Using the Custom Function 'Geomatrical_Delay_Est'
    fprintf('Generating Geometric Delays & Corrections...')
    tic;
    if Band_ID == 3
        [SI1_BU,TVD1,BD1,FODSR1,FOPSR1] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d1_Vec,F1/USF,DCSpecs);
        [SI2_BU,TVD2,BD2,FODSR2,FOPSR2] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d2_Vec,F2/USF,DCSpecs);        
        [SI3_BU,TVD3,BD3,FODSR3,FOPSR3] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d3_Vec,F3/USF,DCSpecs);        
    else
        [SI1,TVD1,BD1,FODSR1,FOPSR1] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d1_Vec,F1,DCSpecs);
        [SI2,TVD2,BD2,FODSR2,FOPSR2] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d2_Vec,F2,DCSpecs);        
        [SI3,TVD3,BD3,FODSR3,FOPSR3] = Geomatrical_Delay_Est(DCen,SPos,T_s,Ns,d3_Vec,F3,DCSpecs);        
    end
    toc;
    
    %% Specifying Sample Time Vectors
    if Band_ID == 3
        %Combine the
        ST_Vec = [double(SI1_BU{1})+double(SI1_BU{2})*USF/F1,...
                  double(SI2_BU{1})+double(SI2_BU{2})*USF/F2,...
                  double(SI3_BU{1})+double(SI3_BU{2})*USF/F3];
    else
        ST_Vec = [double(SI1{1})+double(SI1{2})/F1,...
                  double(SI2{1})+double(SI2{2})/F2,...
                  double(SI3{1})+double(SI3{2})/F3];
    end %IF
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Using the function 'Rand_Seq_TVD_ChirpZ'
    fprintf('Generating Random Test Sequences...')
    tic;
    if Band_ID == 3
        [xBU_Vec,VBU_Vec, ~] = Rand_Seq_TVD_ChirpZ(SSpecs,[F1,F2,F3]/USF,ST_Vec,[TVD1,TVD2,TVD3]);
        %Re-Scaling the Signal to Get Unity variance
        RMS_x_Vec = rms(xBU_Vec,1); %Evaluating the Root Mean Square of Colum Vectors (Along Dimension '1')
        xBU_Vec  = xBU_Vec./RMS_x_Vec;        
    else
        [x_Vec,V_Vec, ~] = Rand_Seq_TVD_ChirpZ(SSpecs,[F1,F2,F3],ST_Vec,[TVD1,TVD2,TVD3]);
        %Re-Scaling the Signal to Get Unity variance
        RMS_x_Vec = rms(x_Vec,1); %Evaluating the Root Mean Square of Colum Vectors (Along Dimension '1')
        x_Vec  = x_Vec./RMS_x_Vec;        
    end
    
    %Quantising        
    if nItt == 1 %Use the Same Scaling for all iterations
        if Band_ID == 3            
            IP_RMS = rms(xBU_Vec,1)*IP_RMS_to_Peak_Ratio;            
        else
            IP_RMS = rms(x_Vec,1)*IP_RMS_to_Peak_Ratio;
        end
    end
    if Band_ID == 3  
        xBUQ_Vec = IP_RMS.*double(sfi(xBU_Vec./IP_RMS,W_In,W_In-1));
    else
        xQ_Vec = IP_RMS.*double(sfi(x_Vec./IP_RMS,W_In,W_In-1));
    end
    toc;
    
    %% Assigning Flags as a Cell Vector
    if Band_ID == 3
        %Note there are two Streams
        Flg_BU_Vec = cell(1,3);
        Flg_BU_Vec{1} = cell(1,1);  Flg_BU_Vec{1}{1} = VBU_Vec(:,1); 
        Flg_BU_Vec{2} = cell(1,1);  Flg_BU_Vec{2}{1} = VBU_Vec(:,2); 
        Flg_BU_Vec{3} = cell(1,1);  Flg_BU_Vec{3}{1} = VBU_Vec(:,3);        
    else
        %Note there are two Streams
        Flg_Vec = cell(1,3); 
        Flg_Vec{1} = cell(1,1); Flg_Vec{1}{1} = V_Vec(:,1); 
        Flg_Vec{2} = cell(1,1); Flg_Vec{2}{1} = V_Vec(:,2); 
        Flg_Vec{3} = cell(1,1); Flg_Vec{3}{1} = V_Vec(:,3);    
    end %IF
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Upsampling in the Band 3
    if Band_ID == 3
        fprintf('Up-Sampling the input Sequence in Band 3 ...');
        tic;
        
        %The Number of Samples after up-Sampling
        Ns = ceil(Ns*USF);
        
        %UnQuantized
        %Initializing the Outputs
        x_Vec = zeros(Ns,3);
        Flg_Vec = cell(1,3);        
        %Up-Sampling with B3_DUC
        [x_Vec(:,1),Flg_Vec{1}] = B3_DUC(xBU_Vec(:,1),Flg_BU_Vec{1},SI1_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,1))));
        [x_Vec(:,2),Flg_Vec{2}] = B3_DUC(xBU_Vec(:,2),Flg_BU_Vec{2},SI2_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,2))));
        [x_Vec(:,3),Flg_Vec{3}] = B3_DUC(xBU_Vec(:,3),Flg_BU_Vec{3},SI3_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,3))));
        clear xBU_Vec;
        
        %Correcting the Integer Sample Delay due to the Up-Sampler
        x_Vec(:,1) = circshift(x_Vec(:,1),[Insft_US_OP,0]);
        x_Vec(:,2) = circshift(x_Vec(:,2),[Insft_US_OP,0]);
        x_Vec(:,3) = circshift(x_Vec(:,3),[Insft_US_OP,0]);
        for n_flg = 1 : N_Flg
            Flg_Vec{1}{n_flg} = circshift(Flg_Vec{1}{n_flg},[Insft_US_OP,0]);
            Flg_Vec{2}{n_flg} = circshift(Flg_Vec{2}{n_flg},[Insft_US_OP,0]);
            Flg_Vec{3}{n_flg} = circshift(Flg_Vec{3}{n_flg},[Insft_US_OP,0]);
        end % FOR n_flg
                
        %Quantized
        %Initializing the Outputs
        xQ_Vec = zeros(Ns,3);
        %Up-Sampling with B3_DUC
        [xQ_Vec(:,1),~] = B3_DUC(xBUQ_Vec(:,1),Flg_BU_Vec{1},SI1_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,1))));
        [xQ_Vec(:,2),~] = B3_DUC(xBUQ_Vec(:,2),Flg_BU_Vec{2},SI2_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,2))));
        [xQ_Vec(:,3),~] = B3_DUC(xBUQ_Vec(:,3),Flg_BU_Vec{3},SI3_BU{2}(1),DUC_Config_para); % figure; plot(abs(fft(sBU_Vec(:,2))));
        clear xBUQ_Vec;

        %Correcting the Integer Sample Delay due to the Up-Sampler
        xQ_Vec(:,1) = circshift(xQ_Vec(:,1),[Insft_US_OP,0]);
        xQ_Vec(:,2) = circshift(xQ_Vec(:,2),[Insft_US_OP,0]);       
        xQ_Vec(:,3) = circshift(xQ_Vec(:,3),[Insft_US_OP,0]);       
                        
        %%Re-Quantizing at the Output of the Band 3 Up-Sampler
        if nItt == 1
            B3_US_OP_RMS = [rms(x_Vec(Flg_Vec{1}{1},1)),rms(x_Vec(Flg_Vec{2}{1},2)),rms(x_Vec(Flg_Vec{3}{1},2))]*US_RMS_to_Peak_Ratio;
        end
        xQ_Vec(:,1) = B3_US_OP_RMS(1)*double(sfi(xQ_Vec(:,1)/B3_US_OP_RMS(1),W_US,W_US-1));
        xQ_Vec(:,2) = B3_US_OP_RMS(2)*double(sfi(xQ_Vec(:,2)/B3_US_OP_RMS(2),W_US,W_US-1));
        xQ_Vec(:,3) = B3_US_OP_RMS(3)*double(sfi(xQ_Vec(:,3)/B3_US_OP_RMS(3),W_US,W_US-1));
        
        %Redefining the Sample Index Vector to Match
        %For the Reference Antenna
        SI0_Temp = transpose(int64((floor(double(SI0_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI0_Sec_Idx = int16(floor(double(SI0_Temp)./F0)) +  SI0_BU{1}(1);
        SI0_SS_Idx = mod(SI0_Temp,F0);
        %Packaging into a cell array
        SI0 = {SI0_Sec_Idx,SI0_SS_Idx};
        clear SI0_Temp SI0_Sec_Idx SI0_SS_Idx;
        
        %For the Antenna #1
        SI1_Temp = transpose(int64((floor(double(SI1_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI1_Sec_Idx = int16(floor(double(SI1_Temp)./F1)) +  SI1_BU{1}(1);
        SI1_SS_Idx = mod(SI1_Temp,F1);
        %Packaging into a cell array
        SI1 = {SI1_Sec_Idx,SI1_SS_Idx};
        clear SI1_Temp SI1_Sec_Idx SI1_SS_Idx
        
        %For the Antenna #2
        SI2_Temp = transpose(int64((floor(double(SI2_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI2_Sec_Idx = int16(floor(double(SI2_Temp)./F2)) +  SI2_BU{1}(1);
        SI2_SS_Idx = mod(SI2_Temp,F2);
        %Packaging into a cell array
        SI2 = {SI2_Sec_Idx,SI2_SS_Idx};
        clear SI2_Temp SI2_Sec_Idx SI2_SS_Idx
                
        %For the Antenna #3
        SI3_Temp = transpose(int64((floor(double(SI3_BU{2}(1))/L0)*M0 + (0 : 1 : Ns-1))));
        %Evalauting Second and Sub-Second (SS) Indecies
        SI3_Sec_Idx = int16(floor(double(SI3_Temp)./F3)) +  SI3_BU{1}(1);
        SI3_SS_Idx = mod(SI3_Temp,F3);
        %Packaging into a cell array
        SI3 = {SI3_Sec_Idx,SI3_SS_Idx};
        clear SI3_Temp SI3_Sec_Idx SI3_SS_Idx
        
        toc;
    end %IF       
    
    %% Applying the Partial Coarse Delay correction with the DDR4 Block Read offset
    %Checking whether there is an Change in the DDR4 Block Delay (DDR4_BD) and
    %if it does End the Processing
    
    %NOTE The Reference sample Sequence Doesn't Need to be Corrected for Bulk
    %Delay Correcting the Bulk Delay in the Antenna
    fprintf('Applying Bulk Delay Corrections....');
    tic;
    %UnQuantized
    [SI_BD_Vec,x_BD_Vec,Flg_BD_Vec] = Bulk_Delay_Corr({SI1,SI2,SI3},x_Vec,Flg_Vec,cat(3,BD1,BD2,BD3),[F1,F2,F3],DDR4BS);
    %Quantized
    [~,xQ_BD_Vec,~] = Bulk_Delay_Corr({SI1,SI2,SI3},xQ_Vec,Flg_Vec,cat(3,BD1,BD2,BD3),[F1,F2,F3],DDR4BS);
    toc;
    clear SI1 SI2 SI3 x_Vec Flg_Vec;
    
      %% Performing Wideband Frequency Shifting SID1
    fprintf('Applying Wideband Frequency Shift....')
    tic;
    %UnQuantized
    x_FS_Vec = WB_FS(x_BD_Vec,SI_BD_Vec,[WB_Fsft,WB_Fsft,WB_Fsft],[F1,F2,F3],WBFS_Specs);
    %Quantized
    xQ_FS_Vec = WB_FS(xQ_BD_Vec,SI_BD_Vec,[WB_Fsft,WB_Fsft,WB_Fsft],[F1,F2,F3],WBFS_Specs_Q);
    toc;
    %NOTE - Frequency Shift is a Sample to Sample Operation and therefore
    %the Valid Flag Does not Change
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Using the Function 'OS_Poly_DFT_FB_SC'
    %Zero-Padding the CoCh Input in order to get Integer number of frames
    CC_IP = [x_FS_Vec; zeros(N_CC_OP*M1-Ns,3)]; %UnQuantize
    CCQ_IP = [xQ_FS_Vec; zeros(N_CC_OP*M1-Ns,3)]; %Quantize
    %Flags
    for n_flg = 1 : N_Flg
        Flg_CC_IP{1}{n_flg} = [Flg_BD_Vec{1}{n_flg}; false(N_CC_OP*M1-Ns,1)];
        Flg_CC_IP{2}{n_flg} = [Flg_BD_Vec{2}{n_flg}; false(N_CC_OP*M1-Ns,1)];
        Flg_CC_IP{3}{n_flg} = [Flg_BD_Vec{3}{n_flg}; false(N_CC_OP*M1-Ns,1)];
    end %For n_flg
    clear x_FS_Vec xQ_FS_Vec Flg_BD_Vec;
    
    %In order to maintain the Correct sample order in Commutator of
    %Shift the Input by
    Nsft_CC_IP = mod([SI_BD_Vec{1}{2}(1),SI_BD_Vec{2}{2}(1),SI_BD_Vec{3}{2}(1)],M1); %In Samples
    
    %Rotating the Input Vector to reflect the sample-order fed to the
    %Commutators of the OS Channelizer
    %Unquantized
    CC_IP(:,1)  = circshift(CC_IP(:,1), [Nsft_CC_IP(1)-1,0]);
    CC_IP(:,2)  = circshift(CC_IP(:,2), [Nsft_CC_IP(2)-1,0]);
    CC_IP(:,3)  = circshift(CC_IP(:,3), [Nsft_CC_IP(3)-1,0]);
    %Quantized
    CCQ_IP(:,1) = circshift(CCQ_IP(:,1),[Nsft_CC_IP(1)-1,0]);
    CCQ_IP(:,2) = circshift(CCQ_IP(:,2),[Nsft_CC_IP(2)-1,0]);
    CCQ_IP(:,3) = circshift(CCQ_IP(:,3),[Nsft_CC_IP(3)-1,0]);
    %Falgs
    for n_flg = 1 : N_Flg
        Flg_CC_IP{1}{n_flg} = circshift(Flg_CC_IP{1}{n_flg},[Nsft_CC_IP(1)-1,0]);
        Flg_CC_IP{2}{n_flg} = circshift(Flg_CC_IP{2}{n_flg},[Nsft_CC_IP(2)-1,0]);
        Flg_CC_IP{3}{n_flg} = circshift(Flg_CC_IP{3}{n_flg},[Nsft_CC_IP(3)-1,0]);
    end %For n_flg
        
    %The Sample Index Vectors for the CC Outputs [SI0,SI1]
    %Reference sequence for A#0
    SI0_CC_Temp = transpose(int64(floor(double(SI0{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI0_Sec_CC_Idx = int16(floor(double(SI0_CC_Temp)./FCC0)) + SI0{1}(1);
    SI0_SS_CC_Idx = mod(SI0_CC_Temp,FCC0);
    %Packaging into a cell array
    SI0_CC = {SI0_Sec_CC_Idx,SI0_SS_CC_Idx}; clear SI0_CC_Temp SI0_Sec_CC_Idx SI0_SS_CC_Idx
    %For  sequence for A#1
    SI1_CC_Temp = transpose(int64(floor(double(SI_BD_Vec{1}{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI1_Sec_CC_Idx = int16(floor(double(SI1_CC_Temp)./FCC1)) + SI_BD_Vec{1}{1}(1);
    SI1_SS_CC_Idx = mod(SI1_CC_Temp,FCC1);
    %Packaging into a cell array
    SI1_CC = {SI1_Sec_CC_Idx,SI1_SS_CC_Idx}; clear SI1_CC_Temp SI1_Sec_CC_Idx SI1_SS_CC_Idx
    %For  sequence for A#2
    SI2_CC_Temp = transpose(int64(floor(double(SI_BD_Vec{2}{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI2_Sec_CC_Idx = int16(floor(double(SI2_CC_Temp)./FCC2)) + SI_BD_Vec{2}{1}(1);
    SI2_SS_CC_Idx = mod(SI2_CC_Temp,FCC2);
    %Packaging into a cell array
    SI2_CC = {SI2_Sec_CC_Idx,SI2_SS_CC_Idx}; clear SI2_CC_Temp SI2_Sec_CC_Idx SI2_SS_CC_Idx
    %clear SI0 SI_BD_Vec;
    %For  sequence for A#3
    SI3_CC_Temp = transpose(int64(floor(double(SI_BD_Vec{3}{2}(1))./M1) + (0 : 1 : N_CC_OP-1)));
    SI3_Sec_CC_Idx = int16(floor(double(SI3_CC_Temp)./FCC3)) + SI_BD_Vec{3}{1}(1);
    SI3_SS_CC_Idx = mod(SI3_CC_Temp,FCC3);
    %Packaging into a cell array
    SI3_CC = {SI3_Sec_CC_Idx,SI3_SS_CC_Idx}; clear SI3_CC_Temp SI3_Sec_CC_Idx SI3_SS_CC_Idx
    %clear SI0 SI_BD_Vec;
    
    %The Barrel-Roller Indices
    BRI = mod([SI1_CC{2}(1),SI2_CC{2}(1),SI3_CC{2}(1)],Num1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Initializing Output
    CC_OP = zeros(N_CC_OP,3); %UnQuantized
    CCQ_OP = zeros(N_CC_OP,3); %Quantized
    Flg_CC_OP  = cell(1,3); %Valid
    
    fprintf('Processing with the Coarse Channelizer...')
    tic;
    %UnQuantized
    %For Sequence #1
    [CC_OP(:,1),~] = OS_Poly_DFT_FB_SC(CC_IP(:,1),Flg_CC_IP{1},BRI(1),OSFB_Config_para);
    %For Sequence #2
    [CC_OP(:,2),~] = OS_Poly_DFT_FB_SC(CC_IP(:,2),Flg_CC_IP{2},BRI(2),OSFB_Config_para);
    %For Sequence #3
    [CC_OP(:,3),~] = OS_Poly_DFT_FB_SC(CC_IP(:,3),Flg_CC_IP{3},BRI(3),OSFB_Config_para);
    %Quantized
    %For Sequence #1
    [CCQ_OP(:,1),Flg_CC_OP{1}] = OS_Poly_DFT_FB_SC(CCQ_IP(:,1),Flg_CC_IP{1},BRI(1),OSFB_Config_para_Q);
    %For Sequence #2
    [CCQ_OP(:,2),Flg_CC_OP{2}] = OS_Poly_DFT_FB_SC(CCQ_IP(:,2),Flg_CC_IP{2},BRI(2),OSFB_Config_para_Q);
    %For Sequence #3
    [CCQ_OP(:,3),Flg_CC_OP{3}] = OS_Poly_DFT_FB_SC(CCQ_IP(:,3),Flg_CC_IP{3},BRI(3),OSFB_Config_para_Q);
    toc;
    clear CC_IP CCQ_IP Flg_CC_IP;
    
    %%Quantizing at the Output of the Coarse Channelizer
    if nItt == 1 %Use the Same Scalingfor all iterations
        CC_OP_RMS = [rms(CC_OP(Flg_CC_OP{1}{1},1)),rms(CC_OP(Flg_CC_OP{2}{1},2)),rms(CC_OP(Flg_CC_OP{3}{1},3))]*CC_RMS_to_Peak_Ratio;
    end
    CCQ_OP(:,1) = CC_OP_RMS(1)*double(sfi(CCQ_OP(:,1)/CC_OP_RMS(1),W_CC,W_CC-1));
    CCQ_OP(:,2) = CC_OP_RMS(2)*double(sfi(CCQ_OP(:,2)/CC_OP_RMS(2),W_CC,W_CC-1));
    CCQ_OP(:,3) = CC_OP_RMS(3)*double(sfi(CCQ_OP(:,3)/CC_OP_RMS(3),W_CC,W_CC-1));
        
    %Correcting the Integer Sample Delay due to Polyphase Filter-Bak
    %For Data
    CC_OP  = [CC_OP(1-Insft_CC_OP:end,:);  zeros(-Insft_CC_OP,3)]; %UnQuantized
    CCQ_OP = [CCQ_OP(1-Insft_CC_OP:end,:); zeros(-Insft_CC_OP,3)]; %Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_CC_OP{1}{n_flg} = [Flg_CC_OP{1}{n_flg}(1-Insft_CC_OP:end,1); false(-Insft_CC_OP,1)];
        Flg_CC_OP{2}{n_flg} = [Flg_CC_OP{2}{n_flg}(1-Insft_CC_OP:end,1); false(-Insft_CC_OP,1)];
        Flg_CC_OP{3}{n_flg} = [Flg_CC_OP{3}{n_flg}(1-Insft_CC_OP:end,1); false(-Insft_CC_OP,1)];
    end %For n_flg
    
    %% Plotting the CoCh Output
    %  figure('name','Coarse Channelizer Output');
    %  subplot(221); hold on; plot(real(CC_OP(:,1)),'b-.'); plot(real(CC_OP(:,2)),'r-.'); plot(real(CCQ_OP(:,1)),'c-.'); plot(real(CCQ_OP(:,2)),'m-.'); hold off; title('Real'); box on; grid on; legend('UA1','UA2','QA1','QA2');
    %  subplot(222); hold on; plot(imag(CC_OP(:,1)),'b-.'); plot(imag(CC_OP(:,2)),'r-.'); plot(imag(CCQ_OP(:,1)),'c-.'); plot(imag(CCQ_OP(:,2)),'m-.'); hold off; title('Imaginary'); box on; grid on; legend('UA1','UA2','QA1','QA2');
    %  subplot(223); hold on; plot(real(CC_OP(:,1)-CCQ_OP(:,1)),'k--'); plot(real(CC_OP(:,2)-CCQ_OP(:,2)),'g--'); hold off; title('Real'); box on; grid on; legend('UA1-QA1','UA2-QA2');
    %  subplot(224); hold on; plot(imag(CC_OP(:,1)-CCQ_OP(:,1)),'k--'); plot(imag(CC_OP(:,2)-CCQ_OP(:,2)),'g--'); hold off; title('Imaginary'); box on; grid on; legend('UA1-QA1','UA2-QA2');
    
    %% Using the function 'ReSampler_ERP_Corr'
    %Initializing
    RS_OP = zeros(N_CC_OP,3); %UnQuantized Data
    RSQ_OP = zeros(N_CC_OP,3); %Quantized Data
    Flg_RS_OP = cell(1,3); %Flags
    
    fprintf('Applying Delay and Phase Corrections...')
    tic;
    %Using the ReSampler Function to Apply both Delay and Phase Corrections
    %UnQunatized
    [RS_OP(:,1),Flg_RS_OP{1}] = ReSampler_ERP_Corr(CC_OP(:,1),Flg_CC_OP{1},FCC1,SI1_CC,SI0_CC,FODSR1,FOPSR1,ReSamp_Config_para);
    [RS_OP(:,2),Flg_RS_OP{2}] = ReSampler_ERP_Corr(CC_OP(:,2),Flg_CC_OP{2},FCC2,SI2_CC,SI0_CC,FODSR2,FOPSR2,ReSamp_Config_para);
    [RS_OP(:,3),Flg_RS_OP{3}] = ReSampler_ERP_Corr(CC_OP(:,3),Flg_CC_OP{3},FCC3,SI3_CC,SI0_CC,FODSR3,FOPSR3,ReSamp_Config_para);
    %Quantized
    [RSQ_OP(:,1),~] = ReSampler_ERP_Corr(CCQ_OP(:,1),Flg_CC_OP{1},FCC1,SI1_CC,SI0_CC,FODSR1,FOPSR1,ReSamp_Config_para_Q);
    [RSQ_OP(:,2),~] = ReSampler_ERP_Corr(CCQ_OP(:,2),Flg_CC_OP{2},FCC2,SI2_CC,SI0_CC,FODSR2,FOPSR2,ReSamp_Config_para_Q);
    [RSQ_OP(:,3),~] = ReSampler_ERP_Corr(CCQ_OP(:,3),Flg_CC_OP{3},FCC3,SI3_CC,SI0_CC,FODSR3,FOPSR3,ReSamp_Config_para_Q);
    toc;
    clear CC_OP CCQ_OP Flg_CC_OP;
   
    %Quantising the ReSampler Outputs
    if nItt == 1 %Use the Same Scalingfor all iterations
        RS_OP_RMS = [rms(RS_OP(Flg_RS_OP{1}{1},1)),rms(RS_OP(Flg_RS_OP{2}{1},2)),rms(RS_OP(Flg_RS_OP{3}{1},3))]*RS_RMS_to_Peak_Ratio;
    end
    RSQ_OP(:,1) = RS_OP_RMS(1)*double(sfi(RSQ_OP(:,1)/RS_OP_RMS(1),W_RS,W_RS-1));
    RSQ_OP(:,2) = RS_OP_RMS(2)*double(sfi(RSQ_OP(:,2)/RS_OP_RMS(2),W_RS,W_RS-1));
    RSQ_OP(:,3) = RS_OP_RMS(3)*double(sfi(RSQ_OP(:,3)/RS_OP_RMS(3),W_RS,W_RS-1));    
    
    %% Plotting ReSampler Output & Error
    %     Est_DError1 = (RS_OP(:,1) - RSQ_OP(:,1)).*Flg_RS_OP{1}{1};
    %     Est_DError2 = (RS_OP(:,2) - RSQ_OP(:,2)).*Flg_RS_OP{2}{1};
    %     Est_DError3 = (RS_OP(:,3) - RSQ_OP(:,3)).*Flg_RS_OP{3}{1};
    %     
    %     Tstart = SI0_CC{2}(1);
    %     Tend = SI0_CC{2}(end) + int64(FCC0*(SI0_CC{1}(end)-SI0_CC{1}(1)));
    %     SI_RS = (Tstart:1:Tend);
    %     
    %     figure('name','ReSampler Output');
    %     
    %     subplot(221);
    %     hold on;
    %     plot(SI_RS,real(RS_OP(:,1)),'b-.');
    %     plot(SI_RS,real(RS_OP(:,2)),'r-.');
    %     plot(SI_RS,real(RS_OP(:,3)),'g-.');
    %     plot(SI_RS,real(RSQ_OP(:,1)),'c-.');
    %     plot(SI_RS,real(RSQ_OP(:,2)),'m-.');
    %     plot(SI_RS,real(RSQ_OP(:,3)),'y-.');
    %     hold off;
    %     box on; grid on;
    %     xlim([Tstart, Tend]);
    %     title('Real-Part');
    %     xlabel('Sample-Index'); ylabel('Amplitude');
    %     legend('UA1','UA2','UA3','QA1','QA2','QA3');
    %     
    %     subplot(222);
    %     hold on;
    %     plot(SI_RS,imag(RS_OP(:,1)),'b-.');
    %     plot(SI_RS,imag(RS_OP(:,2)),'r-.');
    %     plot(SI_RS,imag(RS_OP(:,3)),'g-.');
    %     plot(SI_RS,imag(RSQ_OP(:,1)),'c-.');
    %     plot(SI_RS,imag(RSQ_OP(:,2)),'m-.');
    %     plot(SI_RS,imag(RSQ_OP(:,3)),'y-.');
    %     hold off;
    %     box on; grid on;
    %     xlim([Tstart, Tend]);
    %     title('Imaginary-Part');
    %     xlabel('Sample-Index'); ylabel('Amplitude');
    %     legend('UA1','UA2','UA3','QA1','QA2','QA3');
    %     
    %     subplot(223);
    %     hold on;
    %     plot(SI_RS,real(Est_DError1),'b-.');
    %     plot(SI_RS,real(Est_DError2),'r-.');
    %     plot(SI_RS,real(Est_DError3),'g-.');
    %     hold off;
    %     box on; grid on;
    %     xlim([Tstart, Tend]);
    %     title('Error between Real-Parts');
    %     xlabel('Sample-Index'); ylabel('Amplitude');
    %     legend('UA1-QA1','UA2-QA2','UA3-QA3');
    %     
    %     subplot(224);
    %     hold on;
    %     plot(SI_RS,imag(Est_DError1),'b-.');
    %     plot(SI_RS,imag(Est_DError2),'r-.');
    %     plot(SI_RS,imag(Est_DError3),'g-.');
    %     hold off;
    %     box on; grid on;
    %     xlim([Tstart, Tend]);
    %     title('Error between Imaginary-Part');
    %     xlabel('Sample-Index'); ylabel('Amplitude');
    %     legend('UA1-QA1','UA2-QA2','UA3-QA3');
    
    %% Using the function 'CS_Poly_DFT_FB_Batch'
    
    %Zero-Padding the Input for the Imaging Channelizer Input
    IC_IP  = [RS_OP;  zeros(N_IC_OP*Nc2-N_CC_OP,3)]; %UnQuantized
    ICQ_IP = [RSQ_OP; zeros(N_IC_OP*Nc2-N_CC_OP,3)]; %Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_IC_IP{1}{n_flg} = [Flg_RS_OP{1}{n_flg}; false(N_IC_OP*Nc2-N_CC_OP,1)];
        Flg_IC_IP{2}{n_flg} = [Flg_RS_OP{2}{n_flg}; false(N_IC_OP*Nc2-N_CC_OP,1)]; 
        Flg_IC_IP{3}{n_flg} = [Flg_RS_OP{3}{n_flg}; false(N_IC_OP*Nc2-N_CC_OP,1)]; 
    end % FOR n_flg
    clear RS_OP RSQ_OP Flg_RS_OP;
    
    %In order to maintain the Correct sample order in Commutator of
    %Shift the Input by
    Nsft_IC_IP = mod(SI0_CC{2}(1),Nc2); %In Samples
    %NOTE now both Sequences are have the Same Indices
    
    %Rotating the Input Vector to reflect the sample-order fed to the
    %Commutators of the OS Channelizer
    IC_IP  = circshift(IC_IP, [Nsft_IC_IP-1,0]); %UnQuantized
    ICQ_IP = circshift(ICQ_IP,[Nsft_IC_IP-1,0]); %Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_IC_IP{1}{n_flg} = circshift(Flg_IC_IP{1}{n_flg},[Nsft_IC_IP-1,0]);
        Flg_IC_IP{2}{n_flg} = circshift(Flg_IC_IP{2}{n_flg},[Nsft_IC_IP-1,0]);
        Flg_IC_IP{3}{n_flg} = circshift(Flg_IC_IP{3}{n_flg},[Nsft_IC_IP-1,0]);
    end % FOR n_flg
    
    %The Sample Index Vectors for the CC Outputs [SI0,SI1]
    SI0_IC_Temp = transpose(int64(floor(double(SI0_CC{2}(1))./Nc2) + (0 : 1 : N_IC_OP-1)));
    SI0_Sec_IC_Idx = int16(floor(double(SI0_IC_Temp)./F_IC)) + SI0_CC{1}(1);
    SI0_SS_IC_Idx = mod(SI0_IC_Temp,F_IC);
    %Packaging into a cell array
    SI0_IC = {SI0_Sec_IC_Idx,SI0_SS_IC_Idx}; clear SI0_IC_Temp SI0_Sec_IC_Idx SI0_SS_IC_Idx
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Initialize
    IC_OP = zeros(N_IC_OP,Nc2,2); %UnQuantized
    ICQ_OP = zeros(N_IC_OP,Nc2,2); %Quantized
    Flg_IC_OP = cell(1,3); %Flags
    
    fprintf('Processing the Signals with Imaging Channelizer...')
    tic;
    %UnQuantized
    %For Sequence #1
    [IC_OP(:,:,1),Flg_IC_OP{1}] = CS_Poly_DFT_FB_Batch(IC_IP(:,1),Flg_IC_IP{1},CSFB_Config_para);
    %For Sequence #2
    [IC_OP(:,:,2),Flg_IC_OP{2}] = CS_Poly_DFT_FB_Batch(IC_IP(:,2),Flg_IC_IP{2},CSFB_Config_para);
    %For Sequence #3
    [IC_OP(:,:,3),Flg_IC_OP{3}] = CS_Poly_DFT_FB_Batch(IC_IP(:,3),Flg_IC_IP{3},CSFB_Config_para);
    %Quantized
    %For Sequence #1
    [ICQ_OP(:,:,1),~] = CS_Poly_DFT_FB_Batch(ICQ_IP(:,1),Flg_IC_IP{1},CSFB_Config_para_Q);
    %For Sequence #2
    [ICQ_OP(:,:,2),~] = CS_Poly_DFT_FB_Batch(ICQ_IP(:,2),Flg_IC_IP{2},CSFB_Config_para_Q);
    %For Sequence #3
    [ICQ_OP(:,:,3),~] = CS_Poly_DFT_FB_Batch(ICQ_IP(:,3),Flg_IC_IP{3},CSFB_Config_para_Q);
    toc;
    clear IC_IP ICQ_IP V_IC_IP;
    
    %Correcting the Integer Sample Delay due to Polyphase Filter-Bank
    %For Data
    IC_OP  = [IC_OP(1-Insft_IC_OP:end,:,:);  zeros(-Insft_IC_OP,Nc2,3)]; %UnQuantized
    ICQ_OP = [ICQ_OP(1-Insft_IC_OP:end,:,:); zeros(-Insft_IC_OP,Nc2,3)]; %Quantized
    %Flags
    for n_flg = 1 : N_Flg
        Flg_IC_OP{1}{n_flg} = [Flg_IC_OP{1}{n_flg}(1-Insft_IC_OP:end); false(-Insft_IC_OP,1)];
        Flg_IC_OP{2}{n_flg} = [Flg_IC_OP{2}{n_flg}(1-Insft_IC_OP:end); false(-Insft_IC_OP,1)];
        Flg_IC_OP{3}{n_flg} = [Flg_IC_OP{3}{n_flg}(1-Insft_IC_OP:end); false(-Insft_IC_OP,1)];
    end % FOR n_flg
    
    %Shedding invalid samples at the front and the back before correlating
    First_1_Ant = zeros(1,3);
    Last_1_Ant  = zeros(1,3);
    for n = 1 : 3
        First_1_Ant(1,n) = find(Flg_IC_OP{n}{1},1,'first');
        Last_1_Ant(1,n) = find(Flg_IC_OP{n}{1},1,'last');
    end
    First_1_Idx = min(First_1_Ant);
    Last_1_Idx = max(Last_1_Ant);
    
    %For Data
    IC_OP   = IC_OP(First_1_Idx:Last_1_Idx,:,:);
    ICQ_OP  = ICQ_OP(First_1_Idx:Last_1_Idx,:,:);
    %Flags 
    for n_flg = 1 : N_Flg
        Flg_IC_OP{1}{n_flg} = Flg_IC_OP{1}{n_flg}(First_1_Idx:Last_1_Idx,:,:);
        Flg_IC_OP{2}{n_flg} = Flg_IC_OP{2}{n_flg}(First_1_Idx:Last_1_Idx,:,:);
        Flg_IC_OP{3}{n_flg} = Flg_IC_OP{3}{n_flg}(First_1_Idx:Last_1_Idx,:,:);
    end %FOR n_flg
    %For Sample Indicies
    SI0_IC = {SI0_IC{1}(First_1_Idx:Last_1_Idx), SI0_IC{2}(First_1_Idx:Last_1_Idx)};
            
    %Displaying the Range of Time Indicies evalautes
    fprintf('Imaging Channel Time Series | Evalauted from %d-%d to %d-%d \n',SI0_IC{1}(1),SI0_IC{2}(1),SI0_IC{1}(end),SI0_IC{2}(end));
    
    %% Gain Correction
    [NICE,~] =  size(IC_OP);
    %UnQuantized
    IC_OP(:,:,1) = (ones(NICE,1)*GC_Vec(1,:)).*IC_OP(:,:,1);
    IC_OP(:,:,2) = (ones(NICE,1)*GC_Vec(2,:)).*IC_OP(:,:,2);
    IC_OP(:,:,3) = (ones(NICE,1)*GC_Vec(3,:)).*IC_OP(:,:,3);
    %Quantized
    ICQ_OP(:,:,1) = (ones(NICE,1)*GC_Vec(1,:)).*ICQ_OP(:,:,1);
    ICQ_OP(:,:,2) = (ones(NICE,1)*GC_Vec(2,:)).*ICQ_OP(:,:,2);
    ICQ_OP(:,:,3) = (ones(NICE,1)*GC_Vec(3,:)).*ICQ_OP(:,:,3);
    
    %ReQuantising
    if nItt == 1
        IC_OP_RMS = [rms(IC_OP(Flg_IC_OP{1}{1},:,1));rms(IC_OP(Flg_IC_OP{2}{1},:,2));rms(IC_OP(Flg_IC_OP{3}{1},:,3))]*IC_RMS_to_Peak_Ratio;
    end
    ICQ_OP(:,:,1) = (ones(NICE,1)*IC_OP_RMS(1,:)).*double(sfi(ICQ_OP(:,:,1)./(ones(NICE,1)*IC_OP_RMS(1,:)),W_IC,W_IC-1));
    ICQ_OP(:,:,2) = (ones(NICE,1)*IC_OP_RMS(2,:)).*double(sfi(ICQ_OP(:,:,2)./(ones(NICE,1)*IC_OP_RMS(2,:)),W_IC,W_IC-1));
    ICQ_OP(:,:,3) = (ones(NICE,1)*IC_OP_RMS(3,:)).*double(sfi(ICQ_OP(:,:,3)./(ones(NICE,1)*IC_OP_RMS(3,:)),W_IC,W_IC-1));
    
    %% Comparison of Imaging Channelizer Output for a Given Time-Instant
    %     %Selecting a Time-Instant to Compare
    %     Sel_TS = ceil(rand*(length(ICx0(:,1))-1));
    %     %Sel_TS = 225;
    %
    %     %Unquantized
    %     Sel_ICx0 = ICx0(Sel_TS,:);
    %     Sel_ICx1 = ICx1(Sel_TS,:);
    %     %Quantized
    %     Sel_ICQx0 = ICQx0(Sel_TS,:);
    %     Sel_ICQx1 = ICQx1(Sel_TS,:);
    %
    %     M_Est_ICError0 = abs(Sel_ICx0) - abs(Sel_ICQx0);
    %     M_Est_ICError1 = abs(Sel_ICx1) - abs(Sel_ICQx1);
    %     A_Est_ICError0 = asin(sin(angle(Sel_ICx0) - angle(Sel_ICQx0)));
    %     A_Est_ICError1 = asin(sin(angle(Sel_ICx1) - angle(Sel_ICQx1)));
    %
    %     NFCH_l = ceil(Nc2/Os1/2);
    %     NFCH_u = Nc2-NFCH_l;
    %     Max_SL = 600; %Maximum Signal-Level
    %
    %     figure('name',sprintf('Imaging Channelizer Output | Magnitude - Phase for Time Slice %d',Sel_TS));
    %
    %     subplot(221);
    %     hold on;
    %     plot(abs(Sel_ICx0),'b-.');
    %     plot(abs(Sel_ICx1),'r-.');
    %     plot(abs(Sel_ICQx0),'c-.');
    %     plot(abs(Sel_ICQx1),'m-.');
    %     plot([NFCH_l,NFCH_l],[0,Max_SL],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[0,Max_SL],'k-.','LineWidth',1);
    %     hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Imaging Channels - Magnitude');
    %     xlabel('Channel-Index'); ylabel('Magnitude');
    %     legend('A0-U','A1-U','A0-Q','A1-Q');
    %
    %     subplot(222);
    %     hold on;
    %     plot(angle(Sel_ICx0),'b-.');
    %     plot(angle(Sel_ICx1),'r-.');
    %     plot(angle(Sel_ICQx0),'c-.');
    %     plot(angle(Sel_ICQx1),'m-.');
    %     plot([NFCH_l,NFCH_l],[-1.5*pi,1.5*pi],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[-1.5*pi,1.5*pi],'k-.','LineWidth',1);hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Imaging Channels - Phase Angle');
    %     xlabel('Channel-Index'); ylabel('Angle - rad');
    %     legend('A0-U','A1-U','A0-Q','A1-Q');
    %
    %     subplot(223);
    %     hold on;
    %     plot(M_Est_ICError0,'k-.');
    %     plot(M_Est_ICError1,'g-.');
    %     plot([NFCH_l,NFCH_l],[-Max_SL,Max_SL],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[-Max_SL,Max_SL],'k-.','LineWidth',1);hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Error in Magnitude');
    %     xlabel('Channel-Index'); ylabel('Magnitude');
    %     legend('A0-U - A0-Q','A1-U - A1-Q');
    %
    %     subplot(224);
    %     hold on;
    %     plot(A_Est_ICError0,'k-.');
    %     plot(A_Est_ICError1,'g-.');
    %     plot([NFCH_l,NFCH_l],[-2*pi,2*pi],'k-.','LineWidth',1);
    %     plot([NFCH_u,NFCH_u],[-2*pi,2*pi],'k-.','LineWidth',1);hold off;
    %     box on; grid on;
    %     xlim([0, Nc2-1]);
    %     title('Error In Phase');
    %     xlabel('Channel-Index'); ylabel('Angle - rad');
    %     legend('A0-U - A0-Q','A1-U - A1-Q');
 
    %% Saving the Flags, Imaging Channel Time Series 
    FileName = fullfile(FolderName,sprintf('Data_Pack_%04d',nItt));
    save(FileName,'SI0_IC','Flg_IC_OP','IC_OP','ICQ_OP');
    
    %% Calculating the Auto & Cross Correlations
    
    %Finding out the Indicies of Valid Samples for both Sequences
    Valid_Idx = Flg_IC_OP{1}{1} & Flg_IC_OP{2}{1} & Flg_IC_OP{3}{1};
    
    %UnQuanrized
    AC_A1 = ((nItt-1)*AC_A1 + mean(abs(IC_OP(Valid_Idx,:,1)).^2,1))./nItt;
    AC_A2 = ((nItt-1)*AC_A2 + mean(abs(IC_OP(Valid_Idx,:,2)).^2,1))./nItt;
    AC_A3 = ((nItt-1)*AC_A3 + mean(abs(IC_OP(Valid_Idx,:,3)).^2,1))./nItt;
    
    XC_A1A2 = ((nItt-1)*XC_A1A2 + mean(IC_OP(Valid_Idx,:,1).*conj(IC_OP(Valid_Idx,:,2)),1))./nItt;
    XC_A2A3 = ((nItt-1)*XC_A2A3 + mean(IC_OP(Valid_Idx,:,2).*conj(IC_OP(Valid_Idx,:,3)),1))./nItt;
    XC_A3A1 = ((nItt-1)*XC_A3A1 + mean(IC_OP(Valid_Idx,:,3).*conj(IC_OP(Valid_Idx,:,1)),1))./nItt;
    
    %Quanrized
    ACQ_A1 = ((nItt-1)*ACQ_A1 + mean(abs(ICQ_OP(Valid_Idx,:,1)).^2,1))./nItt;
    ACQ_A2 = ((nItt-1)*ACQ_A2 + mean(abs(ICQ_OP(Valid_Idx,:,2)).^2,1))./nItt;
    ACQ_A3 = ((nItt-1)*ACQ_A3 + mean(abs(ICQ_OP(Valid_Idx,:,3)).^2,1))./nItt;
    
    XCQ_A1A2 = ((nItt-1)*XCQ_A1A2 + mean(ICQ_OP(Valid_Idx,:,1).*conj(ICQ_OP(Valid_Idx,:,2)),1))./nItt;
    XCQ_A2A3 = ((nItt-1)*XCQ_A2A3 + mean(ICQ_OP(Valid_Idx,:,2).*conj(ICQ_OP(Valid_Idx,:,3)),1))./nItt;
    XCQ_A3A1 = ((nItt-1)*XCQ_A3A1 + mean(ICQ_OP(Valid_Idx,:,3).*conj(ICQ_OP(Valid_Idx,:,1)),1))./nItt;
    
    %pause(1);
    fprintf('\n')
end % FOR nItt
 
%% Plotting the Auto and Cross-Correlations
CH_Idx = (-0.5*Nc2:1:0.5*Nc2-1);  
NFCH_l = -0.5*Nc2+ceil(Nc2*(Os1-1)/Os1/2);
NFCH_u = -NFCH_l;

%Converting to dB
%UnQuantized
AC_A1_dB = 10*log10(abs(AC_A1)); Max_PL_dB = max(AC_A1_dB); %Maximum Power-Level
AC_A1_dB = AC_A1_dB - Max_PL_dB;
AC_A2_dB = 10*log10(abs(AC_A2)) - Max_PL_dB;
AC_A3_dB = 10*log10(abs(AC_A3)) - Max_PL_dB;
%
XC_A1A2_dB = 10*log10(abs(XC_A1A2)) - Max_PL_dB;
XC_A2A3_dB = 10*log10(abs(XC_A2A3)) - Max_PL_dB;
XC_A3A1_dB = 10*log10(abs(XC_A3A1)) - Max_PL_dB;

%Quantized
ACQ_A1_dB = 10*log10(abs(ACQ_A1)); Max_PLQ_dB = max(ACQ_A1_dB); %Maximum Power-Level
ACQ_A1_dB = ACQ_A1_dB - Max_PLQ_dB;
ACQ_A2_dB = 10*log10(abs(ACQ_A2)) - Max_PLQ_dB;
ACQ_A3_dB = 10*log10(abs(ACQ_A3)) - Max_PLQ_dB;
%
XCQ_A1A2_dB = 10*log10(abs(XCQ_A1A2)) - Max_PLQ_dB;
XCQ_A2A3_dB = 10*log10(abs(XCQ_A2A3)) - Max_PLQ_dB;
XCQ_A3A1_dB = 10*log10(abs(XCQ_A3A1)) - Max_PLQ_dB;

%% Plotting
%% Magnitude
figure('name','The Magnitude of the Auto and Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,fftshift(AC_A1_dB),'b-.');
plot(CH_Idx,fftshift(AC_A2_dB),'r-.');
plot(CH_Idx,fftshift(AC_A3_dB),'m-.');
plot(CH_Idx,fftshift(XC_A1A2_dB),'c-.');
plot(CH_Idx,fftshift(XC_A2A3_dB),'y-.');
plot(CH_Idx,fftshift(XC_A3A1_dB),'g-.');
%
plot(CH_Idx,fftshift(ACQ_A1_dB),'b--','LineWidth',1);
plot(CH_Idx,fftshift(ACQ_A2_dB),'r--','LineWidth',1);
plot(CH_Idx,fftshift(ACQ_A3_dB),'m--','LineWidth',1);
plot(CH_Idx,fftshift(XCQ_A1A2_dB),'c--','LineWidth',1);
plot(CH_Idx,fftshift(XCQ_A2A3_dB),'y--','LineWidth',1);
plot(CH_Idx,fftshift(XCQ_A3A1_dB),'g--','LineWidth',1);
%
plot([NFCH_l,NFCH_l],[-Max_PL_dB-10,5],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-Max_PL_dB-10,5],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]);
Title = sprintf('The Magnitude of the Auto- and Cross-Correlation Spectra \n for Frequency Slice %d',FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Magnitude - dB');
legend('UA1','UA2','UA3','UA1UA1','UA2UA3','UA3UA1','QA1','QA2','QA3','QA1QA2','QA2QA3','QA3QA1');

%% Phase
%Unquantized
Ang_XC_A1A2 = fftshift(angle(XC_A1A2));
Ang_XC_A2A3 = fftshift(angle(XC_A2A3));
Ang_XC_A3A1 = fftshift(angle(XC_A3A1));
%Quantized
Ang_XCQ_A1A2 = fftshift(angle(XCQ_A1A2));
Ang_XCQ_A2A3 = fftshift(angle(XCQ_A2A3));
Ang_XCQ_A3A1 = fftshift(angle(XCQ_A3A1));

figure('name','The Phase Angle of the Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,Ang_XC_A1A2,'b-.');
plot(CH_Idx,Ang_XC_A2A3,'r-.');
plot(CH_Idx,Ang_XC_A3A1,'k-.');
%
plot(CH_Idx,Ang_XCQ_A1A2,'b:','LineWidth',1);
plot(CH_Idx,Ang_XCQ_A2A3,'r:','LineWidth',1);
plot(CH_Idx,Ang_XCQ_A3A1,'k:','LineWidth',1);
%
plot([NFCH_l,NFCH_l],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); %ylim([-1.15E-2*pi,1.15E-2*pi]);
Title = sprintf('The Phase Angle of the Cross-Correlation Spectra \n for Frequency Slice %d',FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Angle - rad');
legend('UA1UA2','UA2UA3','UA3UA1','QA1QA2','QA2QA3','QA3QA1');

%% Phase Conformity
figure('name','The Sum Phase Angles of the Cross Correlation Spectra - Normal Imaging');

hold on;
plot(CH_Idx,Ang_XC_A1A2+Ang_XC_A2A3+Ang_XC_A3A1,'r-.');
plot(CH_Idx,Ang_XCQ_A1A2+Ang_XCQ_A2A3+Ang_XCQ_A3A1,'b--','LineWidth',1);
%
plot([NFCH_l,NFCH_l],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); %ylim([-1.15E-3*pi,1.15E-3*pi]);
title('The Sum of Phase Angles for the 3 Antennas');
xlabel('Channel-Index'); ylabel('Angle - rad');
legend('Un-Quantized','Quantized');

% profile viewer;
% profile off;