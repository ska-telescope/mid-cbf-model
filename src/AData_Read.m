%This MATLAB script reads the Data of the end-to-end RFI simulation data
%and make it availabel for further analysis or visualization
%
% By Thushara Kanchana Gunaratne - RCO - HAA NRC CANADA
% Start Date : 2019-07-19

close all; clear; clc;

%Parmaters To Specify 
N0 = 001; %Start Index
%Number of Data Sets to load
N_DS = 38;

%% Loading the DATA
%Select Folder Location
Folder_Path = uigetdir(pwd,'Select Folder with Data...');

%Loading Cofiguration parameters
load(fullfile(Folder_Path,'Configuration_Parameters.mat'));

fprintf('Loading Saved Data for Imaging Channel Output Sequences ...');
tic;
%Loading the Image Channelizer Data
%For the first data set
FileName = fullfile(Folder_Path,sprintf('Data_Pack_%04d.mat',N0));
load(FileName);
IC_Vec = IC_OP; %The Output of Unquantized data - No RFI
ICQ_Vec = ICQ_OP; %The Output of Quantized data - No RFI
%R_ICQ_Vec = R_ICQ_OP; %The Output of Quantized data - With RFI
V_Flg_Vec = [Flg_IC_OP{1}{1},Flg_IC_OP{2}{1}]; %The Valid Vector - Logical Array
TD_Flg_Vec = [Flg_IC_OP{1}{2},Flg_IC_OP{2}{2}]; %The Flag Vectors Threshold Detected -Logical Array
%IDL_RFI_Flg_Vec = [Flg_IC_OP{1}{3},Flg_IC_OP{2}{3}]; %The Flag Vectors without RFI - Logical Array
%RFI_Free_Pct_Vec = RFI_Free_Pct;
SI0_IC_Vec = SI0_IC; %Sample Time Indicies

%For the second file and on
for n_ds = N0 + 1 : N0 + N_DS -1
    FileName = fullfile(Folder_Path,sprintf('Data_Pack_%04d.mat',n_ds));
    %fprintf(FileName); fprintf(' \n');
    load(FileName);
    %Concatenate the Matrices
    IC_Vec = [IC_Vec;IC_OP];
    ICQ_Vec = [ICQ_Vec;ICQ_OP];
    %R_ICQ_Vec = [R_ICQ_Vec;R_ICQ_OP];
    V_Flg_Vec = [[V_Flg_Vec(:,1);Flg_IC_OP{1}{1}],[V_Flg_Vec(:,2);Flg_IC_OP{2}{1}]];
    TD_Flg_Vec = [[TD_Flg_Vec(:,1);Flg_IC_OP{1}{2}],[TD_Flg_Vec(:,2);Flg_IC_OP{2}{2}]];
    %IDL_RFI_Flg_Vec = [[IDL_RFI_Flg_Vec(:,1);Flg_IC_OP{1}{3}],[IDL_RFI_Flg_Vec(:,2);Flg_IC_OP{2}{3}]];
    %RFI_Free_Pct_Vec = [RFI_Free_Pct_Vec;RFI_Free_Pct];
    SI0_IC_Vec = {[SI0_IC_Vec{1};SI0_IC{1}],[SI0_IC_Vec{2};SI0_IC{2}]};
    clear IC_OP ICQ_OP R_ICQ_OP Flg_IC_OP SI0_IC;
end %FOR n_ds
toc;

%Determining the number of time Samples, channels and Antennas
[Nt,Nc,NA] = size(IC_Vec);

%% Setting the Configuration Parameters for Threshold Detection
%Short-Temp Epoch for Power Calculation
STL_IC = 6;
%The Threshold Level for Imaging Channels in multiples of Standard Deviation
IC_Thresh_Lvl = 4.5;
%Forgetting factor for Long-Term Power Calculations 
C_IC = 1-2^-6;

%Specifying the Parameters for Threshold Detection after the Imaging Channalizer
Threshold_para_IC.STAL = STL_IC; % Short Term Accumulation Length
Threshold_para_IC.STPTL_Vec = IC_Thresh_Lvl*ones(1,Nc); % Short Term Power Threshold
Threshold_para_IC.LTAC = C_IC; % Long Term Accumulation Coefficient

fprintf('Threshold detection and Flagging Imaging Channel Output Sequences ...');
tic;
%Initializing Output
Flg_TD_IC_OP  = cell(1,2); %Valid

%Using the Custom Function Threshold_Det
[Flg_TD_IC_OP{1},IC_LTPS_Est1,IC_STPS_Est1] = Threshold_Det_NCh(ICQ_Vec(:,:,1),TD_Flg_Vec(:,1),Threshold_para_IC);
[Flg_TD_IC_OP{2},IC_LTPS_Est2,IC_STPS_Est2] = Threshold_Det_NCh(ICQ_Vec(:,:,2),TD_Flg_Vec(:,2),Threshold_para_IC);
toc;

%% Using the Knowladge on the RFI Source to Flag the Adjoing Channels
RFI_Scr_Ch = 10648;
Eff_Chns = (RFI_Scr_Ch-37:RFI_Scr_Ch+37);

for nt = 1 : Nt
    if ~Flg_TD_IC_OP{1}(nt,RFI_Scr_Ch)
        Flg_TD_IC_OP{1}(nt,Eff_Chns) = false;
    end
    
    if ~Flg_TD_IC_OP{2}(nt,RFI_Scr_Ch)
        Flg_TD_IC_OP{2}(nt,Eff_Chns) = false;
    end
    
end %FOR nt

%% Evalauting The Parameters 
%Evalauting the Mitigation Efficincy 
% Definition Mitigation Efficiency = sqrt(RFI_time x RFI_Bandwidth)/sqrt(Mitigated_time x Mitigated_Bandwidth)
%Observation Time
OBS_Time = double(SI0_IC_Vec{1}(end) - SI0_IC_Vec{1}(1)) + double(SI0_IC_Vec{2}(end) - SI0_IC_Vec{2}(1))/13440; %In s

%Actual RFI Time and Bandwidth
RFI_Time = (1-mean(max(RFI_Free_Pct_Vec,[],2)))*OBS_Time; %In s
RFI_BW = 1E6; %In Hz

Os = 10/9;
NFCH_ll = floor(Nc/Os/2);
NFCH_uu = Nc-NFCH_ll;

%When only the RFI Occupied Channels are lost
%Mitigated_time x Mitigated_Bandwidth = Flagged Channels * Channel Tme / Channel Bandwidth
Mit_Time_BW_Ch = sum((~Flg_TD_IC_OP{1}(:,[1:NFCH_ll,NFCH_uu:end]))|(~Flg_TD_IC_OP{2}(:,[1:NFCH_ll,NFCH_uu:end])));
% figure; stairs(Mit_Time_BW_Ch)

Mit_Eff = zeros(1,Nc);
for nc = Eff_Chns(1) : Eff_Chns(end)
    Mit_Eff(nc) = sqrt(RFI_Time*1E6/13440)./sqrt(Mit_Time_BW_Ch(nc));
end
% figure; stairs(Mit_Eff);

%Evalaution of RFI Degradation Factor
% Definition RFI Degradation Factor = 1./sqrt(1-(Mitigated_time x Mitigated_Bandwidth)/(t*BW) )
Obs_Time_BW = OBS_Time*floor(Nc/Os);

RFI_Deg_Fac = ones(1,Nc);
RFI_Deg_Fac(1,[1:NFCH_ll,NFCH_uu:end]) = 1./sqrt(1-Mit_Time_BW_Ch./Obs_Time_BW);
% figure; stairs(RFI_Deg_Fac)
%% Visualising 
Sel_Chns = (10500:10800);
nt = (1:Nt);

%% Instantanious Power Plots
Pwr_IC1 = squeeze(R_ICQ_Vec(:,:,1)).*conj(squeeze(R_ICQ_Vec(:,:,1)));
Pwr_IC2 = squeeze(R_ICQ_Vec(:,:,2)).*conj(squeeze(R_ICQ_Vec(:,:,2)));

figure;
hold on
for n_ch = Sel_Chns(1) : Sel_Chns(end)
    plot3(nt,n_ch*ones(1,Nt),Pwr_IC1(:,n_ch),'b-');
    plot3(nt,n_ch*ones(1,Nt),Pwr_IC2(:,n_ch),'r--');
end
hold off
box on; grid on; view(0,0);
Title = sprintf('Instantanious Power of the Imaging Channels %d - %d',Sel_Chns(1), Sel_Chns(end));
title(Title);
ylabel('Channel Index'); xlabel('Time Index'); zlabel('Magnitude');

%% Short Term Power Sum Plots
%Sel_Chns = (1000: 1200);

figure;
hold on
for n_ch = Sel_Chns(1) : Sel_Chns(end)
    plot3(nt,n_ch*ones(1,Nt),IC_STPS_Est1(:,n_ch),'b-');
    plot3(nt,n_ch*ones(1,Nt),IC_STPS_Est2(:,n_ch),'r--');
end
hold off
box on; grid on; view(30,10);
Title = sprintf('Short Term Power Sum of Imaging Channels %d - %d',Sel_Chns(1), Sel_Chns(end));
title(Title);
ylabel('Channel Index'); xlabel('Time Index'); zlabel('Magnitude');

%% Long Term Power Sum Plots

figure;
hold on
for n_ch = Sel_Chns(1) : Sel_Chns(end)
    plot3(nt,n_ch*ones(1,Nt),IC_LTPS_Est1(:,n_ch),'b-');
    plot3(nt,n_ch*ones(1,Nt),IC_LTPS_Est2(:,n_ch),'r--');
end
hold off
box on; grid on; view(40,40); % view(0,0)
Title = sprintf('Long Term Power Sum of Imaging Channels %d - %d',Sel_Chns(1), Sel_Chns(end));
title(Title);
ylabel('Channel Index'); xlabel('Time Index'); zlabel('Magnitude');

%% Threshold Detected Flags Beyond IC 
figure;

hold on
plot(nt,squeeze(V_Flg_Vec(:,1)),'g-');
plot(nt,squeeze(V_Flg_Vec(:,2)),'y--');
plot(nt,squeeze(TD_Flg_Vec(:,1)),'b-');
plot(nt,squeeze(TD_Flg_Vec(:,2)),'r--');
hold off
box on; grid on; %view(60,30);
Title = sprintf('Saturation Flags of the Imaging Channels %d - %d',Sel_Chns(1), Sel_Chns(end));
title(Title);
xlabel('Time Index'); ylabel('Flag Level');

%% Ideal RFI Flags
figure;

hold on
plot(nt,squeeze(IDL_RFI_Flg_Vec(:,1)),'b-');
plot(nt,squeeze(IDL_RFI_Flg_Vec(:,2)),'r--');
hold off
box on; grid on; %view(60,30); view(0,0);
Title = sprintf('Ideal RFI Flags of the Imaging Channels %d - %d',Sel_Chns(1), Sel_Chns(end));
title(Title);
xlabel('Time Index'); ylabel('Flag Level');

%% Flag Plots

figure;
hold on
for n_ch = Sel_Chns(1) : Sel_Chns(end)    
    plot3(nt,n_ch*ones(1,Nt),Flg_TD_IC_OP{1}(:,n_ch),'b-');
    plot3(nt,n_ch*ones(1,Nt),Flg_TD_IC_OP{2}(:,n_ch),'r--');
end
hold off
box on; grid on; view(0,0);
Title = sprintf('Flags of the Imaging Channels %d - %d - Threshold Detection',Sel_Chns(1), Sel_Chns(end));
title(Title);
xlabel('Time Index'); ylabel('Channel Index');  zlabel('Magnitude');

%% Falgging Efficiency

Flg_Efficency = zeros(Nc,2);
N_Vld = sum(TD_Flg_Vec,1);
N_IDL_RFI_Flg = sum(IDL_RFI_Flg_Vec,1);

for nc = 1 : Nc
    
    if (nc >= Eff_Chns(1)) && (nc <= Eff_Chns(end))
        Flg_Efficency(nc,1) = sum(Flg_TD_IC_OP{1}(:,nc))*100/N_IDL_RFI_Flg(1);
        Flg_Efficency(nc,2) = sum(Flg_TD_IC_OP{2}(:,nc))*100/N_IDL_RFI_Flg(2);
    else
        Flg_Efficency(nc,1) = sum(Flg_TD_IC_OP{1}(:,nc))*100/N_Vld(1);
        Flg_Efficency(nc,2) = sum(Flg_TD_IC_OP{2}(:,nc))*100/N_Vld(2);        
    end %IF
    
end % FOR nc

%% RFI Contaminated Signal - Integration With Flagging

% Evalauting Cross Correlations with Ideal and Derived Flagging 
RFIC_IC_CH_Rng = Eff_Chns; %(10550:10750);
%Ideal Flagging
Flg_I_IC_OP  = cell(1,2); %Valid
Flg_I_IC_OP{1} = true(Nt,Nc);
Flg_I_IC_OP{2} = true(Nt,Nc);
for n2_ch = RFIC_IC_CH_Rng(1) : RFIC_IC_CH_Rng(end)
    Flg_I_IC_OP{1}(:,n2_ch) = IDL_RFI_Flg_Vec(:,1);
    Flg_I_IC_OP{2}(:,n2_ch) = IDL_RFI_Flg_Vec(:,2);
end %FOR n2_ch

%Estimating the Variance of the Mean Estimate Iteratively
Valid_R_ICQ_OP1_I = R_ICQ_Vec(:,:,1).*Flg_I_IC_OP{1}; DVC_R_ICQ_OP1_I = sum(Flg_I_IC_OP{1});
Valid_R_ICQ_OP2_I = R_ICQ_Vec(:,:,2).*Flg_I_IC_OP{2}; DVC_R_ICQ_OP2_I = sum(Flg_I_IC_OP{2});
XP_R_ICQ_A1A2_I = Valid_R_ICQ_OP1_I.*conj(Valid_R_ICQ_OP2_I); DCV_R_ICQ_A1A2_I = sum(Flg_I_IC_OP{1} & Flg_I_IC_OP{2} == true,1);

%Evalauting Auto and Cross Correlation
WF_ACQ_A1_I = sum(Valid_R_ICQ_OP1_I.*conj(Valid_R_ICQ_OP1_I),1)./DVC_R_ICQ_OP1_I;
WF_ACQ_A2_I = sum(Valid_R_ICQ_OP2_I.*conj(Valid_R_ICQ_OP2_I),1)./DVC_R_ICQ_OP2_I;
WF_XCQ_A1A2_I = sum(XP_R_ICQ_A1A2_I,1)./DCV_R_ICQ_A1A2_I;
WF_DXCQ_A1A2_I = XP_R_ICQ_A1A2_I -  ones(Nt,1)*WF_XCQ_A1A2_I;
WF_VXCQ_A1A2_I = sum(WF_DXCQ_A1A2_I.*conj(WF_DXCQ_A1A2_I),1)./(DCV_R_ICQ_A1A2_I-1);

%Empirical Flagging
%Estimating the Variance of the Mean Estimate Iteratively
Valid_R_ICQ_OP1_TD = R_ICQ_Vec(:,:,1).*Flg_TD_IC_OP{1}; DVC_R_ICQ_OP1_TD = sum(Flg_TD_IC_OP{1});
Valid_R_ICQ_OP2_TD = R_ICQ_Vec(:,:,2).*Flg_TD_IC_OP{2}; DVC_R_ICQ_OP2_TD = sum(Flg_TD_IC_OP{2});
XP_R_ICQ_A1A2_TD = Valid_R_ICQ_OP1_TD.*conj(Valid_R_ICQ_OP2_TD); DCV_R_ICQ_A1A2_TD = sum(Flg_TD_IC_OP{1} & Flg_TD_IC_OP{2} == true,1);

%Evalauting Auto and Cross Correlation
WF_ACQ_A1_TD = sum(Valid_R_ICQ_OP1_TD.*conj(Valid_R_ICQ_OP1_TD),1)./DVC_R_ICQ_OP1_TD;
WF_ACQ_A2_TD = sum(Valid_R_ICQ_OP2_TD.*conj(Valid_R_ICQ_OP2_TD),1)./DVC_R_ICQ_OP2_TD;
WF_XCQ_A1A2_TD = sum(XP_R_ICQ_A1A2_TD,1)./DCV_R_ICQ_A1A2_TD;
WF_DXCQ_A1A2_TD = XP_R_ICQ_A1A2_TD -  ones(Nt,1)*WF_XCQ_A1A2_TD;
WF_VXCQ_A1A2_TD = sum(WF_DXCQ_A1A2_TD.*conj(WF_DXCQ_A1A2_TD),1)./(DCV_R_ICQ_A1A2_TD-1);

%% Plotting the Auto and Cross-Correlations
Os = 10/9;
CH_Idx = (-0.5*Nc:1:0.5*Nc-1);  
NFCH_l = -0.5*Nc+ceil(Nc*(Os-1)/Os/2);
NFCH_u = -NFCH_l;
Band_Name_List = {'1','2','3','4','5A','5B'};

Max_PLQ_dB = 0;
%With RFI & Quantized & With Ideal Flagging
% WO_ACQ_A1_I_dB = 10*log10(abs(fftshift(WO_ACQ_A1_I))); Max_PLQ_dB = max(WO_ACQ_A1_I_dB); %Maximum Power-Level
% WO_ACQ_A1_I_dB = WO_ACQ_A1_i_dB - Max_PLQ_dB;
WF_ACQ_A1_I_dB = 10*log10(abs(fftshift(WF_ACQ_A1_I))) - Max_PLQ_dB;
WF_ACQ_A2_I_dB = 10*log10(abs(fftshift(WF_ACQ_A2_I))) - Max_PLQ_dB;
WF_XCQ_A1A2_I_dB = 10*log10(abs(fftshift(WF_XCQ_A1A2_I))) - Max_PLQ_dB;

%With RFI & Quantized & With Empirical Flagging
% WO_ACQ_A1_E_dB = 10*log10(abs(fftshift(WO_ACQ_A1_E))); Max_PLQ_dB = max(WO_ACQ_A1_E_dB); %Maximum Power-Level
% WO_ACQ_A1_E_dB = WO_ACQ_A1_e_dB - Max_PLQ_dB;
WF_ACQ_A1_TD_dB = 10*log10(abs(fftshift(WF_ACQ_A1_TD))) - Max_PLQ_dB;
WF_ACQ_A2_TD_dB = 10*log10(abs(fftshift(WF_ACQ_A2_TD))) - Max_PLQ_dB;
WF_XCQ_A1A2_TD_dB = 10*log10(abs(fftshift(WF_XCQ_A1A2_TD))) - Max_PLQ_dB;

%% With RFI & With Flagging
%%Magnitude
figure('name','The Magnitude of the Auto and Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,WF_ACQ_A1_I_dB,'b-.');
plot(CH_Idx,WF_ACQ_A2_I_dB,'r-.');
plot(CH_Idx,WF_XCQ_A1A2_I_dB,'k-.');
plot(CH_Idx,WF_ACQ_A1_TD_dB,'c-.');
plot(CH_Idx,WF_ACQ_A2_TD_dB,'m-.');
plot(CH_Idx,WF_XCQ_A1A2_TD_dB,'g-.');
plot([NFCH_l,NFCH_l],[-Max_PLQ_dB-80,5],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-Max_PLQ_dB-80,5],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]);
Title = sprintf('The Magnitude of the Auto- and Cross-Correlation Spectra \n for Band %s : Frequency Slice %d - Quantized & With RFI \n  Ideal and Threshold Level Detected Flagging',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Magnitude - dB');
legend('QA1-IF','QA2-IF','QA1QA1-IF','QA1-TDF','QA2-TDF','QA1QA2-TDF');

%%Phase
figure('name','The Phase Angle of the Cross Correlation Spectra - Normal Imaging');
hold on;
plot(CH_Idx,angle(fftshift(WF_XCQ_A1A2_I)),'k-.');
plot(CH_Idx,angle(fftshift(WF_XCQ_A1A2_TD)),'g-.');
plot([NFCH_l,NFCH_l],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-1.15*pi,1.15*pi],'k-.','LineWidth',1);
hold off;
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); %ylim([-1.15E-2*pi,1.15E-2*pi]);
Title = sprintf('The Phase Angle of the Cross-Correlation Spectra \n for Band %s : Frequency Slice %d - Quantized & With RFI \n  Ideal and Threshold Level Detected Flagging',Band_Name_List{Band_ID},FS_ID);
title(Title);
xlabel('Channel-Index'); ylabel('Angle - rad');
legend('QA1QA2-IF','QA1QA2-TDF');

%% Plotting Flagging Efficiency

figure('name','Flagging Efficiency');
hold on;
plot(CH_Idx,fftshift(Flg_Efficency(:,1)),'b-.');
plot(CH_Idx,fftshift(Flg_Efficency(:,2)),'r-.');
plot([NFCH_l,NFCH_l],[-5,105],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-5,105],'k-.','LineWidth',1);
hold off
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); ylim([-1,102]);
Title = sprintf('Flagging Efficiency for Band %s : Frequency Slice %d \n  with Threshold Level Detected Flagging at Level %3.2f',Band_Name_List{Band_ID},FS_ID,IC_Thresh_Lvl);
title(Title);
xlabel('Channel-Index'); ylabel('%');

%% Plotting Mitigation Efficiency 

figure('name','Mitigation Efficiency');
hold on;
plot(CH_Idx,fftshift(Mit_Eff)*100,'b-.');
plot([NFCH_l,NFCH_l],[-5,105],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-5,105],'k-.','LineWidth',1);
hold off
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); ylim([-1,102]);
Title = sprintf('Mitigation Efficiency for Band %s : Frequency Slice %d \n  with Threshold Level Detected Flagging at Level %3.2f',Band_Name_List{Band_ID},FS_ID,IC_Thresh_Lvl);
title(Title);
xlabel('Channel-Index'); ylabel('%');

%% Plotting RFI Degradation Factor

figure('name','RFI Degradation Factor');
hold on;
plot(CH_Idx,fftshift(RFI_Deg_Fac),'b-.');
plot([NFCH_l,NFCH_l],[-0.05*max(RFI_Deg_Fac),1.15*max(RFI_Deg_Fac)],'k-.','LineWidth',1);
plot([NFCH_u,NFCH_u],[-0.05*max(RFI_Deg_Fac),1.15*max(RFI_Deg_Fac)],'k-.','LineWidth',1);
hold off
box on; grid on;
xlim([CH_Idx(1), CH_Idx(end)]); ylim([0.95,1.15*max(RFI_Deg_Fac)]);
Title = sprintf('RFI Degradation Factor for Band %s : Frequency Slice %d \n  with Threshold Level Detected Flagging at Level %3.2f',Band_Name_List{Band_ID},FS_ID,IC_Thresh_Lvl);
title(Title);
xlabel('Channel-Index'); ylabel('%');

%% Evalauting the Correlator Efficiency
% XP_A1A2 = IC_Vec(:,:,1).*conj(IC_Vec(:,:,2));
% XC_A1A2 = mean(XP_A1A2,1);
% DXC_A1A2 = XP_A1A2 - ones(Nt,1)*XC_A1A2;
% VXC_A1A2 = sum(DXC_A1A2.*conj(DXC_A1A2),1)/(Nt-1);

%% Ideal Flagging
% Corr_Eff_WF_I = fftshift((abs(WF_XCQ_A1A2_I)./sqrt(WF_VXCQ_A1A2_I))./(abs(XC_A1A2)./sqrt(VXC_A1A2)));
% 
% figure('name','The Correlator Efficiency - Normal Imaging');
%  
% hold on;
% plot(CH_Idx,Corr_Eff_WF_I*100,'b-.');
% plot([NFCH_l,NFCH_l],[0,115],'k-.','LineWidth',1);
% plot([NFCH_u,NFCH_u],[0,115],'k-.','LineWidth',1);
% hold off;
% box on; grid on;
% xlim([CH_Idx(1), CH_Idx(end)]); ylim([90,102]);
% title('The Correlator Efficiency - With RFI & With Ideal Flagging');
% xlabel('Channel-Index'); ylabel('%');
% 
% %% Threshold Detection Flagging
% Corr_Eff_WF_E = fftshift((abs(WF_XCQ_A1A2_TD)./sqrt(WF_VXCQ_A1A2_TD))./(abs(XC_A1A2)./sqrt(VXC_A1A2)));
% 
% figure('name','The Correlator Efficiency - Normal Imaging');
%  
% hold on;
% plot(CH_Idx,Corr_Eff_WF_E*100,'b-.');
% plot([NFCH_l,NFCH_l],[0,115],'k-.','LineWidth',1);
% plot([NFCH_u,NFCH_u],[0,115],'k-.','LineWidth',1);
% hold off;
% box on; grid on;
% xlim([CH_Idx(1), CH_Idx(end)]); ylim([90,102]);
% title('The Correlator Efficiency - With RFI & With Threshold Detection Flagging');
% xlabel('Channel-Index'); ylabel('%');