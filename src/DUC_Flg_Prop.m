%The custom MATLAB function 
%           Flg_Out = DUC_Flg_Prop(Flg_In,CL,N_Cons,No)
% evaluates the propagation of flags through a polyphase filter structure
% Inputs
% Flg_In =  Flag Input - [Boolean]
% CL = Commutator Length - [UInt16]
% N_Cons = Polyphase Filter Propagation Length [UInt16]
% No = Length of the Output Flags [UInt64]
%
% Outputs
%Flg_Out = Flag_Output - [Boolean]

function Flg_Out = DUC_Flg_Prop(Flg_In,L,M,N_Cons,Nc,No)

%Initialising 
Flg_Out = false(No+1,1); % Output
Inval_Count = N_Cons; % Invalid Count

%% Performing the iteration
for n = 1 : Nc
    if all(Flg_In(n*L:-1:(n-1)*L+1))
        Inval_Count = Inval_Count - M;
        if Inval_Count <= 0
            Flg_Out((n-1)*M+1:(n+0)*M) = true;
            Inval_Count = 0;
        else
            Flg_Out((n-1)*M+1:(n+0)*M) = false;
        end %IF Inval_Count == 0
    else
        Inval_Count = N_Cons;
        Flg_Out((n-1)*M+1:(n+0)*M) = false;
    end %IF all(Flg_In((n-1)*M+1:1:n*M))
end %FOR n

Flg_Out = Flg_Out(1:No,1);