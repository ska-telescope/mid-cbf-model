%Custom MATLAB function
% [y_SC,Flg_Out] = OS_Poly_DFT_FB_SC(x,Flg_In,BRI,FB_Config_para)
%Inputs
%  x = Input Vector (Double-Flaoting)
%  Flg_In = Input Falg Vectos (A Cell array)
%  BRI = Initial Barrol Roller Indixcies (Integer/Double)
%  FB_Config_Para = Configuration parameter (A Structure) contains following
%     FB_Config_para.Nc - Number of Channels
%     FB_Config_para.CL - Commutator Length 
%     FB_Config_para.h2D - the 2D Filter mask
%     FB_Config_para.TWD2D_SC - Twidell Factoers depending on the Selected Channel
%     FB_Config_para.Nprop - Propagation in the output for one input flag
%Outputs
%  y_SC = Output vector of size (No,Nc) (Double-Flaoting)
%  Flg_Out = Flag Vectors for Outputs (Cell array)
%
%The MATLAB function 'OS_Poly_DFT_FB_SC'
%performs the Over-Sampled Channelization of the input vector 'x' and input 
%Flag vector 'Flg_In' yields the time series 'y_SC' and Output flag vector 
%'Flg_Out' for the selected-channel 
%
% By Thushara Gunaratne - RO/RCO - NSI-NRC CANADA
% Start Date - 2017-05-15
% Modified 2018-12-21 > To Add valid Input and Output
% Modified 2019-01-21 > To Add Multiple Flags and Use a Counter-Based
% approach to propagate the Flags across the filter-bank
% Modified 2019-07-18 > To Integrate the Pre Compiled mex files
% "CORE_OSPPFB_SC_mex" and "Poly_Flg_Prop_mex" for faster evalaution
 
function [y_SC,Flg_Out] = OS_Poly_DFT_FB_SC(x,Flg_In,BRI,FB_Config_para)

[Nrow,Ncol] = size(x);
if Ncol ~= 1
    x = x.';
    Ni = Ncol; %Input Vector Length
else
    Ni = Nrow; %Input Vector Length
end 
clear Nrow Ncol;
 
%Evalaute the number of Flags sequences and the length of the Flag sequences
N_Flg = length(Flg_In);
Flg_L = length(Flg_In{1});

if Ni ~= Flg_L
    error('Error! Input Length and Flag Lengths DO NOT match...');
end %IF

%Loading Configuration Parameters
Nc = FB_Config_para.Nc;
Num = FB_Config_para.Num;
Den = FB_Config_para.Den;
h2D = FB_Config_para.h2D;
TWD2D_SC = FB_Config_para.TWD2D_SC; %Twilde factors Depends on the Selected Channel
N_Cons = FB_Config_para.N_Cons; %The Maximum Propagation time in the 
%filter-bank

%The DownSampling Factor
CL = Nc*Den/Num;
 
%Set the 2D Data-Mask
[~,Nt] = size(h2D);
 
%Initiating 
%The Number of Output Samples
No = ceil(Ni/CL);

%% Performing the Polyphase DFT Filter bank
%The Custom MATLAB function
% y_SC = CORE_OSPPFB_SC(x,BRI,Nc,Num,CL,h2D,TWD2D)
%performs the Over-Sampled Channelization of the input vector 'x' and 
%yeilds the time series 'y_SC' for the selected-channel determined by
%TWD2D. This is the MATLAB equivalent of an efficient C function 
%
%Inputs
% x => The Input Vector [Complex Double]
% BRI => Barrel Rotator Index. Adjust this to avoid phase offsets. [UINT16]
% Nc => Number of Channels [UINT16]
% Num => Numerator of the Over-Sampling-Factor Os = Num/Den [UINT16]
% CL => Commutator Length [UINT16]
% h2D => [Nc,Nt] size 2D Matrix containing the Segmented filter
%        coefficients where Nc is the number of channels and Nt is the 
%        maximum number of tap per polyphase arm. [Complex Double]
% TWD2D => The 2D Twiddle Factor Matrix [Complex Double]
% Nt => Length of the Filter [UINT64]
% No => Output Samples [UINT64]
%
%Output
%y_SC = Output Vector [Complex Double]

%Regular MATLAB Function
y_SC = CORE_OSPPFB_SC(x,uint16(BRI),uint16(Nc),uint16(Num),uint16(CL),h2D,TWD2D_SC,uint64(Nt),uint64(No));

%MATLAB MEX Function - Fast but hard to support for every envirnment
%y_SC = CORE_OSPPFB_SC_mex(x,uint16(BRI),uint16(Nc),uint16(Num),uint16(CL),h2D,TWD2D_SC,uint64(Nt),uint64(No));

%% Evaluating Falg Out Cell Array
%The custom MATLAB function 
%           Flg_Out = Poly_Flg_Prop(Flg_In,CL,N_Cons,No)
% evaluates the propagation of flags through a polyphase filter structure
% Inputs
% Flg_In =  Flag Input - [Boolean]
% CL = Commutator Length - [UInt16]
% N_Cons = Polyphase Filter Propagation Length [UInt16]
% No = Length of the Output Flags [UInt64]
%
% Outputs
%Flg_Out = Flag_Output - [Boolean]

Flg_Out = cell(1,N_Flg); %Falg Out Cell Array
for n_flgs = 1 : N_Flg
    Flg_In_Temp = Flg_In{n_flgs};
    
    %Regular MATLAB Function
    Flg_Out{n_flgs} = Poly_Flg_Prop(Flg_In_Temp,uint16(CL),uint16(N_Cons),uint64(No));  
    
    %MATLAB MEX Function - Fast but hard to support for every envirnment
    %Flg_Out{n_flgs} = Poly_Flg_Prop_mex(Flg_In_Temp,uint16(CL),uint16(N_Cons),uint64(No));  
end %FOR n_flg