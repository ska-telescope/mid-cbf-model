%The MATLAB function ReSample_ChirpZ(X,N0,Ns,D) employs the custom
%implementation of Chirp-Z Transform to resample the sequence corresponding
%to the band-Limited spectrum specified by X, producing Ns samples starting
%from the index No, with the spacing D
%By Thushara Kanchana Gunaratne - RO/RCO National Research Council Canada
 
function xR = ReSample_ChirpZ(X,N0,Ns,D)
 
%Length of the Spectrum
Nx = length(X);
 
%The closest 2^n for the Length intermediate fft.
Nfft = power(2,nextpow2(Nx+Ns-1));
 
%Forming the intermediate factors for convolutional kernel
% kk = transpose((-Nx+1):max(Ns-1,Nx-1));
% kk2 = times(kk,kk)./2;
% wPow = times(1j*2*pi/NR, kk2);
% ww = exp(wPow);
ww = exp(1j*pi*D/Nx*(transpose((-Nx+1:max(Ns-1,Nx-1))).^2));
 
nn = transpose(0:(Nx-1));
% aPow = times(1j*2*pi*N1/NR, nn);
% aa = exp(aPow);
aa = exp(2j*pi*N0*D/Nx*nn);
 
%Multiplying the inner core
% aa = times(aa, ww(Nx+nn));
aa = aa.*ww(Nx+nn);
C = (X.*aa);
 
%Fast convolution via FFT.
fy = fft(C,Nfft);
% Chirp Filter Frequency response
fv = fft(1./ww(1:(Ns-1+Nx)),Nfft); 
%Evaluating the Convolution in Frequency Domain and Converting back to
%Sample Domain
z = ifft(fy.*fv);
 
%------- Final multiply.
%The Resampled Sequence
xR = z(Nx:(Nx+Ns-1)).*ww(Nx:(Nx+Ns-1));
% End ReSample_ChirpZ