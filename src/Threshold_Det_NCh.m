%Custom MATLAB function
% [Flg_Out,LTPS_Est,STPS] = Threshold_Det_Nch(x_Vec,Flg_In,Threshold_para)
%Inputs
%   x_Vec = Input Vector (Double-Flaoting) - (Nt,NCh)
%   Flg_In = Input Falg Vectos (A Cell array)
%   Threshold_para = Parameters for Threshold Detection  
%       Threshold_para.STAL = Short Term Accumulation Length
%       Threshold_para.STPTL = Short Term Power Threshold Level - Assumed
%       Threshold_para.LTAC = Long Term Accumulation Coefficient
%Outputs
%   Flg_Out - Output Flag Vector
%   LTPL_Est - Long Term Power Level Estimation
%   STPS_Est - Short Term Power Sum
%
%The custom MATLAB function 'Threshold_Det' flags the data in 'x' if it's
%short term power accumulation goes beyond a certain threshold
% By Thushara Gunaratne - RO/RCO - HAA NRC CANADA
% Start Date - 2019-06-04

function [Flg_Out,LTPS_Est,STPS_Est] = Threshold_Det_NCh(x_Vec,Flg_In,Threshold_para)

%Loading Parameters 
STAL = Threshold_para.STAL; % Short Term Accumulation Length
STPTL_Vec = Threshold_para.STPTL_Vec; % Short Term Power Threshold Vector
LTAC = Threshold_para.LTAC; % Long Term Accumulation Coefficient

%Half Length of Short Term Accumulation
H_STAL = round(0.5*STAL);

%Number of Chanels for detection
Nc = length(STPTL_Vec);

%Checking whether x_Vec is a row or colum vector and converting into a
%column vector if it is not
[Nrow,Ncol] = size(x_Vec);
if Ncol ~= Nc
    x_Vec = x_Vec.';
    Ni = Ncol; %Input Vector Length
else
    Ni = Nrow; %Input Vector Length
end 
clear Nrow Ncol;

%Evalaute the number of Flags sequences and the length of the Flag sequences
if iscell(Flg_In)
    N_Flg = length(Flg_In);
    L_Flg = length(Flg_In{1});
    Vld_In = Flg_In{1};
else
    [L_Flg,N_Flg] = size(Flg_In);
    Vld_In = Flg_In(:,1);
end %IF iscell

if Ni ~= L_Flg
    error('Error! Input Length and Flag Length DO NOT match...');
end %IF

%Initiating the Flags
Vld_Out = false(Ni,Nc); %Replicating the Input Flags for All Channels

%Finiding the Index of the First Valid Input
V0_Idx = find(Vld_In,true,'first');

%Initiating
STPS_Est = zeros(Ni,Nc);
LTPS_Est = zeros(Ni,Nc);

for nc = 1 : Nc
    
    %Long-Term Power Sum - Initiation
    LTPS_Est_nch = zeros(Ni,1);
    LTPS_Est_nch(V0_Idx) = x_Vec(V0_Idx,nc).*conj(x_Vec(V0_Idx,nc));
    
    %Short-Term Power Sum - Initiation
    STPS_Est_nch = zeros(Ni,1);
    STPS_Est_nch(V0_Idx) = LTPS_Est_nch(V0_Idx)/STAL;
    
    %Temorary Valid Out Initiation
    Vld_Out_Temp = Vld_In;
    
    for n = (V0_Idx+1) : Ni
        
        %% Calculation of Short-Term Power
        if n < (V0_Idx+STAL+1)
            
            if Vld_In(n)
                STPS_Est_nch(n) = STPS_Est_nch(n-1) + x_Vec(n,nc).*conj(x_Vec(n,nc))./STAL;
            else
                STPS_Est_nch(n) = STPS_Est_nch(n-1);
            end % IF Vld_In(nx)
            
        else
            
            if (Vld_In(n) && Vld_In(n-STAL))
                STPS_Est_nch(n) =    STPS_Est_nch(n-1) ...
                              + (real(x_Vec(n,nc))-real(x_Vec(n-STAL,nc))).*(real(x_Vec(n,nc))+real(x_Vec(n-STAL,nc)))./STAL ...
                              + (imag(x_Vec(n,nc))-imag(x_Vec(n-STAL,nc))).*(imag(x_Vec(n,nc))+imag(x_Vec(n-STAL,nc)))./STAL;
            elseif (~Vld_In(n) && Vld_In(n-STAL))
                STPS_Est_nch(n) =    STPS_Est_nch(n-1) ...
                              + (0-real(x_Vec(n-STAL,nc))).*(0+real(x_Vec(n-STAL,nc)))./STAL ...
                              + (0-imag(x_Vec(n-STAL,nc))).*(0+imag(x_Vec(n-STAL,nc)))./STAL;
            elseif (Vld_In(n) && ~Vld_In(n-STAL))
                STPS_Est_nch(n) =    STPS_Est_nch(n-1) ...
                              + (real(x_Vec(n,nc))-0).*(real(x_Vec(n,nc))+0)./STAL ...
                              + (imag(x_Vec(n,nc))-0).*(imag(x_Vec(n,nc))+0)./STAL;
            else
                STPS_Est_nch(n) =   STPS_Est_nch(n-1);
            end %IF ELSEIF (Vld_In(nx) && Vld_In(nx-STL))
            
        end %IF nx < STL+1
        
        
        %% Applying the Treshold-Detection to The Sample Stream
        if STPTL_Vec(nc) < STPS_Est_nch(n)
            if (n > H_STAL+1) && (n+H_STAL) <= Ni
                Vld_Out_Temp(n-H_STAL:n+H_STAL) = false;
            elseif (n+H_STAL) <= Ni
                Vld_Out_Temp(1:n+H_STAL) = false;
            else
                Vld_Out_Temp(n-H_STAL:end) = false;
            end %IF
        end % IF
        
        %% Calculation of Long-Term Power
        if Vld_Out_Temp(n)
            LTPS_Est_nch(n) = x_Vec(n,nc).*conj(x_Vec(n,nc)) + LTAC*LTPS_Est_nch(n-1);
        else
            LTPS_Est_nch(n)  = LTPS_Est_nch(n-1);
        end
    end %FOR n
    
   %The Short Term Power 
   STPS_Est(:,nc) = STPS_Est_nch;
   
   %ReScaling the LTPS and LTPS_Q
   LTPS_Est(:,nc)  = (1-LTAC)*LTPS_Est_nch;
   %Updating Valid Output
   Vld_Out(:,nc) = Vld_Out_Temp;
    
end %FOR nc

%Assigning the RFI Flag
Flg_Out = Vld_Out;

% %Synthesizing Output
% Flg_Out = cell(1,N_Flg);
% %Assigning the RFI Flag
% Flg_Out{1} = Vld_Out;
% if N_Flg > 1
%     Flg_Out{2:N_Flg} = Flg_In{2:N_Flg};
% end