%The MATLAB function 
%GNoise_Seq = Gaussian_Noise(Freq_Spec,Fs,Ns,sigma)
%Generates Real-valued Guassian noise sequences of standard deviation
%'sigma' of length 'Ns' that occupies the normalized frequency range specified 
%by 'Freq_Spec'. Note that for normalized frequency sample rate is 1, hence
%max range for 'Freq_Spec' = [-0.5,0.5]
function GNoise_Seq = Gaussian_Noise(Freq_Spec,Fs,Ns,sigma)

if sigma ~= 0
    %Initializing
    N_seq = length(Fs);
    GNoise_Seq = zeros(Ns,N_seq);
    Ns_hlaf = ceil(Ns/2);
    
    for k = 1 : N_seq
        N_l = ceil(2*Ns_hlaf*Freq_Spec(1)/Fs(k));
        N_u = floor(2*Ns_hlaf*Freq_Spec(2)/Fs(k));
        Xh = zeros(Ns_hlaf,1);
        Xh(N_l:N_u) = exp(2j*pi*rand(N_u-N_l+1,1));
        if mod(Ns,2)
            X = [Xh;flipud(conj(Xh(2:end)))];
        else
            X = [Xh;2*real(Xh(end));flipud(conj(Xh(2:end)))];
        end %IF
        x = ifft(X);
        x_RMS = rms(x);
        GNoise_Seq(:,k) = sigma*x/x_RMS;
        
    end %FOR k
    
else
    GNoise_Seq = zeros(Ns,length(Fs));
end %IF