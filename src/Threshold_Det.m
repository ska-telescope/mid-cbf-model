%Custom MATLAB function
% [Flg_Out,LTPS_Est,STPS] = Threshold_Det(x,Flg_In,Threshold_para)
%Inputs
%   x = Input Vector (Double-Flaoting)
%   Flg_In = Input Falg Vectos (A Cell array)
%   Threshold_para = Parameters for Threshold Detection  
%       Threshold_para.STAL = Short Term Accumulation Length
%       Threshold_para.STPTL = Short Term Power Threshold Level - Assumed
%       Threshold_para.LTAC = Long Term Accumulation Coefficient
%Outputs
%   Flg_Out - Output Flag Vector
%   LTPL_Est - Long Term Power Level Estimation
%   STPS_Est - Short Term Power Sum
%
%The custom MATLAB function 'Threshold_Det' flags the data in 'x' if it's
%short term power accumulation goes beyond a certain threshold
% By Thushara Gunaratne - RO/RCO - HAA NRC CANADA
% Start Date - 2019-06-04

function [Flg_Out,LTPS_Est,STPS_Est] = Threshold_Det(x,Flg_In,Threshold_para)

%Checking whether x_Vec is a row or colum vector and converting into a
%column vector if it is not
[Nrow,Ncol] = size(x);
if Ncol ~= 1
    x = x.';
    Ni = Ncol; %Input Vector Length
else
    Ni = Nrow; %Input Vector Length
end 
clear Nrow Ncol;

%Evalaute the number of Flags sequences and the length of the Flag sequences
if iscell(Flg_In)
    Cell_Out = true;
    N_Flg = length(Flg_In);
    L_Flg = length(Flg_In{1});
    Vld_In = Flg_In{1};
else
    Cell_Out = false;
    [L_Flg,N_Flg] = size(Flg_In);
    Vld_In = Flg_In(:,1);
end

if Ni ~= L_Flg
    error('Error! Input Length and Flag Length DO NOT match...');
end %IF

%Loading Parameters 
STAL = Threshold_para.STAL; % Short Term Accumulation Length
STPTL = Threshold_para.STPTL; % Short Term Power Threshold
LTAC = Threshold_para.LTAC; % Long Term Accumulation Coefficient

%Finiding the Index of the First Valid Input
V0_Idx = find(Vld_In,true,'first');

%Initiating the Flags
Vld_Out = Vld_In;

%Long-Term Power Sum - Initiation
LTPS_Est = zeros(Ni,1);
LTPS_Est(V0_Idx) = x(V0_Idx).*conj(x(V0_Idx));

%Short-Term Power Sum - Initiation
STPS_Est = zeros(Ni,1);
STPS_Est(V0_Idx) = LTPS_Est(V0_Idx)/STAL;

for n = (V0_Idx+1) : Ni
    
    %% Calculation of Short-Term Power
    if n < (V0_Idx+STAL+1)
        
        if Vld_In(n)
            STPS_Est(n) = STPS_Est(n-1) + x(n).*conj(x(n))./STAL;
        else
            STPS_Est(n) = STPS_Est(n-1);
        end % IF Vld_In(nx)
            
    else
        
        if (Vld_In(n) && Vld_In(n-STAL))
            STPS_Est(n) =    STPS_Est(n-1) ...
                          + (real(x(n))-real(x(n-STAL))).*(real(x(n))+real(x(n-STAL)))./STAL ...
                          + (imag(x(n))-imag(x(n-STAL))).*(imag(x(n))+imag(x(n-STAL)))./STAL;
        elseif (~Vld_In(n) && Vld_In(n-STAL))
            STPS_Est(n) =    STPS_Est(n-1) ...
                          + (0-real(x(n-STAL))).*(0+real(x(n-STAL)))./STAL ...
                          + (0-imag(x(n-STAL))).*(0+imag(x(n-STAL)))./STAL;
        elseif (Vld_In(n) && ~Vld_In(n-STAL))
            STPS_Est(n) =    STPS_Est(n-1) ...
                          + (real(x(n))-0).*(real(x(n))+0)./STAL ...
                          + (imag(x(n))-0).*(imag(x(n))+0)./STAL;
        else
            STPS_Est(n) = STPS_Est(n-1);
        end %IF ELSEIF (Vld_In(nx) && Vld_In(nx-STL))
        
    end %IF nx < STL+1   
    
    
    %% Applying the Treshold-Detection to The Sample Stream
    if STPTL < STPS_Est(n)
        Vld_Out(n) = false;
    end
    
    %% Calculation of Long-Term Power
    if Vld_Out(n)
        LTPS_Est(n) = x(n).*conj(x(n)) + LTAC*LTPS_Est(n-1);
    else
        LTPS_Est(n)  = LTPS_Est(n-1);
    end
end %For nx

%ReScaling the LTPS and LTPS_Q
LTPS_Est = (1-LTAC)*LTPS_Est;

if Cell_Out
    %Synthesizing Output
    Flg_Out = cell(1,N_Flg);
    %Assigning the RFI Flag
    Flg_Out{1} = Vld_Out;
    if N_Flg > 1
        Flg_Out{2:N_Flg} = Flg_In{2:N_Flg};
    end
else
    Flg_Out = false(L_Flg,N_Flg);
    Flg_Out(:,1) = Vld_Out;
    if N_Flg > 1
        Flg_Out(:,2:N_Flg) = Flg_In(:,2:N_Flg);
    end
end %IF Cell_Out