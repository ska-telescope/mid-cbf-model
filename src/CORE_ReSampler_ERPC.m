%The custom MATLAB function
%    CORE_ReSampler_ERPC(x,Ns,SIdx,FODSR,FOPSR,Config_Para)
%performs the core functions in the ReSampler and Earth Rotation Phase
%Correction
% Inputs
% x = Input Sequence [Complex-Double]
% Flg_In = Flags In {Boolean]
% SIdx0 = Start Index of the Input [Double]
% Frat = Input to Output Frequency Ratio (FCC1/FCC0)
% FODSR = First Order Delay Polynomial Registers 
% FOPSR = First Order Phase Polynomial Registers
% Config_Para = Verious Configuration Parameters
%   Config_Para.SI0 = Sample Indicies of the Reqired Sequence [Int64]
%   Config_Para.FCC0 = Common Frequency
%   Config_Para.DSteps = Number of Delay Steps (I.e. Number of Fractional Delay Filters)
%   Config_Para.PSteps = Number of Phase Steps (I.e. Number of Points in the Since/Cosine LUT)
%   Config_Para.hFD = Coefficient Matrix for [DSteps x N_FD] Fractional Delay Filters
%   Config_Para.SC_LUT = Coefficient Matrix for [PSteps x 1] Sine/Cosine Values

function [xr,Flg_Out] = CORE_ReSampler_ERPC(x,Flg_In,SIdx0,Frat,FODSR,FOPSR,Config_Para)

%% Extracting Configuration Parameters
SI0 = Config_Para.SI0;%Sample Indicies of the Output
FCC0 = Config_Para.FCC0; %Common Frequency
DSteps = Config_Para.DSteps; %Number of Delay Steps (I.e. Number of Fractional Delay Filters)
PSteps = Config_Para.PSteps; %Number of Phase Steps (I.e. Number of Points in the Since/Cosine LUT)
Np = Config_Para.Np; %Number of Coefficients per Fractional Delay filter
hFD = Config_Para.hFD; %Coefficient Matrix for [DSteps x Np] Fractional Delay Filters
SC_LUT = Config_Para.SC_LUT; %Coefficient Matrix for [PSteps x 1] Sine/Cosine Values

%Difining
N1o = length(x);
%Number of Flags
N_Flg = length(Flg_In);
Ns = length(SI0);
Np_Half = 0.5*Np;

%Assigning the Starting Sample Index interms of the original Sample rate
SIdx = SIdx0;

%The Selected Delay/Phase Polynomial Index
n_poly = 1; 
%Converting the Time-Based Delay and Phase Polynomials for Sample Based 
% For Delay :-> Nd = m*FCC1/FCC0*n + c*FCC1
%The Delay Polynomial Registers 
D1_S = FODSR(1,4);
D0_S = FODSR(1,4)*(double(SI0(1))-FODSR(1,2)) + FODSR(1,5); 
%For Phase :-> Nph = m/FCC0*n + c
%The Phase Polynomial Registers 
P1_S = FOPSR(1,4);
P0_S = FOPSR(1,4)*(double(SI0(1))-FOPSR(1,2)) + FOPSR(1,5); 
P0_S = mod(P0_S,1);

%The Index of the Last Sample where the Current Delay/Phase Polynomial is Valid
N_u = double(FODSR(1,3)); %

%Initializing Output
xr = zeros(Ns,1) + 1j*zeros(Ns,1);

%For Flags
Flt_Mask = true(Np,1); %Filter Mask
Flg_Out = cell(1,N_Flg); %Output Falg Vector
for n_flg = 1 : N_Flg
    Flg_Out{n_flg} = false(Ns,1);
end %FOR n_flg
 

for ns = 1 : Ns
    
    %Checking whether the Current Delay Polynomial is Still Valid
    if SI0(ns) > N_u
        
        %Updating the index of the current Delay/Phase Polynomial
        n_poly = n_poly + 1;
        
        %The Delay Polynomial Registers
        D1_S = FODSR(n_poly,4);
        D0_S = FODSR(n_poly,5);
  
        %The Phase Polynomial Registers
        P1_S = FOPSR(n_poly,4);
        P0_S = FOPSR(n_poly,5);
        
        %The Index of the Last Sample where the Current Delay/Phase Polynomial is Valid
        N_u = FCC0*(FODSR(n_poly,1) - FODSR(1,1)) + FODSR(n_poly,3); %
    end
    
    %% The Delay Correction
    
    %The Requred Delay Correction at Original Sample Rate is given by 
    DC = SIdx - D0_S;
    %NOTE Negative Sign to Compensate Delay
    %The Inteder Delay Correction - First Estimate
    DC_I = floor(DC); 
    %The Fractional Sample Delay Correction - First Estimate
    DCF = DC - DC_I;  
    
    %Adding a Delay Dither
    %Note the Delay Dither is Uniformly Distributed in [-0.5, 0.5] Delay-Step
    Dly_Dither = (rand-0.5); 
    
    %The First Estimate of the Fractional Delay Correction Index
    DC_F = round(DSteps*DCF + Dly_Dither);
    
    %If the DC_F = DStep we need to Add Index to DCI to Wrap back the overflow
    if (DC_F == DSteps)
        DC_I = DC_I + 1;
    end %IF
    
    %Now taking the Modulus of DSteps to find the Fractional Delay Correction Index witnin (0,DSteps-1)
    DC_F = mod(DC_F,DSteps);
    
    %% ReSample Filtering
    
     %Checking whether there is a delay jump
    if (DC_I+1-Np_Half) < 1
        if (DC_I+2+Np_Half) < 1
            xTemp = zeros(Np,1);
        else
            xTemp = [zeros(Np_Half-DC_I-1,1); x(1:DC_I+1+Np_Half,1)];
        end %IF (DC_I+2+Np_Half) < 1
    elseif (DC_I+1+Np_Half) > N1o
        if (DC_I+2-Np_Half) > N1o
            xTemp = zeros(Np,1);
        else
        xTemp = [x(DC_I+2-Np_Half:end,1); zeros(DC_I+1+Np_Half-N1o,1)];
        end %IF (DC_I+2-Np_Half) > N1o
    else
        xTemp = x(DC_I+2-Np_Half:DC_I+1+Np_Half,1); 
        %Applying the Flags
        for n_flg = 1 : N_Flg            
            Flg_Out{n_flg}(ns) = all(Flg_In{n_flg}(DC_I+2-Np_Half:DC_I+1+Np_Half,1) & Flt_Mask);
        end %FOR n_flg
    end %IF (DC_I+1-Np_Half) < 1
    
    %Finding the Index of the Fractional Delay Filter
    Idx = mod(DC_F,DSteps)+1;
       
    %Filtering with the Corresponding Fractional-Delay Filter
    ReSamp = sum(xTemp.*hFD(:,Idx));%
    
    %% Calculating and Applying the Phase Rotations
    %The Phase correction Index
    %NOTE Negative Sign to Compensate Delay
    %Adding a Delay Dither
    %Note the Delay Dither is Uniformly Distributed in [-0.5, 0.5] Delay-Step
    Phs_Dither = (rand-0.5);    
    
    PCI = mod(round(P0_S*PSteps + Phs_Dither),PSteps);
    %Adding
    
    %Applying the Quantized Phase Correction factor
    xr(ns) = ReSamp*SC_LUT(PCI+1);
    
    %% Updating the Sample Index, Delay and Phase 
    SIdx = SIdx + Frat;
    D0_S = D0_S + D1_S;
    P0_S = P0_S + P1_S;
    
end %FOR ns

