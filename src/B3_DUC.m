%This MATLAB Function 'Gen_DUC' Performs the Digital Up Conversion of a
%sampled sequence. The input sequence is filtered by the Tunable 
%Filter, extracting the desired bandwidth. The isolated band is 
%up-sampled to the desired rate. The Tunable Filter is implemented with a 
%polyphase structure to reduce the computations.

%By - Thushara Kanchana Gunaratne - RO/RCO - NSI-NRC
%Start Date - 2018-04-11
% Modified 2018-12-21 > To Add valid Input and Output
% Modified 2019-01-22 > To Add Multiple Flags and Use a Counter-Based
% approach to propagate the Flags across the filter-bank

%# codegen
function [y_DUC,Flg_Out] = B3_DUC(x,Flg_In,SI0,DUC_Specs)
%Arranging Input as a row vector and find the number of samples
[Nrow,Ncol] = size(x);
if Ncol ~= 1
    x = x.';
    Ni = Ncol; %Input Vector Length
else
    Ni = Nrow; %Input Vector Length
end 
clear Nrow Ncol;

%Evalaute the number of Flags sequences and the length of the Flag sequences
N_Flg = length(Flg_In);
L_Flg = length(Flg_In{1});

if Ni ~= L_Flg
    error('Error! Input Length and Flag Lengths DO NOT match...');
end %IF

%Loading the parameters
h = DUC_Specs.h;
Nh = length(h);
L = DUC_Specs.L;
M = DUC_Specs.M;

%Shifting the Input
N_sft = mod(SI0,L);
x = [zeros(N_sft,1);x];

%Constituting the Polyphase Filter Coeffiicnet Array
PNh = ceil(Nh/M);
R0 = mod(Nh,M);
if R0 ~= 0
    h = [h, zeros(1,M-R0)];
end %IF

Ph = zeros(M,PNh);
for k = 1 : M
    Ph(k,:) = h(k:M:end);
end %For k

%Initiate the Input Mask that is multiplied with the Filter mask
xM = zeros(1,PNh+L-1);

%Finding the Input Sample Index Order
IP_Idx = zeros(1,M);
PFlt_Idx = zeros(1,M);

for k = 1 : M
    IP_Idx(1,k) = floor(L*(k-1)/M)+1;
    PFlt_Idx(1,k) = mod(L*(k-1),M)+1;
end %For nr

%The Niumber of Output Samples
No = ceil(Ni*M/L); %
Nc = Ni/L;

%% Function
%MATLAB
y_DUC = CORE_Gen_DUC(x,xM,IP_Idx,PFlt_Idx,L,M,PNh,uint64(Nc),uint64(No),Ph);
%MEX - NOTE MEX files is not supported for all platforms
%y_DUC = CORE_Gen_DUC_mex(x,xM,IP_Idx,PFlt_Idx,L,M,PNh,uint64(Nc),uint64(No),Ph);

%% Flags
%Number of Consecutive Flags due to the Input of one flag
N_Cons = ceil(PNh*M/L); %Note this is the Maximum propagation time in the filter 
%interms of Output Samples
%Falg Out Cell Array
Flg_Out_Temp = cell(1,N_Flg); 
for n_flgs = 1 : N_Flg
    %MATLAB
    Flg_Out_Temp{n_flgs} = DUC_Flg_Prop(Flg_In{n_flgs},L,M,N_Cons,Nc,No);
    %MEX - NOTE MEX files is not supported for all platforms
    %Flg_Out_Temp{n_flgs} = DUC_Flg_Prop_mex(Flg_In{n_flgs},L,M,N_Cons,Nc,No);
end %FOR n_flg

%Removing the extra flags at the end
Flg_Out = cell(1,N_Flg); %Falg Out Cell Array
for n_flgs = 1 : N_Flg
    Flg_Out{n_flgs} = Flg_Out_Temp{n_flgs}(1:No);    
end %FOR n_flg
return %Function B3_DUC