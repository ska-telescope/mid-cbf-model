%The Custom MATLAB function
%[GC_Vec] = Coch_MagResp_Correction(Fs_CC_Vec,GCCorrSpecs);
%corrects for the Passband Ripple of the Coarse Channelizer (CoCh)
%The Original Sample Rates from the CoCh are given in the Fs_CC_Vec and
%other parameters are specified in the structure 'GCCorrSpecs'
%GCCorrSpecs.h1 = The coefficients of the Coch
%GCCorrSpecs.M1 = The Down Sampling Factor of the Coch
%GCCorrSpecs.Os1 = The Over-Sampling Sampling Factor of the Coch
%GCCorrSpecs.Nc2 = The Number of Channels in the Subsequent Channelizer or FFT
%GCCorrSpecs.F0 = The Nominal Sample Rate
%GCCorrSpecs.FS_ID = The Index of the Selected Frequency Slice
%GCCorrSpecs.FA_sft = The Frequency Shift applied to Align the Subsequent
%Channels

%By Thushara Gunaratne - RO/RCO - HAA-NRC
%Start Date - 2017-10-06
%Modified - 2018-05-30 Error Correction

function [GC_Vec] = CoCh_MagResp_Correction(Fs_CC_Vec,GCCrctSpecs)

h1 = GCCrctSpecs.h1; %The coefficients of the Coch
M1 = GCCrctSpecs.M1; %The Down SamplingFactor of the Coch
Os1 = GCCrctSpecs.Os1; %The Over SamplingFactor of the Coch
Nc2 = GCCrctSpecs.Nc2; %The Number of Channels in the Subsequent Channelizer or FFT
F0 = GCCrctSpecs.F0; %The Nominal Sample Rate
FS_ID = GCCrctSpecs.FS_ID; %The Index of the Selected Frequency Slice
FA_sft = GCCrctSpecs.FA_Sft; %The Frequency Shift applied to Align the Subsequent Channels
N_Edge = GCCrctSpecs.N_Edge; %The Number of Channels that Excends the Nomial Edge

%Number of Frequencies
Ns = length(Fs_CC_Vec);

%% Evalaution
%Lower Edge
N_l = floor(Nc2/2*(1-1/Os1))-N_Edge;
%Upper Edge
N_u =  ceil(Nc2/2*(1+1/Os1))+N_Edge;
%The Normalized Frequencies at the Center of Fine Channels 
fc0 = (-1:2/Nc2:1-2/Nc2);

%Initialization
GC_Vec = ones(Ns,Nc2);
for k = 1 : Ns
    %Applying the Reverse-Shifts to get the Center Frequencies Corresponding to
    %Frequency Points
    %Frequency Shift due to SCFO Scheme
    SCFO_Sft = FS_ID*(Fs_CC_Vec(k) - F0); %In Hz
    Fc1 = fc0*F0/2 - (SCFO_Sft + FA_sft); %In Hz
    %The Normalized Frequencies relative to Original Sampling Frequency
    fc1 = Fc1/Fs_CC_Vec(k)/M1;
    
    %Evalauting the Frequency Response
    H = abs(freqz(h1,1,fc1,1));
    %Updating Gain Correction factor
    GC_Vec(k,N_l:N_u) = 1./H(N_l:N_u);
end

%FFTshifting to set the Order 
GC_Vec = fftshift(GC_Vec,2);