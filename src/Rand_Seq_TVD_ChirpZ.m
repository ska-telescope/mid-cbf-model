%The Custom MATLAB function 
%[x, Random_Para]= Rand_Seq_TVD_ChirpZ(Specs,ST,TVD) that evaluates the 
%pseudo-random sequences 'x', Valid flag 'V' and the basis parameters 'Random_Para' that
%associated with the Sum-of-Sinusoid' method specified by the 'Specs' and 
%are sampled at sample times given by the vector 'ST' (in s) with associated 
%delays given by the vector 'TVD'.
%
%By Thushara Kanchana Gunaratne - RO/RCO HAA National Research Council
%Start Date -> 2017-05-05
%Modified 2017-09-26 -> To facilitate Nyquist Zone 2 sampling
%Modified 2018-08-05 -> To facilitate Nyquist Zone 2 sampling at differnt  
%Modified 2018-12-20 -> To adda valid flag along with the outputs

function [x,V, Random_Para] = Rand_Seq_TVD_ChirpZ(Sspecs,Fs_Vec,ST_Vec,TVD_Vec)
 
%Specs - A Custom MATLAB structure specifying the Signal Specifications
%Specs.F0 =  %Eg 4.0E9+0.1E9; % The Nominal Sample Rate
%Specs.Fl : %Eg 4.6E9+0.1E9; % The Lower-Limit Frequency in Hz
%Specs.Fu : %Eg  Fl + 2.3E9; % The Upper-Limit Frequency in Hz
%Specs.F_Dsft : %Eg 4.6E9; % The Apparent Frequency Down-Shift in Hz
%Specs.Nqst_Zone = Nqst_Zone; %Either 1 or 2
%Note that Nyquist Zone 2 sampling also causes the spectrum to Flip.
%However, It is assume that the flip is corrected with multiplication of
%(-1)^n
%Specs.dError : %Eg 1E-6: The Tolerable Error in Piece-wise Linear
%Approximation
%
%The column vectors of Sampling Times in seconds - ST = [ST0; ST1;...;STn];
%NOTE Limit the number of sampling frequencies to 4
%
%The column vectors of Time Variable Delays in Seconds
%TVD = [TVD1;TVD2;...;TVDn]
%NOTE the number of Time Variable Delay Vectors should be equal to the
%number of Sampling Frequencies
 
%% Extracting Signal Specs
F0 = Sspecs.F0; % In Hz
Fl = Sspecs.Fl; % In Hz
Fu = Sspecs.Fu; % In Hz
F_Dsft = Sspecs.F_Dsft; % In Hz
Nqst_Zone = Sspecs.Nqst_Zone; %Either 1 or 2
Fs_Rat = Sspecs.Fs_Rat; %The ratio between the Original Sample Rate and the Sample rate at the Input to the Mid.CBF
dError = Sspecs.dError; % In Relative Units
 
%Sampling Frequencies
[Ns, Nseq] = size(ST_Vec);
 
%TVD is the matrix of Ns Time variable Delays in sec for N1 Receivers
[~,N1] = size(TVD_Vec);
 
%Comparing Nseq and N1
if (Nseq ~= N1)
   error('Error. \nThe Number of Sampling Frequencies should Equal to the Number of Delay Vectors!')
end
clear N1;

%Updating the Frequency Shift Depanding on the Nyquist Zone of Sampling 
if Nqst_Zone == 1
    Fsft_Vec = zeros(1,Nseq); % In Hz
elseif Nqst_Zone == 2
    Fsft_Vec = 0.5*Fs_Rat*(Fs_Vec-F0); % In Hz : NOTE (Fn-F0) is the Frequency Offset for each Fn
else
    error('Nyquist Zone should be either 1 or 2!');
end
 
%% Specifying the Spectrum of the Pseudo-Random Signal
 
%The uniformly distributed frequencies between f1 & f2 (normalized) 
%facilitates sampling rate of 1 without aliasing for the test signal
N_fl = round(Fl*Ns/F0); 
N_fu = round(Fu*Ns/F0);
 
%Number of Sinusoids
Nsin = N_fu - N_fl + 1;
 
%The Angular Frequencies of the Sinusoids
omega = 2*pi*transpose((N_fl-1:1:N_fu-1))/Ns; 
 
%The Magnitudes of the Sinusoids
% %Uniform Magnitudes
Mags = ones(Nsin,1);
% %Linearly Varying Magnitudes
% Mags = 0.8 - 0.6*(0:1:Nsin-1)/Nsin;
% figure; stem(omega,Mags);
 
%The Phases of the Sinusoids
%The uniformly distributed phase between 0 & 2pi 
Phas = 2*pi*rand(Nsin,1);
 
%The Random Parameters
Random_Para.omega = omega;
Random_Para.Nsin = Nsin;
Random_Para.Mags = Mags;
Random_Para.Phas = Phas;
 
%% Evaluate the Pricewise Linear Approximation to the Delay Vector
%Specifying the Maximum Tolerable Error in Piece-wise Linear Approximation
%of STDV
%dError = 5E-8;
 
%The custom MATLAB function LineSegments = Piecewise_Linear_Fit(Curve,x,dError) 
%returns the Piecewise Linear Approximation of a Smooth Polynomial 'Curve' 
%defined on the interval 'x', based on Maximum Absolute Error 'dError'.
%The line segment is expressed by the 'Gradient' m and 'Intercept' c, 
%as y = mx + c
 
%Using the Custom MATLAB Function Piecewise_Linear_Fit
%Initiating
x = zeros(Ns,Nseq);
%The Nominal Sample Indices
n = transpose((0 : 1 : (Ns-1)));
for k = 1 : Nseq
    %The custom MATLAB function 
    %LineSegments (LS) = Piecewise_Linear_Fit(Curve,x,dError)
    %returns the Piecewise Linear Approximation of a Smooth Polynomial 'Curve'
    %defined on the interval 'x', based on Maximum Absolute Error 'dError'.
    %The line segment is expressed by the 'Gradient' m and 'Intercept' c,
    %as y = mx + c
    
    %LS = Piecewise_Linear_Fit((ST_Vec(:,k)+TVD_Vec(:,k)-ST_Vec(1,1)-TVD_Vec(1,1))*F0,n,dError);
    LS = Piecewise_Linear_Fit((ST_Vec(:,k)+TVD_Vec(:,k))*F0,n,dError);
    % figure; plot(ST_Vec(:,k),(ST_Vec(:,k)+TVD_Vec(:,k))*Fs,'b-');
    
    %The Number of Line Segments
    [NLine, ~] = size(LS);
    
    for nLine = 1 : NLine
        
        %Length of the Linear Segment
        MLine = LS(nLine,2) - LS(nLine,1)+1;
        
        %The Phase Modulation of the Spectrum
        XM = [zeros(N_fl-1,1);Mags.*exp(1j*(Phas + LS(nLine,4)*omega));zeros(round(Ns/2)-N_fu+0,1)];
        Temp_x = ReSample_ChirpZ(XM,LS(nLine,1)-1,MLine,LS(nLine,3)/2);
        Phase_sft = exp(2j*pi*F_Dsft*TVD_Vec(LS(nLine,1):LS(nLine,2),k));
        Phase_sft = Phase_sft.*exp(-2j*pi*Fsft_Vec(k)*ST_Vec(LS(nLine,1):LS(nLine,2),k));
        x(LS(nLine,1):LS(nLine,2),k) = 2.*real(Temp_x.*Phase_sft);
        
    end %FOR nLine
    
end %FOR k

%% Adding the Valid Flag. 
%Due to the Chirp-Z transform Method, ALL outputs are Valid
V = true(Ns,Nseq);