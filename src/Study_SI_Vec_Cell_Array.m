%This MATLAB script test the cell array for Sample Index 
%Here it is studied how to convert back and forth

% By Thushara Kanchana Gunaratne - RCO National Research Council Canada
%Start Date -> 2018-01-10

close all; clear; clc;
%% Parameter Specification
Ns = 10E6; %Number of Samples
T_s = 5.999; % In Sec - Start Time

%Sampling Frequency
Fs = 3963617280; % In HZ - 13440*2^14*18
%Fs = 5945425920; % In Hz - 13440*2^14*18
%Fs = 3.96E9 + 1.8E3*10; % In Hz - Band 1, 2 and 3 
%Fs = 5.94E9 + 2.7E3*10; % In Hz - Band 4, 5a and 5b 

%The Down Sampling Factors
%For the CoCh
M1 = 18; % For Band 1, 2 and 3 
%M1 = 27; % For Band 4, 5a and 5b 

%For the Imaging Channelizer
Nc2 = 16384; % 

%% Specifying the Sample Indecies in the Wideband
SI_Sec_Start = floor(T_s);
SI_Temp = transpose(int64((round((T_s-SI_Sec_Start)*Fs) + (0 : 1 : Ns-1))));
%Evalauting Second and Sub-Second (SS) Indecies
Sec_Idx = int16(floor(double(SI_Temp)./Fs) +  SI_Sec_Start);
SS_Idx = mod(SI_Temp,Fs);
%Packaging into a cell array
SI = {Sec_Idx,SS_Idx};

%% Specifying the Sample Indecies in CoCh outputs
N_CC_OP = ceil(Ns/M1); %Number of Samples after the 
F_CC = Fs/M1; %In Hz

SI_CC_Sec_Strat = SI{1}(1); %First Sample of the Wideband Seq - Second Index
SI_CC_SS_Start = SI{2}(1); %First Sample of the Wideband Seq - Sub-Second Index

SI_CC_Temp = transpose(int64(floor(double(SI_CC_SS_Start)./M1) + 0 : 1 : N_CC_OP-1));
Sec_CC_Idx = int16(floor(double(SI_CC_Temp)./F_CC)) + SI_CC_Sec_Strat;
SS_CC_Idx = mod(SI_CC_Temp,F_CC);
%Packaging into a cell array
SI_CC = {Sec_CC_Idx,SS_CC_Idx};

%% Specifying the Sample Indecies in IC outputs
N_IC_OP = ceil(N_CC_OP/Nc2); %Number of Samples after the 
F_IC = F_CC/Nc2; %In Hz

SI_IC_Sec_Strat = SI_CC{1}(1); %First Sample of the Wideband Seq - Second Index
SI_IC_SS_Start = SI_CC{2}(1); %First Sample of the Wideband Seq - Sub-Second Index

SI_IC_Temp = transpose(floor(double(SI_IC_SS_Start)./Nc2) + int64(0 : 1 : N_IC_OP-1));
Sec_IC_Idx = int16(floor(double(SI_IC_Temp)./F_IC)) + SI_IC_Sec_Strat;
SS_IC_Idx = mod(SI_IC_Temp,F_IC);
%Packaging into a cell array
SI_IC = {Sec_IC_Idx,SS_IC_Idx};

