%The Custom MATLAB Function 'CC_Scale_Fac' evalaute the Scaling factor for 
%the CoCh output considering the spectral occupancy of the input signal and 
%the selected Coarse Channel 
%Inputs
%   Fs - Input Sampling Frequency - in samples per second
%   CC_Scale_Fac_Config.FS_ID - Frequency Slice Index
%   CC_Scale_Fac_Config.M - Down Sampling Factor
%   CC_Scale_Fac_Config.Nc - Number of Channels
%   CC_Scale_Fac_Config.Fl - Lower Edge of the Spectrum - in Hz
%   CC_Scale_Fac_Config.Fu - Uppder Edge of the Spectrum - in Hz
%   CC_Scale_Fac_Config.WB_Fsft - Wideband Frequency Shift - in Hz
%   CC_Scale_Fac_Config.C_Fac - Correction Factor
%
%Output
%   Rel_Spect_Ocp = Relative Spectral occupancy

function Scaling_Factor = CC_Scale_Fac(Fs,CC_Scale_Fac_Config)

%Un packing parameters
FS_ID = CC_Scale_Fac_Config.FS_ID; %Frequency Slice Index
M = CC_Scale_Fac_Config.M; %Down Sampling Factor
Nc = CC_Scale_Fac_Config.Nc; %Number of Channels
Fl = CC_Scale_Fac_Config.Fl; %Lower Edge of the Spectrum - in Hz
Fu = CC_Scale_Fac_Config.Fu; %Uppder Edge of the Spectrum - in Hz
WB_Fsft = CC_Scale_Fac_Config.WB_Fsft; %Wideband Frequency Shift - in Hz
C_Fac = CC_Scale_Fac_Config.C_Fac; %Correction Factor

%The Band edges after windeband Frequency Shift
Fl = Fl + WB_Fsft;
Fu = Fu + WB_Fsft;

%% Evalauting Important Parmenters
%Sampling Rate of the Frequency Slice
Fs_FS = Fs/M;
%The center frequency of the Frequency Slice
Fc_FS = FS_ID*Fs/Nc;
%The Lower Edge of the Frequency Slice
FS_Fl = Fc_FS - 0.5*Fs_FS;
%The Lower Edge of the Frequency Slice
FS_Fu = Fc_FS + 0.5*Fs_FS;

%% Determining the Spectral Occupancy
if (Fl <= FS_Fl) && (Fu >= FS_Fu)
    Rel_Spect_Ocp_CC = 1;
elseif (Fl > FS_Fl) && (Fu >= FS_Fu)
    Rel_Spect_Ocp_CC = (FS_Fu - Fl)/Fs_FS;
elseif (Fl <= FS_Fl) && (Fu < FS_Fu)
    Rel_Spect_Ocp_CC = (Fu - FS_Fl)/Fs_FS;
else
    Rel_Spect_Ocp_CC = (Fu - Fl)/Fs_FS;
end

%The Spectral occupancy of the Band
Rel_Spect_Ocp_Band = (Fu-Fl)/(0.5*Fs);


% Evalauting the Scaling factor
Scaling_Factor = C_Fac*sqrt(Rel_Spect_Ocp_CC/Rel_Spect_Ocp_Band);

    