%This Custom MATLAB function
% [x_Out,V_Out] = ReSampler_ERP_Corr(x_In,V_In,F_In,SI1_Cell,SI0_Cell,FODSR,FOPSR,ReSamp_Config_para)
%Inputs
%x_In - The original sample vector; 
%V_In - The valid flag for the original sample vector; 
%F_In - The sampling rate of the original sample vector; 
%SI1_Cell - The sample indecies vector for the original sample vector
%SI0_Cell - The sample indecies vector for the resampled sample vector
%NOTE - SI1_Cell and SI0_Cell are cell arrays that arranbge the Sample
%Indicies in Second - Sub-Second Index form
%FODSR - First Order Delay Syntiziser Registers - Applicable
%FOPSR - First Order Phase Syntiziser Registers - Applicable
%DPCSpecs - Additional Specs Needed for Delay and Phase Correction
% DPCSpecs.F0 = F0; %The ReSampled Sample Rate
% DPCSpecs.hFD_FB = hFD_FB; %Coefficients of the Fractional Delay Filter-bak 
% DPCSpecs.SC_LUT = SC_LUT; %The Sine-Cosine Look-up-Table
%
%Outputs
%x_Out - Resampled sequence of length 1xNs; Complex-Valued
%V_Out - Valid vector of length 1xNs; Logical
%
%The ReSampler have to be configured with the parameter set 
%'ReSampler_Config.mat' that contains
%h_FD - the fractional delay filter bank for ReSampling
%SC_LUT - the sine-cosine LUT for Phase Correction
%
%This MATLAB function 'ReSampler_ERP_Corr evaluates' the complex-valued 
%resampled sequence 'x1r' of the input complex-valued sequence 'x1o' as
%specified by the First Order Delay Polynomial 'FODP' and the First Order 
%Phase Polynomial 'FOPP' and specified by other parameters 'Frat' the ratio
%between the actual and nominal sample rates, 'SI1' sample indecies of the
%original signal and the required resampling indecies
%The time delay is applied by integer sample shifts and with a ReSampling
%Filter-Bank 'h_FD' and the Phase Rotation is applied by a sine-cosine
%Look-Up Table 'SC_LUT'.
 
%By Thushara Kanchana Gunaratne - RCO HAA, NRC CANADA
%Start date > 2017-07-31
%Modified 2017-09-01 > To separate Delay polynomial from the SCFO scheme
%and insert the Frequency Shift to align the fine channels of all Frequency
%Slices
%Modified 2017-11-28 > To Add Delay and Phase Dithering 
%Modified 2018-06-25 > To pass configuration parameters as an argument
%Modified 2018-12-21 > To Add Valid Input
%Modified 2019-01-14 > To fascilitate Second | Sub-Second Time Stamp
%Indexing 
%Modified 2019-07-19 > To perform part of the code in MEX function in order
%to speed things

function [x1r,Flg_Out] = ReSampler_ERP_Corr(x1o,Flg_In,F1,SI1_Cell,SI0_Cell,FODSR,FOPSR,ReSamp_Config_para)

%% Declaration and Parameter Derivations
 
%Loading the Prototype Filter as an initiation task
F0 = ReSamp_Config_para.F0;
hFD = flipud(ReSamp_Config_para.hFD_FB);
SC_LUT = ReSamp_Config_para.SC_LUT;

%Input to Output Frequency Ratio
Frat = F1/F0;

%Number of Fractional-Delay Steps 'DSteps,' and Number of Taps per a fractional
%delay filter 'Np'
[Np,DSteps] = size(hFD);
%The Number of Sample-Points in the Sine-Cosine LUT
PSteps = length(SC_LUT);
 
%% Other Parameters
%Length of the Input Sample vector
N1o = length(x1o); 

%Checking whether the Input Vector is at the same length as the Sample
%Indecies
if (N1o ~= length(SI1_Cell{1})) || (N1o ~= length(Flg_In{1}))
    error('Error! The Number of Samples in the Input Vector is different from the Length of the Input Sample Indcies')
end

%Starting Index of the Original Sample Series
SI1_Int = double(SI1_Cell{2}(1)) + F1*double(SI1_Cell{1}(1) - SI0_Cell{1}(1));
%Defining a Temporary Sample Sequence for Output Sample Indicies
SI0 = SI0_Cell{2} + F0*int64((SI0_Cell{1} - SI0_Cell{1}(1)));

%Finiding the Starting Sample Index interms of the original Sample rate
SIdx0 = Frat*double(SI0(1)) - SI1_Int;

%The Number of Delay Polynomials 
[Npoly, ~] = size(FODSR);
[Npoly1, ~] = size(FOPSR);
if ( Npoly ~= Npoly1 )
    error('Error! The Number of Delay Polynomials is different from the Number of Phase Polynomials')
end
clear Npoly1;

%% Initializing
%% ReSampling with the custom function 
Config_Para.SI0 = SI0;%Sample Indicies of the Output
Config_Para.FCC0 = F0; %Common Frequency
Config_Para.DSteps = DSteps; %Number of Delay Steps (I.e. Number of Fractional Delay Filters)
Config_Para.PSteps = PSteps; %Number of Phase Steps (I.e. Number of Points in the Since/Cosine LUT)
Config_Para.Np = Np; %Number of Coefficients per Fractional Delay filter
Config_Para.hFD = hFD; %Coefficient Matrix for [DSteps x Np] Fractional Delay Filters
Config_Para.SC_LUT = SC_LUT; %Coefficient Matrix for [PSteps x 1] Sine/Cosine Values

%Regular MATLAB Function
[x1r,Flg_Out] = CORE_ReSampler_ERPC(x1o,Flg_In,SIdx0,Frat,FODSR,FOPSR,Config_Para);

%%MATLAB MEX Function - Fast but hard to support for every envirnment
%[x1r,Flg_Out] = CORE_ReSampler_ERPC_mex(x1o,Flg_In,SIdx0,Frat,FODSR,FOPSR,Config_Para);

