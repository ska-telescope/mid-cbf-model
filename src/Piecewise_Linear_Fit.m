%This MATLAB function returns the Piecewise Linear Approximation of a
%Smooth Polynomial 'Curve' defined on the interval 'x', based on Maximum 
%Absolute Error 'dError' as proposed by Ivan Tomek as the algorithm 1 in 
%"Two Algorithms for Piecewise-Linear Continuous Approximation of Functions 
%of One Variable", IEEE Transactions on Computers, April 1974, pp. 445 -448 
 
%By Thushara Kanchana Gunaratne - RCO national Research Council Canada
%Start date : 2017-04-06
 
function LineSegments = Piecewise_Linear_Fit(y,x,Er)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%Number of Samples
Ns = length(y)-1;
 
%% Iteratively Finding the break Points where the MaxError > dError
 
%Initiating
LineSegments = [];
j = 1; %Start of the Segment
i = 2; %Second Element of the Segment
 
%Coordinates of the Origin of 1st Tangent
A1 = [x(j),y(j)+Er];
%Coordinates of the Origin of 2nd Tangent
A2 = [x(j),y(j)-Er];
%Gradient of the Current Secant Line 1 from A1 
mC1 = (y(i) - A1(2))/(x(i) - A1(1));
%Gradient of the Current Secant Line 1 from A2 
mC2 = (y(i) - A2(2))/(x(i) - A2(1));
 
while (i <= Ns)
    
    %Increasing the Index
    i = i + 1;
    
    %Gradient of the Next Secant Line 1 from A1
    mN1 = (y(i) - A1(2))/(x(i) - A1(1));
    
    %Checking whether the next point is
    if mN1 > mC1
        mC1 = mN1;
    end;
    
    %Gradient of the Next Secant Line 1 from A1
    mN2 = (y(i) - A2(2))/(x(i) - A2(1));
    
    %Checking whether the next point
    if mN2 < mC2
        mC2 = mN2;
    end;
    
    %Checking the mC1 & mC2 are diverging
    if mC1 >=0
        if mC1 > mC2
            %Adding Line Segments
            m = (mC1 + mC2)/2;
            c = y(j) - m*x(j);
            LineSegments = [LineSegments; [j,i,m,c]];
            %Updating the Next Segment
            if i <= Ns-1
                i = i + 1;
                j = i ;
                %Coordinates of the Origin of 1st Tangent
                A1 = [x(j),y(j)+Er];
                %Coordinates of the Origin of 2nd Tangent
                A2 = [x(j),y(j)-Er];
                %Gradient of the Current Secant Line 1 from A1
                mC1 = (y(i) - A1(2))/(x(i) - A1(1));
                %Gradient of the Current Secant Line 1 from A2
                mC2 = (y(i) - A2(2))/(x(i) - A2(1));
            end; %IF i <= Ns-1
        end; %IF mC1 > mC2
    else
        if mC1 > mC2
            %Adding Line Segments
            m = (mC1 + mC2)/2;
            c = y(j) - m*x(j);
            LineSegments = [LineSegments; [j,i,m,c]];            
            %Updating the Next Segment
            if i <= Ns-1
                i = i + 1;
                j = i ;
                %Coordinates of the Origin of 1st Tangent
                A1 = [x(j),y(j)+Er];
                %Coordinates of the Origin of 2nd Tangent
                A2 = [x(j),y(j)-Er];
                %Gradient of the Current Secant Line 1 from A1
                mC1 = (y(i) - A1(2))/(x(i) - A1(1));
                %Gradient of the Current Secant Line 1 from A2
                mC2 = (y(i) - A2(2))/(x(i) - A2(1));
            end; %IF i <= Ns-1
        end; %IF mC1 < mC2
    end; %IF mC1 >=0
    
end; %WHILE
%Adding the final Line Segments
m = (mC1 + mC2)/2;
c = y(j) - m*x(j);
LineSegments = [LineSegments; [j,i,m,c]];